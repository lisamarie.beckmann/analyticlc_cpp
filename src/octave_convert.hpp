#pragma once

#include <Eigen/Dense>
#include <igl/repmat.h>

#include <iostream>

using namespace Eigen;

inline bool is_approx(double a, double b, double range = 0.01) {
    return std::fabs(a-b)<range;
}

inline ArrayXd oct_colon_operator(double low, double high, double step) {
		size_t size = ((high-low)/step)+1;
		return ArrayXd::LinSpaced(size, low, low+step*(size-1));
}

inline MatrixXd oct_repmat(MatrixXd A, size_t i, size_t j) {
    MatrixXd B;
    igl::repmat(A,i,j,B);
    B = A.replicate(i,j);
    return B;
}

inline MatrixXcd oct_repmat_c(MatrixXcd A, size_t i, size_t j) {
    MatrixXcd B;
    igl::repmat(A,i,j,B);
    B = A.replicate(i,j);
    return B;
}


inline MatrixXd subtractVectortoMatrix(const Eigen::VectorXd& A, const Eigen::VectorXd& B) {
    Eigen::MatrixXd result(A.size(), B.size());

    for (int i = 0; i < A.size(); ++i) {
        for (int j=0; j<B.size();++j){
            result(i,j) = A(i) - B(j);
        }
    }
    return result;
}

inline MatrixXcd multiplyVectorstoMatrix(const Eigen::VectorXcd& A, const Eigen::VectorXcd& B) {
    Eigen::MatrixXcd result(A.size(), B.size());

    for (int i = 0; i < A.size(); ++i) {
        for (int j=0; j<B.size();++j){
            result(i,j) = A(i) * B(j);
        }
    }
    return result;
}

inline VectorXd oct_repmatvec(double a, size_t i, size_t j = 1) {
    VectorXd A = VectorXd::Constant(1, a);
    MatrixXd B;
    igl::repmat(A,i,j,B);
    B = A.replicate(i,j);
    return B;
}

inline VectorXcd oct_repmatvec(std::complex<double> a, size_t i, size_t j = 1) {
    VectorXcd A = VectorXcd::Constant(1, a);
    MatrixXcd B;
    igl::repmat(A,i,j,B);
    B = A.replicate(i,j);
    return B;
}

inline MatrixXcd oct_repmatvec(VectorXcd A, size_t i, size_t j) {
    MatrixXcd B;
    igl::repmat(A,i,j,B);
    B = A.replicate(i,j);
    return B;
}