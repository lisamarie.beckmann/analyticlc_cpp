#include "analyticlc.hpp"
#include "octave_convert.hpp"

#include <Eigen/src/Core/Array.h>
#include <iostream>
#include "laplacecoeffs.hpp"
#include "helper_functions.hpp"
#include "AnalyticLc/MALookupTable.h"
#include "AnalyticLc/OrbitalElements2TransitParams.h"
#include "AnalyticLc/ForcedElements1.h"
#include "AnalyticLc/ForcedMeanLongitudes3PlanetsCrossTerms.hpp"
#include <fstream>
#include <string>
#include <sstream>

LaplaceCoeff lc;
MALookupTable MALookupTab;

void alc::from_json(const json &j, AnalyticLCparams &opt){
    opt.P = j.at("P").get<VectorXd>();
    opt.Tmid0 = j.at("Tmid0").get<VectorXd>();
    opt.ror = j.at("ror").get<VectorXd>();
    opt.aor = j.at("aor").get<VectorXd>();
    opt.mu = j.at("mu").get<VectorXd>();
    opt.ex0 = j.at("ex0").get<VectorXd>();
    opt.ey0 = j.at("ey0").get<VectorXd>();
    opt.I0 = j.at("I0").get<VectorXd>();
    opt.Omega0 = j.at("Omega0").get<VectorXd>();
    opt.t = j.at("t").get<VectorXd>();
    opt.u1 = j.at("u1").get<double>();
    opt.u2 = j.at("u2").get<double>();
    opt.BinningTime = j.at("BinningTime").get<VectorXd>();
    opt.CadenceType = j.at("CadenceType").get<VectorXi>();
    opt.MaxTTV = j.at("MaxTTV").get<VectorXd>();
    opt.tRV = j.at("tRV").get<VectorXd>();
    opt.ExtraIters = j.at("ExtraIters").get<double>();
    opt.OrdersAll = j.at("OrdersAll").get<VectorXd>();
    int boolint = j.at("Calculate3PlanetsCrossTerms").get<int>();
    if (boolint == 1){
        opt.Calculate3PlanetsCrossTerms = true;
    } else {
        opt.Calculate3PlanetsCrossTerms = false;
    }
    opt.MaxPower = j.at("MaxPower").get<int>();
    boolint = j.at("OrbitalElementsAsCellArrays").get<int>();
    if (boolint == 1){
        opt.OrbitalElementsAsCellArrays = true;
    } else {
        opt.OrbitalElementsAsCellArrays = false;
    }
    opt.J2 = j.at("J2").get<double>();
}

void alc::from_json(const json &j, AnalyticLCout &opt){
    opt.LC = j.at("LC").get<VectorXd>();
    opt.RV_o_r = j.at("RV_o_r").get<VectorXd>();
    opt.Y_o_r = j.at("Y_o_r").get<VectorXd>();
    opt.Z_o_r = j.at("Z_o_r").get<VectorXd>();
}

void alc::from_json(const nlohmann::json &j, alc::OutputList &opt) {
    /*
        VectorXd tT;
        VectorXd tRV;
        MatrixXcd zT;
        MatrixXcd zRV;
        MatrixXcd uT;
        MatrixXcd uRV;
        MatrixXd D;
        MatrixXd Tau;
        MatrixXd w;
        MatrixXd d;
        MatrixXd b;
        MatrixXd AssociatedTmid;
        MatrixXi AssociatedInd;
        MatrixXd AllLC;
        std::vector<Eigen::VectorXd> TmidActualCell;
        MatrixXcd free_e_T;
        MatrixXcd free_I_T;
        MatrixXcd free_e_RV;
        MatrixXcd free_I_RV;
        std::vector<Eigen::VectorXd> TTVCell;
        std::vector<Eigen::VectorXcd> dzCell;
        std::vector<Eigen::VectorXcd> duCell;
        std::vector<Eigen::VectorXd> dLambdaCell;
        std::vector<Eigen::VectorXcd> zfreeCell;
        VectorXd dbdt;
        VectorXd b0;
        MatrixXd Lambda_T;
        VectorXd P;
        VectorXi BeginIdx;
        VectorXi EndIdx;
        VectorXi Ntr;
     * */
    opt.tT = j.at("tT").get<VectorXd>();
    opt.tRV = j.at("tRV").get<VectorXd>();
    //opt.zT = j.at("zT_real").get<MatrixXd>() + j.at("zT_imag").get<MatrixXd>() * 1i;
    //opt.zRV = j.at("zRV_real").get<MatrixXd>() + j.at("zRV_imag").get<MatrixXd>() * 1i;
    //opt.uT = j.at("uT_real").get<MatrixXd>() + j.at("uT_imag").get<MatrixXd>() * 1i;
    //opt.uRV = j.at("uRV_real").get<MatrixXd>() + j.at("uRV_imag").get<MatrixXd>() * 1i;
    opt.zT = j.at("zT").get<MatrixXd>();
    opt.zRV = j.at("zRV").get<MatrixXd>();
    opt.uT = j.at("uT").get<MatrixXd>();
    opt.uRV = j.at("uRV").get<MatrixXd>();
    opt.D = j.at("D").get<MatrixXd>();
    opt.Tau = j.at("Tau").get<MatrixXd>();
    opt.w = j.at("w").get<MatrixXd>();
    opt.d = j.at("d").get<MatrixXd>();
    opt.b = j.at("b").get<MatrixXd>();
    opt.AssociatedTmid = j.at("AssociatedTmid").get<MatrixXd>();
    opt.AssociatedInd = j.at("AssociatedInd").get<MatrixXi>();
    opt.AllLC = j.at("AllLC").get<MatrixXd>();
    //get structure of type std::vector<Eigen::VectorXd>
    opt.TmidActualCell = j.at("TmidActualCell").get<std::vector<Eigen::VectorXd>>();
    //opt.free_e_T = j.at("free_e_T_real").get<MatrixXd>() + j.at("free_e_T_imag").get<MatrixXd>() * 1i;
    opt.free_e_T = j.at("free_e_T").get<MatrixXd>();
    //opt.free_I_T = j.at("free_I_T_real").get<MatrixXd>() + j.at("free_I_T_imag").get<MatrixXd>() * 1i;
    opt.free_I_T = j.at("free_I_T").get<MatrixXd>();
    opt.free_I_RV = j.at("free_I_RV").get<MatrixXd>();
    opt.free_e_RV = j.at("free_e_RV").get<MatrixXd>();
    opt.TTVCell = j.at("TTVCell").get<std::vector<Eigen::VectorXd>>();
    //get structure of type std::vector<Eigen::VectorXcd>
    opt.dzCell = j.at("dzCell").get<std::vector<Eigen::VectorXcd>>();
    opt.duCell = j.at("duCell").get<std::vector<Eigen::VectorXcd>>();
    opt.dLambdaCell = j.at("dLambdaCell").get<std::vector<Eigen::VectorXd>>();
    //get structure of type std::vector<Eigen::VectorXcd>
    opt.zfreeCell = j.at("zfreeCell").get<std::vector<Eigen::VectorXcd>>();
    opt.dbdt = j.at("dbdt").get<VectorXd>();
    opt.b0 = j.at("b0").get<VectorXd>();
    opt.Lambda_T = j.at("Lambda_T").get<MatrixXd>();
    opt.P = j.at("P").get<VectorXd>();
    opt.BeginIdx = j.at("BeginIdx").get<VectorXi>();
    opt.EndIdx = j.at("EndIdx").get<VectorXi>();
    opt.Ntr = j.at("Ntr").get<VectorXi>();
}

std::tuple<VectorXcd, VectorXcd> calczandu(alc::calcforcedin &params) {
    //calculates z and u from the free eccentricity and inclination. Usually calculated for every t, but can also be calculated for every tRV.
    //define the imaginary free eccentricity and inclination
    VectorXcd free_e_0 = params.ex0 + 1i * params.ey0;
    VectorXcd free_I_0 = params.I0.array() * exp(1i * params.Omega0.array());

    //define the secular interatction matrices of eccentricity and inclination
    //vectors
    VectorXd n = 2 * M_PI / params.P.array();
    auto [A, B] = SecularInteractionsMatrix(n, params.mu, params.aor, params.J2);
    VectorXcd free_e_T;
    VectorXcd free_I_T;
    VectorXcd free_e_RV;
    VectorXcd free_I_RV;

    auto [tT, BeginIndx, EndIdx, Ntr] = DynamicsTimeAxis(params.P, params.Tmid0, params.t);
    VectorXcd z;
    VectorXcd u;
    if (params.t.size() != 0) { //if at least one time stamp is required for the light curve calculation
        //calculate the secular motion of the eccentricities and inclinations
        int NtT = tT.size();
        MatrixXd t0_T = -subtractVectortoMatrix(params.Tmid0, tT).transpose();
        MatrixXd repmatn = oct_repmat(n.transpose(), NtT, 1);
        MatrixXd Lambda_T = (t0_T.array() * repmatn.array());

        free_e_T = Vector1stOrderDE(A, free_e_0,
                                             tT.array().transpose() - params.t(0));
        free_I_T = Vector1stOrderDE(B, free_I_0,
                                             tT.array().transpose() - params.t(0));

        auto [dz, dLambda, du, da_oa] = ForcedElements1({lc, params.P, Lambda_T, params.mu, free_e_T, free_I_T, params.OrdersAll, params.MaxPower});


        VectorXcd z = free_e_0+ dz;
        VectorXcd u = free_I_0 + du;
    }
    if (params.tRV.size() > 0) {
        MatrixXd repmatNrv = oct_repmat(n.transpose(), params.tRV.size(), 1);
        MatrixXd t0_RV = -subtractVectortoMatrix(params.Tmid0, params.tRV).transpose();
        MatrixXd Lambda_RV = (t0_RV.array() * repmatNrv.array());

        //secular interactions at the RV time stamps
        free_e_RV = Vector1stOrderDE(A, free_e_0,
                                     params.tRV.array() - params.t(0));
        free_I_RV = Vector1stOrderDE(B, free_I_0,
                                     params.tRV.array() - params.t(0));

        //near-resonant interactions at the RV time stamps
        auto [dz, dLambda, du, da_oa] = ForcedElements1({lc, params.P, Lambda_RV, params.mu, free_e_RV, free_I_RV, params.OrdersAll, params.MaxPower});

        VectorXcd z = free_e_0+ dz;
        VectorXcd u = free_I_0 + du;
    }

    return std::make_tuple(z,u);
}

//function [free_e_T, free_I_T] = PreForcedElements(P,mu,ex0,ey0,I0,Omega0,tT,t)
//%prepare parameters for ForcedElements1, mainly calculate the free matrices
//
//%define the imaginary free eccentricity and inclination
//free_e_0 = ex0+1i*ey0;
//free_I_0 = I0.*exp(1i.*Omega0);
//
//%define the secular interatction matrices of eccentricity and inclination
//%vectors
//n = 2*pi./P;
//[A,B] = SecularInteractionsMatrix2(n,mu);
//
//if ~isempty(t) %if at least one time stamp is required for the light curve calculation
//    %calculate the secular motion of the eccentricities and inclinations
//    free_e_T = Vector1stOrderDE(A,free_e_0.',horizontal(tT)-t(1));
//    free_I_T = Vector1stOrderDE(B,free_I_0.',horizontal(tT)-t(1));
//end
std::tuple<alc::AnalyticLCout, alc::OutputList> AnalyticLCfunc(alc::AnalyticLCparams params) {
//% AnalyticLC: generate an analytic light-curve, and optionally RV and astrometry data, from a set of initial (free) orbital
//% elements.
//
//% The full logic of AnalyticLC and the model derivation are described in the paper "An Accurate 3D
//% Analytic Model for Exoplanetary Photometry, Radial Velocity and
//% Astrometry" by Judkovsky, Ofir and Aharonson (2021), please cite this
//% paper upon usage. Model and enjoy :)
//
//%Inputs:
//% P - vector of orbital periods,
//% Tmid0 - vector of refernce times-of-mid-transit,
//% ror - vector of planets/star radii ratio,
//% aor - semi-major-axis in stellar radius of innermost planet,
//% mu - vector of planets/star mass ratio,
//% ex0 and ey0 - vectors of initial FREE eccentricities components of planets
//% I0 = vector of initial FREE inclinations (radians),
//% Omega0 - vector of initial FREE longiudes of ascending nodes (radians),
//% t - time vector to calculate light curve model at,
//% u1,u2 - limb darkening coefficients.
//% Eccentricity components and inclination are defined with respect to the system plane, and NOT with respect to the sky plane. The system plane is
//% defined such that the x axis points from the star to the observer, and y
//% is on the sky plane such that the inclinations are measured with respect
//% to the xy plane. Transits are expected to occur for small
//% values of sin(I0)*sin(Omega), which is equal to the cosine of the
//% sky-plane inclination.
//% The time units are to the user's choice, and should be consistent in all
//% parameters (t,P,Tmid etc.).
//
//% Examples for usage: T
//% LC = AnalyticLC(P,Tmid0,ror,aor,mu,ex0,ey0,I0,Omega0,t,u1,u2,'OutputList','');
//% LC = AnalyticLC(P,Tmid0,ror,aor,mu,ex0,ey0,I0,Omega0,t,u1,u2,'BinningTime',BinningTime,'OutputList','all');
//% [LC,RV_o_r,Y_o_r,Z_o_r,O] = AnalyticLC(P,Tmid0,ror,aor,mu,ex0,ey0,I0,Omega0,t,u1,u2,'tRV',tRV);system(system),
    //lc.loadCacheFromFile("cache.dat");

    VectorXd P = params.P;
    VectorXd Tmid0 = params.Tmid0;
    VectorXd ror = params.ror;
    VectorXd aor = params.aor;
    VectorXd mu = params.mu;
    VectorXd ex0 = params.ex0;
    VectorXd ey0 = params.ey0;
    VectorXd I0 = params.I0;
    VectorXd Omega0 = params.Omega0;
    VectorXd t = params.t;
    double u1 = params.u1;
    double u2 = params.u2;
    VectorXd BinningTime = params.BinningTime;
    VectorXi CadenceType = params.CadenceType;
    VectorXd MaxTTV = params.MaxTTV;
    VectorXd tRV = params.tRV;
    double ExtraIters = params.ExtraIters;
    VectorXd OrdersAll = params.OrdersAll;
    bool Calculate3PlanetsCrossTerms = params.Calculate3PlanetsCrossTerms;
    int MaxPower = params.MaxPower;
    bool OrbitalElementsAsCellArrays = params.OrbitalElementsAsCellArrays;
    double J2 = params.J2;
    bool CalcDurations = params.CalcDurations;
    bool CalcAstrometry = params.CalcAstrometry;

    VectorXd LC;
    VectorXd RV_o_r;
    VectorXd Y_o_r;
    VectorXd Z_o_r;
    alc::OutputList O;
    O.P = P;
    O.tRV = tRV;

    MatrixXcd dz;
    MatrixXd dLambda;
    MatrixXcd du;
    MatrixXd da_oa;
    //std::ostringstream ossi;
    //std::cout << params;
    //std::ofstream outfile("outtmp");
    //if (outfile.is_open()) {
    //    std::ostringstream oss;
    //    oss << params;
    //    outfile << oss.str() << std::endl;
    //    outfile.close();
    //} else {
    //    throw std::runtime_error("Unable to open file");
    //}

    //printEigen("P", params.P);
    //printEigen("Tmid0", params.Tmid0);
//    printEigen("ror", params.ror);
//    printEigen("aor", params.aor);
//    printEigen("mu", params.mu);
//    printEigen("ex0", params.ex0);
//    printEigen("ey0", params.ey0);
//    printEigen("I0", params.I0);
//    printEigen("Omega0", params.Omega0);
//    printEigen("t", params.t);
//    std::cout << "u1: " << params.u1 << "\n\n";
//    std::cout << "u2: " << params.u2 << "\n\n";
//    printEigen("BinningTime", params.BinningTime);
//    printEigen("CadenceType", params.CadenceType);
//    printEigen("MaxTTV", params.MaxTTV);
//    printEigen("tRV", params.tRV);
//    std::cout << "ExtraIters: " << params.ExtraIters << "\n\n";
//    printEigen("OrdersAll", params.OrdersAll);
//    std::cout << "Calculate3PlanetsCrossTerms: " << params.Calculate3PlanetsCrossTerms << "\n\n";
//    std::cout << "MaxPower: " << params.MaxPower << "\n\n";
//    std::cout << "OrbitalElementsAsCellArrays: " << params.OrbitalElementsAsCellArrays << "\n\n";
//    std::cout << "J2: " << params.J2 << "\n\n";
//    std::cout << "CalcDurations: " << params.CalcDurations << "\n\n";
//    std::cout << "CalcAstrometry: " << params.CalcAstrometry << "\n\n";


    if (CadenceType.size() == 0) {
        //cadence type should label each time stamp. values must be consecutive natural integers: 1, 2,...
        CadenceType = VectorXi::Ones(t.size());
    }
    if (MaxTTV.size() == 0) {
        //maximal TTV amplitude allowed for each planet. A value per planet is allowed, or a single value for all planets.
        MaxTTV = VectorXd::Ones(P.size()) * Infinity;
    }

    //number of planets and number of time stamps and number of cadence types
    int Npl = P.size();
    int Nt = t.size();
    int Ncad = CadenceType.maxCoeff();
    int Nrv = tRV.size();

    //prepare a time axis of one point per transit for the calculation of the
    //orbital dynamics
    std::tie(O.tT, O.BeginIdx, O.EndIdx, O.Ntr) = DynamicsTimeAxis(P, Tmid0, t);
    int NtT = O.tT.size();

    //make sure that each cadence type is characterized by its own binning time
    if (BinningTime[0] == 0) {
        BinningTime[0] = t.maxCoeff() - t.minCoeff();
    }
    if (BinningTime.size() == 1) {
        BinningTime = BinningTime[0] * VectorXd::Ones(Ncad);
    }

    //make sure that each planet receives a max TTV value
    if (MaxTTV.size() == 1) {
        MaxTTV = MaxTTV[0] * VectorXd::Ones(Npl);
    }


    //calculate mean motion
    VectorXd n = 2 * M_PI / P.array();
    MatrixXd t0_T;
    MatrixXd t0_RV;
    //calculate time relative to first mid-transit
    t0_T = -subtractVectortoMatrix(Tmid0, O.tT).transpose();
    t0_RV = -subtractVectortoMatrix(Tmid0, O.tRV).transpose();
    
    //printEigen("n", n);
    
    //calculate mean longitudes
    MatrixXd repmatn = oct_repmat(n.transpose(), NtT, 1);
    O.Lambda_T = (t0_T.array() * repmatn.array());

    //calculate period ratio
    VectorXd PeriodRatio = P.tail(Npl - 1) / P(0);

    //calculate semi-major axis om_storage = {Eigen::DenseStorage<double, -1, -1, -1, 0>} f outer planets, deduced from Period Ratio
    if (aor.size() == 1) {
        aor.conservativeResize(Npl);
        for (int i = 1; i < Npl; i++) {
            aor(i) = aor(0) * pow(PeriodRatio(i - 1), 2.0 / 3.0) * pow((1 + mu(i)) / (1 + mu(0)), 1.0 / 3.0);
        }
    }

    //calculate the imaginary free eccentricity and inclination
    VectorXcd free_e_0 = ex0 + 1i * ey0;
    VectorXcd free_I_0 = I0.array() * exp(1i * Omega0.array());

    auto [A, B] = SecularInteractionsMatrix(n, mu, aor, J2);

    MatrixXcd TTV;
    if (t.size() != 0) { //if at least one time stamp is required for the light curve calculation
        //calculate the secular motion of the eccentricities and inclinations
	//
	//printEigen("t",t);
        O.free_e_T = Vector1stOrderDE(A, free_e_0,
                                         O.tT.array().transpose() - t(0));
        O.free_I_T = Vector1stOrderDE(B, free_I_0,
                                         O.tT.array().transpose() - t(0)); //TODO: maybe not transpose

        //calculate the near-resonant interactions and their resulting TTV's

        auto [dz, dLambda, du, da_oa, dTheta, O_zT, O_uT, TTV] = ResonantInteractions2(P, mu, O.Lambda_T, O.free_e_T, O.free_I_T,
                                                                                   O.tT, O.BeginIdx, O.EndIdx, ExtraIters,
                                                                                   OrdersAll,
                                                                                   Calculate3PlanetsCrossTerms,
                                                                                   MaxPower);
        O.zT = O_zT;
        O.uT = O_uT;
        //check if the solution is invalid. Reasons for invalidity:
        //if the TTV's do not exceed the maximal value, and if it does - return nans
        //innermost planet hits star: a(1-e)<Rs
        //e>1 are invalid
        MatrixXd abszT = O.zT.array().abs();
        VectorXd TTV_max = TTV.array().abs().colwise().maxCoeff();
        if ((TTV_max.array() > MaxTTV.array()).any() || (1 - abszT(0, 0)) * aor(0) < 1 ||
            abszT.maxCoeff() > 1) {
            //printEigen("TTV_max", TTV_max);
	    //printEigen("MaxTTV", MaxTTV);
	    //printEigen("abszT", abszT); 
	    LC = VectorXd::Ones(Nt) * NAN;
            RV_o_r = VectorXd::Ones(Nrv) * NAN;
            Y_o_r = VectorXd::Ones(Nrv) * NAN;
            Z_o_r = VectorXd::Ones(Nrv) * NAN;
            alc::AnalyticLCout out = {LC, RV_o_r, Y_o_r, Z_o_r};
    
	    //printEigen("t",t);
            return std::make_tuple(out, O);
        }

	//printEigen("t",t);
        //translate the orbital elements to transits properties
        //<MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, MatrixXi, VectorXd, VectorXd>
        std::vector<Eigen::VectorXd> D1;
        std::vector<Eigen::VectorXd> Tau1;
        std::vector<Eigen::VectorXd> w1;
        std::vector<Eigen::VectorXd> d1;
        std::vector<Eigen::VectorXd> b1;

        std::tie(O.D, D1, O.Tau, Tau1, O.w, w1, O.d, d1, O.b, b1, O.AssociatedTmid, O.AssociatedInd, O.dbdt, O.b0) = TransitsProperties(Nt,
                                                                                                                   Npl,
                                                                                                                   O.BeginIdx,
                                                                                                                   O.EndIdx,
                                                                                                                   t,
                                                                                                                   O.tT,
                                                                                                                   TTV,
                                                                                                                   P, n,
                                                                                                                   da_oa,
                                                                                                                   aor,
                                                                                                                   O.zT,
                                                                                                                   O.uT,
                                                                                                                   ror,
                                                                                                                   CalcDurations);

	//printEigen("t",t);
        //construct the light-curve for each planet
        O.AllLC = GenerateLightCurve(Nt, Npl, Ncad, CadenceType, t, BinningTime, O.AssociatedTmid, O.w, O.d, O.b, ror, u1,
                                        u2);
	//printEigen("O.AllLC", O.AllLC);
        //sum all planets light-curves to get the total light curve. The possibility of mutual transits is neglected.
        ArrayXd AllLCsums = O.AllLC.rowwise().sum().array();
        LC = AllLCsums - Npl + 1;
    } else {
        t = VectorXd::Zero(1);
        LC = VectorXd::Zero(0);
    }
    //printEigen("LC", LC);
 
    //calculate the forced elements of the RV time stamps if the output requires doing so
    if (Nrv > 0) {
        MatrixXd repmatNrv = oct_repmat(n.transpose(), Nrv, 1);
        MatrixXd Lambda_RV = (t0_RV.array() * repmatNrv.array());

        //secular interactions at the RV time stamps
        O.free_e_RV = Vector1stOrderDE(A, free_e_0,
                                          O.tRV.array() - t(0));
        O.free_I_RV = Vector1stOrderDE(B, free_I_0,
                                          O.tRV.array() - t(0));

        //near-resonant interactions at the RV time stamps
        auto [forced_e_RV, dLambda_RV, forced_I_RV, tmp] = ForcedElements1(
                {lc, P, Lambda_RV, mu, O.free_e_RV, O.free_I_RV, OrdersAll, MaxPower});

        if (Calculate3PlanetsCrossTerms) {
            dLambda_RV = dLambda_RV + ForcedMeanLongitudes3PlanetsCrossTerms(lc, P, Lambda_RV, mu, O.free_e_RV);
        }

        O.zRV = O.free_e_RV + forced_e_RV;
        O.uRV = O.free_I_RV + forced_I_RV;
        Lambda_RV += dLambda_RV;

        //calculate the radial velocity/astrometry model
        std::tie(RV_o_r, Y_o_r, Z_o_r) = CalcRV(Lambda_RV, O.zRV, O.uRV, mu, aor, n, CalcAstrometry);

        MatrixXd in = Lambda_RV;
    } else {
        //Eigen::PlainObjectBase<Eigen::Matrix<double, -1, -1, 0, -1, -1> > = {Eigen::PlainObjectBase<Eigen::Matrix>}
        MatrixXcd forced_e_RV = MatrixXcd::Zero(Nrv, Npl);
        MatrixXcd Lambda_RV = MatrixXcd::Zero(Nrv, Npl);
        MatrixXcd free_e_RV = MatrixXcd::Zero(Nrv, Npl);
        MatrixXcd free_I_RV = MatrixXcd::Zero(Nrv, Npl);
        MatrixXcd forced_I_RV = MatrixXcd::Zero(Nrv, Npl);
        O.zRV = free_e_RV + forced_e_RV;
        O.uRV = free_I_RV + forced_I_RV;
        RV_o_r = VectorXd::Zero(0);
        Y_o_r = VectorXd::Zero(0);
        Z_o_r = VectorXd::Zero(0);
    }
    if (OrbitalElementsAsCellArrays && !((t.array() == 0).all())) {
        auto TmidActualCell = std::vector<VectorXcd>(Npl);
        auto TTVCell = std::vector<VectorXcd>(Npl);
        O.dzCell = std::vector<VectorXcd>(Npl);
        auto duCell = std::vector<VectorXcd>(Npl);
        auto dLambdaCell = std::vector<VectorXd>(Npl);
        auto zfreeCell = std::vector<VectorXcd>(Npl);
        auto ufreeCell = std::vector<VectorXcd>(Npl);
        auto da_oaCell = std::vector<VectorXd>(Npl);

        for (size_t j = 0; j < Npl; j++) {
            VectorXi Indx = VectorXi::Zero(O.EndIdx(j) - O.BeginIdx(j));
            for (int i = O.BeginIdx(j); i <= O.EndIdx(j); i++) {
                Indx(i) = i;
            }
            VectorXd tTindxtrans = O.tT(Indx).transpose();
            TmidActualCell[j] = tTindxtrans + TTV(Indx, j).transpose();
            TTVCell[j] = TTV(Indx, j).transpose();
            O.dzCell[j] = dz(Indx, j).transpose();
            duCell[j] = du(Indx, j).transpose();
            dLambdaCell[j] = dLambda(Indx, j).transpose();
            zfreeCell[j] = O.free_e_T(Indx, j).transpose();
            ufreeCell[j] = O.free_I_T(Indx, j).transpose();
            da_oaCell[j] = da_oa(Indx, j).transpose();
        }
    }
    lc.saveCacheToFile("cache.dat");

    alc::AnalyticLCout out = {LC, RV_o_r, Y_o_r, Z_o_r};
    return std::make_tuple(out, O);
}

void alc::construct(alc::AnalyticLCparams &params, const VectorXd &P, const VectorXd &Tmid0, const VectorXd &ror, const VectorXd &aor, const VectorXd &mu, const VectorXd &ex0, const VectorXd &ey0, const VectorXd &I0, const VectorXd &Omega0, const VectorXd &t, double u1, double u2){
    params.P = P;
    params.Tmid0 = Tmid0;
    params.ror = ror;
    params.aor = aor;
    params.mu = mu;
    params.ex0 = ex0;
    params.ey0 = ey0;
    params.I0 = I0;
    params.Omega0 = Omega0;
    params.t = t;
    params.u1 = u1;
    params.u2 = u2;
    params.BinningTime = VectorXd::Zero(1);
    params.BinningTime[0] = matlabrange(t);
    params.CadenceType = VectorXi::Ones(t.size());
    params.MaxTTV = VectorXd::Ones(P.size()) * Infinity;
    params.tRV = VectorXd::Zero(0);
    params.ExtraIters = 0;
    params.OrdersAll = VectorXd::Zero(0);
    params.Calculate3PlanetsCrossTerms = false;
    params.MaxPower = 4;
    params.OrbitalElementsAsCellArrays = false;
    params.J2 = 0;
    params.CalcDurations = true;
    params.CalcAstrometry = true;
}




//
//%if required, give more outputs then just the light-curve and the RV
//%values. These outputs are specified in the optinal input variable
//%OutputList.
//if nargout>4
//
//    %upon the value of "all", set OutputList to include a default list of
//    %variables
//    if strcmp(OutputList,'all')
//        OutputList = {'tT','tRV','zT','zRV','uT','uRV','D','Tau','w','d','b','AssociatedTmid','AssociatedInd','AllLC','TmidActualCell','free_e_T','free_I_T','free_e_RV','free_I_RV','TTVCell','dzCell','duCell','dLambdaCell','zfreeCell','dbdt','b0','Lambda_T','P','BeginIdx','EndIdx','Ntr'}; %a list of parameters to give as an additional output, if required
//        OrbitalElementsAsCellArrays = 1;
//    end
//
//    %generate cell arrays for the orbital elements
//    if OrbitalElementsAsCellArrays && ~isequal(t,0)
//
//        TmidActualCell = cell(1,Npl);
//        TTVCell = cell(1,Npl);
//        dzCell = cell(1,Npl);
//        duCell = cell(1,Npl);
//        dLambdaCell = cell(1,Npl);
//        zfreeCell = cell(1,Npl);
//        ufreeCell = cell(1,Npl);
//        da_oaCell = cell(1,Npl);
//
//        for j = 1:Npl
//            TmidActualCell{j} = (tT(BeginIdx(j):EndIdx(j)))'+TTV(BeginIdx(j):EndIdx(j),j).';
//            TTVCell{j} = TTV(BeginIdx(j):EndIdx(j),j).';
//            dzCell{j} = dz(BeginIdx(j):EndIdx(j),j).';
//            duCell{j} = du(BeginIdx(j):EndIdx(j),j).';
//            dLambdaCell{j} = dLambda(BeginIdx(j):EndIdx(j),j).';
//            zfreeCell{j} = free_e_T(BeginIdx(j):EndIdx(j),j).';
//            ufreeCell{j} = free_I_T(BeginIdx(j):EndIdx(j),j).';
//            da_oaCell{j} = da_oa(BeginIdx(j):EndIdx(j),j).';
//        end
//
//    end
//
//    %move all variables specified in OutputList to a single structure
//    %variable O
//    for j = 1:length(OutputList)
//        try
//            eval(['O.',OutputList{j},'=',OutputList{j},';']);
//        catch
//            fprintf('Requested variable %s does not exist \n',OutputList{j});
//        end
//    end
//end


std::tuple<MatrixXcd, MatrixXcd> SecularInteractionsMatrix(
        VectorXd n,
        VectorXd mu,
        VectorXd aor,
        double J2
) {
//calculate the interaction matrix that will describe the equation dz/dt=Az
//where z is a vector of the complex eccentricities and for the similar
//equation for I*exp(i*Omega) using the matrix BB
    int Npl = n.size();
    MatrixXcd AA = MatrixXcd::Ones(Npl, Npl);
    MatrixXcd BB = MatrixXcd::Ones(Npl, Npl);
    for (int j1 = 0; j1 < Npl; ++j1) {
        for (int j2 = j1; j2 < Npl; ++j2) {
            if (j1 != j2) {
                double alph = pow(n(j2) / n(j1), 2.0 / 3.0) * pow((1 + mu(j1)) / (1 + mu(j2)), 1.0 / 3.0);
                double A1 = lc.calculate(alph, 1.0 / 2.0, 1);

                double DA1 = lc.calculate(alph, 1.0 / 2.0, 1, 1);
                double D2A1 = lc.calculate(alph, 1.0 / 2.0, 1, 2);

                double f10 = 0.5 * A1 - 0.5 * alph * alph * D2A1; // Calculate f2 and f10 for j=0
                AA(j1, j2) = n(j1) * mu(j2) / (1 + mu(j1)) * alph * f10;
                AA(j2, j1) = n(j2) * mu(j1) / (1 + mu(j2)) * f10;

                double B1 = lc.calculate(alph, 3.0 / 2.0, 1);
                double f14 = alph * B1;
                BB(j1, j2) = 0.25 * n(j1) * mu(j2) / (1 + mu(j1)) * alph * f14;
                BB(j2, j1) = 0.25 * n(j2) * mu(j1) / (1 + mu(j2)) * f14;
            } else {
                // Handle the case when j1 == j2
                VectorXd alphvec(Npl);
                for (int i = 0; i < j1; i++) {
                    alphvec[i] = pow((n[i] / n[j1]), -2. / 3.) * pow((1 + mu[i]) / (1 + mu[j1]), 1. / 3.);
                }
                for (int i = j1 + 1; i < Npl; i++) {
                    alphvec[i] = pow((n[i] / n[j1]), 2. / 3.) * pow((1 + mu[i]) / (1 + mu[j1]), -1. / 3.);
                }

                auto DA0 = lc.calculate(alphvec, 1 / 2., 0, 1);
                auto D2A0 = lc.calculate(alphvec, 1 / 2., 0, 2);
                VectorXd f2 = 0.25 * alphvec.array() * DA0.array() + (1 / 8.) * alphvec.array().pow(2) * D2A0.array();

                VectorXd TotalVecA(Npl);
                for (int i = 0; i < j1; i++) {
                    TotalVecA[i] = mu[i] / (1. + mu[j1]) * f2[i];
                }
                for (int i = j1; i < Npl; i++) {
                    TotalVecA[i] = alphvec[i] * mu[i] / (1. + mu[j1]) * f2[i];
                }
                AA(j1, j2) = n(j1) * 2. * (TotalVecA.sum()) +
                             n[j1] * (3. / 2. * J2 / (aor[j1] * aor[j1]) - 9. / 8. * J2 * J2 / pow(aor[j1], 4.));

                VectorXd B1 = lc.calculate(alphvec, 3 / 2., 1);
                VectorXd f3 = -0.5 * alphvec.array() * B1.array();
                VectorXd TotalVecB(Npl);
                for (int i = 0; i < j1; i++) {
                    TotalVecB[i] = mu[i] * f3[i];
                }
                for (int i = j1; i < Npl; i++) {
                    TotalVecB[i] = alphvec[i] * mu[i] * f3[i];
                }
                BB(j1, j2) = 0.25 * n(j1) * 2 * (TotalVecB.sum()) -
                             n(j1) * (3. / 2. * J2 / (aor[j1] * aor[j1]) - 27. / 8. * J2 * J2 / pow(aor[j1], 4.));
            }
        }
    }

    for (int i = 0; i < Npl; ++i) {
        for (int j = 0; j < Npl; ++j) {
            AA(i, j) = 1i * AA(i, j);
            BB(i, j) = 1i * BB(i, j);
        }
    }

    return std::make_tuple(AA, BB);
}

std::tuple<MatrixXcd, MatrixXcd, MatrixXcd, MatrixXd, MatrixXcd, MatrixXcd, MatrixXcd, MatrixXd>
ResonantInteractions2(
        VectorXd P,
        VectorXd mu,
        MatrixXd Lambda,
        MatrixXcd free_all,
        MatrixXcd free_i,
        VectorXd tT,
        VectorXi BeginIdx,
        VectorXi EndIdx,
        int ExtraIters,
        VectorXd OrdersAll,
        bool Calculate3PlanetsCrossTerms,
        int MaxPower
) {
//calculate the forced orbital elements due to near-resonant interactions

    VectorXd n = 2 * M_PI / P.array();
    LaplaceCoeff lc;
//pair-wise interactions
    auto [dz, dLambda, du, da_oa] = ForcedElements1({lc, P, Lambda, mu, free_all, free_i, OrdersAll, MaxPower});

//triplets interactions
    if (Calculate3PlanetsCrossTerms) {
        dLambda += ForcedMeanLongitudes3PlanetsCrossTerms(lc, P, Lambda, mu, free_all);
    }

//iterative solution to the equations, if desired
    for (size_t jj = 0; jj < ExtraIters; ++jj) {
        MatrixXd new_Lambda = Lambda + dLambda;
        MatrixXcd new_free_all = free_all + dz;
        MatrixXcd new_free_i = free_i + du;
        auto [dz, dLambda, du, da_oa] = ForcedElements1({lc, P, new_Lambda, mu, new_free_all, new_free_i, OrdersAll,
                                                         MaxPower});
        if (Calculate3PlanetsCrossTerms) {
            dLambda += ForcedMeanLongitudes3PlanetsCrossTerms(lc, P, new_Lambda, mu, new_free_all);
        }
    }

//calculate the total eccentricity and inclination for each nominal transit time
    MatrixXcd z = free_all + dz;
    MatrixXcd u = free_i + du;

//translate to true lonEigen::PlainObjectBase<Eigen::Matrix<std::complex<double>, -1, -1, 0, -1, -1> > = {Eigen::PlainObjectBase<Eigen::Matrix>} gitude#
    MatrixXd dTheta;

MatrixXd dTheta1 = 2 * (z.conjugate().array() * dLambda.array()
                        + dz.conjugate().array() / 1i).array().real();
MatrixXd dTheta2 = 5. / 2. *
                   (z.conjugate().array().pow(2) *
                    dLambda.array() +
                    z.conjugate().array()*
                    dz.conjugate().array()* z.conjugate().array() /
                    1i).array().real();
MatrixXd dTheta3 = 13. / 4. * (z.conjugate().array().pow(3) * dLambda.array() +
                               z.conjugate().array().pow(2) * dz.conjugate().array() / 1i).array().real();
MatrixXd dTheta4 = - 1. / 4. * ((z.array() * dz.conjugate().array() + z.conjugate().array() * dz.array()).array() *
                                z.conjugate().array() / 1i + z.array() * z.conjugate().array() *
                                                             (z.conjugate().array() * dLambda.array() +
                                                              dz.conjugate().array() / 1i)).array().real();
MatrixXd dTheta5 = 103. / 24 * (z.conjugate().array().pow(4) * dLambda.array() +
                                z.conjugate().array().pow(3) * dz.conjugate().array() / 1i).array().real();
MatrixXd dTheta6 = - 11. / 24 *
                   ((z.array() * dz.conjugate().array() + z.conjugate().array() * dz.array()).array() * z.conjugate().array().pow(2) /
                    1i + 2 * z.array() * z.conjugate().array() *
                         (z.conjugate().array().pow(2) * dLambda.array() + z.conjugate().array() * dz.conjugate().array() / 1i)).array().real();
    dTheta = dLambda + dTheta1 + dTheta2 + dTheta3 + dTheta4 + dTheta5+ dTheta6;



//translate the variations in true longitude to variations in time using the planetary angular velocity, taking into account the variations in n

    auto reshaped_n = oct_repmat(n.transpose(), z.rows(), 1);
    MatrixXd TTV = -dTheta.array() / (reshaped_n.array() * (1 - 1.5 * da_oa.array())).array();
    TTV = TTV.array() * ((1 - z.array().abs().pow(2)).array().pow(3. / 2)).array();
    TTV = (TTV.array() / ((z.array().real().array() + 1).array().pow(2))).matrix();

    return std::make_tuple(dz, dLambda, du, da_oa, dTheta, z, u, TTV);
}

/*
std::tuple<MatrixXcd, MatrixXcd,MatrixXcd,MatrixXcd,MatrixXcd, MatrixXcd, MatrixXcd, MatrixXcd> ResonantInteractions1(
        VectorXd P,
        VectorXd mu,
        VectorXd aor,
        VectorXd Lambda,
        VectorXd free_all,
        VectorXd free_i,
        VectorXd tT,
        VectorXi BeginIdx,
        VectorXi EndIdx,
        int ExtraIters,
        VectorXd OrdersAll,
        bool Calculate3PlanetsCrossTerms,
        int MaxPower
        ){
// calculate the forced orbital elements due to near-resonant interactions
VectorXd n = 2 * M_PI/P;
LaplaceCoeff lc;
// pair-wise interactions
auto [dz,dLambda,du,da_oa] = ForcedElements1(lc,P, Lambda, system.mu, free_all, free_i, OrdersAll, MaxPower);

//Triplets interactions
if (Calculate3PlanetsCrossTerms) {
    dLambda += ForcedMeanLongitudes3PlanetsCrossTerms(lc,P, Lambda, system.mu, free_all);
}

//iterative solution to the equations, if desired
for (int jj = 0; jj < ExtraIters; ++jj){
    auto [dz,dLambda,du,da_oa] = ForcedElements1(lc, P, Lambda+dLambda, system.mu, free_all+dz, free_i+du, OrdersAll, MaxPower);
    if (Calculate3PlanetsCrossTerms) {
        dLambda += ForcedMeanLongitudes3PlanetsCrossTerms(lc,P, Lambda+dLambda, system.mu, free_all+dz);
    }
}

// Calculate the total eccentricity and inclination for each nominal transit time
auto z = free_all + dz;
auto u = free_i + du;

//Translate to true longitude
VectorXd dTheta = dLambda +
                  2.0 * (z.conjugate().array() * dLambda.array() + dz.conjugate().array() / complex<double>(0.0, 1.0)).real() +
                  5.0 / 2.0 * (z.array().pow(2) * dLambda.array() + z.array() * dz.conjugate().array() * z.conjugate().array() / complex<double>(0.0, 1.0)).real() +
                  13.0 / 4.0 * (z.array().pow(3) * dLambda.array() + z.array().pow(2) * dz.conjugate().array() / complex<double>(0.0, 1.0)).real() -
                  1.0 / 4.0 * ((z.array() * dz.conjugate().array() + z.array().conjugate() * dz.array()).array() * z.conjugate().array() / complex<double>(0.0, 1.0) +
                               z.array() * z.array().conjugate() * (z.conjugate().array() * dLambda.array() + dz.conjugate().array() / complex<double>(0.0, 1.0))).real() +
                  103.0 / 24.0 * (z.array().pow(4) * dLambda.array() + z.array().pow(3) * dz.conjugate().array() / complex<double>(0.0, 1.0)).real() -
                  11.0 / 24.0 * ((z.array() * dz.conjugate().array() + z.array().conjugate() * dz.array()).array() * z.array().pow(2) / complex<double>(0.0, 1.0) +
                                 2.0 * z.array() * z.array().conjugate() * (z.array().pow(2) * dLambda.array() + z.array() * dz.conjugate().array() / complex<double>(0.0, 1.0))).real();

// Translate the variations in true longitude to variations in time using the planetary angular velocity, taking into account the variations in n
auto TTV = -dTheta.array()/ (oct_repmat(n,z.size(),1).array()*(1-1.5*da_oa.array())).array()*((1-z.abs().array().pow(2)).array().pow(3/2)).array()/((1+z.array().real()).array().pow(2));
return std::make_tuple(dz,dLambda,du,da_oa,dTheta,z,u,TTV);
}
*/
std::tuple<VectorXd, VectorXd, VectorXd> CalcRV(MatrixXd Lambda, MatrixXcd z, MatrixXcd u, VectorXd mu, VectorXd aor, VectorXd n, bool CalcAstrometry) {
    int Nt = z.rows();
    MatrixXd Pom = MatrixXd::Zero(z.rows(), z.cols());
    for (size_t i = 0; i < z.rows(); i++) {
        for (size_t j = 0; j < z.cols(); j++) {
            Pom(i, j) = std::atan2(z(i, j).imag(), z(i, j).real());
        }
    }
    MatrixXd Om = MatrixXd::Zero(z.rows(), z.cols());
    for (int i = 0; i < z.rows(); i++) {
        for (int j = 0; j < z.cols(); j++) {
            Om(i, j) = std::atan2(u(i, j).imag(), u(i, j).real());
        }
    }
    MatrixXd e = (z.array().abs());
    MatrixXd I = (u.array().abs());
    MatrixXd om = Pom - Om;
    MatrixXd M = Lambda - Pom;

    // Calculate sin(f) and cos(f) to 2nd order in e, Solar System Dynamics eq. 2.84 and 2.85, page 40
    MatrixXd sinf = (M.array().sin() + e.array() * sin(2.0 * M.array()).array() + e.array().square() * (9.0 / 8.0 * sin(3.0 * M.array()).array() - 7.0 / 8.0 * M.array().sin()).array() + e.array().cube() * (4.0 / 3.0 * sin(4.0 * M.array().array()).array() - 7.0 / 6.0 * sin(2.0 * M.array()).array()) + e.array().pow(4) * (17.0 / 192.0 * M.array().sin() - 207.0 / 128.0 * sin(3.0 * M.array().array() + 625.0 / 384.0 * sin(5.0 * M.array()).array())));
    MatrixXd cosf = (M.array().cos() + e.array() * (cos(2.0 * M.array()).array() - 1.0) + 9.0 / 8.0 * e.array().square() * (cos(3.0 * M.array()).array() - M.array().cos()) + 4.0 / 3.0 * e.array().cube() * (cos(4.0 * M.array()).array() - cos(2.0 * M.array()).array()) + e.array().pow(4) * (25.0 / 192.0 * cos(M.array()) - 225.0 / 128.0 * cos(3.0 * M.array()) + 625.0 / 384.0 * cos(5.0 * M.array()).array()));

    // Calculate the planets astrocentric velocities on the x axis
    MatrixXd n_rep = oct_repmat(n.transpose(), Nt, 1);
    MatrixXd aor_rep = oct_repmat(aor.transpose(), Nt, 1);
    MatrixXd mu_rep = oct_repmat(mu.transpose(), Nt, 1);
    MatrixXd Xdot_o_r = -n_rep.array()*aor_rep.array() / (1.0 - e.array().square()).sqrt() * ((cos(om.array()).array() * cos(Om.array()).array() - sin(om.array()).array() * sin(Om.array()).array() * cos(I.array()).array()).array() * sinf.array() + (sin(om.array()).array() * cos(Om.array()).array() + cos(om.array()).array() * sin(Om.array()).array() * cos(I.array()).array()).array() * (e.array() + cosf.array()).array());
    VectorXd Xsdot_o_r = -(mu_rep.array() * Xdot_o_r.array()).array().rowwise().sum() / (1.0 + mu.array().sum());
    VectorXd RV_o_r = -Xsdot_o_r;
    VectorXd Y_o_r;

    VectorXd Z_o_r;
    if (CalcAstrometry) {
        // Calculate the planets' astrocentric positions on the y and z axes
        MatrixXd y1 = aor_rep.array() * (1.0 - e.array().square()).array() / (1.0 + e.array()*cosf.array()).array();
        MatrixXd y2 = sin(Om.array()).array() * (cos(om.array()).array() * cosf.array() - sin(om.array()).array() * sinf.array()) + cos(Om.array()).array() * cos(I.array()).array() * (sin(om.array()).array() * cosf.array() + cos(om.array()).array()*sinf.array());
        MatrixXd y = y1.array() * y2.array();

        MatrixXd z = y1.array() * sin(I.array()).array()*(sin(om.array()).array()*cosf.array()+cos(om.array()).array()*sinf.array());
                //sin(I).*(sin(om).*cosf+cos(om).*sinf);
        Y_o_r = -(mu_rep.array() * y.array()).array().rowwise().sum() / (1.0 + mu.array().sum());
        Z_o_r = -(mu_rep.array() * z.array()).array().rowwise().sum() / (1.0 + mu.array().sum());
    }

    return std::make_tuple(RV_o_r, Y_o_r, Z_o_r);
}

std::tuple<MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, MatrixXi, VectorXd, VectorXd>
TransitsProperties(
        int Nt,
        int Npl,
        VectorXi BeginIdx,
        VectorXi EndIdx,
        VectorXd t,
        VectorXd tT,
        MatrixXd TTV,
        VectorXd P,
        VectorXd n,
        MatrixXd da_oa,
        VectorXd aor,
        MatrixXcd zT,
        MatrixXcd uT,
        VectorXd ror,
        bool CalcDurations
) {
    //translate orbital elements at transits to transits parameters

    //pre-allocate memory for transit properties: duration, ingress-egress, angular velocity, planet-star separation, impact parameter.
    MatrixXd D = MatrixXd::Zero(Nt, Npl);       //duration
    MatrixXd Tau = MatrixXd::Zero(Nt, Npl);
    MatrixXd w = MatrixXd::Zero(Nt, Npl);   //angular-momentum
    MatrixXd d = MatrixXd::Zero(Nt, Npl);
    MatrixXd b = MatrixXd::Zero(Nt, Npl);


    std::vector<Eigen::VectorXd> w1(Npl);
    std::vector<Eigen::VectorXd> d1(Npl);
    std::vector<Eigen::VectorXd> b1(Npl);
    std::vector<Eigen::VectorXd> D1(Npl);
    std::vector<Eigen::VectorXd> Tau1(Npl);

    MatrixXd AssociatedTmid = MatrixXd::Zero(Nt, Npl);
    MatrixXi AssociatedInd = MatrixXi::Zero(Nt, Npl);

    VectorXd dbdt0 = VectorXd::Zero(Npl);
    VectorXd b00 = VectorXd::Zero(Npl);

    for (size_t j = 0; j < Npl; ++j) {
        //indices
        VectorXi Idx = VectorXi::LinSpaced(EndIdx(j) - BeginIdx(j) + 1, BeginIdx(j), EndIdx(j));

        //Associate time-of-mid-transit to each time stamp
        auto [AssociatedTmid0, AssociatedInd0] = AssociateTmid(t, tT(Idx).array() + TTV(Idx, j).array(), P(j));
        AssociatedTmid.col(j) = AssociatedTmid0;
        AssociatedInd.col(j) = AssociatedInd0;

        //based on the presumed orbital elements as a function of time, calculate
        //the transit parameters - Duration, Ingress-Egress, angular velocity,
        //planet-star distance and impact parameter, and translate them to
        //light-curve
        if (!CalcDurations) {
            auto [w1_j, d1_j, b1_j, D1_j, Tau1_j] = OrbitalElements2TransitParams(
                    n(j) * (1 - 1.5 * da_oa(Idx, j).array()), aor(j) * (1 + da_oa(Idx, j).array()),
                    zT(Idx, j).array().real(), zT(Idx, j).array().imag(), uT(Idx, j).array().abs(),
                    uT(Idx, j).array().arg(), ror(j)); //treating I, Omega, aor, n as varying
            w1[j] = w1_j;
            d1[j] = d1_j;
            b1[j] = b1_j;
            for (int i = 0; i < AssociatedInd.rows(); ++i) {
                w(i, j) = w1_j(AssociatedInd(i, j));
                d(i, j) = d1_j(AssociatedInd(i, j));
                b(i, j) = b1_j(AssociatedInd(i, j));
            }
        } else {
            auto [w1_j, d1_j, b1_j, D1_j, Tau1_j] = OrbitalElements2TransitParams(
                    n(j) * (1 - 1.5 * da_oa(Idx, j).array()), aor(j) * (1 + da_oa(Idx, j).array()),
                    zT(Idx, j).array().real(), zT(Idx, j).array().imag(), uT(Idx, j).array().abs(),
                    uT(Idx, j).array().arg(), ror(j)); //treating I, Omega, aor, n as varying
            w1[j] = w1_j;
            d1[j] = d1_j;
            b1[j] = b1_j;
            D1[j] = D1_j;
            Tau1[j] = Tau1_j;

            int rows = AssociatedInd.rows();
            for (int i = 0; i < rows; ++i) {
                w(i, j) = w1_j(AssociatedInd(i, j));
                d(i, j) = d1_j(AssociatedInd(i, j));
                b(i, j) = b1_j(AssociatedInd(i, j));
                Tau(i, j) = Tau1_j(AssociatedInd(i, j));
                D(i, j) = D1_j(AssociatedInd(i, j));
            }
        }


        //calculate db_dt and b0

         if (Idx.size()<=1){
            dbdt0[j] = 0;
            b00[j] = 0;
        }
         else {
             auto [dbdt0_j, b00_j, aerr, berr] = fitLinear(tT(Idx), b1[j]);


             dbdt0[j] = dbdt0_j;
             b00[j] = b00_j;
         }

    }
    return std::make_tuple(D, D1, Tau, Tau1, w, w1, d, d1, b, b1, AssociatedTmid, AssociatedInd, dbdt0, b00);
}

//function to find median of Eigen::VectorXd
double median(VectorXd vec) {
    std::nth_element(vec.data(), vec.data() + vec.size() / 2, vec.data() + vec.size());
    return vec(vec.size() / 2);
}


//vertical func from matlab
VectorXd vertical(VectorXd vec) {
    VectorXd vec1 = vec.transpose();
    return vec1;
}


MatrixXd GenerateLightCurve(const int Nt,
                            const int Npl,
                            const int Ncad,
                            const VectorXi &CadenceType,
                            const VectorXd &t,
                            const VectorXd &BinningTime,
                            const MatrixXd &AssociatedTmid_val,
                            const MatrixXd &w,
                            const MatrixXd &d,
                            const MatrixXd &b,
                            const VectorXd &ror,
                            double u1,
                            double u2) {

    //construct the light-curve for each planet based on the transit properties
    //of each individual event

    // Initialize the light curve matrix
    MatrixXd AllLC = MatrixXd::Zero(Nt, Npl);

    // Go over cadence types
    for (size_t jc = 0; jc < Ncad; ++jc) {
        // Index of data corresponding to the cadence type
        VectorXi indc(0);
        VectorXd t_indc(0);
        //MatrixXd w_indc(0);
        //MatrixXd d_indc(0);
        //MatrixXd b_indc(0);
        for (size_t j2 = 0; j2 < CadenceType.size(); ++j2) { //TODO: Error vielleicht, dass matlab direkt überspringt, wenn er median(leer) aufruft? -> Überprüfen
            if (CadenceType(j2) == jc + 1) {
                indc.conservativeResize(indc.size() + 1);
                indc(indc.size() - 1) = jc + 1;
                //indc (j2) = 1;
                t_indc.conservativeResize(t_indc.size() + 1);
                t_indc(t_indc.size() - 1) = t[j2];
                /*w_indc.conservativeResize(w_indc.size()+1);
                w_indc(w_indc.size()-1) = w[j2];
                d_indc.conservativeResize(d_indc.size()+1);
                d_indc(d_indc.size()-1) = d[j2];
                b_indc.conservativeResize(b_indc.size()+1);
                b_indc(b_indc.size()-1) = b[j2];
                */
            }
            //else{
            //    indc(j2)=0;
            //}
        }

        //VectorXd t_indc = t(indc);
        VectorXd diff_t_indc(t_indc.size() - 1);
        for (size_t i = 1; i < t_indc.size(); ++i) {
            diff_t_indc[i - 1] = t_indc[i] - t_indc[i - 1];
        }


        // construct the integration vector based on half integration time and number
        // of neighbors on each side, which is a function of the binning time

        double dtHalfIntegration = calculateMedian(diff_t_indc) / 2;
        int nNeighbors = round(dtHalfIntegration / BinningTime(jc));
        double IntegrationStep = 2 * dtHalfIntegration / (2 * nNeighbors + 1);
        VectorXd IntegrationVector(2 * nNeighbors + 1);
        for (int i = 0; i <= 2 * nNeighbors; ++i) {
            IntegrationVector[i] = -dtHalfIntegration + IntegrationStep * (0.5 + i);
        }
        int NI = IntegrationVector.size();

        // Create a table of times around the original time
        MatrixXd t1 = t_indc.replicate(1, IntegrationVector.size()) +
                      IntegrationVector.transpose().replicate(t_indc.size(), 1);
        // Go over planets one by one and generate planetary light curve
        for (int j = 0; j < Npl; ++j) {
            // Calculate the phase (for the specific cadence type jc)
            VectorXd astmid = AssociatedTmid_val.col(j);
            MatrixXd astmid_repmat = oct_repmat(astmid, 1, NI);
            MatrixXd w_repmat = oct_repmat(w(indc, j), 1, NI);
            MatrixXd Phi = (t1.array() - astmid_repmat.array()).array() * w_repmat.array();

            // Calculate position on the sky with respect to the stellar center in units of stellar radii and using its own w, a, b (for the specific cadence type jc)
            MatrixXd x = oct_repmat(d(indc, j), 1, NI).array() * sin(Phi.array()).array();
            MatrixXd y = oct_repmat(b(indc, j), 1, NI).array() * cos(Phi.array()).array();
            // Calculate the distance from stellar disk center. For phases larger than 0.25 null the Mandel-Agol model by using Rsky>1+Rp (for the specific cadence type jc)
            MatrixXd Rsky = (x.array().square() + y.array().square()).array().sqrt();
            for (size_t ro = 0; ro < Rsky.rows(); ++ro) {
                for (size_t colu = 0; colu < Rsky.cols(); ++colu) {
                    if (abs(Phi(ro, colu)) > 0.25) {
                        Rsky(ro, colu) = 5.0;
                    }
                }
            }
            VectorXd Rsky_vector = Eigen::Map<VectorXd>(Rsky.data(), Rsky.size());
            // Calculate the instantaneous Mandel-Agol model for each time stamp in the binned time vector (for the specific cadence type jc)
            VectorXd lc = MALookupTab.LookupValue(ror(j), Rsky_vector, u1, u2);

            int rows = Rsky.rows();
            int cols = Rsky.cols();
            MatrixXd lc_reshape = Eigen::Map<Eigen::MatrixXd>(lc.data(), rows, cols);
            // Bin the light-curve by averaging and assign it to the relevant positions of AllLC
            //        AllLC(indc,j) = mean(reshape(lc,size(Rsky)),2);
            VectorXd test = lc_reshape.rowwise().mean();
            for (int i = 0; i < indc.size(); ++i) {
                if(!indc(i)) {
                    continue;
                }
                AllLC(i, j) = lc_reshape.rowwise().mean()(i);
            }
            //AllLC(indc.array()-1, j) = lc_reshape.rowwise().mean(); //TODO: this line is way different in matlab and might have to be changed, but raised errors
        }
    }
    return AllLC;
}

