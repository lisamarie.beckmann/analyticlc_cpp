#pragma once

#include <helper_functions.hpp>

VectorXd rc(const VectorXd& x, const VectorXd& y, double ERRTOL);
double rc(double x, double y, double ERRTOL);