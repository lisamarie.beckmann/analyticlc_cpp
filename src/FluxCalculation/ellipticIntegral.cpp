#include <complex>
#include "ellipticIntegral.hpp"
#include <Eigen/Dense>

double ellecVec(double k) {
// Computes polynomial approximation for the complete elliptic
// integral of the second kind (Hasting's approximation):
    double m1 = 1 - k * k;
    if (m1 < std::numeric_limits<double>::epsilon()) {
        m1 = std::numeric_limits<double>::epsilon();
    }

    double a1 = 0.44325141463;
    double a2 = 0.06260601220;
    double a3 = 0.04757383546;
    double a4 = 0.01736506451;
    double b1 = 0.24998368310;
    double b2 = 0.09200180037;
    double b3 = 0.04069697526;
    double b4 = 0.00526449639;

    double ee1 = 1 + m1 * (a1 + m1 * (a2 + m1 * (a3 + m1 * a4)));
    double ee2 = m1 * (b1 + m1 * (b2 + m1 * (b3 + m1 * b4))) * (-1 * log(m1));

    return ee1 + ee2;
}

Eigen::VectorXd ellecVec(Eigen::VectorXd k) {
    Eigen::VectorXd result(k.size());
    for (int i = 0; i < k.size(); i++) {
        result(i) = ellecVec(k(i));
    }
    return result;
}

double ellkVec(double k) {
// Computes polynomial approximation for the complete elliptic
// integral of the first kind (Hasting's approximation):
    double m1 = 1 - k * k;
    if (m1 < std::numeric_limits<double>::epsilon()) {
        m1 = std::numeric_limits<double>::epsilon();
    }

    double a0 = 1.38629436112;
    double a1 = 0.09666344259;
    double a2 = 0.03590092383;
    double a3 = 0.03742563713;
    double a4 = 0.01451196212;
    double b0 = 0.5;
    double b1 = 0.12498593597;
    double b2 = 0.06880248576;
    double b3 = 0.03328355346;
    double b4 = 0.00441787012;

    double ek1 = a0 + m1 * (a1 + m1 * (a2 + m1 * (a3 + m1 * a4)));
    double ek2 = (b0 + m1 * (b1 + m1 * (b2 + m1 * (b3 + m1 * b4)))) * log(m1);

    return ek1 - ek2;
}

Eigen::VectorXd ellkVec(Eigen::VectorXd k) {
    Eigen::VectorXd result(k.size());
    for (int i = 0; i < k.size(); i++) {
        result(i) = ellkVec(k(i));
    }
    return result;
}

