#include "Flux_rj.hpp"
#include "Flux_rc.hpp"
#include <algorithm>


VectorXd rj(double x_d, VectorXd y, double z_d, VectorXd p, double ERRTOL) {
    VectorXd fac = VectorXd::Ones(y.size());
    VectorXd x = VectorXd::Constant(y.size(), x_d);
    VectorXd z = VectorXd::Constant(y.size(), z_d);
    VectorXd xt = x;
    VectorXd yt = y;
    VectorXd zt = z;
    VectorXd pt = p;
    VectorXd a = VectorXd::Zero(y.size());
    VectorXd b = VectorXd::Zero(y.size());
    VectorXd rho = VectorXd::Zero(y.size());
    VectorXd tau = VectorXd::Zero(y.size());
    VectorXd rcx = VectorXd::Zero(y.size());
    VectorXd sqrtx = VectorXd::Zero(y.size());
    VectorXd sqrty = VectorXd::Zero(y.size());
    VectorXd sqrtz = VectorXd::Zero(y.size());
    VectorXd alamb = VectorXd::Zero(y.size());
    VectorXd alpha = VectorXd::Zero(y.size());
    VectorXd beta = VectorXd::Zero(y.size());
    VectorXd Sum = VectorXd::Zero(y.size());
    VectorXd ave = VectorXd::Zero(y.size());
    MatrixXd ave4 (y.size(), 4);

//% built for x,z scalars and y,p vectors
//x=x*fac; z=z*fac;
// pt=rj; a=rj; b=rj; rho=rj; tau=rj; rcx=rj;
//sqrtx=rj; sqrty=rj; sqrtz=rj; alamb=rj; alpha=rj; beta=rj; Sum=rj; ave=rj;
//ave4=ave*ones(1,4);
    ERRTOL = 0.05 * ERRTOL;
    double C1 = 3.0 / 14.0;
    double C2 = 1.0 / 3.0;
    double C3 = 3.0 / 22.0;
    double C4 = 3.0 / 26.0;
    double C5 = 0.75 * C3;
    double C6 = 1.5 * C4;
    double C7 = 0.5 * C2;
    double C8 = C3 + C3;

    for (int i = 0; i < y.size(); i++) {
        if (p[i] <= 0) {
            double min_xyz = std::min({x[i], y[i], z[i]});
            double max_xyz = std::max({x[i], y[i], z[i]});
            xt[i] = min_xyz;
            yt[i] = x[i] + y[i] + z[i] - min_xyz - max_xyz;
            zt[i] = max_xyz;
            a[i] = 1.0 / (yt[i] - p[i]);
            b[i] = a[i] * (zt[i] - yt[i]) * (yt[i] - xt[i]);
            pt[i] = yt[i] + b[i];
            rho[i] = xt[i] * zt[i] / yt[i];
            tau[i] = p[i] * pt[i] / yt[i];
            rcx[i] = rc(rho[i], tau[i], ERRTOL);
        }
    }
    Eigen::MatrixXd del4(y.size(), 4);
    bool del4_too_big = true;
    while (del4_too_big) {
        del4_too_big = false;
        for (int i = 0; i < y.size(); i++) {
            sqrtx[i] = sqrt(xt[i]);
            sqrty[i] = sqrt(yt[i]);
            sqrtz[i] = sqrt(zt[i]);
            alamb[i] = sqrtx[i] * (sqrty[i] + sqrtz[i]) + sqrty[i] * sqrtz[i];
            alpha[i] = pow(pt[i] * (sqrtx[i] + sqrty[i] + sqrtz[i]) + sqrtx[i] * sqrty[i] * sqrtz[i], 2);
            beta[i] = pt[i] * pow(pt[i] + alamb[i], 2);
            Sum[i] += fac[i] * rc(alpha[i], beta[i], ERRTOL);
            fac[i] *= 0.25;
            xt[i] = 0.25 * (xt[i] + alamb[i]);
            yt[i] = 0.25 * (yt[i] + alamb[i]);
            zt[i] = 0.25 * (zt[i] + alamb[i]);
            pt[i] = 0.25 * (pt[i] + alamb[i]);
            ave[i] = 0.2 * (xt[i] + yt[i] + zt[i] + pt[i] + pt[i]);
            ave4.row(i) = ave[i] * Eigen::VectorXd::Ones(4);;
        }

        del4 << xt, yt, zt, pt;
        del4 = (ave4 - del4).array()/ave4.array();

        for(int i = 0; i < del4.rows(); ++i) {
            for(int j = 0; j < del4.cols(); ++j) {
                del4_too_big = del4_too_big || (fabs(del4(i, j)) > ERRTOL);
            }
        }

    }
    VectorXd ea = (del4.col(0).array() * (del4.col(1).array() + del4.col(2).array()) + del4.col(1).array() * del4.col(2).array());
    VectorXd eb = del4.col(0).array() * del4.col(1).array() * del4.col(2).array();
    VectorXd ec = del4.col(3).array().pow(2);
    VectorXd ed = ea.array() - 3 * ec.array();
    VectorXd ee = eb.array() + 2 * del4.col(3).array() * (ea - ec).array();

    VectorXd rj = 3 * Sum.array() +
                  fac.array() * (
                          1 + ed.array() * (-C1 + C5 * ed.array() - C6 * ee.array()) +
                          eb.array() * (C7 + del4.col(3).array() * (-C8 + del4.col(3).array() * C4)) +
                          del4.col(3).array() * ea.array() * (C2 - del4.col(3).array() * C3) -
                          C2 * del4.col(3).array() * ec.array()
                  ) / (ave.array() * ave.array().sqrt());


    for (int j = 0; j < p.size(); j++) {
        if (p[j] <= 0) {
            assert(false);
            //rj[j] = a[j] * (b[j] * rj[j] + 3 * (rcx[j] - rf(xt[j], yt[j], zt[j], ERRTOL)));
        }
    }
    return rj;
}

double rj(double x_d, double y, double z_d, double p, double ERRTOL){
    VectorXd p_vec = VectorXd::Constant(1, p);
    VectorXd y_vec = VectorXd::Constant(1, y);
    VectorXd rj_m = rj(x_d, y_vec, z_d, p_vec, ERRTOL);
    return rj_m[0];
}