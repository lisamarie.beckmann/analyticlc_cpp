#pragma once

#include "FluxCalculation/Flux_rc.hpp"
#include "FluxCalculation/Flux_rj.hpp"
#include "FluxCalculation/Flux_rf.hpp"

#include <cmath>
#include <complex>
#include "helper_functions.hpp"
#include <functional>
#include <iostream>

#include "ellipticIntegral.hpp"


using namespace Eigen;

VectorXd polyval(const VectorXd& coeffs, const VectorXd& x);
double polyval(const VectorXd& coeffs, const double x);
VectorXd polyfit(const VectorXd& x, const VectorXd& y, int degree);
std::tuple<VectorXd, VectorXd> occultquadVec(double p, VectorXd z, double u1, double u2, double TolFactor = 1, bool ToSmooth = true);
std::tuple<double,double> occultquadVec(double p, double z, double u1, double u2, double TolFactor=1, bool TooSmooth = true);