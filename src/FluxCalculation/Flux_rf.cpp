#include "Flux_rf.hpp"


//AS FAR AS I CAN SEE THIS FUNCTION IS NEVER USED
/*
VectorXd rf(const VectorXd& x, const VectorXd& y, const VectorXd& z, double ERRTOL) {
    //Carlson symmetric elliptic integral RF(x, y, z)
    int n = x.size()+y.size()+z.size();
    VectorXd xyz = VectorXd::Zero(n);
    xyz.head(x.size()) = x;
    xyz.segment(x.size(),y.size()) = y;
    xyz.tail(z.size()) = z;
    VectorXd sqrtxyz = xyz.array().sqrt();
    VectorXd alamb=VectorXd::Zero(n);
    VectorXd ave=VectorXd::Zero(n);
    VectorXd ave3=VectorXd::Zero(n);
    VectorXd del3=VectorXd::Zero(n);

    ERRTOL = 0.08*ERRTOL;
    double THIRD = 1.0/3.0;
    double C1 = 1.0/24.0;
    double C2 = 0.1;
    double C3 = 3.0/44.0;
    double C4 = 1.0/14.0;
    VectorXd xyzt = xyz;

    bool done = false;
    while (!done) {
        for (int i = 0; i < n; i++) {
            alamb(i) = sqrtxyz(i) * (sqrtxyz(i) + sqrtxyz(i + n) + sqrtxyz(i + 2 * n));
            xyzt(i) = 0.25 * (xyzt(i) + alamb(i));
            ave(i) = THIRD * (xyzt(i) + xyzt(i + n) + xyzt(i + 2 * n));
            ave3(i) = ave(i);
            del3(i) = (ave3(i) - xyzt(i)) / ave3(i);
        }

        bool max_del3_exceeded = false;
        for (int i = 0; i < n; i++) {
            if (fabs(del3(i)) > ERRTOL) {
                max_del3_exceeded = true;
                break;
            }
        }

        done = !max_del3_exceeded;


        VectorXd e2(n);
        VectorXd e3(n);
        VectorXd rf_values(n);
        for (int i = 0; i < n; i++) {
            e2(i) = del3(i) * del3(i + n) - del3(i + 2 * n) * del3(i + 2 * n);
            e3(i) = del3(i) * del3(i + n) * del3(i + 2 * n);
            rf_values(i) = 1.0 + (C1 * e2(i) - C2 - C3 * e3(i)) * e2(i) + C4 * e3(i);
            rf_values(i) = sqrt(ave(i)) / rf_values(i);
        }

        return rf_values;
    }

}
 */