#pragma once

#include <helper_functions.hpp>

VectorXd rj(double x_d, VectorXd y, double z_d, VectorXd p, double ERRTOL);
double rj(double x_d, double y, double z_d, double p, double ERRTOL);