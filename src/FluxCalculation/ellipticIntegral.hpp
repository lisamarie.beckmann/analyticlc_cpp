#pragma once

#include <limits>
#include <functional>
#include <Eigen/Dense>

double ellecVec(double k);
Eigen::VectorXd ellecVec(Eigen::VectorXd k);
double ellkVec(double k);
Eigen::VectorXd ellkVec(Eigen::VectorXd k);