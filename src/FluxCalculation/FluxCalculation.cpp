#include "helper_functions.hpp"
#include <functional>
#include <iostream>
#include <Eigen/Dense>

#include "ellipticIntegral.hpp"
#include "Flux_rj.hpp"

VectorXd polyval(const VectorXd& coeffs, const VectorXd& x) {
    int degree = coeffs.size() - 1;  // Degree of the polynomial

    VectorXd result(x.size());

    for (int i = 0; i < x.size(); ++i) {
        double value = 0.0;

        for (int j = 0; j <= degree; ++j) {
            value += coeffs[j] * std::pow(x[i], degree - j);
        }

        result[i] = value;
    }

    return result;
}

double polyval(const VectorXd& coeffs, const double x){
    int degree = coeffs.size() - 1;  // Degree of the polynomial

    double result;

    double value = 0.0;

    for (int j = 0; j <= degree; ++j) {
        value += coeffs[j] * std::pow(x, degree - j);
    }

    result = value;

    return result;
}

VectorXd polyfit(const VectorXd& x, const VectorXd& y, int degree) {
    int numPoints = x.size();

    // Construct the Vandermonde matrix
    MatrixXd A(numPoints, degree + 1);
    for (int i = 0; i < numPoints; ++i) {
        for (int j = 0; j <= degree; ++j) {
            A(i, j) = std::pow(x[i], degree - j);
        }
    }

    // Solve the least squares problem to get the coefficients
    VectorXd coeffs = A.householderQr().solve(y);

    return coeffs;
}

std::tuple<VectorXd, VectorXd> occultquadVec(double p, VectorXd z, double u1, double u2, double TolFactor = 1, bool ToSmooth = true) {
    // ==================================================
    // v.0.32 - vectorize rc,rj,rf
    //        - Allow correcting behaviour near z==p, z=1-p
    //        - Added optinal parameter TolFactor for tolerances control
    //        - Removed all "CloseToOne" and added ToSmooth Parameter
    //
    //%%%%%%%%%%%%%%%% - Mandel Agol comments
    //
    //   This routine computes the lightcurve for occultation
    //   of a quadratically limb-darkened source without microlensing.
    //   Please cite Mandel & Agol (2002) if you make use of this routine
    //   in your research.  Please report errors or bugs agol@tapir.caltech.edu
    //%       implicit none
    //%       integer i,nz
    //%       double precision z0(nz),u1,u2,p,muo1(nz),mu0(nz),
    //%      &       mu(nz),lambdad(nz),etad(nz),lambdae(nz),lam,
    //%      &       pi,x1,x2,x3,z,omega,kap0,kap1,q,Kk,Ek,Pk,n,ellec,ellk,rj
    //%
    //%  Input:
    //%
    //%  rs   radius of the source (set to unity)
    //%  z    impact parameter in units of rs
    //%  p    occulting star radius in units of rs
    //%  u1   linear    limb-darkening coefficient (gamma_1 in paper)
    //%  u2   quadratic limb-darkening coefficient (gamma_2 in paper)

    //%  Tols:     A factor controling the three "ERRTOL" parameters for the three helper functions. If given, all three default ERRTOLs wil be scaled by it: [Default =1 ]
    //%  ToSmooth: if true, smooths the original Mandel-Agol model around z=p and around abs(z-p)=1 to produce better-behaving derivative. The value themselves are virtually unchanged. [default=true]
    //%
    //%  Output:
    //%
    //%  muo1 fraction of flux at each z for a limb-darkened source
    //%  mu0  fraction of flux at each z for a uniform source
    //%
    //%  Limb darkening has the form:
    //%   I(r)=[1-u1*(1-sqrt(1-(r/rs)^2))-u2*(1-sqrt(1-(r/rs)^2))^2]/(1-u1/3-u2/6)/pi
    //%  To use this routine
    //%
    //%  Now, compute pure occultation curve:
    //
    //% Setting the extra inputs for the case in which only p,z,u1,u2 are given

    // make z a column vector if its not already one
    // (z.size() > 1) {
    //    z.transposeInPlace();
    //}

    //set zeros to lambdad, lambda3, etad, kap0, kap1
    VectorXd lambdad = VectorXd::Zero(z.size());
    VectorXd lambdae = VectorXd::Zero(z.size());
    VectorXd etad = VectorXd::Zero(z.size());
    VectorXd kap0 = VectorXd::Zero(z.size());
    VectorXd kap1 = VectorXd::Zero(z.size());

    //if p is close to 0.5, make it 0.5
    if (abs(p - 0.5) < 1e-3) {
        p = 0.5;
    }

    double omega = 1 - u1 / 3. - u2 / 6.;
    VectorXd x1 = (p - z.array()).array().square();
    VectorXd x2 = (p + z.array()).array().square();
    VectorXd x3 = p*p - z.array().square();

    VectorXd mu0 = VectorXd::Zero(z.size());
    VectorXd muo1 = VectorXd::Zero(z.size());

    for (int i = 0; i < z.size(); i++) {
        // the source is unocculted: Table 3, I.
        if (z[i] >= 1 + p) {
            lambdad[i] = 0;
            etad[i] = 0;
            lambdae[i] = 0;
        }
            // the source is partly occulted and the occulting object crosses the limb: Equation (26)
        else if (z[i] >= abs(1 - p) && z[i] <= 1 + p) {
            kap1[i] = acos(fmin((1 - p * p + z[i] * z[i]) / 2. / z[i], 1));
            kap0[i] = acos(fmin((p * p + z[i] * z[i] - 1) / 2. / p / z[i], 1));
            lambdae[i] = p * p * kap0[i] + kap1[i];
            lambdae[i] = (lambdae[i] - 0.5 * sqrt(fmax(
                    4 * z[i] * z[i] - (1 + z[i] * z[i] - p * p) * (1 + z[i] * z[i] - p * p), 0))) / M_PI;
        }
            // the occulting objects transits the source star (but doesn't completely cover it)
        else if (z[i] <= 1 - p) {
            lambdae[i] = p * p;
        }
            //the edge of the occulting star lies at the origin- special expressions in this case:
        if (abs(z[i] - p) < 1e-4 * (z[i] + p)) {
            //% Table 3, Case V.

                if (z[i] >= 0.5) {
                double q = 0.5 / p;
                double Kk = ellkVec(q);
                double Ek = ellecVec(q);
                //% Equation 34: lambda_3
                lambdad[i] = 1 / 3 + 16 * p / 9. / M_PI * (2 * p * p - 1) * Ek -
                             (32 * p * p * p * p - 20. * p * p + 3) / 9. / M_PI / p * Kk;
                //% Equation 34: eta_1
                etad[i] = 1. / 2. / M_PI * (kap1[i] + p * p * (p * p + 2 * z[i]) * kap0[i] -
                                          (1 + 5 * p * p + z[i] * z[i]) / 4 * sqrt((1 - x1[i]) * (x2[i] - 1)));
                if (p == 0.5) {
                    //% Case VIII: p=1/2, z=1/2
                    lambdad[i] = 1. / 3. - 4. / M_PI / 9.;
                    etad[i] = 3. / 32.;
                }
            } else {
                //% Table 3, Case VI.
                double q = 2 * p;
                double Kk = ellkVec(q);
                double Ek = ellecVec(q);
                //% Equation 34: lambda_4
                lambdad[i] = 1 / 3. + 2. / 9. / M_PI * (4 * (2 * p * p - 1) * Ek + (1 - 4 * p * p) * Kk);
                //% Equation 34: eta_2
                etad[i] = p * p / 2 * (p * p + 2 * z[i] * z[i]);
            }
        }
            // the occulting star partly occults the source and crosses the limb:
            // Table 3, Case III
        if ((z[i] >= 0.5 + std::abs(p - 0.5) && z[i] < 1 + p) ||
                 (p > 0.5 && z[i] > std::abs(1 - p) * (1 + 10 * std::numeric_limits<double>::epsilon()) && z[i] < p)) {
            double q = sqrt((1 - (p - z[i]) * (p - z[i])) / 4 / z[i] / p);
            double Kk = ellkVec(q);
            double Ek = ellecVec(q);
            double n = 1 / x1[i] - 1;
            auto Pk = Kk - n / 3 * rj(0, 1 - q * q, 1, 1 + n, TolFactor);
            // Equation 34, lambda_1:
            auto term1 = 1 / 9. / M_PI / sqrt(p * z[i]);
            auto term2 = (((1 - x2[i]) * (2 * x2[i] + x1[i] - 3) - 3 * x3[i] * (x2[i] - 2)) * Kk +
                          4 * p * z[i] * (z[i] * z[i] + 7 * p * p - 4) * Ek - 3 * x3[i] / x1[i] * Pk);
            lambdad[i] = term1 * term2;
            if (z[i] < p) {
                lambdad[i] = lambdad[i] + 2. / 3.;
            }
            // Equation 34, eta_1:
            etad[i] = 1. / 2. / M_PI * (kap1[i] + p * p * (p * p + 2. * z[i] * z[i]) * kap0[i] -
                                      (1 + 5 * p * p + z[i] * z[i]) / 4. * sqrt((1 - x1[i]) * (x2[i] - 1)));

        }
            // the occulting star transits the source: Table 3, Case IV.
        if (p <= 1 && z[i] <= (1 - p) * 1.0001 && abs(z[i] - p) > 10 * std::numeric_limits<double>::epsilon()) {
            double q = sqrt((x2[i] - x1[i]) / (1 - x1[i]));
            double Kk = ellkVec(q);
            double Ek = ellecVec(q);
            double n = x2[i] / x1[i] - 1;
            double Pk = Kk - n / 3 * rj(0, 1 - q * q, 1, 1 + n, TolFactor);
            // Equation 34, lambda_2:
            lambdad[i] = 2. / 9. / M_PI / sqrt(1 - x1[i]) * ((1 - 5 * z[i] * z[i] + p * p + x3[i] * x3[i]) * Kk +
                                                           (1 - x1[i]) * (z[i] * z[i] + 7 * p * p - 4) * Ek -
                                                           3 * x3[i] / x1[i] * Pk);
            if (z[i] < p) {
                lambdad[i] = lambdad[i] + 2. / 3.;
            }
            if (abs(p + z[i] - 1) <= 1e-8) {
                lambdad[i] = 2. / 3. / std::numbers::pi * acos(1 - 2 * p) -
                             4. / 9. / std::numbers::pi * sqrt(p * (1 - p)) * (3 + 2 * p - 8 * p * p);
            }
            // Equation 34, eta_2:
            etad[i] = p * p / 2. * (p * p + 2 * z[i] * z[i]);
        }

        // Now, using equation (33):
        muo1[i] = 1 - ((1 - u1 - 2 * u2) * lambdae[i] + (u1 + 2 * u2) * lambdad[i] + u2 * etad[i]) / omega;
        // Equation 25
        mu0[i] = 1 - lambdae[i];

        // the source is completely occulted:
        // Table 3, II.
        if (p >= 1) {
            if (z[i] <= p - 1) {
                muo1[i] = 0;
                mu0[i] = 0;
            }
        }
    }
    if (ToSmooth){
         int deg = 2;
         double CorrectionRange = 2e-4;
         double FitRange = 5 * CorrectionRange;
         for (size_t i = 0; i<z.size(); i++) {
             VectorXd tmpz;
             VectorXd tmpz_tmp;
             if (z[i] >= 1 - p - CorrectionRange && z[i] <= 1 - p) { // inner than II/III contact
                 tmpz_tmp = abs(1-p)+ ArrayXd::LinSpaced(deg*10, -FitRange, -CorrectionRange);
             }
             if (z[i] >= 1 - p && z[i] <= 1 - p + CorrectionRange) { // outer than II/III contact
                 tmpz_tmp = abs(1-p)+ ArrayXd::LinSpaced(deg*10, CorrectionRange, FitRange);
             }
             for(size_t j = 0; j<tmpz_tmp.size(); j++){
                 if(tmpz_tmp[j] >= 0) {
                     tmpz.conservativeResize(tmpz.size() + 1);
                     tmpz(tmpz.size() - 1) = tmpz_tmp[j];
                 }
             }
             if(tmpz.size() > 0) {
                 auto [tmp_muo1, tmp_mu0] = occultquadVec(p, tmpz, u1, u2, 1, false);
                 auto pol = polyfit(tmpz, tmp_muo1, deg);
                 muo1[i] = polyval(pol, z[i]);
             }
         }

        if (p > 0.5) {
            deg = 3;
        }
        CorrectionRange = 1e-2;
        FitRange = 5 * CorrectionRange;
        for(size_t i = 0; i<z.size(); i++) {
            VectorXd tmpz;
            VectorXd tmpz_tmp;

            if(p < 0.5) {
                // i=find(z-p>-CorrectionRange & z-p<min(CorrectionRange,1-2*p)); % near z~p
                if(z[i] > p - CorrectionRange && z[i] < p + fmin(CorrectionRange, 1-2*p)) {
                    tmpz_tmp = p+ ArrayXd::LinSpaced(deg*10, -FitRange, fmin(FitRange,1-2*p));
                }
            } else {
                // i=find(z-p>max(-CorrectionRange,1-2*p) & z-p<CorrectionRange); % near z~p
                if (z[i] > p + fmax(CorrectionRange,1-2*p) && z[i] <= p + CorrectionRange) {
                    tmpz_tmp = p + ArrayXd::LinSpaced(deg * 10, -FitRange, fmin(FitRange, 1 - 2 * p));
                }
            }
            for(size_t j = 0; j<tmpz_tmp.size(); j++){
                // inversed
                // tmpz(abs(tmpz-p)<CorrectionRange | tmpz<0)=[];
                if(tmpz_tmp[j] >= 0 && abs(tmpz_tmp[j]-p)>=CorrectionRange) {
                    tmpz.conservativeResize(tmpz.size() + 1);
                    tmpz(tmpz.size() - 1) = tmpz_tmp[j];
                }
            }
            if(tmpz.size() > 0) {
                auto [tmp_muo1, tmp_mu0] = occultquadVec(p, tmpz, u1, u2, 1, false);
                auto pol = polyfit(tmpz, tmp_muo1, deg);
                muo1[i] = polyval(pol, z[i]);
            }
        }
     }
    return std::make_tuple(muo1, mu0);
}

// occultquadvec with double z instead of Vector z
std::tuple<double,double> occultquadVec(double p, double z, double u1, double u2, double TolFactor=1, bool TooSmooth = true){
    auto [out1, out2] = occultquadVec(p, VectorXd::Constant(1, z), u1, u2, TolFactor, TooSmooth);
    return std::make_tuple(out1[0], out2[0]);
}
