#pragma once

#include <helper_functions.hpp>

VectorXd rf(const VectorXd& x, const VectorXd& y, const VectorXd& z, double ERRTOL);
