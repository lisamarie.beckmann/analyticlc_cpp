#include "Flux_rc.hpp"

//
// Based on:
// (C) Copr . 1986-92 Numerical Recipes Software
VectorXd rc(const VectorXd& x, const VectorXd& y, double ERRTOL) {
    int N = x.size();
    VectorXd xt(N), yt(N), w(N), s(N), alamb(N), ave(N);

    ERRTOL = 0.04 * ERRTOL;
    const double THIRD = 1.0 / 3.0;
    const double C1 = 0.3;
    const double C2 = 1.0 / 7.0;
    const double C3 = 0.375;
    const double C4 = 9.0 / 22.0;

    // Initialize vectors
    for (int i = 0; i < N; i++) {
        if (y[i] > 0) {
            xt[i] = x[i];
            yt[i] = y[i];
            w[i] = 1.0;
        } else {
            xt[i] = x[i] - y[i];
            yt[i] = -y[i];
            w[i] = sqrt(x[i]) / sqrt(xt[i]);
        }
    }

    // Iteratively compute rc

    bool continueIteration = true;
    while (continueIteration) {
        for (int i = 0; i < N; i++) {
            alamb[i] = 2.0 * sqrt(xt[i]) * sqrt(yt[i]) + yt[i];
            xt[i] = 0.25 * (xt[i] + alamb[i]);
            yt[i] = 0.25 * (yt[i] + alamb[i]);
            ave[i] = THIRD * (xt[i] + yt[i] + yt[i]);
            s[i] = (yt[i] - ave[i]) / ave[i];
            continueIteration = continueIteration && (fabs(s[i]) > ERRTOL);
        }
    }


    // Compute rc using the final values
    return w.array() * (1.0 + s.array().square() * (C1 + s.array() * (C2 + s.array() * (C3 + s.array() * C4)))) / sqrt(ave.array());
}

double rc(double x, double y, double ERRTOL) {
    return rc(VectorXd::Constant(1, x), VectorXd::Constant(1, y), ERRTOL)[0];
}
