#include <iostream>
#include <Eigen/Dense>
#include "analyticlc.hpp"

int main () {
  ArrayXd P = {{12.3456, 29.3}};
  ArrayXd Tmid0 = {{9.8765432, 15.4029}};
  ArrayXd ror = {{0.021, 0.027}};
  ArrayXd mu = {{1.24e-5, 3e-5}};
  ArrayXd ex0 = {{0.01234, 0.00998}};
  ArrayXd ey0 = {{0.02345, 0.0189}};
  ArrayXd I0 = {{0.01745329, 0.0203}};
  ArrayXd Omega0 = {{0.4014257, 0.369}};

  ArrayXd aor = {{30}};
  double u1 = 0.5;
  double u2 = 0.3;
  ArrayXd t = ArrayXd::LinSpaced((100/2)+1, 0, 100);
  ArrayXd tRV = ArrayXd::Zero(1);

  alc::AnalyticLCparams params;
  alc::construct(params, P, Tmid0, ror, mu, ex0, ey0, I0, Omega0, aor, t, u1, u2);

  auto [out1, out2] = AnalyticLCfunc(params);

} 
