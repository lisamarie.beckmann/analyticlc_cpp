#include "DisturbingFunctionMultTermVariations.hpp"
#include <cmath>
#include <eigen_helpers.hpp>
#include "fast_sin.h"

using namespace alc;

void alc::from_json(const json &j, CalculateDisturbingFunctionParams &opt) {
    j.at("n").get_to(opt.n);
    j.at("e").get_to(opt.e);
    j.at("pom").get_to(opt.pom);
    j.at("I").get_to(opt.I);
    j.at("Om").get_to(opt.Om);
}

void alc::from_json(const json &j, DisturbingFunctionOutput &opt) {
    j.at("dL1_pair").get_to(opt.dLambda1);
    j.at("dL2_pair").get_to(opt.dLambda2);
    opt.dz1 = j.at("dz1_pair_r").get<VectorXd>() + j.at("dz1_pair_i").get<VectorXd>() * 1i;
    opt.dz2 = j.at("dz2_pair_r").get<VectorXd>() + j.at("dz2_pair_i").get<VectorXd>() * 1i;
    opt.du1 = j.at("du1_pair_r").get<VectorXd>() + j.at("du1_pair_i").get<VectorXd>() * 1i;
    opt.du2 = j.at("du2_pair_r").get<VectorXd>() + j.at("du2_pair_i").get<VectorXd>() * 1i;
    j.at("daoa1_pair").get_to(opt.da_oa1);
    j.at("daoa2_pair").get_to(opt.da_oa2);
}

using namespace alc;

DisturbingFunctionParams calculate_disturbing_params(int j1, int j2, const ForcedElementsParams &params,
                                                     const CalculateDisturbingFunctionParams &params_part) {
    DisturbingFunctionParams result;

    auto &lc = params.lc;
    auto &P = params.P;
    auto &mu = params.mu;
    auto &Lambda = params.Lambda;
    auto &OrdersAll = params.OrdersAll;
    auto &MaxPower = params.MaxPower;

    auto &n = params_part.n;
    auto &e = params_part.e;
    auto &pom = params_part.pom;
    auto &I = params_part.I;
    auto &Om = params_part.Om;

    //ratio of semi-major axes
    result.alph = pow(n[j2] / n[j1], 2.0 / 3.0) * pow((1 + mu[j1]) / (1 + mu[j2]), 1.0 / 3.0);

    //write the orbital elements for this specific pair
    result.Lambda1 = Lambda.col(j1);
    result.Lambda2 = Lambda.col(j2);

    //write the complex eccentricities, magnitude and angle of the
    //currently analyzed pair
    result.e1 = e.col(j1);
    result.e2 = e.col(j2);
    result.Pom1 = pom.col(j1);
    result.Pom2 = pom.col(j2);

    //write the complex inclinations, magnitude and angle of the
    //currently analyzed pair
    result.I1 = I.col(j1);
    result.I2 = I.col(j2);
    result.Om1 = Om.col(j1);
    result.Om2 = Om.col(j2);

    //translate from I to s - as defined in the disturbing function
    //expansion (Solar System Dynamics)
    result.s1 = sin(result.I1.array() / 2);
    result.s2 = sin(result.I2.array() / 2);

    //write the relative masses and the mean motions of the currently analyzed pair
    result.mu1 = mu[j1];
    result.mu2 = mu[j2];
    result.n1 = n[j1];
    result.n2 = n[j2];

    //set number of orders, if not given, by the proximity to closest MMRs
    double Orders;
    if (OrdersAll.size() == 0) {
        VectorXd J = VectorXd::LinSpaced(MaxPower, 1, MaxPower).array() * P[j2] /
                     (P[j2] - P[j1]); //TODO: check linspace size
        VectorXi Ordersvec = J.unaryExpr([](double elem) { return static_cast<int>(std::round(elem)); });
        int jmax = Ordersvec.maxCoeff();
        Orders = fmax(6, jmax) + 1;
    } else {
        Orders = OrdersAll[0];
    }
    //prepare the structure Terms, which will include the table elements
    //MatrixXcd Terms = MatrixXcd::Zero(Orders.size(),P.size());
    std::map <std::string, VectorXd> AllTerms;



    //here, instead of the loop on j, make a loop on the different
    //terms, to be continued
    std::map <std::string, VectorXd> Terms;
    for (int j = Orders * (-1); j < Orders + 1; ++j) {
        // Calculate SMA Functions
        // Calculate Laplace coefficients and their semi-major axis functions - Solar
        // System Dynamics (Murray & Dermott 1999), Appendix B

        auto [sVec, jVec, DVec] = sjDVec(j);


        // PrepareExpansionTerms
        //Write down the disturbing function terms powers and resonant orders - Solar
        //System Dynamics (Murray & Dermott 1999), Appendix B

        //if //constant fields, not depending on the value of alpha or j, should be written once and for all
        if (Terms.find("k") == Terms.end()) {
            auto [k, A, Ap, B, Bp, C, Cp, D, Dp] = Calc_AtoD();
            Terms["k"] = k;
            Terms["A"] = A;
            Terms["Ap"] = Ap;
            Terms["B"] = B;
            Terms["Bp"] = Bp;
            Terms["C"] = C;
            Terms["Cp"] = Cp;
            Terms["D"] = D;
            Terms["Dp"] = Dp;
        }

        auto [f, Df, fE, fI, CoeffsVec] = Calc_Df(lc, result.alph, sVec, jVec, DVec, j);
        Terms["f"] = f;
        // indirect terms due to an external perturber. The prefactor of alph
        // comes from the definition of the disturbing function (Solar System
        // Dynamics equation 6.44).


        Terms["fE"] = fE;
        // indirect terms due to an internal perturber. The prefactor of 1/alph^2
        // comes from the definition of the disturbing function (Solar System
        // Dynamics equation 6.45).

        Terms["fI"] = fI;

        Terms["Df"] = Df;
        VectorXd DfE = fE.array() / result.alph; //deriving alph*constant is the same as dividing by alph
        VectorXd DfI =
                fI.array() * (-2 / result.alph); //deriving alph^(-2)*constant is the same as multiplying by (-2/alph)
        Terms["DfE"] = DfE;
        Terms["DfI"] = DfI;
        //one vectorized operation for all terms, including vectorizing on j
        int N = Terms["A"].size();
        VectorXd j_vec = VectorXd::Ones(N).array() * j;
        Terms["j"] = j_vec;
        if (!AllTerms.empty()) {
            for (auto &pair: Terms) {
                auto AllTermsA = AllTerms[pair.first];
                auto TermsA = pair.second;

                int oldSize = AllTermsA.size();
                int newSize = oldSize + TermsA.size();

                AllTermsA.conservativeResize(newSize);
                AllTermsA.segment(oldSize, TermsA.size()) = TermsA;

                AllTerms[pair.first] = AllTermsA;
            }
        } else {
            AllTerms = Terms;
        }
    }
    //One vectorized operation for all terms, including vectorizing on j
    VectorXd TermsInd;
    for (size_t i = 0; i < AllTerms["j"].size(); ++i) {
        if ((AllTerms["j"][i] != 0. || AllTerms["k"][i] != 0.) &&
            (AllTerms["A"][i] + AllTerms["Ap"][i] + AllTerms["B"][i] + AllTerms["Bp"][i]) <= MaxPower) {
            TermsInd.conservativeResize(TermsInd.size() + 1);
            TermsInd(TermsInd.size() - 1) = i;
        }
    }

    result.A = AllTerms["A"](TermsInd);
    result.Ap = AllTerms["Ap"](TermsInd);
    result.B = AllTerms["B"](TermsInd);
    result.Bp = AllTerms["Bp"](TermsInd);
    result.C = AllTerms["C"](TermsInd);
    result.Cp = AllTerms["Cp"](TermsInd);
    result.D = AllTerms["D"](TermsInd);
    result.Dp = AllTerms["Dp"](TermsInd);
    result.f = AllTerms["f"](TermsInd);
    result.fE = AllTerms["fE"](TermsInd);
    result.fI = AllTerms["fI"](TermsInd);
    result.Df = AllTerms["Df"](TermsInd);
    result.DfE = AllTerms["DfE"](TermsInd);
    result.DfI = AllTerms["DfI"](TermsInd);
    result.j = AllTerms["j"](TermsInd);
    result.k = AllTerms["k"](TermsInd);

    return result;
}

void alc::from_json(const json &j, DisturbingFunctionParams &opt) {
    j.at("n1").get_to(opt.n1);
    j.at("n2").get_to(opt.n2);
    j.at("alph").get_to(opt.alph);
    j.at("Lambda1").get_to(opt.Lambda1);
    j.at("Lambda2").get_to(opt.Lambda2);
    j.at("mu1").get_to(opt.mu1);
    j.at("mu2").get_to(opt.mu2);
    j.at("jAll").get_to(opt.j);
    j.at("kAll").get_to(opt.k);
    j.at("e1").get_to(opt.e1);
    j.at("e2").get_to(opt.e2);
    j.at("pom1").get_to(opt.Pom1);
    j.at("pom2").get_to(opt.Pom2);
    j.at("I1").get_to(opt.I1);
    j.at("I2").get_to(opt.I2);
    j.at("s1").get_to(opt.s1);
    j.at("s2").get_to(opt.s2);
    j.at("Om1").get_to(opt.Om1);
    j.at("Om2").get_to(opt.Om2);
    j.at("AAll").get_to(opt.A);
    j.at("ApAll").get_to(opt.Ap);
    j.at("BAll").get_to(opt.B);
    j.at("BpAll").get_to(opt.Bp);
    j.at("CAll").get_to(opt.C);
    j.at("CpAll").get_to(opt.Cp);
    j.at("DAll").get_to(opt.D);
    j.at("DpAll").get_to(opt.Dp);
    j.at("Allf").get_to(opt.f);
    j.at("AllfE").get_to(opt.fE);
    j.at("AllfI").get_to(opt.fI);
    j.at("AllDf").get_to(opt.Df);
    j.at("DfE").get_to(opt.DfE);
    j.at("DfI").get_to(opt.DfI);
}

std::string
alc::print_diff_error(const DisturbingFunctionParams &lhs, const DisturbingFunctionParams &rhs, double error) {
    std::string result = "{";
    if (abs(lhs.n1 - rhs.n1) > error) {
        result += "\"n1\": " + std::to_string(lhs.n1) + " != " + std::to_string(rhs.n1) + ", ";
    }
    if (abs(lhs.n2 - rhs.n2) > error) {
        result += "\"n2\": " + std::to_string(lhs.n2) + " != " + std::to_string(rhs.n2) + ", ";
    }
    if (abs(lhs.alph - rhs.alph) > error) {
        result += "\"alph\": " + std::to_string(lhs.alph) + " != " + std::to_string(rhs.alph) + ", ";
    }
    for (int i = 0; i < lhs.Lambda1.size(); ++i) {
        if (abs(lhs.Lambda1(i) - rhs.Lambda1(i)) > error) {
            result += "\"Lambda1\": " + to_string(lhs.Lambda1) + " != " + to_string(rhs.Lambda1) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Lambda2.size(); ++i) {
        if (abs(lhs.Lambda2(i) - rhs.Lambda2(i)) > error) {
            result += "\"Lambda2\": " + to_string(lhs.Lambda2) + " != " + to_string(rhs.Lambda2) + ", ";
            break;
        }
    }
    if (abs(lhs.mu1 - rhs.mu1) > error) {
        result += "\"mu1\": " + std::to_string(lhs.mu1) + " != " + std::to_string(rhs.mu1) + ", ";
    }
    if (abs(lhs.mu2 - rhs.mu2) > error) {
        result += "\"mu2\": " + std::to_string(lhs.mu2) + " != " + std::to_string(rhs.mu2) + ", ";
    }
    for (int i = 0; i < lhs.j.size(); ++i) {
        if (abs(lhs.j(i) - rhs.j(i)) > error) {
            result += "\"j\": " + to_string(lhs.j) + " != " + to_string(rhs.j) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.k.size(); ++i) {
        if (abs(lhs.k(i) - rhs.k(i)) > error) {
            result += "\"k\": " + to_string(lhs.k) + " != " + to_string(rhs.k) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.e1.size(); ++i) {
        if (abs(lhs.e1(i) - rhs.e1(i)) > error) {
            result += "\"e1\": " + to_string(lhs.e1) + " != " + to_string(rhs.e1) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.e2.size(); ++i) {
        if (abs(lhs.e2(i) - rhs.e2(i)) > error) {
            result += "\"e2\": " + to_string(lhs.e2) + " != " + to_string(rhs.e2) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Pom1.size(); ++i) {
        if (abs(lhs.Pom1(i) - rhs.Pom1(i)) > error) {
            result += "\"Pom1\": " + to_string(lhs.Pom1) + " != " + to_string(rhs.Pom1) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Pom2.size(); ++i) {
        if (abs(lhs.Pom2(i) - rhs.Pom2(i)) > error) {
            result += "\"Pom2\": " + to_string(lhs.Pom2) + " != " + to_string(rhs.Pom2) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.I1.size(); ++i) {
        if (abs(lhs.I1(i) - rhs.I1(i)) > error) {
            result += "\"I1\": " + to_string(lhs.I1) + " != " + to_string(rhs.I1) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.I2.size(); ++i) {
        if (abs(lhs.I2(i) - rhs.I2(i)) > error) {
            result += "\"I2\": " + to_string(lhs.I2) + " != " + to_string(rhs.I2) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.s1.size(); ++i) {
        if (abs(lhs.s1(i) - rhs.s1(i)) > error) {
            result += "\"s1\": " + to_string(lhs.s1) + " != " + to_string(rhs.s1) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.s2.size(); ++i) {
        if (abs(lhs.s2(i) - rhs.s2(i)) > error) {
            result += "\"s2\": " + to_string(lhs.s2) + " != " + to_string(rhs.s2) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Om1.size(); ++i) {
        if (abs(lhs.Om1(i) - rhs.Om1(i)) > error) {
            result += "\"Om1\": " + to_string(lhs.Om1) + " != " + to_string(rhs.Om1) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Om2.size(); ++i) {
        if (abs(lhs.Om2(i) - rhs.Om2(i)) > error) {
            result += "\"Om2\": " + to_string(lhs.Om2) + " != " + to_string(rhs.Om2) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.A.size(); ++i) {
        if (abs(lhs.A(i) - rhs.A(i)) > error) {
            result += "\"A\": " + to_string(lhs.A) + " != " + to_string(rhs.A) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Ap.size(); ++i) {
        if (abs(lhs.Ap(i) - rhs.Ap(i)) > error) {
            result += "\"Ap\": " + to_string(lhs.Ap) + " != " + to_string(rhs.Ap) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.B.size(); ++i) {
        if (abs(lhs.B(i) - rhs.B(i)) > error) {
            result += "\"B\": " + to_string(lhs.B) + " != " + to_string(rhs.B) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Bp.size(); ++i) {
        if (abs(lhs.Bp(i) - rhs.Bp(i)) > error) {
            result += "\"Bp\": " + to_string(lhs.Bp) + " != " + to_string(rhs.Bp) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.C.size(); ++i) {
        if (abs(lhs.C(i) - rhs.C(i)) > error) {
            result += "\"C\": " + to_string(lhs.C) + " != " + to_string(rhs.C) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Cp.size(); ++i) {
        if (abs(lhs.Cp(i) - rhs.Cp(i)) > error) {
            result += "\"Cp\": " + to_string(lhs.Cp) + " != " + to_string(rhs.Cp) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.D.size(); ++i) {
        if (abs(lhs.D(i) - rhs.D(i)) > error) {
            result += "\"D\": " + to_string(lhs.D) + " != " + to_string(rhs.D) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.Dp.size(); ++i) {
        if (abs(lhs.Dp(i) - rhs.Dp(i)) > error) {
            result += "\"Dp\": " + to_string(lhs.Dp) + " != " + to_string(rhs.Dp) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.f.size(); ++i) {
        if (abs(lhs.f(i) - rhs.f(i)) > error) {
            result += "\"f\": " + to_string(lhs.f) + " != " + to_string(rhs.f) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.fE.size(); ++i) {
        if (abs(lhs.fE(i) - rhs.fE(i)) > error) {
            result += "\"fE\": " + to_string(lhs.fE) + " != " + to_string(rhs.fE) + ", ";
            break;
        }
    }
    for (int i = 0; i < lhs.fI.size(); ++i) {
        if (abs(lhs.fI(i) - rhs.fI(i)) > error) {
            //result += "\"fI\": " + to_string(lhs.fI) + " != " + to_string(rhs.fI) + ", ";
            result += " fI diff: " + std::to_string(lhs.fI(i) - rhs.fI(i)) + " index: " + std::to_string(i);
            //break;
        }
    }
    for (int i = 0; i < lhs.Df.size(); ++i) {
        if (abs(lhs.Df(i) - rhs.Df(i)) > error) {
            //result += "\"Df\": " + to_string(lhs.Df) + " != " + to_string(rhs.Df) + ", ";
            result += "Df diff: " + std::to_string(lhs.Df(i) - rhs.Df(i)) + "index: " + std::to_string(i);
            //break;
        }
    }
    for (int i = 0; i < lhs.DfE.size(); ++i) {
        if (abs(lhs.DfE(i) - rhs.DfE(i)) > error) {
            //result += "\"DfE\": " + to_string(lhs.DfE) + " != " + to_string(rhs.DfE) + ", ";
            result += " DfE diff: " + std::to_string(lhs.DfE(i) - rhs.DfE(i)) + " index: " + std::to_string(i);
            //break;
        }
    }
    for (int i = 0; i < lhs.DfI.size(); ++i) {
        if (abs(lhs.DfI(i) - rhs.DfI(i)) > error) {
            //result += "\"DfI\": " + to_string(lhs.DfI) + " != " + to_string(rhs.DfI) + ", ";
            result += " DfI diff: " + std::to_string(lhs.DfI(i) - rhs.DfI(i)) + " index: " + std::to_string(i);
            //break;
        }
    }

    result += "}";
    return result;
}

std::string alc::print_diff(const DisturbingFunctionParams &lhs, const DisturbingFunctionParams &rhs) {
    std::string result = "{";
    if (lhs.n1 != rhs.n1) {
        result += "\"n1\": " + std::to_string(lhs.n1) + " != " + std::to_string(rhs.n1) + ", ";
    }
    if (lhs.n2 != rhs.n2) {
        result += "\"n2\": " + std::to_string(lhs.n2) + " != " + std::to_string(rhs.n2) + ", ";
    }
    if (lhs.alph != rhs.alph) {
        result += "\"alph\": " + std::to_string(lhs.alph) + " != " + std::to_string(rhs.alph) + ", ";
    }
    if (lhs.Lambda1 != rhs.Lambda1) {
        result += "\"Lambda1\": " + to_string(lhs.Lambda1) + " != " + to_string(rhs.Lambda1) + ", ";
    }
    if (lhs.Lambda2 != rhs.Lambda2) {
        result += "\"Lambda2\": " + to_string(lhs.Lambda2) + " != " + to_string(rhs.Lambda2) + ", ";
    }
    if (lhs.mu1 != rhs.mu1) {
        result += "\"mu1\": " + std::to_string(lhs.mu1) + " != " + std::to_string(rhs.mu1) + ", ";
    }
    if (lhs.mu2 != rhs.mu2) {
        result += "\"mu2\": " + std::to_string(lhs.mu2) + " != " + std::to_string(rhs.mu2) + ", ";
    }
    if (lhs.j != rhs.j) {
        result += "\"j\": " + to_string(lhs.j) + " != " + to_string(rhs.j) + ", ";
    }
    if (lhs.k != rhs.k) {
        result += "\"k\": " + to_string(lhs.k) + " != " + to_string(rhs.k) + ", ";
    }
    if (lhs.e1 != rhs.e1) {
        result += "\"e1\": " + to_string(lhs.e1) + " != " + to_string(rhs.e1) + ", ";
    }
    if (lhs.e2 != rhs.e2) {
        result += "\"e2\": " + to_string(lhs.e2) + " != " + to_string(rhs.e2) + ", ";
    }
    if (lhs.Pom1 != rhs.Pom1) {
        result += "\"Pom1\": " + to_string(lhs.Pom1) + " != " + to_string(rhs.Pom1) + ", ";
    }
    if (lhs.Pom2 != rhs.Pom2) {
        result += "\"Pom2\": " + to_string(lhs.Pom2) + " != " + to_string(rhs.Pom2) + ", ";
    }
    if (lhs.I1 != rhs.I1) {
        result += "\"I1\": " + to_string(lhs.I1) + " != " + to_string(rhs.I1) + ", ";
    }
    if (lhs.I2 != rhs.I2) {
        result += "\"I2\": " + to_string(lhs.I2) + " != " + to_string(rhs.I2) + ", ";
    }
    if (lhs.s1 != rhs.s1) {
        result += "\"s1\": " + to_string(lhs.s1) + " != " + to_string(rhs.s1) + ", ";
    }
    if (lhs.s2 != rhs.s2) {
        result += "\"s2\": " + to_string(lhs.s2) + " != " + to_string(rhs.s2) + ", ";
    }
    if (lhs.Om1 != rhs.Om1) {
        result += "\"Om1\": " + to_string(lhs.Om1) + " != " + to_string(rhs.Om1) + ", ";
    }
    if (lhs.Om2 != rhs.Om2) {
        result += "\"Om2\": " + to_string(lhs.Om2) + " != " + to_string(rhs.Om2) + ", ";
    }
    if (lhs.A != rhs.A) {
        result += "\"A\": " + to_string(lhs.A) + " != " + to_string(rhs.A) + ", ";
    }
    if (lhs.Ap != rhs.Ap) {
        result += "\"Ap\": " + to_string(lhs.Ap) + " != " + to_string(rhs.Ap) + ", ";
    }
    if (lhs.B != rhs.B) {
        result += "\"B\": " + to_string(lhs.B) + " != " + to_string(rhs.B) + ", ";
    }
    if (lhs.Bp != rhs.Bp) {
        result += "\"Bp\": " + to_string(lhs.Bp) + " != " + to_string(rhs.Bp) + ", ";
    }
    if (lhs.C != rhs.C) {
        result += "\"C\": " + to_string(lhs.C) + " != " + to_string(rhs.C) + ", ";
    }
    if (lhs.Cp != rhs.Cp) {
        result += "\"Cp\": " + to_string(lhs.Cp) + " != " + to_string(rhs.Cp) + ", ";
    }
    if (lhs.D != rhs.D) {
        result += "\"D\": " + to_string(lhs.D) + " != " + to_string(rhs.D) + ", ";
    }
    if (lhs.Dp != rhs.Dp) {
        result += "\"Dp\": " + to_string(lhs.Dp) + " != " + to_string(rhs.Dp) + ", ";
    }
    if (lhs.f != rhs.f) {
        result += "\"f\": " + to_string(lhs.f) + " != " + to_string(rhs.f) + ", ";
    }
    if (lhs.fE != rhs.fE) {
        result += "\"fE\": " + to_string(lhs.fE) + " != " + to_string(rhs.fE) + ", ";
    }
    if (lhs.fI != rhs.fI) {
        result += "\"fI\": " + to_string(lhs.fI) + " != " + to_string(rhs.fI) + ", ";
    }
    if (lhs.Df != rhs.Df) {
        result += "\"Df\": " + to_string(lhs.Df) + " != " + to_string(rhs.Df) + ", ";
    }
    if (lhs.DfE != rhs.DfE) {
        result += "\"DfE\": " + to_string(lhs.DfE) + " != " + to_string(rhs.DfE) + ", ";
    }
    if (lhs.DfI != rhs.DfI) {
        result += "\"DfI\": " + to_string(lhs.DfI) + " != " + to_string(rhs.DfI) + ", ";
    }
    result += "}";

    return result;
}



// Define the fast_sin approximation for scalar input
inline double fast_sin_scalar(double x) {
    const double x2 = x * x;
    return x * (1.0 - x2 * (1.0 / 6.0) + x2 * x2 * (1.0 / 120.0)) + x2 * x * x2 * (1.0 / 5040.0);
}

// Define the fast_sin for VectorXd input
Eigen::VectorXd FastSin_Vec(const Eigen::VectorXd &x) {

    VectorXd result(x.size());
    FastSin<double,33> fsin;
    for (int i = 0; i < x.size(); i++) {
        result(i) = fsin(x(i));
    }
    return result;
}

VectorXd FastCos_Vec(const VectorXd &x) {
    VectorXd result(x.size());
    FastSin<double,32> fsin;
    for (int i = 0; i < x.size(); i++) {
        result(i) = fsin(x(i));
    }
    return result;
}

std::string alc::to_string(const DisturbingFunctionParams &x) {
    std::string result = "{";
    result += "\"n1\": " + std::to_string(x.n1) + ", ";
    result += "\"n2\": " + std::to_string(x.n2) + ", ";
    result += "\"alph\": " + std::to_string(x.alph) + ", ";
    result += "\"Lambda1\": " + to_string(x.Lambda1) + ", ";
    result += "\"Lambda2\": " + to_string(x.Lambda2) + ", ";
    result += "\"mu1\": " + std::to_string(x.mu1) + ", ";
    result += "\"mu2\": " + std::to_string(x.mu2) + ", ";
    result += "\"j\": " + to_string(x.j) + ", ";
    result += "\"k\": " + to_string(x.k) + ", ";
    result += "\"e1\": " + to_string(x.e1) + ", ";
    result += "\"e2\": " + to_string(x.e2) + ", ";
    result += "\"Pom1\": " + to_string(x.Pom1) + ", ";
    result += "\"Pom2\": " + to_string(x.Pom2) + ", ";
    result += "\"I1\": " + to_string(x.I1) + ", ";
    result += "\"I2\": " + to_string(x.I2) + ", ";
    result += "\"s1\": " + to_string(x.s1) + ", ";
    result += "\"s2\": " + to_string(x.s2) + ", ";
    result += "\"Om1\": " + to_string(x.Om1) + ", ";
    result += "\"Om2\": " + to_string(x.Om2) + ", ";
    result += "\"A\": " + to_string(x.A) + ", ";
    result += "\"Ap\": " + to_string(x.Ap) + ", ";
    result += "\"B\": " + to_string(x.B) + ", ";
    result += "\"Bp\": " + to_string(x.Bp) + ", ";
    result += "\"C\": " + to_string(x.C) + ", ";
    result += "\"Cp\": " + to_string(x.Cp) + ", ";
    result += "\"D\": " + to_string(x.D) + ", ";
    result += "\"Dp\": " + to_string(x.Dp) + ", ";
    result += "\"f\": " + to_string(x.f) + ", ";
    result += "\"fE\": " + to_string(x.fE) + ", ";
    result += "\"fI\": " + to_string(x.fI) + ", ";
    result += "\"Df\": " + to_string(x.Df) + ", ";
    result += "\"DfE\": " + to_string(x.DfE) + ", ";
    result += "\"DfI\": " + to_string(x.DfI);
    result += "}";
    return result;
}

DisturbingFunctionOutput DisturbingFunctionMultTermVariations(DisturbingFunctionParams &params) {
    DisturbingFunctionOutput result(params.Lambda1.size(), params.Lambda2.size());

    for (int idx = 0; idx < params.Lambda1.size(); idx++) {
        double n1 = params.n1;
        double n2 = params.n2;
        auto alph = params.alph;
        double Lambda1 = params.Lambda1[idx];
        double Lambda2 = params.Lambda2[idx];
        double mu1 = params.mu1;
        double mu2 = params.mu2;
        VectorXd j = params.j;
        VectorXd k = params.k;
        double e1 = params.e1[idx];
        double e2 = params.e2[idx];
        double Pom1 = params.Pom1[idx];
        double Pom2 = params.Pom2[idx];
        double I1 = params.I1[idx];
        double I2 = params.I2[idx];
        double s1 = params.s1[idx];
        double s2 = params.s2[idx];
        double Om1 = params.Om1[idx];
        double Om2 = params.Om2[idx];
        VectorXd A = params.A;
        VectorXd Ap = params.Ap;
        VectorXd B = params.B;
        VectorXd Bp = params.Bp;
        VectorXd C = params.C;
        VectorXd Cp = params.Cp;
        VectorXd D = params.D;
        VectorXd Dp = params.Dp;
        VectorXd f = params.f;
        VectorXd fE = params.fE;
        VectorXd fI = params.fI;
        VectorXd Df = params.Df;
        VectorXd DfE = params.DfE;
        VectorXd DfI = params.DfI;

        double SQ1 = sqrt(1 - e1 * e1);
        double SQ2 = sqrt(1 - e2 * e2);
        VectorXd f_inner = f.array() + fE.array();
        VectorXd f_outer = f.array() + fI.array();
        VectorXd Df_inner = Df.array() + DfE.array();
        VectorXd Df_outer = Df.array() + DfI.array();
        //calculate the cosine argument
        VectorXd Phi = (j.array() * Lambda2 + (k.array() - j.array()) * Lambda1 - C.array() * Pom1 - Cp.array() * Pom2 -
                       D.array() * Om1 - Dp.array() * Om2).eval();

        //calculate Cjk and Sjk
        //VectorXd PowersFactor = pow(e1.array(), A.array()) * pow(e2.array(), Ap.array()) * pow(s1.array(), B.array()) * pow(s2.array(), Bp.array());
        double log_e1 = std::log(e1);
        double log_e2 = std::log(e2);
        double log_s1 = std::log(s1);
        double log_s2 = std::log(s2);

        // Element-wise operation using Eigen
        VectorXd PowersFactor = (log_e1 * A.array() + log_e2 * Ap.array() + log_s1 * B.array() + log_s2 * Bp.array()).exp();
        //original code: VectorXd PowersFactor = pow(e1, A.array()) * pow(e2, Ap.array()) * pow(s1, B.array()) * pow(s2, Bp.array());
        VectorXd Cjk = PowersFactor.array() * (Phi.array().cos());
        VectorXd Sjk = PowersFactor.array() * (Phi.array().sin());
        // if you want to speed up you can use FastCos/FastSin but this leads to errors in the lc data of up to 2e-2
        // VectorXd Cjk = PowersFactor.array() * FastCos_Vec(Phi).array();
        //VectorXd Sjk = PowersFactor.array() * FastSin_Vec(Phi).array();
        //VectorXd Cjk_pre = PowersFactor.array() * Phi.array().cos();
        //VectorXd Sjk_pre = PowersFactor.array() * Phi.array().sin();
        //double max_diff = (Cjk - Cjk_pre).cwiseAbs().maxCoeff();
        //double max_diff2 = (Sjk - Sjk_pre).cwiseAbs().maxCoeff();
        //calculate the mean-motion of the longitude of conjunction
        VectorXd njk = j.array() * n2 + (k.array() - j.array()) * n1;

        //orbital elements variations - inner planet
        VectorXd da_oa1 =
                2 * mu2 / (1 + mu1) * alph * (f_inner.array()) * (k.array() - j.array()) * n1 / njk.array() *
                Cjk.array();

        VectorXd dLambda1 = mu2 / (1 + mu1) * alph * n1 / njk.array() * Sjk.array() *
                            (3 * (f_inner.array()) * (j.array() - k.array()) * n1 / njk.array() -
                             2 * alph * (Df_inner.array()) +
                             f_inner.array() * A.array() * (SQ1 * (1 - SQ1)) / (e1 * e1) +
                             f_inner.array() * B.array() / 2. / SQ1);

        VectorXd de1 = mu2 / (1 + mu1) * alph * f_inner.array() * SQ1 / e1 * n1 / njk.array() * Cjk.array() *
                       ((j.array() - k.array()) * (1 - SQ1) + C.array());

        VectorXd dPom1 = mu2 / (1 + mu1) * alph * f_inner.array() * n1 / njk.array() * Sjk.array() *
                         (A.array() * SQ1 / (e1 * e1) + B.array() / 2.0 / SQ1);

        VectorXd dI1 = mu2 / (1 + mu1) * alph * f_inner.array() / SQ1 * n1 / njk.array() * Cjk.array() *
                       ((j.array() - k.array() + C.array()) * tan(I1 / 2.0) + D.array() / sin(I1));

        VectorXd dOm1 =
                mu2 / (1 + mu1) * alph * (f_inner.array()) * B.array() / 2. * std::cos(I1 / 2) / std::sin(I1 / 2) /
                (SQ1 * sin(I1)) * n1 / njk.array() * Sjk.array();

        //orbital elements variations - outer planet
        VectorXd da_oa2 = 2 * mu1 / (1 + mu2) * (f_outer.array()) * j.array() * n2 / njk.array() * Cjk.array();
        VectorXd dLambda2 = mu1 / (1 + mu2) * n2 / njk.array() * Sjk.array() *
                            (-3 * (f_outer.array()) * j.array() * n2 / njk.array() +
                             2 * (f_outer.array() + alph * Df_outer.array()) +
                             (f_outer.array()) * Ap.array() * SQ2 * (1 - SQ2) / (e2 * e2) +
                             (f_outer.array()) * Bp.array() / 2. / SQ2);
        VectorXd de2 = mu1 / (1 + mu2) * (f_outer.array()) * SQ2 / e2 * n2 / njk.array() * Cjk.array() *
                       (-1 * (1 - SQ2) * j.array() + Cp.array());

        VectorXd dPom2 = mu1 / (1 + mu2) * (f_outer.array()) * n2 / njk.array() * Sjk.array() *
                         (Ap.array() * SQ2 / (e2 * e2) + Bp.array() / 2. / SQ2);

        VectorXd dI2 = mu1 / (1 + mu2) * (f_outer.array()) / SQ2 * n2 / njk.array() * Cjk.array() *
                       ((Cp.array() - j.array()) * tan(I2 / 2.) + Dp.array() / sin(I2));

        VectorXd dOm2 = mu1 / (1 + mu2) * (f_outer.array()) * Bp.array() / 2. * std::cos(I2 / 2) / std::sin(I2 / 2) /
                        (SQ2 * sin(I2)) * n2 / njk.array() * Sjk.array();

        // translate to the complex form
        result.dz1[idx] = (de1.sum() + 1i * dPom1.sum() * e1) * exp(1i * Pom1);
        result.dz2[idx] = (de2.sum() + 1i * dPom2.sum() * e2) * exp(1i * Pom2);
        result.du1[idx] = (dI1.sum() + 1i * dOm1.sum() * I1) * exp(1i * Om1);
        result.du2[idx] = (dI2.sum() + 1i * dOm2.sum() * I2) * exp(1i * Om2);
        result.da_oa1[idx] = da_oa1.sum();
        result.da_oa2[idx] = da_oa2.sum();
        result.dLambda1[idx] = dLambda1.sum();
        result.dLambda2[idx] = dLambda2.sum();
    }
    return result;
}

