#pragma once

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "FluxCalculation/FluxCalculation.hpp"

class MALookupTable {
private:
    Eigen::VectorXd rVec;
    Eigen::VectorXd zVec;
    double u1;
    double u2;
    Eigen::MatrixXd MATable;

public:
    MALookupTable();
    MALookupTable(Eigen::VectorXd r, Eigen::VectorXd z, double u1Val, double u2Val);
    VectorXd LookupValue(double r, VectorXd z, double Requested_u1, double Requested_u2);
    double LookupValue(double r, double z);
    double LookupValue(double r, double z, double Requested_u1, double Requested_u2);

    void GenerateMATable();
    void PrintMATable();
};