#include "ForcedElements1.h"
#include "laplacecoeffs.hpp"
#include "AnalyticLc/DisturbingFunctionMultTermVariations.hpp"

using namespace std::complex_literals;

void alc::from_json(const json &j, ForcedElementsParams &opt) {
    opt.P = j.at("P").get<VectorXd>();
    j.at("Lambda").get_to(opt.Lambda);
    opt.mu = j.at("mu").get<VectorXd>();
    opt.z = j.at("z_r").get<MatrixXd>() + j.at("z_i").get<MatrixXd>() * 1i;
    opt.u = j.at("u_r").get<MatrixXd>() + j.at("u_i").get<MatrixXd>() * 1i;
    opt.OrdersAll = j.at("OrdersAll").get<VectorXd>();
    opt.MaxPower = j.at("MaxPower");
}

/*
calculateForcedElements(calculateForcedEl_params params){
    free_e_0 = ex0 + 1i * ey0;
    free_I_0 = I0 * exp(1i * Omega0);

    VectorXd n = 2 * M_PI / params.P.array();
    // define the secular interaction matrices of ecc and inc
    auto [A,B] = Secu

}
*/

std::tuple<MatrixXcd, MatrixXd, MatrixXcd, MatrixXd>
ForcedElements1(ForcedElementsParams params) {
    // ForcedElements1: calculate the forced eccentricities and mean longitudes
    // by a disturbing function expansion.
    // Difference from ForcedElements:
    // rather than writing down a series of terms, use a general expression for
    // the disturbing function terms contributions and sum them up.
    //
    // Inputs: P - orbital periods, Lambda - mean longitudes, mu - planets/star
    // mass ratio, z - free eccentricities (if they are not given, zero values
    // are assumed), u - free inclinations (=I*exp(i*Omega) (if they are not given, zero values
    // are assumed). Labmda, z and u can be given as columns of multiple values.
    // OrdersAll: the number of j values taken in the expansion. If given an
    // empty value [], the code will decide by itself what is the required
    // number. MaxPower: the maximal (joint) power of eccentricities and
    // inclinations taken in the expansion. Default is 4.
    //

    // Yair Judkovsky, 6.12.2020. Coded with lots of blood, swear and ink :)
    if (params.z.size() == 1 && params.P.size() > 1) {
        params.z = VectorXd::Zero(params.P.size());
    }
    if (params.u.size() == 1 && params.P.size() > 1) {
        params.u = VectorXd::Zero(params.P.size());
    }
    //mean motions
    MatrixXd n = 2 * M_PI / params.P.array();

    //absolute values and angles of eccentricities
    MatrixXd e = params.z.array().abs();
    MatrixXd pom = params.z.array().arg();

    //absolute values and angles of inclinations
    MatrixXd I = params.u.array().abs();
    MatrixXd Om = params.u.array().arg();

    //allocate space for 3d matrices that will hold the values of dLambda and dz
    //according to these dimensios: orders, data points, number of planets
    MatrixXd dLambdaMatrix = MatrixXd::Zero(params.z.rows(), params.P.size());
    MatrixXcd dzMatrix = MatrixXcd::Zero(params.z.rows(), params.P.size());
    MatrixXcd duMatrix = MatrixXd::Zero(params.z.rows(), params.P.size());
    MatrixXd da_oaMatrix = MatrixXd::Zero(params.z.rows(), params.P.size());

    //for each pair of planets, calculate the 2nd order forced
    //eccentricity. secular terms are ignored.
    for (int j1 = 0; j1 < params.P.size(); ++j1) {
        for (int j2 = j1 + 1; j2 < params.P.size(); ++j2) {
            alc::CalculateDisturbingFunctionParams params_part = {n, e, pom, I, Om};
            auto disturbing_params = calculate_disturbing_params(j1, j2, params, params_part);

            auto disturbing_result = DisturbingFunctionMultTermVariations(disturbing_params);

            dLambdaMatrix.col(j1) = dLambdaMatrix.col(j1) + disturbing_result.dLambda1;
            dzMatrix.col(j1) = dzMatrix.col(j1) + disturbing_result.dz1;
            duMatrix.col(j1) = duMatrix.col(j1) + disturbing_result.du1;
            da_oaMatrix.col(j1) = da_oaMatrix.col(j1) + disturbing_result.da_oa1;

            dLambdaMatrix.col(j2) = dLambdaMatrix.col(j2) + disturbing_result.dLambda2;
            dzMatrix.col(j2) = dzMatrix.col(j2) + disturbing_result.dz2;
            duMatrix.col(j2) = duMatrix.col(j2) + disturbing_result.du2;
            da_oaMatrix.col(j2) = da_oaMatrix.col(j2) + disturbing_result.da_oa2;
        }
    }
    return std::make_tuple(dzMatrix, dLambdaMatrix, duMatrix, da_oaMatrix);
}

std::tuple<VectorXd, VectorXi, VectorXi> sjDVec(int j) {
    VectorXd sVec(42 + 24 + 6);
    sVec.segment(0, 42).setConstant(0.5);
    sVec.segment(42, 24).setConstant(1.5);
    sVec.segment(66, 6).setConstant(2.5);

    Eigen::VectorXi jVec(72);
    jVec.segment(0, 6).setConstant(j);
    jVec.segment(6, 6).setConstant(j + 1);
    jVec.segment(12, 6).setConstant(j + 2);
    jVec.segment(18, 6).setConstant(j - 1);
    jVec.segment(24, 6).setConstant(j - 2);
    jVec.segment(30, 6).setConstant(j - 3);
    jVec.segment(36, 6).setConstant(j - 4);
    jVec.segment(42, 4).setConstant(j);
    jVec.segment(46, 4).setConstant(j - 1);
    jVec.segment(50, 4).setConstant(j - 2);
    jVec.segment(54, 4).setConstant(j - 3);
    jVec.segment(58, 4).setConstant(j + 1);
    jVec.segment(62, 4).setConstant(j + 2);
    jVec.segment(66, 1).setConstant(j);
    jVec.segment(67, 1).setConstant(j);
    jVec.segment(68, 1).setConstant(j - 2);
    jVec.segment(69, 1).setConstant(j - 2);
    jVec.segment(70, 1).setConstant(j + 2);
    jVec.segment(71, 1).setConstant(j + 2);

    VectorXi DVec(0);

    // Append values to the vector
    for (int i2 = 0; i2 < 7; i2++) {
        for (int i = 0; i <= 5; ++i) {
            DVec.conservativeResize(DVec.size() + 1);
            DVec(DVec.size() - 1) = i;
        }
    }
    for (int i2 = 0; i2 < 6; i2++) {
        for (int i = 0; i <= 3; ++i) {
            DVec.conservativeResize(DVec.size() + 1);
            DVec(DVec.size() - 1) = i;
        }
    }

    for (int i2 = 0; i2 < 3; i2++) {
        for (int i = 0; i <= 1; ++i) {
            DVec.conservativeResize(DVec.size() + 1);
            DVec(DVec.size() - 1) = i;
        }
    }
    return std::make_tuple(sVec, jVec, DVec);
}

std::tuple<VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd> Calc_AtoD() {
    VectorXd k = VectorXd::Zero(38 + 22 + 46 + 10 + 19);
    for (int i = 38; i < 38 + 22; ++i) {
        k(i) = 1;
    }
    for (int i = 38 + 22; i < 38 + 22 + 46; ++i) {
        k(i) = 2;
    }
    for (int i = 38 + 22 + 46; i < 38 + 22 + 46 + 10; ++i) {
        k(i) = 3;
    }
    for (int i = 38 + 22 + 46 + 10; i < 38 + 22 + 46 + 10 + 19; ++i) {
        k(i) = 4;
    }

    VectorXd A(135);
    A << 0, 2, 0, 0, 0, 4, 2, 0, 2, 0, 2, 0, 0, 0, 0,
            1, 3, 1, 1, 1, 0, 2, 0, 0, 0, 2, 2, 1, 0, 2, 1, 1, 1, 0, 2, 1, 0, 0,
            1, 3, 1, 1, 1, 0, 2, 0, 0, 0, 2, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0,
            2, 4, 2, 2, 2, 1, 3, 1, 1, 1, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0,
            0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 3, 1, 1, 1, 2, 2, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0,
            3, 2, 1, 0, 1, 0, 1, 0, 1, 0,
            4, 3, 2, 1, 0, 2, 1, 0, 0, 2, 1, 0, 0, 2, 1, 0, 0, 0, 0;
    VectorXd Ap(135);
    Ap << 0, 0, 2, 0, 0, 0, 2, 4, 0, 2, 0, 2, 0, 0, 0,
            1, 1, 3, 1, 1, 0, 0, 2, 0, 0, 2, 0, 1, 2, 0, 1, 1, 1, 2, 0, 1, 2, 0,
            0, 0, 2, 0, 0, 1, 1, 3, 1, 1, 1, 2, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1,
            0, 0, 2, 0, 0, 1, 1, 3, 1, 1, 2, 2, 4, 2, 2, 0, 0, 2, 0, 0,
            0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 1, 3, 1, 1, 0, 0, 1, 1, 1, 1, 2, 2, 0, 1, 1, 0,
            0, 1, 2, 3, 0, 1, 0, 1, 0, 1,
            0, 1, 2, 3, 4, 0, 1, 2, 0, 0, 1, 2, 0, 0, 1, 2, 0, 0, 0;

    VectorXd B(135);
    B << 0, 0, 0, 2, 0, 0, 0, 0, 2, 2, 0, 0, 4, 0, 2,
            0, 0, 0, 2, 0, 1, 1, 1, 3, 1, 0, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 2,
            0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 2, 2, 1, 1, 1, 1, 1, 1, 0, 0,
            0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 2, 2, 2, 4, 2,
            1, 1, 1, 3, 1, 0, 0, 0, 2, 0, 0, 0, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0, 1,
            0, 0, 0, 0, 2, 2, 1, 1, 0, 0,
            0, 0, 0, 0, 0, 2, 2, 2, 4, 1, 1, 1, 3, 0, 0, 0, 2, 1, 0;


    VectorXd Bp(135);
    Bp << 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 2, 0, 4, 2,
            0, 0, 0, 0, 2, 1, 1, 1, 1, 3, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2,
            0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2,
            0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2,
            1, 1, 1, 1, 3, 2, 2, 2, 2, 4, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3,
            0, 0, 0, 0, 0, 0, 1, 1, 2, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 4;

    VectorXd C = VectorXd::Zero(25);
    for (int i = 15; i < 15 + 5; ++i) {
        C(i) = 1;
    }

    VectorXd C_con(110);
    C_con << 2, -2, -1, 0, -2, 1, 1, -1, 0, -2, -1, 0, 0,
            1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, -1, -1, 0, 1, 1, -1, 0, 0, 0, -1, 0,
            2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, -1, -1, 1, 2, 2, 1, 1, -1, 1, 0, 0, 0, -1, 1, 0,
            3, 2, 1, 0, 1, 0, 1, 0, 1, 0,
            4, 3, 2, 1, 0, 2, 1, 0, 0, 2, 1, 0, 0, 2, 1, 0, 0, 0, 0;
    C.conservativeResize(C.size() + C_con.size());
    C.tail(C_con.size()) = C_con;

    VectorXd Cp = VectorXd::Zero(25);
    for (int i = 15; i < 15 + 5; ++i) {
        Cp(i) = -1;
    }
    VectorXd Cp_con(110);
    Cp_con << -2, 0, -1, -2, 0, -1, -1, -1, -2, 0, -1, -2, 0,
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1, -1, 2, 0, -1, 0, 0, 0, 1, 1, -1, 0, -1,
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 3, 1, -1, 0, 0, 1, 1, 1, -1, 2, 2, 0, 1, -1, 0,
            0, 1, 2, 3, 0, 1, 0, 1, 0, 1,
            0, 1, 2, 3, 4, 0, 1, 2, 0, 0, 1, 2, 0, 0, 1, 2, 0, 0, 0;
    Cp.conservativeResize(Cp.size() + Cp_con.size());
    Cp.tail(Cp_con.size()) = Cp_con;

    VectorXd D_con = VectorXd::Zero(25);
    for (size_t i = 20; i < 20 + 5; ++i) {
        D_con(i) = 1;
    }
    VectorXd D_con1(13);
    D_con1 << 0, 2, 2, 2, 1, -1, 1, 1, 1, 0, 0, 0, 2; //Oth order
    VectorXd D_con2 = VectorXd::Zero(10);
    VectorXd D_con3(12);
    D_con3 << 0, 0, 2, 2, -1, 1, 1, -1, 1, 1, 0, 0;//1st order
    VectorXd D_con4 = VectorXd::Zero(15);
    VectorXd D_con5(60);
    D_con5 << 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
            0, 0, 2, 2, -1, 1, -1, 1, 1, 1, -1, 1, 3, 0, 0, -1,
            0, 0, 0, 0, 2, 2, 1, 1, 0, 0,
            0, 0, 0, 0, 0, 2, 2, 2, 4, 1, 1, 1, 3, 0, 0, 0, 2, 1, 0;
    VectorXd D(135);
    D << D_con, D_con1, D_con2, D_con3, D_con4, D_con5;


    VectorXd Dp_con = VectorXd::Zero(25);
    for (size_t i = 20; i < 20 + 5; ++i) {
        Dp_con(i) = -1;
    }
    VectorXd Dp_con1(13);
    Dp_con1 << 0, 0, 0, 0, 1, 1, -1, 1, 1, 2, 2, 2, -2; //Oth order
    VectorXd Dp_con2 = VectorXd::Zero(10);
    VectorXd Dp_con3(12);
    Dp_con3 << 0, 0, 0, 0, 1, -1, 1, 1, -1, 1, 2, 2; //1st order
    VectorXd Dp_con4 = VectorXd::Zero(20);
    VectorXd Dp_con5(55);
    Dp_con5 << 1, 1, 1, 1, 1, 2, 2, 2, 2, 2,
            0, 0, 0, 0, 1, -1, 1, -1, 1, 1, 1, -1, -1, 2, 2, 3,
            0, 0, 0, 0, 0, 0, 1, 1, 2, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 4;

    VectorXd Dp(135);
    Dp << Dp_con, Dp_con1, Dp_con2, Dp_con3, Dp_con4, Dp_con5;
    return std::make_tuple(k, A, Ap, B, Bp, C, Cp, D, Dp);
}

std::tuple<VectorXd, VectorXd, VectorXd, VectorXd, VectorXd>
Calc_Df(LaplaceCoeff &lc, double alph, VectorXd sVec, VectorXi jVec, VectorXi DVec, int j) {
    VectorXd CoeffsVec = lc.calculate(alph, sVec, jVec.cast<double>(), DVec);

    auto Aj = CoeffsVec[0];
    auto DAj = CoeffsVec[1];
    auto D2Aj = CoeffsVec[2];
    auto D3Aj = CoeffsVec[3];
    auto D4Aj = CoeffsVec[4];
    auto D5Aj = CoeffsVec[5];

    auto Ajp1 = CoeffsVec[6];
    auto DAjp1 = CoeffsVec[7];
    auto D2Ajp1 = CoeffsVec[8];
    auto D3Ajp1 = CoeffsVec[9];
    auto D4Ajp1 = CoeffsVec[10];
    auto D5Ajp1 = CoeffsVec[11];

    auto Ajp2 = CoeffsVec[12];
    auto DAjp2 = CoeffsVec[13];
    auto D2Ajp2 = CoeffsVec[14];
    auto D3Ajp2 = CoeffsVec[15];
    auto D4Ajp2 = CoeffsVec[16];
    auto D5Ajp2 = CoeffsVec[17];

    auto Ajm1 = CoeffsVec[18];
    auto DAjm1 = CoeffsVec[19];
    auto D2Ajm1 = CoeffsVec[20];
    auto D3Ajm1 = CoeffsVec[21];
    auto D4Ajm1 = CoeffsVec[22];
    auto D5Ajm1 = CoeffsVec[23];

    auto Ajm2 = CoeffsVec[24];
    auto DAjm2 = CoeffsVec[25];
    auto D2Ajm2 = CoeffsVec[26];
    auto D3Ajm2 = CoeffsVec[27];
    auto D4Ajm2 = CoeffsVec[28];
    auto D5Ajm2 = CoeffsVec[29];

    auto Ajm3 = CoeffsVec[30];
    auto DAjm3 = CoeffsVec[31];
    auto D2Ajm3 = CoeffsVec[32];
    auto D3Ajm3 = CoeffsVec[33];
    auto D4Ajm3 = CoeffsVec[34];
    auto D5Ajm3 = CoeffsVec[35];

    auto Ajm4 = CoeffsVec[36];
    auto DAjm4 = CoeffsVec[37];
    auto D2Ajm4 = CoeffsVec[38];
    auto D3Ajm4 = CoeffsVec[39];
    auto D4Ajm4 = CoeffsVec[40];
    auto D5Ajm4 = CoeffsVec[41];

    auto Bj = CoeffsVec[42];
    auto DBj = CoeffsVec[43];
    auto D2Bj = CoeffsVec[44];
    auto D3Bj = CoeffsVec[45];

    auto Bjm1 = CoeffsVec[46];
    auto DBjm1 = CoeffsVec[47];
    auto D2Bjm1 = CoeffsVec[48];
    auto D3Bjm1 = CoeffsVec[49];

    auto Bjm2 = CoeffsVec[50];
    auto DBjm2 = CoeffsVec[51];
    auto D2Bjm2 = CoeffsVec[52];
    auto D3Bjm2 = CoeffsVec[53];

    auto Bjm3 = CoeffsVec[54];
    auto DBjm3 = CoeffsVec[55];
    auto D2Bjm3 = CoeffsVec[56];
    auto D3Bjm3 = CoeffsVec[57];

    auto Bjp1 = CoeffsVec[58];
    auto DBjp1 = CoeffsVec[59];
    auto D2Bjp1 = CoeffsVec[60];
    auto D3Bjp1 = CoeffsVec[61];

    auto Bjp2 = CoeffsVec[62];
    auto DBjp2 = CoeffsVec[63];
    auto D2Bjp2 = CoeffsVec[64];
    auto D3Bjp2 = CoeffsVec[65];

    auto Cj = CoeffsVec[66];
    auto DCj = CoeffsVec[67];

    auto Cjm2 = CoeffsVec[68];
    auto DCjm2 = CoeffsVec[69];

    auto Cjp2 = CoeffsVec[70];
    auto DCjp2 = CoeffsVec[71];

    // 0th order semi-major axis functions
    auto f1 = 0.5 * Aj;
    auto Df1 = 0.5 * DAj;

    auto f2 = 1. / 8. * (-4. * j * j * Aj + 2 * alph * DAj + alph * alph * D2Aj);
    auto Df2 =
            1. / 8 * (-4 * j * j * DAj + 2 * DAj + 2 * alph * D2Aj + 2 * alph * D2Aj + alph * alph * D3Aj);

    auto f3 = -0.25 * alph * Bjm1 - 0.25 * alph * Bjp1;
    auto Df3 = -0.25 * Bjm1 - 0.25 * alph * DBjm1 - 0.25 * Bjp1 - 0.25 * alph * DBjp1;

    auto f4 = 1 / 128. * ((-9 * j * j + 16 * j * j * j * j) * Aj + (-8 * j * j) * alph * DAj +
                          (-8 * j * j) * alph * alph * D2Aj + 4 * alph * alph * alph * D3Aj +
                          alph * alph * alph * alph * D4Aj);
    auto Df4 = 1. / 128. * ((-9 * j * j + 16 * j * j * j * j) * DAj +
            (-8 * j * j) * (DAj + alph * D2Aj) +
                            (-8 * j * j) * (2 * alph * D2Aj + alph * alph * D3Aj) +
                            4 * (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj) +
                            (4 * alph * alph * alph * D4Aj + alph * alph * alph * alph + D5Aj));

    auto f5 = 1 / 32. * ((16 * j * j * j * j) * Aj + (4 - 16 * j * j) * alph * DAj +
                         (14 - 8 * j * j) * alph * alph * D2Aj + 8 * alph * alph * alph * D3Aj +
                         alph * alph * alph * alph * D4Aj);
    auto Df5 = 1 / 32. * ((16 * j * j * j * j) * DAj + (4 - 16 * j * j) * (DAj + alph * D2Aj) +
                          (14 - 8 * j * j) * (2 * alph * D2Aj + alph * alph * D3Aj) +
                          8 * (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj) +
                          (4 * alph * alph * alph * D4Aj + alph * alph * alph * alph * D5Aj));

    auto f6 = 1 / 128. * ((-17 * j * j + 16 * j * j * j * j) * Aj + (24 - 24 * j * j) * alph * DAj +
                          (36 - 8 * j * j) * alph * alph * D2Aj + 12 * alph * alph * alph * D3Aj +
                          alph * alph * alph * alph * D4Aj);
    auto Df6 = 1 / 128. *
               ((-17 * j * j + 16 * j * j * j * j) * DAj + (24 - 24 * j * j) * (DAj + alph * D2Aj) +
                (36 - 8 * j * j) * (2 * alph * D2Aj + alph * alph * D3Aj) +
                12 * (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj) +
                (4 * alph * alph * alph * D4Aj + alph * alph * alph * alph * D5Aj));

    auto f7 = 1 / 16. * ((-2 + 4 * j * j) * alph * Bjm1 + (-4) * alph * alph * DBjm1 +
                         (-1) * alph * alph * alph * D2Bjm1) + 1 / 16. * ((-2 + 4 * j * j) * alph * Bjp1 +
                                                                          (-4) * alph * alph * DBjp1 +
                                                                          (-1) * alph * alph * alph *
                                                                          D2Bjp1);
    auto Df7 = 1 / 16. * ((-2 + 4 * j * j) * (Bjm1 + alph * DBjm1) +
                          (-4) * (2 * alph * DBjm1 + alph * alph * D2Bjm1) +
                          (-1) * (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1)) + 1 / 16. *
                                  ((-2 + 4 * j * j) *
                                                                                              (Bjp1 + alph *
                                                                                                      DBjp1) +
                                                                                              (-4) *
                                                                                              (2 * alph *
                                                                                               DBjp1 +
                                                                                               alph * alph *
                                                                                               D2Bjp1) +
                                                                                              (-1) *
                                                                                              (3 * alph *
                                                                                               alph *
                                                                                               D2Bjp1 +
                                                                                               alph * alph *
                                                                                               alph *
                                                                                               D3Bjp1));

    auto f8 = alph * alph * (3 / 16. * Cjm2 + 0.75 * Cj + 3 / 16. * Cjp2);
    auto Df8 = 2 * alph * (3 / 16. * Cjm2 + 0.75 * Cj + 3 / 16. * Cjp2) +
               alph * alph * (3 / 16. * DCjm2 + 0.75 * DCj + 3 / 16. * DCjp2);

    auto f9 = 0.25 * alph * (Bjm1 + Bjp1) + 3 / 8. * alph * alph * Cjm2 + 15 / 4. * alph * alph * Cj +
              3 / 8. * alph * alph * Cjp2;
    auto Df9 = 0.25 * ((Bjm1 + Bjp1) + alph * (DBjm1 + DBjp1)) +
               3 / 8. * (2 * alph * Cjm2 + alph * alph * DCjm2) +
               15 / 4. * (2 * alph * Cj + alph * alph * DCj) +
               3 / 8. * (2 * alph * Cjp2 + alph * alph * DCjp2);

    auto f10 = 0.25 * ((2 + 6 * j + 4 * j * j) * Ajp1 - 2 * alph * DAjp1 - alph * alph * D2Ajp1);
    auto Df10 = 0.25 * ((2 + 6 * j + 4 * j * j) * DAjp1 - 2 * (DAjp1 + alph * D2Ajp1) -
                        (2 * alph * D2Ajp1 + alph * alph * D3Ajp1));

    auto f11 = 1 / 32. * ((-6 * j - 26 * j * j - 36 * j * j * j - 16 * j * j * j * j) * Ajp1 +
                          (6 * j + 12 * j * j) * alph * DAjp1 + (-4 + 7 * j) * alph * alph * D2Ajp1 -
                          6 * alph * alph * alph * D3Ajp1 - alph * alph * alph * alph * D4Ajp1);
    auto Df11 = 1 / 32. * ((-6 * j - 26 * j * j - 36 * j * j * j - 16 * j * j * j * j) * DAjp1 +
                           (6 * j + 12 * j * j) * (DAjp1 + alph * D2Ajp1) +
                           (-4 + 7 * j) * (2 * alph * D2Ajp1 + alph * alph * D3Ajp1) -
                           6 * (3 * alph * alph * D3Ajp1 + alph * alph * alph * D4Ajp1) -
                           (4 * alph * alph * alph * D4Ajp1 + alph * alph * alph * alph * D5Ajp1));

    auto f12 = 1 / 32. * ((4 + 2 * j - 22 * j * j - 36 * j * j * j - 16 * j * j * j * j) * Ajp1 +
                          (-4 + 22 * j + 20 * j * j) * alph * DAjp1 +
                          (-22 + 7 * j + 8 * j * j) * alph * alph * D2Ajp1 -
                          10 * alph * alph * alph * D3Ajp1 - alph * alph * alph * alph * D4Ajp1);
    auto Df12 = 1. / 32. * ((4 + 2 * j - 22 * j * j - 36 * j * j * j - 16 * j * j * j * j) * DAjp1 +
                            (-4 + 22 * j + 20 * j * j) * (DAjp1 + alph * D2Ajp1) +
                            (-22 + 7 * j + 8 * j * j) * (2 * alph * D2Ajp1 + alph * alph * D3Ajp1) -
                            10 * (3 * alph * alph * D3Ajp1 + alph * alph * alph * D4Ajp1) -
                            (4 * alph * alph * alph * D4Ajp1 + alph * alph * alph * alph * D5Ajp1));

    auto f13 = 1 / 8. * ((-6 * j - 4 * j * j) * alph * (Bj + Bjp2) + 4 * alph * alph * (DBj + DBjp2) +
                         alph * alph * alph * (D2Bj + D2Bjp2));
    auto Df13 = 1 / 8. * ((-6 * j - 4 * j * j) * ((Bj + Bjp2) + alph * (DBj + DBjp2)) +
                          4 * (2 * alph * (DBj + DBjp2) + alph * alph * (D2Bj + D2Bjp2)) +
                          (3 * alph * alph * (D2Bj + D2Bjp2) + alph * alph * alph * (D3Bj + D3Bjp2)));

    auto f14 = alph * Bjp1;
    auto Df14 = Bjp1 + alph * DBjp1;

    auto f15 =
            0.25 * ((2 - 4 * j * j) * alph * Bjp1 + 4 * alph * alph * DBjp1 + alph * alph * alph * D2Bjp1);
    auto Df15 = 0.25 *
                ((2 - 4 * j * j) * (Bjp1 + alph * DBjp1) + 4 * (2 * alph * DBjp1 + alph * alph * D2Bjp1) +
                 (3 * alph * alph * D2Bjp1 + alph * alph * alph * D3Bjp1));

    auto f16 = 0.5 * (-alph) * Bjp1 + 3 * (-alph * alph) * Cj + 1.5 * (-alph * alph) * Cjp2;
    auto Df16 = 0.5 * ((-1) * Bjp1 + (-alph) * DBjp1) + 3 * ((-2 * alph) * Cj + (-alph * alph) * DCj) +
                1.5 * ((-2 * alph) * Cjp2 + (-alph * alph) * DCjp2);

    auto f17 = 1 / 64. * ((12 + 64 * j + 109 * j * j + 72 * j * j * j + 16 * j * j * j * j) * Ajp2 +
                          (-12 - 28 * j - 16 * j * j) * alph * DAjp2 +
                          (6 - 14 * j - 8 * j * j) * alph * alph * D2Ajp2 +
                          8 * alph * alph * alph * D3Ajp2 + alph * alph * alph * alph * D4Ajp2);
    auto Df17 = 1 / 64. * ((12 + 64 * j + 109 * j * j + 72 * j * j * j + 16 * j * j * j * j) * DAjp2 +
                           (-12 - 28 * j - 16 * j * j) * (DAjp2 + alph * D2Ajp2) +
                           (6 - 14 * j - 8 * j * j) * (2 * alph * D2Ajp2 + alph * alph * D3Ajp2) +
                           8 * (3 * alph * alph * D3Ajp2 + alph * alph * alph * D4Ajp2) +
                           (4 * alph * alph * alph * D4Ajp2 + alph * alph * alph * alph * D5Ajp2));

    auto f18 = 1 / 16. * ((12 - 15 * j + 4 * j * j) * alph * Bjm1 + (8 - 4 * j) * alph * alph * DBjm1 +
                          alph * alph * alph * D2Bjm1);
    auto Df18 = 1 / 16. * ((12 - 15 * j + 4 * j * j) * (Bjm1 + alph * DBjm1) +
                           (8 - 4 * j) * (2 * alph * DBjm1 + alph * alph * D2Bjm1) +
                           (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f19 = 1 / 8. * ((6 * j - 4 * j * j) * alph * Bj + (-4 + 4 * j) * alph * alph * DBj -
                         alph * alph * alph * D2Bj);
    auto Df19 = 1 / 8. * ((6 * j - 4 * j * j) * (Bj + alph * DBj) +
                          (-4 + 4 * j) * (2 * alph * DBj + alph * alph * D2Bj) -
                          (3 * alph * alph * D2Bj + alph * alph * alph * D3Bj));

    auto f20 = 1 / 16. * ((3 * j + 4 * j * j) * alph * Bjp1 - 4 * j * alph * alph * DBjp1 +
                          alph * alph * alph * D2Bjp1);
    auto Df20 = 1 / 16. * ((3 * j + 4 * j * j) * (Bjp1 + alph * DBjp1) -
                           4 * j * (2 * alph * DBjp1 + alph * alph * D2Bjp1) +
                           (3 * alph * alph * D2Bjp1 + alph * alph * alph * D3Bjp1));

    auto f21 = 1 / 8. * ((-12 + 15 * j - 4 * j * j) * alph * Bjm1 + (-8 + 4 * j) * alph * alph * DBjm1 -
                         alph * alph * alph * D2Bjm1);
    auto Df21 = 1 / 8. * ((-12 + 15 * j - 4 * j * j) * (Bjm1 + alph * DBjm1) +
                          (-8 + 4 * j) * (2 * alph * DBjm1 + alph * alph * D2Bjm1) -
                          (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f22 = 0.25 * ((6 * j + 4 * j * j) * alph * Bj - 4 * alph * alph * DBj - alph * alph * alph * D2Bj);
    auto Df22 = 0.25 *
                ((6 * j + 4 * j * j) * (Bj + alph * DBj) - 4 * (2 * alph * DBj + alph * alph * D2Bj) -
                 (3 * alph * alph * D2Bj + alph * alph * alph * D3Bj));

    auto f23 = 0.25 *
               ((6 * j + 4 * j * j) * alph * Bjp2 - 4 * alph * alph * DBjp2 - alph * alph * alph * D2Bjp2);
    auto Df23 = 0.25 * ((6 * j + 4 * j * j) * (Bjp2 + alph * DBjp2) -
                        4 * (2 * alph * DBjp2 + alph * alph * D2Bjp2) -
                        (3 * alph * alph * D2Bjp2 + alph * alph * alph * D3Bjp2));

    auto f24 = 0.25 * ((-6 * j + 4 * j * j) * alph * Bj + (4 - 4 * j) * alph * alph * DBj +
                       alph * alph * alph * D2Bj);
    auto Df24 = 0.25 * ((-6 * j + 4 * j * j) * (Bj + alph * DBj) +
                        (4 - 4 * j) * (2 * alph * DBj + alph * alph * D2Bj) +
                        (3 * alph * alph * D2Bj + alph * alph * alph * D3Bj));

    auto f25 = 1 / 8. * ((-3 * j - 4 * j * j) * alph * Bjp1 + 4 * j * alph * alph * DBjp1 -
                         alph * alph * alph * D2Bjp1);
    auto Df25 = 1 / 8. * ((-3 * j - 4 * j * j) * (Bjp1 + alph * DBjp1) +
                          4 * j * (2 * alph * DBjp1 + alph * alph * D2Bjp1) -
                          (3 * alph * alph * D2Bjp1 + alph * alph * alph * D3Bjp1));

    auto f26 = 0.5 * alph * Bjp1 + 0.75 * alph * alph * Cj + 1.5 * alph * alph * Cjp2;
    auto Df26 = 0.5 * (Bjp1 + alph * DBjp1) + 0.75 * (2 * alph * Cj + alph * alph * DCj) +
                1.5 * (2 * alph * Cjp2 + alph * alph * DCjp2);

    //1st order semi-major axis functions
    auto f27 = 0.5 * (-2 * j * Aj - alph * DAj);
    auto Df27 = 0.5 * (-2 * j * DAj - DAj - alph * D2Aj);

    auto f28 = 1 / 16. * ((2 * j - 10 * j * j + 8 * j * j * j) * Aj + (3 - 7 * j + 4 * j * j) * alph * DAj +
                          (-2 - 2 * j) * alph * alph * D2Aj - alph * alph * alph * D3Aj);
    auto Df28 = 1 / 16. * ((2 * j - 10 * j * j + 8 * j * j * j) * DAj +
                           (3 - 7 * j + 4 * j * j) * (DAj + alph * D2Aj) +
                           (-2 - 2 * j) * (2 * alph * D2Aj + alph * alph * D3Aj) -
                           (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj));

    auto f29 = 1 / 8. * ((8 * j * j * j) * Aj + (-2 - 4 * j + 4 * j * j) * alph * DAj +
                         (-4 - 2 * j) * alph * alph * D2Aj - alph * alph * alph * D3Aj);
    auto Df29 = 1 / 8. * ((8 * j * j * j) * DAj + (-2 - 4 * j + 4 * j * j) * (DAj + alph * D2Aj) +
                          (-4 - 2 * j) * (2 * alph * D2Aj + alph * alph * D3Aj) -
                          (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj));

    auto f30 = 0.25 * ((1 + 2 * j) * alph * (Bjm1 + Bjp1) + alph * alph * (DBjm1 + DBjp1));
    auto Df30 = 0.25 * ((1 + 2 * j) * ((Bjm1 + Bjp1) + alph * (DBjm1 + DBjp1)) +
                        (2 * alph * (DBjm1 + DBjp1) + alph * alph * (D2Bjm1 + D2Bjp1)));

    auto f31 = 0.5 * ((-1 + 2 * j) * Ajm1 + alph * DAjm1);
    auto Df31 = 0.5 * ((-1 + 2 * j) * DAjm1 + DAjm1 + alph * D2Ajm1);

    auto f32 = 1 / 8. * ((4 - 16 * j + 20 * j * j - 8 * j * j * j) * Ajm1 +
                         (-4 + 12 * j - 4 * j * j) * alph * DAjm1 + (3 + 2 * j) * alph * alph * D2Ajm1 +
                         alph * alph * alph * D3Ajm1);
    auto Df32 = 1 / 8. * ((4 - 16 * j + 20 * j * j - 8 * j * j * j) * DAjm1 +
                          (-4 + 12 * j - 4 * j * j) * (DAjm1 + alph * D2Ajm1) +
                          (3 + 2 * j) * (2 * alph * D2Ajm1 + alph * alph * D3Ajm1) +
                          (3 * alph * alph * D3Ajm1 + alph * alph * alph * D4Ajm1));

    auto f33 = 1 / 16. *
               ((-2 - j + 10 * j * j - 8 * j * j * j) * Ajm1 + (2 + 9 * j - 4 * j * j) * alph * DAjm1 +
                (5 + 2 * j) * alph * alph * D2Ajm1 + alph * alph * alph * D3Ajm1);
    auto Df33 = 1 / 16. * ((-2 - j + 10 * j * j - 8 * j * j * j) * DAjm1 +
                           (2 + 9 * j - 4 * j * j) * (DAjm1 + alph * D2Ajm1) +
                           (5 + 2 * j) * (2 * alph * D2Ajm1 + alph * alph * D3Ajm1) +
                           (3 * alph * alph * D3Ajm1 + alph * alph * alph * D4Ajm1));

    auto f34 = 0.25 * (-2 * j * alph * (Bjm2 + Bj) - alph * alph * (DBjm2 + DBj));
    auto Df34 = 0.25 * (-2 * j * ((Bjm2 + Bj) + alph * (DBjm2 + DBj)) -
                        (2 * alph * (DBjm2 + DBj) + alph * alph * (D2Bjm2 + D2Bj)));

    auto f35 = 1 / 16. *
               ((1 - j - 10 * j * j - 8 * j * j * j) * Ajp1 + (-1 - j - 4 * j * j) * alph * DAjp1 +
                (3 + 2 * j) * alph * alph * D2Ajp1 + alph * alph * alph * D3Ajp1);
    auto Df35 = 1 / 16. * ((1 - j - 10 * j * j - 8 * j * j * j) * DAjp1 +
                           (-1 - j - 4 * j * j) * (DAjp1 + alph * D2Ajp1) +
                           (3 + 2 * j) * (2 * alph * D2Ajp1 + alph * alph * D3Ajp1) +
                           (3 * alph * alph * D3Ajp1 + alph * alph * alph * D4Ajp1));

    auto f36 = 1 / 16. * ((-8 + 32 * j - 30 * j * j + 8 * j * j * j) * Ajm2 +
                          (8 - 17 * j + 4 * j * j) * alph * DAjm2 + (-4 - 2 * j) * alph * alph * D2Ajm2 -
                          alph * alph * alph * D3Ajm2);
    auto Df36 = 1 / 16. * ((-8 + 32 * j - 30 * j * j + 8 * j * j * j) * DAjm2 +
                           (8 - 17 * j + 4 * j * j) * (DAjm2 + alph * D2Ajm2) +
                           (-4 - 2 * j) * (2 * alph * D2Ajm2 + alph * alph * D3Ajm2) -
                           (3 * alph * alph * D3Ajm2 + alph * alph * alph * D4Ajm2));

    auto f37 = 0.25 * ((-5 + 2 * j) * alph * Bjm1 - alph * alph * DBjm1);
    auto Df37 = 0.25 * ((-5 + 2 * j) * (Bjm1 + alph * DBjm1) - (2 * alph * DBjm1 + alph * alph * D2Bjm1));

    auto f38 = 0.25 * (-2 * j * alph * Bj + alph * alph * DBj);
    auto Df38 = 0.25 * (-2 * j * (Bj + alph * DBj) + (2 * alph * DBj + alph * alph * D2Bj));

    auto f39 = 0.5 * ((-1 - 2 * j) * alph * Bjm1 - alph * alph * DBjm1);
    auto Df39 = 0.5 * ((-1 - 2 * j) * (Bjm1 + alph * DBjm1) - (2 * alph * DBjm1 + alph * alph * D2Bjm1));

    auto f40 = 0.5 * ((-1 - 2 * j) * alph * Bjp1 - alph * alph * DBjp1);
    auto Df40 = 0.5 * ((-1 - 2 * j) * (Bjp1 + alph * DBjp1) - (2 * alph * DBjp1 + alph * alph * D2Bjp1));

    auto f41 = 0.5 * ((5 - 2 * j) * alph * Bjm1 + alph * alph * DBjm1);
    auto Df41 = 0.5 * ((5 - 2 * j) * (Bjm1 + alph * DBjm1) + (2 * alph * DBjm1 + alph * alph * D2Bjm1));

    auto f42 = 0.5 * (2 * j * alph * Bjm2 + alph * alph * DBjm2);
    auto Df42 = 0.5 * (2 * j * (Bjm2 + alph * DBjm2) + (2 * alph * DBjm2 + alph * alph * D2Bjm2));

    auto f43 = 0.5 * (2 * j * alph * Bj + alph * alph * DBj);
    auto Df43 = 0.5 * (2 * j * (Bj + alph * DBj) + (2 * alph * DBj + alph * alph * D2Bj));

    auto f44 = 0.5 * (2 * j * alph * Bj - alph * alph * DBj);
    auto Df44 = 0.5 * (2 * j * (Bj + alph * DBj) - (2 * alph * DBj + alph * alph * D2Bj));

    //2nd order semi-major axis functions
    auto f45 = 1 / 8. * ((-5 * j + 4 * j * j) * Aj + (-2 + 4 * j) * alph * DAj + alph * alph * D2Aj);
    auto Df45 = 1 / 8. * ((-5 * j + 4 * j * j) * DAj + (-2 + 4 * j) * (DAj + alph * D2Aj) +
                          (2 * alph * D2Aj + alph * alph * D3Aj));

    auto f46 = 1 / 96. * ((22 * j - 64 * j * j + 60 * j * j * j - 16 * j * j * j * j) * Aj +
                          (16 - 46 * j + 48 * j * j - 16 * j * j * j) * alph * DAj +
                          (-12 + 9 * j) * alph * alph * D2Aj + 4 * j * alph * alph * alph * D3Aj +
                          alph * alph * alph * alph * D4Aj);
    auto Df46 = 1 / 96. * ((22 * j - 64 * j * j + 60 * j * j * j - 16 * j * j * j * j) * DAj +
                           (16 - 46 * j + 48 * j * j - 16 * j * j * j) * (DAj + alph * D2Aj) +
                           (-12 + 9 * j) * (2 * alph * D2Aj + alph * alph * D3Aj) +
                           4 * j * (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj) +
                           (4 * alph * alph * alph * D4Aj + alph * alph * alph * alph * D5Aj));

    auto f47 = 1 / 32. * ((20 * j * j * j - 16 * j * j * j * j) * Aj +
                          (-4 - 2 * j + 16 * j * j - 16 * j * j * j) * alph * DAj +
                          (-2 + 11 * j) * alph * alph * D2Aj + (4 + 4 * j) * alph * alph * alph * D3Aj +
                          alph * alph * alph * alph * D4Aj);
    auto Df47 = 1 / 32. * ((20 * j * j * j - 16 * j * j * j * j) * DAj +
                           (-4 - 2 * j + 16 * j * j - 16 * j * j * j) * (DAj + alph * D2Aj) +
                           (-2 + 11 * j) * (2 * alph * D2Aj + alph * alph * D3Aj) +
                           (4 + 4 * j) * (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj) +
                           (4 * alph * alph * alph * D4Aj + alph * alph * alph * alph * D5Aj));

    auto f48 = 1 / 16. *
               ((2 + j - 4 * j * j) * alph * (Bjm1 + Bjp1) - 4 * j * alph * alph * (DBjm1 + DBjp1) -
                alph * alph * alph * (D2Bjm1 + D2Bjp1));
    auto Df48 = 1 / 16. * ((2 + j - 4 * j * j) * ((Bjm1 + Bjp1) + alph * (DBjm1 + DBjp1)) -
                           4 * j * (2 * alph * (DBjm1 + DBjp1) + alph * alph * (D2Bjm1 + D2Bjp1)) -
                           (3 * alph * alph * (D2Bjm1 + D2Bjp1) + alph * alph * alph * (D3Bjm1 + D3Bjp1)));

    auto f49 = 0.25 * ((-2 + 6 * j - 4 * j * j) * Ajm1 + (2 - 4 * j) * alph * DAjm1 - alph * alph * D2Ajm1);
    auto Df49 = 0.25 * ((-2 + 6 * j - 4 * j * j) * DAjm1 + (2 - 4 * j) * (DAjm1 + alph * D2Ajm1) -
                        (2 * alph * D2Ajm1 + alph * alph * D3Ajm1));

    auto f50 = 1 / 32. * ((20 - 86 * j + 126 * j * j - 76 * j * j * j + 16 * j * j * j * j) * Ajm1 +
                          (-20 + 74 * j - 64 * j * j + 16 * j * j * j) * alph * DAjm1 +
                          (14 - 17 * j) * alph * alph * D2Ajm1 +
                          (-2 - 4 * j) * alph * alph * alph * D3Ajm1 - alph * alph * alph * alph *
                                                                       D4Ajm1); //in this term there appears Aj- instead of Aj-1; this is explained in the erratum http://ssdbook.maths.qmul.ac.uk/errors.pdf
    auto Df50 = 1 / 32. * ((20 - 86 * j + 126 * j * j - 76 * j * j * j + 16 * j * j * j * j) * DAjm1 +
                           (-20 + 74 * j - 64 * j * j + 16 * j * j * j) * (DAjm1 + alph * D2Ajm1) +
                           (14 - 17 * j) * (2 * alph * D2Ajm1 + alph * alph * D3Ajm1) +
                           (-2 - 4 * j) * (3 * alph * alph * D3Ajm1 + alph * alph * alph * D4Ajm1) -
                           (4 * alph * alph * alph * D4Ajm1 + alph * alph * alph * alph * D5Ajm1));

    auto f51 = 1 / 32. * ((-4 + 2 * j + 22 * j * j - 36 * j * j * j + 16 * j * j * j * j) * Ajm1 +
                          (4 + 6 * j - 32 * j * j + 16 * j * j * j) * alph * DAjm1 +
                          (-2 - 19 * j) * alph * alph * D2Ajm1 +
                          (-6 - 4 * j) * alph * alph * alph * D3Ajm1 - alph * alph * alph * alph * D4Ajm1);
    auto Df51 = 1 / 32. * ((-4 + 2 * j + 22 * j * j - 36 * j * j * j + 16 * j * j * j * j) * DAjm1 +
                           (4 + 6 * j - 32 * j * j + 16 * j * j * j) * (DAjm1 + alph * D2Ajm1) +
                           (-2 - 19 * j) * (2 * alph * D2Ajm1 + alph * alph * D3Ajm1) +
                           (-6 - 4 * j) * (3 * alph * alph * D3Ajm1 + alph * alph * alph * D4Ajm1) -
                           (4 * alph * alph * alph * D4Ajm1 + alph * alph * alph * alph * D5Ajm1));

    auto f52 = 1 / 8. * ((-2 * j + 4 * j * j) * alph * (Bjm2 + Bj) + 4 * j * alph * alph * (DBjm2 + DBj) +
                         alph * alph * alph * (D2Bjm2 + D2Bj));
    auto Df52 = 1 / 8. * ((-2 * j + 4 * j * j) * ((Bjm2 + Bj) + alph * (DBjm2 + DBj)) +
                          4 * j * (2 * alph * (DBjm2 + DBj) + alph * alph * (D2Bjm2 + D2Bj)) +
                          (3 * alph * alph * (D2Bjm2 + D2Bj) + alph * alph * alph * (D3Bjm2 + D3Bj)));

    auto f53 =
            1 / 8. * ((2 - 7 * j + 4 * j * j) * Ajm2 + (-2 + 4 * j) * alph * DAjm2 + alph * alph * D2Ajm2);
    auto Df53 = 1 / 8. * ((2 - 7 * j + 4 * j * j) * DAjm2 + (-2 + 4 * j) * (DAjm2 + alph * D2Ajm2) +
                          (2 * alph * D2Ajm2 + alph * alph * D3Ajm2));

    //            f54 =   1/32*((-32+144*j-184*j^2+92*j^3-16*j^4)*Ajm2  +(32-102*j+80*j^2-16*j^3)*alph*DAjm2                +(-16+25*j)*alph^2*D2Ajm2                           +(4+4*j)*alph^3*D3Ajm2                              +alph^4*D4Ajm2);

    auto f54 = 1 / 32. * ((-32 + 144 * j - 184 * j * j + 92 * j * j * j - 16 * j * j * j * j) * Ajm2 +
                          (32 - 102 * j + 80 * j * j - 16 * j * j * j) * alph * DAjm2 +
                          (-16 + 25 * j) * alph * alph * D2Ajm2 +
                          (4 + 4 * j) * alph * alph * alph * D3Ajm2 + alph * alph * alph * alph * D4Ajm2);
    auto Df54 = 1 / 32. * ((-32 + 144 * j - 184 * j * j + 92 * j * j * j - 16 * j * j * j * j) * DAjm2 +
                           (32 - 102 * j + 80 * j * j - 16 * j * j * j) * (DAjm2 + alph * D2Ajm2) +
                           (-16 + 25 * j) * (2 * alph * D2Ajm2 + alph * alph * D3Ajm2) +
                           (4 + 4 * j) * (3 * alph * alph * D3Ajm2 + alph * alph * alph * D4Ajm2) +
                           (4 * alph * alph * alph * D4Ajm2 + alph * alph * alph * alph * D5Ajm2));

    auto f55 = 1 / 96. * ((12 - 14 * j - 40 * j * j + 52 * j * j * j - 16 * j * j * j * j) * Ajm2 +
                          (-12 - 10 * j + 48 * j * j - 16 * j * j * j) * alph * DAjm2 +
                          (6 + 27 * j) * alph * alph * D2Ajm2 +
                          (8 + 4 * j) * alph * alph * alph * D3Ajm2 +
                          alph * alph * alph * alph * D4Ajm2);
    auto Df55 = 1 / 96. * ((12 - 14 * j - 40 * j * j + 52 * j * j * j - 16 * j * j * j * j) * DAjm2 +
                           (-12 - 10 * j + 48 * j * j + -16 * j * j * j) * (DAjm2 + alph * D2Ajm2) +
                           (6 + 27 * j) * (2 * alph * D2Ajm2 + alph * alph * D3Ajm2) +
                           (8 + 4 * j) * (3 * alph * alph * D3Ajm2 + alph * alph * alph * D4Ajm2) +
                           (4 * alph * alph * alph * D4Ajm2 + alph * alph * alph * alph * D5Ajm2));

    //            f55 =   1/96*((12-14*j-40*j^2+52*j^3-16*j^4)*Ajm2  +(-12-10*j+48*j^2-16*j^3)*alph*DAjm2                +(6+27*j)*alph^2*D2Ajm2                           +(8+4*j)*alph^3*D3Ajm2                               +alph^4*D4Ajm2);
    //            Df55 = 1/96*((12-14*j-40*j^2+52*j^3-16*j^4)*DAjm2+(-12-10*j+48*j^2-16*j^3)*(DAjm2+alph*D2Ajm2)+(6+27*j)*(2*alph*D2Ajm2+alph^2*D3Ajm2)+(8+4*j)*(3*alph^2*D3Ajm2+alph^3*D4Ajm2)+(4*alph^3*D4Ajm2+alph^4*D5Ajm2));

    auto f56 = 1 / 16. *
               ((3 * j - 4 * j * j) * alph * (Bjm3 + Bjm1) - 4 * j * alph * alph * (DBjm3 + DBjm1) -
                alph * alph * alph * (D2Bjm3 + D2Bjm1));
    auto Df56 = 1 / 16. * ((3 * j - 4 * j * j) * ((Bjm3 + Bjm1) + alph * (DBjm3 + DBjm1)) -
                           4 * j * (2 * alph * (DBjm3 + DBjm1) + alph * alph * (D2Bjm3 + D2Bjm1)) -
                           (3 * alph * alph * (D2Bjm3 + D2Bjm1) + alph * alph * alph * (D3Bjm3 + D3Bjm1)));
    //            f56 =   1/16*((3*j-4*j^2)*alph*(Bjm3+Bjm1)                            -4*j*alph^2*(DBjm3+DBjm1)                                         -alph^3*(D2Bjm3+D2Bjm1));
    //            Df56 = 1/16*((3*j-4*j^2)*((Bjm3+Bjm1)+alph*(DBjm3+DBjm1))-4*j*(2*alph*(DBjm3+DBjm1)+alph^2*(D2Bjm3+D2Bjm1))-(3*alph^2*(D2Bjm3+D2Bjm1)+alph^3*(D3Bjm3+D3Bjm1)));

    auto f57 = 0.5 * alph * Bjm1;
    auto Df57 = 0.5 * (Bjm1 + alph * DBjm1);

    auto f58 = 1 / 8. * ((-14 + 16 * j - 4 * j * j) * alph * Bjm1 + 4 * alph * alph * DBjm1 +
                         alph * alph * alph * D2Bjm1);
    auto Df58 = 1 / 8. * ((-14 + 16 * j - 4 * j * j) * (Bjm1 + alph * DBjm1) +
                          4 * (2 * alph * DBjm1 + alph * alph * D2Bjm1) +
                          (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f59 = 1 / 8. *
               ((2 - 4 * j * j) * alph * Bjm1 + 4 * alph * alph * DBjm1 + alph * alph * alph * D2Bjm1);
    auto Df59 = 1 / 8. *
                ((2 - 4 * j * j) * (Bjm1 + alph * DBjm1) + 4 * (2 * alph * DBjm1 + alph * alph * D2Bjm1) +
                 (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f60 = 0.75 * (-alph * alph) * (Cjm2 + Cj);
    auto Df60 = 0.75 * ((-2 * alph) * (Cjm2 + Cj) + (-alph * alph) * (DCjm2 + DCj));

    auto f61 = 0.5 * (-alph) * Bjm1 + 0.75 * (-alph * alph) * Cjm2 + 15. / 4. * (-alph * alph) * Cj;
    auto Df61 = 0.5 * ((-1) * Bjm1 + (-alph) * DBjm1) +
                0.75 * ((-2 * alph) * Cjm2 + (-1 * (alph * alph)) * DCjm2) +
                15. / 4. * ((-2 * alph) * Cj + (-alph * alph) * DCj);

    auto f62 = -alph * Bjm1;
    auto Df62 = -Bjm1 - alph * DBjm1;

    auto f63 = 0.25 * ((14 - 16 * j + 4 * j * j) * alph * Bjm1 - 4 * alph * alph * DBjm1 -
                       alph * alph * alph * D2Bjm1);
    auto Df63 = 0.25 * ((14 - 16 * j + 4 * j * j) * (Bjm1 + alph * DBjm1) -
                        4 * (2 * alph * DBjm1 + alph * alph * D2Bjm1) -
                        (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f64 =
            0.25 * ((-2 + 4 * j * j) * alph * Bjm1 - 4 * alph * alph * DBjm1 - alph * alph * alph * D2Bjm1);
    auto Df64 = 0.25 *
                ((-2 + 4 * j * j) * (Bjm1 + alph * DBjm1) - 4 * (2 * alph * DBjm1 + alph * alph * D2Bjm1) -
                 (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f65 = 0.5 * alph * Bjm1 + 3 * alph * alph * Cjm2 + 1.5 * alph * alph * Cj;
    auto Df65 = 0.5 * (Bjm1 + alph * DBjm1) + 3 * (2 * alph * Cjm2 + alph * alph * DCjm2) +
                1.5 * (2 * alph * Cj + alph * alph * DCj);

    auto f66 = 0.5 * alph * Bjm1 + 1.5 * alph * alph * Cjm2 + 3 * alph * alph * Cj;
    auto Df66 = 0.5 * (Bjm1 + alph * DBjm1) + 1.5 * (2 * alph * Cjm2 + alph * alph * DCjm2) +
                3 * (2 * alph * Cj + alph * alph * DCj);

    auto f67 = 0.5 * (-alph) * Bjm1 + 15. / 4 * (-alph * alph) * Cjm2 + 0.75 * (-alph * alph) * Cj;
    auto Df67 = 0.5 * ((-1) * Bjm1 + (-alph) * DBjm1) +
                15. / 4 * ((-2 * alph) * Cjm2 + (-1 * (alph * alph)) * DCjm2) +
                0.75 * ((-2 * alph) * Cj + (-alph * alph) * DCj);

    auto f68 = 1 / 96. * ((4 - 2 * j - 26 * j * j - 4 * j * j * j + 16 * j * j * j * j) * Ajp1 +
                          (-4 - 2 * j + 16 * j * j * j) * alph * DAjp1 +
                          (6 - 3 * j) * alph * alph * D2Ajp1 + (-2 - 4 * j) * alph * alph * alph * D3Ajp1 -
                          alph * alph * alph * alph * D4Ajp1);
    auto Df68 = 1 / 96. * ((4 - 2 * j - 26 * j * j - 4 * j * j * j + 16 * j * j * j * j) * DAjp1 +
                           (-4 - 2 * j + 16 * j * j * j) * (DAjp1 + alph * D2Ajp1) +
                           (6 - 3 * j) * (2 * alph * D2Ajp1 + alph * alph * D3Ajp1) +
                           (-2 - 4 * j) * (3 * alph * alph * D3Ajp1 + alph * alph * alph * D4Ajp1) -
                           (4 * alph * alph * alph * D4Ajp1 + alph * alph * alph * alph * D5Ajp1));

    auto f69 = 1 / 96. * ((36 - 186 * j + 238 * j * j - 108 * j * j * j + 16 * j * j * j * j) * Ajm3 +
                          (-36 + 130 * j - 96 * j * j + 16 * j * j * j) * alph * DAjm3 +
                          (18 - 33 * j) * alph * alph * D2Ajm3 +
                          (-6 - 4 * j) * alph * alph * alph * D3Ajm3 - alph * alph * alph * alph * D4Ajm3);
    auto Df69 = 1 / 96. * ((36 - 186 * j + 238 * j * j - 108 * j * j * j + 16 * j * j * j * j) * DAjm3 +
                           (-36 + 130 * j - 96 * j * j + 16 * j * j * j) * (DAjm3 + alph * D2Ajm3) +
                           (18 - 33 * j) * (2 * alph * D2Ajm3 + alph * alph * D3Ajm3) +
                           (-6 - 4 * j) * (3 * alph * alph * D3Ajm3 + alph * alph * alph * D4Ajm3) -
                           (4 * alph * alph * alph * D4Ajm3 + alph * alph * alph * alph * D5Ajm3));

    auto f70 = 1 / 8. * ((-14 * j + 4 * j * j) * alph * Bjm2 - 8 * alph * alph * DBjm2 -
                         alph * alph * alph * D2Bjm2);
    auto Df70 = 1 / 8. * ((-14 * j + 4 * j * j) * (Bjm2 + alph * DBjm2) -
                          8 * (2 * alph * DBjm2 + alph * alph * D2Bjm2) -
                          (3 * alph * alph * D2Bjm2 + alph * alph * alph * D3Bjm2));

    auto f71 = 1 / 8. * ((-2 * j + 4 * j * j) * alph * Bj - alph * alph * alph * D2Bj);
    auto Df71 = 1 / 8. * ((-2 * j + 4 * j * j) * (Bj + alph * DBj) -
                          (3 * alph * alph * D2Bj + alph * alph * alph * D3Bj));

    auto f72 = 1 / 8. * ((-2 - j + 4 * j * j) * alph * Bjm1 + 4 * j * alph * alph * DBjm1 +
                         alph * alph * alph * D2Bjm1);
    auto Df72 = 1 / 8. * ((-2 - j + 4 * j * j) * (Bjm1 + alph * DBjm1) +
                          4 * j * (2 * alph * DBjm1 + alph * alph * D2Bjm1) +
                          (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f73 = 1 / 8. * ((-2 - j + 4 * j * j) * alph * Bjp1 + 4 * j * alph * alph * DBjp1 +
                         alph * alph * alph * D2Bjp1);
    auto Df73 = 1 / 8. * ((-2 - j + 4 * j * j) * (Bjp1 + alph * DBjp1) +
                          4 * j * (2 * alph * DBjp1 + alph * alph * D2Bjp1) +
                          (3 * alph * alph * D2Bjp1 + alph * alph * alph * D3Bjp1));

    auto f74 = 0.25 * ((2 * j - 4 * j * j) * alph * Bjm2 - 4 * j * alph * alph * DBjm2 -
                       alph * alph * alph * D2Bjm2);
    auto Df74 = 0.25 * ((2 * j - 4 * j * j) * (Bjm2 + alph * DBjm2) -
                        4 * j * (2 * alph * DBjm2 + alph * alph * D2Bjm2) -
                        (3 * alph * alph * D2Bjm2 + alph * alph * alph * D3Bjm2));

    auto f75 = 0.25 *
               ((2 * j - 4 * j * j) * alph * Bj - 4 * j * alph * alph * DBj - alph * alph * alph * D2Bj);
    auto Df75 = 0.25 *
                ((2 * j - 4 * j * j) * (Bj + alph * DBj) - 4 * j * (2 * alph * DBj + alph * alph * D2Bj) -
                 (3 * alph * alph * D2Bj + alph * alph * alph * D3Bj));

    auto f76 = 0.25 *
               ((14 * j - 4 * j * j) * alph * Bjm2 + 8 * alph * alph * DBjm2 + alph * alph * alph * D2Bjm2);
    auto Df76 = 0.25 * ((14 * j - 4 * j * j) * (Bjm2 + alph * DBjm2) +
                        8 * (2 * alph * DBjm2 + alph * alph * D2Bjm2) +
                        (3 * alph * alph * D2Bjm2 + alph * alph * alph * D3Bjm2));

    auto f77 = 0.25 * ((2 * j - 4 * j * j) * alph * Bj + alph * alph * alph * D2Bj);
    auto Df77 = 0.25 * ((2 * j - 4 * j * j) * (Bj + alph * DBj) +
                        (3 * alph * alph * D2Bj + alph * alph * alph * D3Bj));

    auto f78 = 1 / 8. * ((-3 * j + 4 * j * j) * alph * Bjm3 + 4 * j * alph * alph * DBjm3 +
                         alph * alph * alph * D2Bjm3);
    auto Df78 = 1 / 8. * ((-3 * j + 4 * j * j) * (Bjm3 + alph * DBjm3) +
                          4 * j * (2 * alph * DBjm3 + alph * alph * D2Bjm3) +
                          (3 * alph * alph * D2Bjm3 + alph * alph * alph * D3Bjm3));

    auto f79 = 1 / 8. * ((-3 * j + 4 * j * j) * alph * Bjm1 + 4 * j * alph * alph * DBjm1 +
                         alph * alph * alph * D2Bjm1);
    auto Df79 = 1 / 8. * ((-3 * j + 4 * j * j) * (Bjm1 + alph * DBjm1) +
                          4 * j * (2 * alph * DBjm1 + alph * alph * D2Bjm1) +
                          (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f80 = 1.5 * alph * alph * Cj;
    auto Df80 = 1.5 * (2 * alph * Cj + alph * alph * DCj);

    auto f81 = 1.5 * alph * alph * Cjm2;
    auto Df81 = 1.5 * (2 * alph * Cjm2 + alph * alph * DCjm2);

    //3rd order semi-major axis functions
    auto f82 = 1 / 48. *
               ((-26 * j + 30 * j * j - 8 * j * j * j) * Aj + (-9 + 27 * j - 12 * j * j) * alph * DAj +
                (6 - 6 * j) * alph * alph * D2Aj - alph * alph * alph * D3Aj);
    auto Df82 = 1 / 48. * ((-26 * j + 30 * j * j - 8 * j * j * j) * DAj +
                           (-9 + 27 * j - 12 * j * j) * (DAj + alph * D2Aj) +
                           (6 - 6 * j) * (2 * alph * D2Aj + alph * alph * D3Aj) -
                           (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj));

    auto f83 = 1 / 16. * ((-9 + 31 * j - 30 * j * j + 8 * j * j * j) * Ajm1 +
                          (9 - 25 * j + 12 * j * j) * alph * DAjm1 + (-5 + 6 * j) * alph * alph * D2Ajm1 +
                          alph * alph * alph * D3Ajm1);
    auto Df83 = 1 / 16. * ((-9 + 31 * j - 30 * j * j + 8 * j * j * j) * DAjm1 +
                           (9 - 25 * j + 12 * j * j) * (DAjm1 + alph * D2Ajm1) +
                           (-5 + 6 * j) * (2 * alph * D2Ajm1 + alph * alph * D3Ajm1) +
                           (3 * alph * alph * D3Ajm1 + alph * alph * alph * D4Ajm1));

    auto f84 = 1 / 16. * ((8 - 32 * j + 30 * j * j - 8 * j * j * j) * Ajm2 +
                          (-8 + 23 * j - 12 * j * j) * alph * DAjm2 + (4 - 6 * j) * alph * alph * D2Ajm2 -
                          alph * alph * alph * D3Ajm2);
    auto Df84 = 1 / 16. * ((8 - 32 * j + 30 * j * j - 8 * j * j * j) * DAjm2 +
                           (-8 + 23 * j - 12 * j * j) * (DAjm2 + alph * D2Ajm2) +
                           (4 - 6 * j) * (2 * alph * D2Ajm2 + alph * alph * D3Ajm2) -
                           (3 * alph * alph * D3Ajm2 + alph * alph * alph * D4Ajm2));

    auto f85 = 1 / 48. * ((-6 + 29 * j - 30 * j * j + 8 * j * j * j) * Ajm3 +
                          (6 - 21 * j + 12 * j * j) * alph * DAjm3 + (-3 + 6 * j) * alph * alph * D2Ajm3 +
                          alph * alph * alph * D3Ajm3);
    auto Df85 = 1 / 48. * ((-6 + 29 * j - 30 * j * j + 8 * j * j * j) * DAjm3 +
                           (6 - 21 * j + 12 * j * j) * (DAjm3 + alph * D2Ajm3) +
                           (-3 + 6 * j) * (2 * alph * D2Ajm3 + alph * alph * D3Ajm3) +
                           (3 * alph * alph * D3Ajm3 + alph * alph * alph * D4Ajm3));

    auto f86 = 0.25 * ((3 - 2 * j) * alph * Bjm1 - alph * alph * DBjm1);
    auto Df86 = 0.25 * ((3 - 2 * j) * (Bjm1 + alph * DBjm1) - (2 * alph * DBjm1 + alph * alph * D2Bjm1));

    auto f87 = 0.25 * (2 * j * alph * Bjm2 + alph * alph * DBjm2);
    auto Df87 = 0.25 * (2 * j * (Bjm2 + alph * DBjm2) + (2 * alph * DBjm2 + alph * alph * D2Bjm2));

    auto f88 = 0.5 * ((-3 + 2 * j) * alph * Bjm1 + alph * alph * DBjm1);
    auto Df88 = 0.5 * ((-3 + 2 * j) * (Bjm1 + alph * DBjm1) + (2 * alph * DBjm1 + alph * alph * D2Bjm1));

    auto f89 = 0.5 * (-2 * j * alph * Bjm2 - alph * alph * DBjm2);
    auto Df89 = 0.5 * (-2 * j * (Bjm2 + alph * DBjm2) - (2 * alph * DBjm2 + alph * alph * D2Bjm2));

    //4th order semi-major axis functions
    auto f90 = 1 / 384. * ((-206 * j + 283 * j * j - 120 * j * j * j + 16 * j * j * j * j) * Aj +
                           (-64 + 236 * j - 168 * j * j + 32 * j * j * j) * alph * DAj +
                           (48 - 78 * j + 24 * j * j) * alph * alph * D2Aj +
                           (-12 + 8 * j) * alph * alph * alph * D3Aj + alph * alph * alph * alph * D4Aj);
    auto Df90 = 1 / 384. * ((-206 * j + 283 * j * j - 120 * j * j * j + 16 * j * j * j * j) * DAj +
                            (-64 + 236 * j - 168 * j * j + 32 * j * j * j) * (DAj + alph * D2Aj) +
                            (48 - 78 * j + 24 * j * j) * (2 * alph * D2Aj + alph * alph * D3Aj) +
                            (-12 + 8 * j) * (3 * alph * alph * D3Aj + alph * alph * alph * D4Aj) +
                            (4 * alph * alph * alph * D4Aj + alph * alph * alph * alph * D5Aj));

    auto f91 = 1 / 96. * ((-64 + 238 * j - 274 * j * j + 116 * j * j * j - 16 * j * j * j * j) * Ajm1 +
                          (64 - 206 * j + 156 * j * j - 32 * j * j * j) * alph * DAjm1 +
                          (-36 + 69 * j - 24 * j * j) * alph * alph * D2Ajm1 +
                          (10 - 8 * j) * alph * alph * alph * D3Ajm1 - alph * alph * alph * alph * D4Ajm1);
    auto Df91 = 1 / 96. * ((-64 + 238 * j - 274 * j * j + 116 * j * j * j - 16 * j * j * j * j) * DAjm1 +
                           (64 - 206 * j + 156 * j * j - 32 * j * j * j) * (DAjm1 + alph * D2Ajm1) +
                           (-36 + 69 * j - 24 * j * j) * (2 * alph * D2Ajm1 + alph * alph * D3Ajm1) +
                           (10 - 8 * j) * (3 * alph * alph * D3Ajm1 + alph * alph * alph * D4Ajm1) -
                           (4 * alph * alph * alph * D4Ajm1 + alph * alph * alph * alph * D5Ajm1));

//
    auto f92 = 1 / 64. * ((52 - 224 * j + 259 * j * j - 112 * j * j * j + 16 * j * j * j * j) * Ajm2 +
                          (-52 + 176 * j - 144 * j * j + 32 * j * j * j) * alph * DAjm2 +
                          (26 - 60 * j + 24 * j * j) * alph * alph * D2Ajm2 +
                          (-8 + 8 * j) * alph * alph * alph * D3Ajm2 + alph * alph * alph * alph * D4Ajm2);
    auto Df92 = 1 / 64. * ((52 - 224 * j + 259 * j * j - 112 * j * j * j + 16 * j * j * j * j) * DAjm2 +
                           (-52 + 176 * j - 144 * j * j + 32 * j * j * j) * (DAjm2 + alph * D2Ajm2) +
                           (26 - 60 * j + 24 * j * j) * (2 * alph * D2Ajm2 + alph * alph * D3Ajm2) +
                           (-8 + 8 * j) * (3 * alph * alph * D3Ajm2 + alph * alph * alph * D4Ajm2) +
                           (4 * alph * alph * alph * D4Ajm2 + alph * alph * alph * alph * D5Ajm2));

    auto f93 = 1 / 96. * ((-36 + 186 * j - 238 * j * j + 108 * j * j * j - 16 * j * j * j * j) * Ajm3 +
                          (36 - 146 * j + 132 * j * j - 32 * j * j * j) * alph * DAjm3 +
                          (-18 + 51 * j - 24 * j * j) * alph * alph * D2Ajm3 +
                          (6 - 8 * j) * alph * alph * alph * D3Ajm3 - alph * alph * alph * alph * D4Ajm3);
    auto Df93 = 1 / 96. * ((-36 + 186 * j - 238 * j * j + 108 * j * j * j - 16 * j * j * j * j) * DAjm3 +
                           (36 - 146 * j + 132 * j * j - 32 * j * j * j) * (DAjm3 + alph * D2Ajm3) +
                           (-18 + 51 * j - 24 * j * j) * (2 * alph * D2Ajm3 + alph * alph * D3Ajm3) +
                           (6 - 8 * j) * (3 * alph * alph * D3Ajm3 + alph * alph * alph * D4Ajm3) -
                           (4 * alph * alph * alph * D4Ajm3 + alph * alph * alph * alph * D5Ajm3));

    auto f94 = 1 / 384. * ((24 - 146 * j + 211 * j * j - 104 * j * j * j + 16 * j * j * j * j) * Ajm4 +
                           (-24 + 116 * j - 120 * j * j + 32 * j * j * j) * alph * DAjm4 +
                           (12 - 42 * j + 24 * j * j) * alph * alph * D2Ajm4 +
                           (-4 + 8 * j) * alph * alph * alph * D3Ajm4 + alph * alph * alph * alph * D4Ajm4);
    auto Df94 = 1 / 384. * ((24 - 146 * j + 211 * j * j - 104 * j * j * j + 16 * j * j * j * j) * DAjm4 +
                            (-24 + 116 * j - 120 * j * j + 32 * j * j * j) * (DAjm4 + alph * D2Ajm4) +
                            (12 - 42 * j + 24 * j * j) * (2 * alph * D2Ajm4 + alph * alph * D3Ajm4) +
                            (-4 + 8 * j) * (3 * alph * alph * D3Ajm4 + alph * alph * alph * D4Ajm4) +
                            (4 * alph * alph * alph * D4Ajm4 + alph * alph * alph * alph * D5Ajm4));

    auto f95 = 1 / 16. * ((16 - 17 * j + 4 * j * j) * alph * Bjm1 + (-8 + 4 * j) * alph * alph * DBjm1 +
                          alph * alph * alph * D2Bjm1);
    auto Df95 = 1 / 16. * ((16 - 17 * j + 4 * j * j) * (Bjm1 + alph * DBjm1) +
                           (-8 + 4 * j) * (2 * alph * DBjm1 + alph * alph * D2Bjm1) +
                           (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f96 = 1 / 8. * ((10 * j - 4 * j * j) * alph * Bjm2 + (4 - 4 * j) * alph * alph * DBjm2 -
                         alph * alph * alph * D2Bjm2);
    auto Df96 = 1 / 8. * ((10 * j - 4 * j * j) * (Bjm2 + alph * DBjm2) +
                          (4 - 4 * j) * (2 * alph * DBjm2 + alph * alph * D2Bjm2) -
                          (3 * alph * alph * D2Bjm2 + alph * alph * alph * D3Bjm2));

    auto f97 = 1 / 16. * ((-3 * j + 4 * j * j) * alph * Bjm3 + 4 * j * alph * alph * DBjm3 +
                          alph * alph * alph * D2Bjm3);
    auto Df97 = 1 / 16. * ((-3 * j + 4 * j * j) * (Bjm3 + alph * DBjm3) +
                           4 * j * (2 * alph * DBjm3 + alph * alph * D2Bjm3) +
                           (3 * alph * alph * D2Bjm3 + alph * alph * alph * D3Bjm3));

    auto f98 = 3. / 8 * alph * alph * Cjm2;
    auto Df98 = 3. / 8 * (2 * alph * Cjm2 + alph * alph * DCjm2);

    auto f99 = 1 / 8. * ((-16 + 17 * j - 4 * j * j) * alph * Bjm1 + (8 - 4 * j) * alph * alph * DBjm1 -
                         alph * alph * alph * D2Bjm1);
    auto Df99 = 1 / 8. * ((-16 + 17 * j - 4 * j * j) * (Bjm1 + alph * DBjm1) +
                          (8 - 4 * j) * (2 * alph * DBjm1 + alph * alph * D2Bjm1) -
                          (3 * alph * alph * D2Bjm1 + alph * alph * alph * D3Bjm1));

    auto f100 = 0.25 * ((-10 * j + 4 * j * j) * alph * Bjm2 + (-4 + 4 * j) * alph * alph * DBjm2 +
                        alph * alph * alph * D2Bjm2);
    auto Df100 = 0.25 * ((-10 * j + 4 * j * j) * (Bjm2 + alph * DBjm2) +
                         (-4 + 4 * j) * (2 * alph * DBjm2 + alph * alph * D2Bjm2) +
                         (3 * alph * alph * D2Bjm2 + alph * alph * alph * D3Bjm2));

    auto f101 = 1 / 8. * ((3 * j - 4 * j * j) * alph * Bjm3 - 4 * j * alph * alph * DBjm3 -
                          alph * alph * alph * D2Bjm3);
    auto Df101 = 1 / 8. * ((3 * j - 4 * j * j) * (Bjm3 + alph * DBjm3) -
                           4 * j * (2 * alph * DBjm3 + alph * alph * D2Bjm3) -
                           (3 * alph * alph * D2Bjm3 + alph * alph * alph * D3Bjm3));

    auto f102 = 1.5 * (-alph * alph) * Cjm2;
    auto Df102 = 1.5 * ((-2 * alph) * Cjm2 + (-alph * alph) * DCjm2);

    auto f103 = 9 / 4. * (alph * alph) * Cjm2;
    auto Df103 = 9 / 4. * (2 * alph * Cjm2 + alph * alph * DCjm2);
    //f91
    VectorXd f(135);
    f << f1, f2, f2, f3, f3, f4, f5, f6, f7, f7, f7, f7, f8, f8, f9, //15
            f10, f11, f12, f13, f13, f14, f15, f15, f16, f16, f17, f18, f19, f20, f21, f22, f23, f24, f25, f18, f19, f20, f26, //23 38; Oth order
            f27, f28, f29, f30, f30, f31, f32, f33, f34, f34, f35, f36, f37, f38, f39, f40, f41, f42, f43, f44, f37, f38, //22; 60 1st order
            f45, f46, f47, f48, f48, f49, f50, f51, f52, f52, f53, f54, f55, f56, f56, f57, f58, f59, f60, f61, f62, f63, f64, f65, f66, f57, f58, f59, f67, f60, //30 90
            f68, f69, f70, f71, f72, f73, f74, f75, f76, f77, f78, f79, f80, f70, f71, f81, //106 2nd order
            f82, f83, f84, f85, f86, f87, f88, f89, f86, f87, //3rd order
            f90, f91, f92, f93, f94, f95, f96, f97, f98, f99, f100, f101, f102, f95, f96, f97, f103, f102, f98; //4th order
    VectorXd Df(135);
    Df << Df1, Df2, Df2, Df3, Df3, Df4, Df5, Df6, Df7, Df7, Df7, Df7, Df8, Df8, Df9,
            Df10, Df11, Df12, Df13, Df13, Df14, Df15, Df15, Df16, Df16, Df17, Df18, Df19, Df20, Df21, Df22, Df23, Df24, Df25, Df18, Df19, Df20, Df26,
            Df27, Df28, Df29, Df30, Df30, Df31, Df32, Df33, Df34, Df34, Df35, Df36, Df37, Df38, Df39, Df40, Df41, Df42, Df43, Df44, Df37, Df38,
            Df45, Df46, Df47, Df48, Df48, Df49, Df50, Df51, Df52, Df52, Df53, Df54, Df55, Df56, Df56, Df57, Df58, Df59, Df60, Df61, Df62, Df63, Df64, Df65, Df66, Df57, Df58, Df59, Df67, Df60,
            Df68, Df69, Df70, Df71, Df72, Df73, Df74, Df75, Df76, Df77, Df78, Df79, Df80, Df70, Df71, Df81,
            Df82, Df83, Df84, Df85, Df86, Df87, Df88, Df89, Df86, Df87,
            Df90, Df91, Df92, Df93, Df94, Df95, Df96, Df97, Df98, Df99, Df100, Df101, Df102, Df95, Df96, Df97, Df103, Df102, Df98; //4th order

    VectorXd fE(135);
    //Formula for fE taken from matlab:
    // Terms.fE = alph*[(j==1)*[-1 0.5 0.5 1 1 1/64  -0.25 1/64 -0.5 -0.5 -0.5 -0.5 0 0 -1] ...
    //                (j==-2)*[-1  0.75 0.75 1 1] (j==-1)*[-2 1 1 1 1] (j==-1)*(-1/64)+(j==-3)*(-81/64) ...
    //                (j==1)*(-1/8) 0  (j==-1)*(-1/8) (j==1)*0.25 0 (j==-2)*(-2) 0 (j==-1)*0.25 ...
    //                (j==1)*(-1/8) 0 (j==-1)*(-1/8) (j==-1)*(-1) ... %up to here - 0th order indirect
    //                (j==-1)*[-0.5 3/8 0.25 0.5 0.5]+(j==1)*[1.5 0 -0.75 -1.5 -1.5] ...
    //                (j==2)*[-2 1 1.5 2 2] (j==-2)*(-0.75) (j==1)*3/16+(j==3)*(-27/16)...
    //                (j==1)*1.5 0 (j==1)*3 (j==-1)*(-1) (j==1)*(-3) (j==2)*(-4) 0 0  (j==1)*1.5 0 ... %up to here - 1st order indirect
    //                (j==-1)*[-3/8 3/8 3/16 3/8 3/8]+(j==1)*[-1/8 -1/24 1/16 1/8 1/8] (j==2)*[3 0 -9/4 -3 -3] ...
    //                (j==1)*[-1/8 1/16 -1/24 1/8 1/8]+(j==3)*[-27/8 27/16 27/8 27/8 27/8]...
    //                (j==1)*[-1 0.5 0.5 0 1] (j==1)*[2 -1 -1 -1 -1] (j==1)*[-1 0.5 0.5 1 0]...
    //                (j==-2)*(-2/3) (j==2)*(0.25)+(j==4)*(-8/3) (j==2)*3 0 ...
    //                (j==1)*(-0.25) (j==-1)*(-0.75) (j==2)*6 0 (j==2)*(-6) 0 ...
    //                (j==3)*(-27/4) (j==1)*(-0.25) 0 (j==2)*3 0 0 ... %up to here - 2nd order indirect
    //                (j==-1)*(-1/3)+(j==1)*(-1/24) (j==2)*(-0.25) (j==1)*(-1/16)+(j==3)*(81/16) ...
    //                (j==2)*(-1/6)+(j==4)*(-16/3) (j==1)*(-0.5) (j==2)*(-2) (j==1)*1 (j==2)*4 (j==1)*(-0.5) (j==2)*(-2) ... %up to here - 3rd order indirect
    //                (j==-1)*(-125/384)+(j==1)*(-3/128) (j==2)*(-1/12) (j==1)*(-3/64)+(j==3)*(-27/64) ...
    //                (j==2)*(-1/12)+(j==4)*(8) (j==3)*(-27/128)+(j==5)*(-3125/384) (j==1)*(-3/8) (j==2)*(-1) (j==3)*(-27/8) 0 ...
    //                (j==1)*(0.75) (j==2)*(2) (j==3)*(27/4) 0 (j==1)*(-3/8) (j==2)*(-1) (j==3)*(-27/8) 0 0 0 ... %up to here - 4th order indirect
    //                ];
    if (j == 1) {
        fE
                << -1, 0.5, 0.5, 1, 1, 0.015625, -0.25, 0.015625, -0.5, -0.5, -0.5, -0.5, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.125, 0, 0, 0.25, 0, 0, 0, 0, -0.125, 0, 0, 0, 1.5, 0, -0.75, -1.5, -1.5, 0, 0, 0, 0, 0, 0, 0.1875, 1.5, 0, 3, 0, -3, 0, 0, 0, 1.5, 0, -0.125, -0.0416667, 0.0625, 0.125, 0.125, 0, 0, 0, 0, 0, -0.125, 0.0625, -0.0416667, 0.125, 0.125, -1, 0.5, 0.5, 0, 1, 2, -1, -1, -1, -1, -1, 0.5, 0.5, 1, 0, 0, 0, 0, 0, -0.25, 0, 0, 0, 0, 0, 0, -0.25, 0, 0, 0, 0, -0.0416667, 0, -0.0625, 0, -0.5, 0, 1, 0, -0.5, 0, -0.0234375, 0, -0.046875, 0, 0, -0.375, 0, 0, 0, 0.75, 0, 0, 0, -0.375, 0, 0, 0, 0, 0;
    } else if (j == 2) {
        fE
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 1, 1.5, 2, 2, 0, 0, 0, 0, 0, 0, 0, -4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, -2.25, -3, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25, 3, 0, 0, 0, 6, 0, -6, 0, 0, 0, 0, 3, 0, 0, 0, -0.25, 0, -0.166667, 0, -2, 0, 4, 0, -2, 0, -0.0833333, 0, -0.0833333, 0, 0, -1, 0, 0, 0, 2, 0, 0, 0, -1, 0, 0, 0, 0;
    } else if (j == 3) {
        fE
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1.6875, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3.375, 1.6875, 3.375, 3.375, 3.375, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -6.75, 0, 0, 0, 0, 0, 0, 0, 5.0625, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.421875, 0, -0.210938, 0, 0, -3.375, 0, 0, 0, 6.75, 0, 0, 0, -3.375, 0, 0, 0;
    } else if (j == 4) {
        fE
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2.66667, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -5.33333, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else if (j == 5) {
        fE
                << -0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, -0, 0, -0, 0, 0, -0, 0, 0, -0, 0, -0, -0, 0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, 0, 0, -0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, 0, -0, -0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, -0, -0, 0, 0, -0, 0, -0, -0, 0, 0, 0, 0, -0, -0, 0, -0, -0, -0, 0, 0, -0, -0, -0, -0, -0, 0, -8.13802, -0, -0, -0, 0, 0, 0, 0, 0, -0, -0, -0, 0, 0, 0;
    } else if (j == -1) {
        fE
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 1, 1, 1, 1, -0.015625, 0, 0, -0.125, 0, 0, 0, 0, 0.25, 0, 0, -0.125, -1, -0.5, 0.375, 0.25, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, -0.375, 0.375, 0.1875, 0.375, 0.375, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.333333, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.325521, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else if (j == -2) {
        fE
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0.75, 0.75, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.666667, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else if (j == -3) {
        fE
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1.26562, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else {
        fE
                << -0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, -0, 0, -0, 0, 0, -0, 0, 0, -0, 0, -0, -0, 0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, 0, 0, -0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, 0, -0, -0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, -0, -0, 0, 0, -0, 0, -0, -0, 0, 0, 0, 0, -0, -0, 0, -0, -0, -0, 0, 0, -0, -0, -0, -0, -0, 0, -0, -0, -0, -0, 0, 0, 0, 0, 0, -0, -0, -0, 0, 0, 0;
    }
    fE = alph * fE.array();

    VectorXd fI(135);
    // Formular for fI from matlab
    //fI = 1/alph^2*[(j==1)*[-1 0.5 0.5 1 1 1/64  -0.25 1/64 -0.5 -0.5 -0.5 -0.5 0 0 -1] ...
//                +(j==-2)*[-1  0.75 0.75 1 1] (j==-1)*[-2 1 1 1 1] (j==-1)*(-1/64)+(j==-3)*(-81/64) ...
//                (j==1)*(-1/8) 0  (j==-1)*(-1/8) (j==1)*0.25 0 (j==-2)*(-2) 0 (j==-1)*0.25 ...
//                (j==1)*(-1/8) 0 (j==-1)*(-1/8) (j==-1)*(-1) ... %up to here - 0th order indirect
//                (j==-1)*[-2 1.5 1 2 2] ...
//                (j==0)*[1.5 -0.75 0 -1.5 -1.5]+(j==2)*[-0.5 0.25 3/8 0.5 0.5] ...
//                (j==0)*3/16+(j==-2)*(-27/16)...
//                (j==3)*(-0.75) 0 (j==0)*1.5 0 (j==-1)*(-4) 0 (j==2)*(-1) (j==0)*3 (j==0)*(-3) 0 (j==0)*1.5 ... %up to here - 1st order indirect
//                (j==-1)*[-27/8 27/8 27/16 27/8 27/8]+(j==1)*[-1/8 -1/24 1/16 1/8 1/8]...
//                (j==2)*[3 -9/4 0 -3 -3] (j==1)*[-1/8 1/16 -1/24 1/8 1/8]+(j==3)*[-3/8 3/16 3/8 3/8 3/8] ...
//                (j==1)*[-1 0.5 0.5 0 1] (j==1)*[2 -1 -1 -1 -1] (j==1)*[-1 0.5 0.5 1 0] ...
//                (j==0)*(0.25)+(j==-2)*(-8/3) (j==4)*(-2/3) 0 (j==0)*3 (j==1)*(-0.25) (j==-1)*(-27/4)...
//                0 (j==0)*6 0 (j==0)*(-6) (j==3)*(-0.75) (j==1)*(-0.25) 0 0 (j==0)*3 0 ... %up to here - 2nd order indirect
//                (j==-1)*(-16/3)+(j==1)*(-1/6) (j==0)*(81/16)+(j==2)*(-1/16) (j==1)*(-0.25) ...
//                (j==2)*(-1/24)+(j==4)*(-1/3) (j==1)*(-2) (j==2)*(-0.5) (j==1)*4 (j==2)*1 (j==1)*(-2) (j==2)*(-0.5) ... %up to here - 3rd order indirect
//                (j==-1)*(-3125/384)+(j==1)*(-27/128) (j==0)*(8)+(j==2)*(-1/12) (j==1)*(-27/64)+(j==3)*(-3/64) ...
//                (j==2)*(-1/12) (j==3)*(-3/128)+(j==5)*(-125/384) (j==1)*(-27/8) (j==2)*(-1) (j==3)*(-3/8) 0 ...
//                (j==1)*(27/4) (j==2)*(2) (j==3)*(0.75) 0 (j==1)*(-27/8) (j==2)*(-1) (j==3)*(-3/8) 0 0 0 ... %up to here - 4th order indirect
//                ];
    if (j == 1) {
        fI
                << -1, 0.5, 0.5, 1, 1, 0.015625, -0.25, 0.015625, -0.5, -0.5, -0.5, -0.5, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.125, 0, 0, 0.25, 0, 0, 0, 0, -0.125, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.125, -0.0416667, 0.0625, 0.125, 0.125, 0, 0, 0, 0, 0, -0.125, 0.0625, -0.0416667, 0.125, 0.125, -1, 0.5, 0.5, 0, 1, 2, -1, -1, -1, -1, -1, 0.5, 0.5, 1, 0, 0, 0, 0, 0, -0.25, 0, 0, 0, 0, 0, 0, -0.25, 0, 0, 0, 0, -0.166667, 0, -0.25, 0, -2, 0, 4, 0, -2, 0, -0.210938, 0, -0.421875, 0, 0, -3.375, 0, 0, 0, 6.75, 0, 0, 0, -3.375, 0, 0, 0, 0, 0;
    } else if (j == 2) {
        fI
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5, 0.25, 0.375, 0.5, 0.5, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, -2.25, 0, -3, -3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.0625, 0, -0.0416667, 0, -0.5, 0, 1, 0, -0.5, 0, -0.0833333, 0, -0.0833333, 0, 0, -1, 0, 0, 0, 2, 0, 0, 0, -1, 0, 0, 0, 0;
    } else if (j == 3) {
        fI
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.375, 0.1875, 0.375, 0.375, 0.375, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.046875, 0, -0.0234375, 0, 0, -0.375, 0, 0, 0, 0.75, 0, 0, 0, -0.375, 0, 0, 0;
    } else if (j == 4) {
        fI
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.666667, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.333333, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else if (j == 5) {
        fI
                << 0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, -0, 0, -0, 0, 0, -0, 0, 0, -0, 0, -0, -0, -0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0, 0, 0, 0, -0, 0, -0, 0, -0, 0, 0, -0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, 0, -0, -0, -0, -0, -0, 0, 0, 0, 0, 0, -0, 0, 0, -0, -0, 0, 0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, -0, 0, -0, -0, -0.325521, -0, -0, -0, 0, 0, 0, 0, 0, -0, -0, -0, 0, 0, 0;
    } else if (j == 0) {
        fI
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.5, -0.75, 0, -1.5, -1.5, 0.1875, 0, 0, 1.5, 0, 0, 0, 0, 3, -3, 0, 1.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25, 0, 0, 3, 0, 0, 0, 6, 0, -6, 0, 0, 0, 0, 3, 0, 0, 5.0625, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else if (j == -1) {
        fI
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 1, 1, 1, 1, -0.015625, 0, 0, -0.125, 0, 0, 0, 0, 0.25, 0, 0, -0.125, -1, -2, 1.5, 1, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -4, 0, 0, 0, 0, 0, 0, -3.375, 3.375, 1.6875, 3.375, 3.375, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -6.75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -5.33333, 0, 0, 0, 0, 0, 0, 0, 0, 0, -8.13802, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else if (j == -2) {
        fI
                << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0.75, 0.75, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1.6875, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2.66667, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    } else if (j == -3) {
        fI
                << -0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -1.26562, -0, 0, -0, 0, 0, -0, 0, 0, -0, 0, -0, -0, -0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0, 0, 0, 0, -0, 0, -0, 0, -0, 0, 0, -0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, 0, -0, -0, -0, -0, -0, 0, 0, 0, 0, 0, -0, 0, 0, -0, -0, 0, 0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, -0, 0, -0, -0, -0, -0, -0, -0, 0, 0, 0, 0, 0, -0, -0, -0, 0, 0, 0;
    } else {
        fI
                << 0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, -0, -0, 0, -0, 0, 0, -0, 0, 0, -0, 0, -0, -0, -0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0, 0, 0, 0, -0, 0, -0, 0, -0, 0, 0, -0, 0, 0, 0, 0, 0, -0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, 0, 0, 0, 0, -0, -0, -0, -0, -0, 0, 0, 0, 0, 0, -0, 0, 0, -0, -0, 0, 0, 0, -0, -0, -0, 0, 0, 0, 0, -0, 0, -0, -0, -0, -0, 0, 0, -0, -0, -0, 0, -0, -0, -0, -0, -0, -0, 0, 0, 0, 0, 0, -0, -0, -0, 0, 0, 0;
    }
    fI = 1 / (alph * alph) * fI.array();
    return std::make_tuple(f, Df, fE, fI, CoeffsVec);
}

/*
 * std::tuple<double, double, double, double> VariationsPair(LaplaceCoeff &lc, double n1, double n2, double alph, double Lambda1, double Lambda2, double mu1, double mu2, double j, double z1, double z2, double e1, double e2, double pom1, double pom2, double u1, double u2, double s1, double s2, double Om1, double Om2)
{
    //This function is the algebraic "heart" of the code, as it calculates the
    //integrals of the rhs of Lagrange's planetary equations to yield variations
    //in the eccentricities and mean longitudes.

    //the following two flags, for adding additional terms, remain here for
    //debugging and backward compatibility purposes, though for operational usage they will remain hard-coded as 1.
    AddSecondOrderIncTerms = 1;
    AddThirdOrder = 1;

    //calculate laplace coefficients
    auto Aj = lc.calculate(alph,0.5,j,0);
    auto DAj = lc.calculate(alph,0.5,j,1);
    auto D2Aj = lc.calculate(alph,0.5,j,2);
    auto D3Aj = lc.calculate(alph,0.5,j,3);

    auto Ajp1 = lc.calculate(alph,0.5,j+1,0);
    auto DAjp1 = lc.calculate(alph,0.5,j+1,1);
    auto D2Ajp1 = lc.calculate(alph,0.5,j+1,2);
    auto D3Ajp1 = lc.calculate(alph,0.5,j+1,3);

    auto Ajm1 = lc.calculate(alph,0.5,j-1,0);
    auto DAjm1 = lc.calculate(alph,0.5,j-1,1);
    auto D2Ajm1 = lc.calculate(alph,0.5,j-1,2);
    auto D3Ajm1 = lc.calculate(alph,0.5,j-1,3);

    auto Ajm2 = lc.calculate(alph,0.5,j-2,0);
    auto DAjm2 = lc.calculate(alph,0.5,j-2,1);
    auto D2Ajm2 = lc.calculate(alph,0.5,j-2,2);
    auto D3Ajm2 = lc.calculate(alph,0.5,j-2,3);

    auto f1 = 0.5*Aj;
    auto f2 = -0.5*j*j*Aj+0.25*alph*DAj+1/8.*alph*alph*D2Aj;
    auto f10 = 0.25*(2+6*j+4*j*j)*Ajp1-0.5*alph*DAjp1-0.25*alph*alph*D2Ajp1;

    auto f27 = -j*Aj-0.5*alph*DAj;
    auto f31 = (j-0.5)*Ajm1+0.5*alph*DAjm1;

    auto f45 = 1/8.*(-5*j+4*j*j)*Aj+0.5*j*alph*DAj+1/8.*alph*alph*D2Aj;
    auto f49 = 0.25*(-2+6*j-4*j*j)*Ajm1+0.25*alph*(2-4*j)*DAjm1-0.25*alph*alph*D2Ajm1;
    auto f53 = 1/8.*(2-7*j+4*j*j)*Ajm2+1/8.*(-2*alph+4*j*alph)*DAjm2+1/8.*alph*alph*D2Ajm2;

    auto Df1 = 0.5*DAj;
    auto Df2 = -0.5*j*j*DAj+0.25*DAj+0.25*alph*D2Aj+1/4.*alph*D2Aj+1/8.*alph*alph*D3Aj;
    auto Df10 = 0.25*(2+6*j+4*j*j)*DAjp1-0.5*DAjp1-0.5*alph*D2Ajp1-0.5*alph*D2Ajp1-0.25*alph*alph*D3Ajp1;

    auto Df27 = -j*DAj-0.5*DAj-0.5*alph*D2Aj;
    auto Df31 = (j-0.5)*DAjm1+0.5*DAjm1+0.5*alph*D2Ajm1;

    auto Df45 = 1/8.*(-5*j+4*j*j)*DAj+0.5*j*DAj+0.5*j*alph*D2Aj+1/4.*alph*D2Aj+1/8.*alph*alph*D3Aj;
    auto Df49 = 0.25*(-2+6*j-4*j*j)*DAjm1+0.25*(2-4*j)*DAjm1+0.25*alph*(2-4*j)*D2Ajm1-0.5*alph*D2Ajm1-0.25*alph*alph*D3Ajm1;
    auto Df53 = 1/8.*(2-7*j+4*j*j)*DAjm2+1/8.*(-2+4*j)*DAjm2+1/8.*(-2*alph+4*j*alph)*D2Ajm2+1/4.*alph*D2Ajm2+1/8.*alph*alph*D3Ajm2;

    //second order inclination-related and third-order inclination related terms
    auto Bjm1 = lc.calculate(alph,1.5,j-1,0);
    auto DBjm1 = lc.calculate(alph,1.5,j-1,1);
    auto f57 = 0.5*alph*Bjm1;
    auto Df57 = 0.5*Bjm1+0.5*alph*DBjm1;
    auto f62 = -2*f57;
    auto Df62 = -2*Df57;

    //define relative angle (as used at Hadden et al. 2019)
    auto Psi = j*Lambda2-j*Lambda1;

    //define resonant angles
    auto Lambda_j1 = j*Lambda2+(1-j)*Lambda1;if (input["Lambda"].type()==json::value_t:: )

    auto Lambda_j2 = j*Lambda2+(2-j)*Lambda1;
    auto Lambda_j3 = j*Lambda2+(3-j)*Lambda1;

    //define "super mean motions"
    auto nj1 = j*n2+(1-j)*n1;
    auto nj2 = j*n2+(2-j)*n1;
    auto nj3 = j*n2+(3-j)*n1;

    //da/a term - inner planet
    double Order0Term;
    if (j!=0)
    {
        Order0Term = ((f1-alph*(j==1)+(f2+0.5*alph*(j==1))*(e1*e1+e2*e2))*sin(Psi)+(f10-alph*(j==-2))*e1*e2*sin(Psi+pom2-pom1))/(-j)/((n2-n1)*(n2-n1)); //hier noch ggf auf arrays achten
    }
    else
    {
        Order0Term = 0;
    }
*/
//function [dLambda1_j,dz1_j,dLambda2_j,dz2_j] = VariationsPair(n1,n2,alph,Lambda1,Lambda2,mu1,mu2,j,z1,z2,e1,e2,pom1,pom2,u1,u2,s1,s2,Om1,Om2)
//

//
//if j~=0
//    Order0Term = ((f1-alph*(j==1)+(f2+0.5*alph*(j==1))*(e1.^2+e2.^2)).*sin(Psi)...
//        +(f10-alph*(j==-2))*e1.*e2.*sin(Psi+pom2-pom1))/(-j)/((n2-n1).^2);
//else
//    Order0Term = 0;
//end
//
//Order1Term = (...
//    (f27+3/2*alph*(j==1)-0.5*alph*(j==-1)).*e1.*sin(Lambda_j1-pom1)+...
//    (f31-2*alph*(j==2)).*e2.*sin(Lambda_j1-pom2)...
//    )...
//    *(1-j)/((nj1).^2);
//
//Order2Term = (...
//    (f45-3/8*alph*(j==-1)-1/8*alph*(j==1))*e1.^2.*sin(Lambda_j2-2*pom1)+...
//    (f49+3*alph*(j==2)).*e1.*e2.*sin(Lambda_j2-pom1-pom2)...
//    +(f53-1/8*alph*(j==1)-27/8*alph*(j==3))*e2.^2.*sin(Lambda_j2-2*pom2)...
//    )...
//    .*(2-j)/((nj2).^2);
//
//Int_da_over_a_term = -3*alph*n1^2*(mu2/(1+mu1))*(Order0Term+Order1Term+Order2Term);
//
//%dR/dalpha terms - inner planet
//if j~=0
//    Order0Term = (...
//        (Df1-(j==1)+(Df2+0.5*(j==1))*(e1.^2+e2.^2)).*sin(Psi)...
//        +(Df10-(j==-2))*e1.*e2.*sin(Psi+pom2-pom1))/j./(n2-n1);
//else
//    Order0Term = 0;
//end
//
//Order1Term = (...
//    (Df27+3/2*(j==1)-0.5*(j==-1)).*e1.*sin(Lambda_j1-pom1)...
//    +(Df31-2*(j==2)).*e2.*sin(Lambda_j1-pom2)...
//    )...
//    ./(nj1);
//
//Order2Term = (...
//    (Df45-3/8*(j==-1)-1/8*(j==1)).*e1.^2.*sin(Lambda_j2-2*pom1)...
//    +(Df49+3*(j==2)).*e1.*e2.*sin(Lambda_j2-pom1-pom2)...
//    +(Df53-1/8*(j==1)-27/8*(j==3)).*e2.^2.*sin(Lambda_j2-2*pom2)...
//    )...
//    ./(nj2);
//
//Int_dR_dalph_term = -2*alph^2*n1*(mu2/(1+mu1))*(Order0Term+Order1Term+Order2Term);
//
//%e-derivative term - inner planet
//if j~=0
//    Order0Term = (...
//        (2*f2+alph*(j==1))*e1.*sin(Psi)+...
//        (f10-alph*(j==-2))*e2.*sin(Psi-pom1+pom2)...
//        )...
//        ./(j*(n2-n1));
//else
//    Order0Term = 0;
//end
//
//Order1Term =  (f27+3/2*alph*(j==1)-0.5*alph*(j==-1))*sin(Lambda_j1-pom1)./(nj1);
//
//Order2Term = (...
//    2*(f45-3/8*alph*(j==-1)-1/8*alph*(j==1))*e1.*sin(Lambda_j2-2*pom1)+...
//    (f49+3*alph*(j==2))*e2.*sin(Lambda_j2-pom1-pom2)...
//    )...
//    ./(nj2);
//
//e_term = alph/2.*e1.*n1*(mu2/(1+mu1)).*(Order0Term+Order1Term+Order2Term);
//
//
//dLambda1_j = Int_da_over_a_term+Int_dR_dalph_term+e_term;
//
//if AddSecondOrderIncTerms
//    %inclination terms of the inner planet
//    dl1Order2IncTerm1 = 3*mu2/(1+mu1)*alph*n1^2/(nj2)^2*(j-2)*(f57*s1.^2.*sin(Lambda_j2-2*Om1)+f62*s1.*s2.*sin(Lambda_j2-Om1-Om2)+f57*s2.^2.*sin(Lambda_j2-2*Om2));
//    dl1Order2IncTerm2 = -2*mu2/(1+mu1)*alph^2*n1/(nj2)*(Df57*s1.^2.*sin(Lambda_j2-2*Om1)+Df62*s1.*s2.*sin(Lambda_j2-Om1-Om2)+Df57*s2.^2.*sin(Lambda_j2-2*Om2));
//    dl1Order2IncTerm3 = mu2/(1+mu1)*alph*n1/nj2*(2*f57*s1.^2.*sin(Lambda_j2-2*Om1)+f62*s1.*s2.*sin(Lambda_j2-Om1-Om2));
//    dLambda1_j = dLambda1_j+dl1Order2IncTerm1+dl1Order2IncTerm2+dl1Order2IncTerm3;
//end
//
//%z variations - inner planet
//if j~=0
//    Order0Term = (...
//        ((2*f2+alph*(j==1)).*sin(Psi)+1i*j*(0.5*f1-0.5*alph*(j==1))*(-1)*cos(Psi)).*z1...
//        +(f10-alph*(j==-2))*exp(1i*(Psi)).*z2/1i...
//        )...
//        ./(j*(n2-n1));
//else
//    Order0Term = 0;
//end
//
//Order1Term =  (...
//    (f27+3/2*alph*(j==1)-0.5*alph*(j==-1))...
//    *(...
//    (1-0.5*e1.^2).*exp(1i*(Lambda_j1))./(1i)...
//    +0.5i*(j-1).*e1.^2.*exp(1i*pom1).*(-1).*cos(Lambda_j1-pom1)...
//    )...
//    +...
//    0.5i*(f31-2*alph*(j==2))*(j-1)*e1.*e2.*exp(1i*pom1).*(-1).*cos(Lambda_j1-pom2)...
//    )...
//    ./(nj1);
//
//Order2Term = (...
//    2*(f45-3/8*alph*(j==-1)-1/8*alph*(j==1)).*exp(1i*(Lambda_j2))/1i.*conj(z1)+...
//    (f49+3*alph*(j==2)).*exp(1i*(Lambda_j2))/1i.*conj(z2)...
//    )...
//    ./(nj2);
//
//
//dz1_j = 1i*n1*alph*mu2/(1+mu1).*(Order0Term+Order1Term+Order2Term);
//
//
//%da'/a' term - outer planet
//if j~=0
//    Order0Term = (...
//        -(f1-1/alph^2*(j==1)+(f2+0.5/alph^2*(j==1))*(e1.^2+e2.^2)).*sin(Psi)...
//        -(f10-1/alph^2*(j==-2))*e1.*e2.*sin(Psi+pom2-pom1)...
//        )...
//        /(-j)/((n2-n1).^2);
//else
//    Order0Term = 0;
//end
//
//Order1Term = (...
//    -(f27-2/alph^2*(j==-1))*e1.*sin(Lambda_j1-pom1)...
//    -(f31+3/2/alph^2*(j==0)-1/2/alph^2*(j==2)).*e2.*sin(Lambda_j1-pom2)...
//    )...
//    *(-j)/((nj1).^2);
//
//Order2Term = (...
//    -(f45-27/8/alph^2*(j==-1)-1/8/alph^2*(j==1))*e1.^2.*sin(Lambda_j2-2*pom1)...
//    -(f49+3/alph^2*(j==2)).*e1.*e2.*sin(Lambda_j2-pom1-pom2)...
//    -(f53-1/8/alph^2*(j==1)-3/8/alph^2*(j==3))*e2.^2.*sin(Lambda_j2-2*pom2)...
//    )...
//    .*(-j)./((nj2).^2);
//
//Int_da_over_a_term = -3*n2^2*(mu1/(1+mu2)).*(Order0Term+Order1Term+Order2Term);
//
//%dR'/dalpha terms - outer planet - corrected
//if j~=0
//    Order0Term = (...
//        (f1+alph*Df1+(j==1)/alph^2+(f2+alph*Df2-(j==1)/(2*alph^2)).*(e1.^2+e2.^2)).*sin(Psi)...
//        +(f10+alph*Df10+(j==-2)/alph^2).*e1.*e2.*sin(Psi+pom2-pom1)...
//        )...
//        /j./(n2-n1);
//else
//    Order0Term = 0;
//end
//
//Order1Term = (...
//    (f27+alph*Df27+2/(alph^2)*(j==-1)).*e1.*sin(Lambda_j1-pom1)...
//    +(f31+alph*Df31-3/alph^2*(j==0)+1/(2*alph^2)*(j==2)).*e2.*sin(Lambda_j1-pom2)...
//    )...
//    ./(nj1);
//
//Order2Term = (...
//    (f45+alph*Df45+27/8/alph^2*(j==-1)+1/8/alph^2*(j==1)).*e1.^2.*sin(Lambda_j2-2*pom1)...
//    +(f49+alph*Df49-3/alph^2*(j==2)).*e1.*e2.*sin(Lambda_j2-pom1-pom2)...
//    +(f53+alph*Df53+1/8/alph^2*(j==1)+3/8/alph^2*(j==3)).*e2.^2.*sin(Lambda_j2-2*pom2)...
//    )...
//    ./(nj2);
//
//
//Int_dR_dalph_term = +2*n2*(mu1/(1+mu2))*(Order0Term+Order1Term+Order2Term);
//
//%e'-derivative term - outer planet
//if j~=0
//    Order0Term = (...
//        (2*f2+1/alph^2*(j==1))*e2.*sin(Psi)+...
//        (f10-1/alph^2*(j==-2))*e1.*sin(Psi+pom2-pom1)...
//        )...
//        ./(j*(n2-n1));
//else
//    Order0Term = 0;
//end
//
//Order1Term =  (f31+3/2/alph^2*(j==0)-1/2/alph^2*(j==2))*sin(Lambda_j1-pom2)...
//    ./(nj1);
//
//Order2Term = (...
//    (f49+3/alph^2*(j==2))*e1.*sin(Lambda_j2-pom1-pom2)+...
//    2*(f53-1/8/alph^2*(j==1)-3/8/alph^2*(j==3))*e2.*sin(Lambda_j2-2*pom2)...
//    )...
//    ./(nj2);
//
//e_term = e2/2*n2*(mu1/(1+mu2)).*(Order0Term+Order1Term+Order2Term);
//
//dLambda2_j = (Int_da_over_a_term+Int_dR_dalph_term+e_term);
//
//
//if AddSecondOrderIncTerms
//    %inclination terms of the outer planet
//    dl2Order2IncTerm1 = -3*mu1/(1+mu2)*n2^2/(nj2)^2*(j)*(f57*s1.^2.*sin(Lambda_j2-2*Om1)+f62*s1.*s2.*sin(Lambda_j2-Om1-Om2)+f57*s2.^2.*sin(Lambda_j2-2*Om2));
//    dl2Order2IncTerm2 = +2*mu1/(1+mu2)*n2/(nj2)*((f57+alph*Df57)*s1.^2.*sin(Lambda_j2-2*Om1)+(f62+alph*Df62)*s1.*s2.*sin(Lambda_j2-Om1-Om2)+(f57+alph*Df57)*s2.^2.*sin(Lambda_j2-2*Om2));
//    dl2Order2IncTerm3 = mu1/(1+mu2)*n2/nj2*(2*f57*s2.^2.*sin(Lambda_j2-2*Om2)+f62*s1.*s2.*sin(Lambda_j2-Om1-Om2));
//    dLambda2_j = dLambda2_j+dl2Order2IncTerm1+dl2Order2IncTerm2+dl2Order2IncTerm3;
//end
//
//%z' variations - outer planet
//if j~=0
//    Order0Term = (...
//        ((2*f2+1/alph^2*(j==1)).*sin(Psi)+1i*j*(-0.5*f1+0.5/alph^2*(j==1))*(-1)*cos(Psi)).*z2...
//        +(f10-1/alph^2*(j==-2))*exp(-1i*j*(Lambda2-Lambda1))/(-1i).*z1...
//        )...
//        ./(j*(n2-n1));
//else
//    Order0Term = 0;
//end
//
//Order1Term =  (...
//    (f31+3/2/alph^2*(j==0)-0.5/alph^2*(j==2))*(1-0.5*e2.^2).*exp(1i*(Lambda_j1))./(1i)...
//    -0.5i*j*(f27-1/alph^2*(j==-1)).*e1.*e2.*exp(1i*pom2).*(-1).*cos(Lambda_j1-pom1)...
//    -0.5i*j*(f31+3/2/alph^2*(j==0)-1/2/alph^2*(j==2))*e2.^2.*exp(1i*pom2).*(-1).*cos(Lambda_j1-pom2)...
//    )...
//    ./(nj1);
//
//Order2Term = (...
//    (f49+3/alph^2*(j==2))*exp(1i*(Lambda_j2))/1i.*conj(z1)+...
//    2*(f53-1/8/alph^2*(j==1)-3/8/alph^2*(j==3)).*exp(1i*(Lambda_j2))/1i.*conj(z2)...
//    )...
//    ./(nj2);
//
//
//dz2_j = 1i*n2*mu1/(1+mu2)*(Order0Term+Order1Term+Order2Term);
//
//
//
//
//if AddThirdOrder
//
//    %calculate Laplace Coefficients required for 3rd order resonant terms
//    Ajm3 = LaplaceCoeffs(alph,0.5,j-3,0);
//    DAjm3 = LaplaceCoeffs(alph,0.5,j-3,1);
//    D2Ajm3 = LaplaceCoeffs(alph,0.5,j-3,2);
//    D3Ajm3 = LaplaceCoeffs(alph,0.5,j-3,3);
//
//    D4Aj = LaplaceCoeffs(alph,0.5,j,4);
//    D4Ajm1 = LaplaceCoeffs(alph,0.5,j-1,4);
//    D4Ajm2 = LaplaceCoeffs(alph,0.5,j-2,4);
//    D4Ajm3 = LaplaceCoeffs(alph,0.5,j-3,4);
//
//    %calculate the coefficients from Murray&Dermott appendix B related to
//    %3rd order resonant terms
//    f82 = 1/48*(-26*j*Aj+30*j^2*Aj-8*j^3*Aj-9*alph*DAj+27*j*alph*DAj-12*j^2*alph*DAj+6*alph^2*D2Aj-6*j*alph^2*D2Aj-alph^3*D3Aj);
//    f83 = 1/16*(-9*Ajm1+31*j*Ajm1-30*j^2*Ajm1+8*j^3*Ajm1+9*alph*DAjm1-25*j*alph*DAjm1+12*j^2*alph*DAjm1-5*alph^2*D2Ajm1+6*j*alph^2*D2Ajm1+alph^3*D3Ajm1);
//    f84 = 1/16*(8*Ajm2-32*j*Ajm2+30*j^2*Ajm2-8*j^3*Ajm2-8*alph*DAjm2+23*j*alph*DAjm2-12*j^2*alph*DAjm2+4*alph^2*D2Ajm2-6*j*alph^2*D2Ajm2-alph^3*D3Ajm2);
//    f85 = 1/48*(-6*Ajm3+29*j*Ajm3-30*j^2*Ajm3+8*j^3*Ajm3+6*alph*DAjm3-21*j*alph*DAjm3+12*j^2*alph*DAjm3-3*alph^2*D2Ajm3+6*j*alph^2*D2Ajm3+alph^3*D3Ajm3);
//
//    Df82 = 1/48*(-26*j*DAj+30*j^2*DAj-8*j^3*DAj-9*DAj-9*alph*D2Aj+27*j*DAj+27*j*alph*D2Aj-12*j^2*DAj-12*j^2*alph*D2Aj+12*alph*D2Aj+6*alph^2*D3Aj-12*j*alph*D2Aj-6*j*alph^2*D3Aj-3*alph^2*D3Aj-alph^3*D4Aj);
//    Df83 = 1/16*(-9*DAjm1+31*j*DAjm1-30*j^2*DAjm1+8*j^3*DAjm1+9*DAjm1+9*alph*D2Ajm1-25*j*DAjm1-25*j*alph*D2Ajm1+12*j^2*DAjm1+12*j^2*alph*D2Ajm1-10*alph*D2Ajm1-5*alph^2*D3Ajm1+12*j*alph*D2Ajm1+6*j*alph^2*D3Ajm1+3*alph^2*D3Ajm1+alph^3*D4Ajm1);
//    Df84 = 1/16*(8*DAjm2-32*j*DAjm2+30*j^2*DAjm2-8*j^3*DAjm2-8*DAjm2-8*alph*D2Ajm2+23*j*DAjm2+23*j*alph*D2Ajm2-12*j^2*DAjm2-12*j^2*alph*D2Ajm2+8*alph*D2Ajm2+4*alph^2*D3Ajm2-12*j*alph*D2Ajm2-6*j*alph^2*D3Ajm2-3*alph^2*D3Ajm2-alph^3*D4Ajm2);
//    Df85 = 1/48*(-6*DAjm3+29*j*DAjm3-30*j^2*DAjm3+8*j^3*DAjm3+6*DAjm3+6*alph*D2Ajm3-21*j*DAjm3-21*j*alph*D2Ajm3+12*j^2*DAjm3+12*j^2*alph*D2Ajm3-6*alph*D2Ajm3-3*alph^2*D3Ajm3+12*j*alph*D2Ajm3+6*j*alph^2*D3Ajm3+3*alph^2*D3Ajm3+alph^3*D4Ajm3);
//
//
//    D2Bjm1 = LaplaceCoeffs(alph,1.5,j-1,2);
//    Bjm2 = LaplaceCoeffs(alph,1.5,j-2,0);
//    DBjm2 = LaplaceCoeffs(alph,1.5,j-2,1);
//    D2Bjm2 = LaplaceCoeffs(alph,1.5,j-2,2);
//
//
//    f86 = 0.75*alph*Bjm1-0.5*j*alph*Bjm1-0.25*alph^2*DBjm1;
//    f87 = 0.5*j*alph*Bjm2+0.25*alph^2*DBjm2;
//    f88 = -2*f86;
//    f89 = -2*f87;
//    Df86 = 0.75*Bjm1+0.75*alph*DBjm1  -0.5*j*Bjm1-0.5*j*alph*DBjm1   -0.5*alph*DBjm1-0.25*alph^2*D2Bjm1;
//    Df87 = 0.5*j*Bjm2+0.5*j*alph*DBjm2+   0.5*alph*DBjm2+0.25*alph^2*D2Bjm2;
//    Df88 = -2*Df86;
//    Df89 = -2*Df87;
//
//    %calculate the 3rd order resonant terms contribution to the forced eccentricity
//    dz1Order3Term1 = mu2/(1+mu1)*alph*n1/(nj3)*exp(1i*pom1).*(3*f82*e1.^2.*exp(1i*(Lambda_j3-3*pom1))          +2*f83.*e1.*e2.*exp(1i*(Lambda_j3-2*pom1-pom2))+1*f84*e2.^2.*exp(1i*(Lambda_j3-pom1-2*pom2)));
//    dz2Order3Term1 = mu1/(1+mu2)        *n2/(nj3)*exp(1i*pom2).*(1*f83*e1.^2.*exp(1i*(Lambda_j3-2*pom1-pom2))+2*f84*e1.*e2.*exp(1i*(Lambda_j3-pom1-2*pom2))+3*(f85-(j==4)/(3*alph^2))*e2.^2.*exp(1i*(Lambda_j3-3*pom2)));
//
//    %add the contribution of the terms involving both e and I (or s in
//    %Murray&Dermott formulae) - these with the coefficients f86-f89
//    dz1Order3Term2 = mu2/(1+mu1)*alph*n1/(nj3).*(f86*s1.^2.*exp(1i*(Lambda_j3-2*Om1))+f88*s1.*s2.*exp(1i*(Lambda_j3-Om1-Om2))+f86*s2.^2.*exp(1i*(Lambda_j3-2*Om2)));
//    dz2Order3Term2 = mu1/(1+mu2)        *n2/(nj3).*(f87*s1.^2.*exp(1i*(Lambda_j3-2*Om1))+f89*s1.*s2.*exp(1i*(Lambda_j3-Om1-Om2))+f87*s2.^2.*exp(1i*(Lambda_j3-2*Om2)));
//
//    %calculate the 3rd order resonant terms contribution to the forced
//    %mean longitude
//
//    %dn/n contribution
//    dl1Order3Term1 = 3*(j-3)*mu2/(1+mu1)*alph*n1^2/(nj3)^2*(f82*e1.^3.*sin(Lambda_j3-3*pom1)+f83*e1.^2.*e2.*sin(Lambda_j3-2*pom1-pom2)+f84*e1.*e2.^2.*sin(Lambda_j3-pom1-2*pom2)+(f85-(j==4)*16/3*alph)*e2.^3.*sin(Lambda_j3-3*pom2));
//    dl2Order3Term1 = 3*(-j)*mu1/(1+mu2)*n2^2/(nj3)^2*(f82*e1.^3.*sin(Lambda_j3-3*pom1)+f83*e1.^2.*e2.*sin(Lambda_j3-2*pom1-pom2)+f84*e1.*e2.^2.*sin(Lambda_j3-pom1-2*pom2)+(f85-(j==4)/(3*alph^2))*e2.^3.*sin(Lambda_j3-3*pom2));
//
//    %dR/da and dR/da' contribution
//    dl1Order3Term2 = -2*mu2/(1+mu1)*alph^2*n1/(nj3)*(Df82*e1.^3.*sin(Lambda_j3-3*pom1)+Df83*e1.^2.*e2.*sin(Lambda_j3-2*pom1-pom2)+Df84*e1.*e2.^2.*sin(Lambda_j3-pom1-2*pom2)+(Df85-16/3*(j==4))*e2.^3.*sin(Lambda_j3-3*pom2));
//    dl2Order3Term2 = 2*mu1/(1+mu2)*nDisturbingFunctionMultTermVariations2/(nj3)*((f82+alph*Df82)*e1.^3.*sin(Lambda_j3-3*pom1)+(f83+alph*Df83)*e1.^2.*e2.*sin(Lambda_j3-2*pom1-pom2)+(f84+alph*Df84)*e1.*e2.^2.*sin(Lambda_j3-pom1-2*pom2)+(f85+alph*Df85+2/(3*alph^3)*(j==4))*e2.^3.*sin(Lambda_j3-3*pom2));
//
//    %add the contribution of the terms involving both e and I (or s in
//    %Murray&Dermott formulae) - these with the coefficients f86-f89
//
//    %dn/n contribution
//    dl1Order3Term3 = 3*(j-3)*mu2/(1+mu1)*alph*n1^2/(nj3)^2*(f86*e1.*s1.^2.*sin(Lambda_j3-pom1-2*Om1) +f87*e2.*s1.^2.*sin(Lambda_j3-pom2-2*Om1) +f88*e1.*s1.*s2.*sin(Lambda_j3-pom1-Om1-Om2) +f89*e2.*s1.*s2.*sin(Lambda_j3-pom2-Om1-Om2) + f86*e1.*s2.^2.*sin(Lambda_j3-pom1-2*Om2) +f87*e2.*s2.^2.*sin(Lambda_j3-pom2-2*Om2));
//    dl2Order3Term3 = 3*(-j)  *mu1/(1+mu2)*n2^2      ./(nj3)^2*(f86*e1.*s1.^2.*sin(Lambda_j3-pom1-2*Om1) +f87*e2.*s1.^2.*sin(Lambda_j3-pom2-2*Om1) +f88*e1.*s1.*s2.*sin(Lambda_j3-pom1-Om1-Om2) +f89*e2.*s1.*s2.*sin(Lambda_j3-pom2-Om1-Om2) + f86*e1.*s2.^2.*sin(Lambda_j3-pom1-2*Om2) +f87*e2.*s2.^2.*sin(Lambda_j3-pom2-2*Om2));
//
//    %dR/da and dR/da' contribution
//    dl1Order3Term4 = -2*mu2/(1+mu1)*alph^2*n1/(nj3)* (Df86*e1.*s1.^2.*sin(Lambda_j3-pom1-2*Om1) + Df87*e2.*s1.^2.*sin(Lambda_j3-pom2-2*Om1) +Df88*e1.*s1.*s2.*sin(Lambda_j3-pom1-Om1-Om2) +Df89*e2.*s1.*s2.*sin(Lambda_j3-pom2-Om1-Om2) + Df86*e1.*s2.^2.*sin(Lambda_j3-pom1-2*Om2) + Df87*e2.*s2.^2.*sin(Lambda_j3-pom2-2*Om2));
//    dl2Order3Term4 = 2*mu1/(1+mu2)*n2/(nj3)*              ((f86+alph*Df86)*e1.*s1.^2.*sin(Lambda_j3-pom1-2*Om1) + (f87+alph*Df87)*e2.*s1.^2.*sin(Lambda_j3-pom2-2*Om1) +(f88+alph*Df88)*e1.*s1.*s2.*sin(Lambda_j3-pom1-Om1-Om2) +(f89+alph*Df89)*e2.*s1.*s2.*sin(Lambda_j3-pom2-Om1-Om2) + (f86+alph*Df86)*e1.*s2.^2.*sin(Lambda_j3-pom1-2*Om2) + (f87+alph*Df87)*e2.*s2.^2.*sin(Lambda_j3-pom2-2*Om2));
//
//    %sum all the contributions to delta-lambda
//    dl1Order3 = dl1Order3Term1+dl1Order3Term2+dl1Order3Term3+dl1Order3Term4;
//    dl2Order3 = dl2Order3Term1+dl2Order3Term2+dl2Order3Term3+dl2Order3Term4;
//
//    %sum all the contributions to delta-z
//    dz1Order3 = dz1Order3Term1+dz1Order3Term2;
//    dz2Order3 = dz2Order3Term1+dz2Order3Term2;
//
//    %add the 3rd order contribution to the total forced eccentricities and
//    %mean longitudes
//    dLambda1_j = dLambda1_j+dl1Order3;
//    dz1_j = dz1_j+dz1Order3;
//    dLambda2_j = dLambda2_j+dl2Order3;
//    dz2_j = dz2_j+dz2Order3;
//
//end