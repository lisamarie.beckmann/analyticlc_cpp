#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include "FluxCalculation/FluxCalculation.hpp"
#include "MALookupTable.h"

MALookupTable::MALookupTable() {
    // Default constructor
    rVec = Eigen::VectorXd::LinSpaced(100, 0.005, 0.2);
    zVec = Eigen::VectorXd::LinSpaced(63, 0.0, 1.25);
    u1 = 0.36;
    u2 = 0.28;

    // Generate the table of values
    GenerateMATable();
}

MALookupTable::MALookupTable(Eigen::VectorXd r, Eigen::VectorXd z, double u1Val, double u2Val) {
    // Constructor with custom parameters
    rVec = r;
    zVec = z;
    u1 = u1Val;
    u2 = u2Val;

    // Generate the table of values
    GenerateMATable();
}

double MALookupTable::LookupValue(double r, double z) {
    int row = -1;
    int col = -1;

    for (int i = 0; i < rVec.size(); ++i) {
        if (r >= rVec(i)) {
            row = i;
        }
    }

    for (int i = 0; i < zVec.size(); ++i) {
        if (z <= zVec(i)) {
            col = i;
            break;
        }
    }

    if (row == -1 || col == -1) {
        auto [o1, o2] = occultquadVec(r, z, u1, u2);
        return o1;
    }

    return MATable(row, col);
}

double MALookupTable::LookupValue(double r, double z, double Requested_u1, double Requested_u2) {
    if (Requested_u1 != u1 || Requested_u2 != u2) {
        auto [o1, o2] = occultquadVec(r, z, Requested_u1, Requested_u2);
        return o1;
    } else {
        return LookupValue(r, z);
    }
}

VectorXd MALookupTable::LookupValue(double r, VectorXd z, double Requested_u1, double Requested_u2) {
    VectorXd result = VectorXd::Zero(z.size());

    for (int i = 0; i < z.size(); ++i) {
        result(i) = LookupValue(r, z(i), Requested_u1, Requested_u2);
    }

    return result;
}

void MALookupTable::GenerateMATable() {
    MATable = Eigen::MatrixXd::Zero(rVec.size(), zVec.size());

    for (int i = 0; i < rVec.size(); ++i) {
        for (int j = 0; j < zVec.size(); ++j) {
            auto [o1, o2] = occultquadVec(rVec(i), zVec(j), u1, u2);
            MATable(i, j) = o1;
        }
    }
}

void MALookupTable::PrintMATable() {
    for (int i = 0; i < rVec.size(); ++i) {
        for (int j = 0; j < zVec.size(); ++j) {
            std::cout << "MATable(" << i << ", " << j << ") = " << MATable(i, j) << std::endl;
        }
    }
}

