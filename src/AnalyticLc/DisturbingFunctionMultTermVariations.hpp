#pragma once

#include <helper_functions.hpp>
#include <cmath>
#include <complex>

#include "ForcedElements1.h"

using namespace Eigen;
namespace alc {
    struct CalculateDisturbingFunctionParams {
        VectorXd n;
        MatrixXd e;
        MatrixXd pom;
        MatrixXd I;
        MatrixXd Om;
    };

    void from_json(const json &j, CalculateDisturbingFunctionParams &opt);


    struct DisturbingFunctionParams {
        double n1;
        double n2;
        double alph;
        VectorXd Lambda1;
        VectorXd Lambda2;
        double mu1;
        double mu2;
        VectorXd j;
        VectorXd k;
        VectorXd e1;
        VectorXd e2;
        VectorXd Pom1;
        VectorXd Pom2;
        VectorXd I1;
        VectorXd I2;
        VectorXd s1;
        VectorXd s2;
        VectorXd Om1;
        VectorXd Om2;
        VectorXd A;
        VectorXd Ap;
        VectorXd B;
        VectorXd Bp;
        VectorXd C;
        VectorXd Cp;
        VectorXd D;
        VectorXd Dp;
        VectorXd f;
        VectorXd fE;
        VectorXd fI;
        VectorXd Df;
        VectorXd DfE;
        VectorXd DfI;

    };

    inline bool operator==(const DisturbingFunctionParams &lhs, const DisturbingFunctionParams &rhs) {
        return lhs.n1 == rhs.n1 &&
               lhs.n2 == rhs.n2 &&
               lhs.alph == rhs.alph &&
               lhs.Lambda1 == rhs.Lambda1 &&
               lhs.Lambda2 == rhs.Lambda2 &&
               lhs.mu1 == rhs.mu1 &&
               lhs.mu2 == rhs.mu2 &&
               lhs.j == rhs.j &&
               lhs.k == rhs.k &&
               lhs.e1 == rhs.e1 &&
               lhs.e2 == rhs.e2 &&
               lhs.Pom1 == rhs.Pom1 &&
               lhs.Pom2 == rhs.Pom2 &&
               lhs.I1 == rhs.I1 &&
               lhs.I2 == rhs.I2 &&
               lhs.s1 == rhs.s1 &&
               lhs.s2 == rhs.s2 &&
               lhs.Om1 == rhs.Om1 &&
               lhs.Om2 == rhs.Om2 &&
               lhs.A == rhs.A &&
               lhs.Ap == rhs.Ap &&
               lhs.B == rhs.B &&
               lhs.Bp == rhs.Bp &&
               lhs.C == rhs.C &&
               lhs.Cp == rhs.Cp &&
               lhs.D == rhs.D &&
               lhs.Dp == rhs.Dp &&
               lhs.f == rhs.f &&
               lhs.fE == rhs.fE &&
               lhs.fI == rhs.fI &&
               lhs.Df == rhs.Df &&
               lhs.DfE == rhs.DfE &&
               lhs.DfI == rhs.DfI;
    }

    std::string
    print_diff_error(const DisturbingFunctionParams &lhs, const DisturbingFunctionParams &rhs, double error = 1e-4);

    std::string print_diff(const DisturbingFunctionParams &lhs, const DisturbingFunctionParams &rhs);

    std::string to_string(const DisturbingFunctionParams &x);

    inline bool operator!=(const DisturbingFunctionParams &lhs, const DisturbingFunctionParams &rhs) {
        return !(lhs == rhs);
    }

    void from_json(const json &j, DisturbingFunctionParams &opt);

    struct DisturbingFunctionOutput {
        VectorXd dLambda1;
        VectorXd dLambda2;
        VectorXcd dz1;
        VectorXcd dz2;
        VectorXcd du1;
        VectorXcd du2;
        VectorXd da_oa1;
        VectorXd da_oa2;

        DisturbingFunctionOutput() = default;

        DisturbingFunctionOutput(size_t lambda1_size, size_t lambda2_size) {
            this->dLambda1 = VectorXd::Zero(lambda1_size);
            this->dLambda2 = VectorXd::Zero(lambda2_size);
            this->dz1 = VectorXcd::Zero(lambda1_size);
            this->dz2 = VectorXcd::Zero(lambda2_size);
            this->du1 = VectorXcd::Zero(lambda1_size);
            this->du2 = VectorXcd::Zero(lambda2_size);
            this->da_oa1 = VectorXd::Zero(lambda1_size);
            this->da_oa2 = VectorXd::Zero(lambda2_size);
        }
    };

    void from_json(const json &j, DisturbingFunctionOutput &opt);
}

// j1 is first planet index
// j2 is second planet index
DisturbingFunctionParams calculate_disturbing_params(int j1, int j2, const ForcedElementsParams &params,
                                                     const CalculateDisturbingFunctionParams &params_part);

DisturbingFunctionOutput DisturbingFunctionMultTermVariations(DisturbingFunctionParams &params);
