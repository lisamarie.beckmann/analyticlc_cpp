#pragma once

#include <helper_functions.hpp>
#include <laplacecoeffs.hpp>
#include "json_helpers.h"

using json = nlohmann::json;

namespace alc {
    struct ForcedElementsParams {
        LaplaceCoeff &lc;
        VectorXd P;
        MatrixXd Lambda;
        VectorXd mu;
        MatrixXcd z;
        MatrixXcd u;
        VectorXd OrdersAll;
        int MaxPower;
    };

    void from_json(const json &j, ForcedElementsParams &opt);

    struct calculateForcedEl_params {
        LaplaceCoeff &lc;
        VectorXd P;
        VectorXd mu;
        VectorXd ex0;
        VectorXd ey0;
        VectorXd I0;
        VectorXd Omega0;
        VectorXd tT;
        VectorXd t;
    };
}

using namespace alc;

std::tuple<MatrixXcd, MatrixXd, MatrixXcd, MatrixXd> ForcedElements1(ForcedElementsParams params);

std::tuple<VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd, VectorXd> Calc_AtoD();

std::tuple<VectorXd, VectorXd, VectorXd, VectorXd, VectorXd>
Calc_Df(LaplaceCoeff &lc, double alph, VectorXd sVec, VectorXi jVec, VectorXi DVec, int j);

std::tuple<VectorXd, VectorXi, VectorXi> sjDVec(int j);

