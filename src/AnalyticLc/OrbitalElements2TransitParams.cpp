//
// Created by lisa on 11/28/23.
//

#include "OrbitalElements2TransitParams.h"
#include <Eigen/Dense>
#include <Eigen/src/Core/Array.h>
#include <iostream>


std::tuple<VectorXd, VectorXd, VectorXd, VectorXd,VectorXd> OrbitalElements2TransitParams(VectorXd n, VectorXd aor, VectorXd ex, VectorXd ey, VectorXd I, VectorXd Omega, double r) {
        //returns the duration, ingress-egress time and angular velocity as a
        //function of the orbital properties.

        //inputs: n - mean motion, aor - semi-major axis over stellar radius, ex,ey - eccentricity vector components, x
        //pointing at observer, I,Omega - inclination with respect to the planet xy,
        //where x points at observer. Outputs: T=Duration, Tau=ingress/egress time,
        //w = angular velocity at mid-transit, d = planet-star distance at
        //mid-transit, b = impact parameter
        //VectorXd w = n.array()*(1+ex.array()*ex.array()).array()/pow(1-ex.array()*ex.array()-ey.array()*ey.array(),3/2).array();
        VectorXd w = n.array()*(1+ex.array()).array().pow(2)/pow(1-pow(ex.array(),2)-pow(ey.array(),2),3./2).array();
        VectorXd d = aor.array()*(1-pow(ex.array(),2)-pow(ey.array(),2))/(1+ex.array()).array();
        VectorXd b = d.array()*sin(I.array()).array()*sin(Omega.array()).array();

        VectorXd asin0 = asin(sqrt(((1+ex.array()).pow(2)/(pow(aor.array(),2)*pow((1-pow(ex.array(),2)-pow(ey.array(),2)),2))-pow(sin(I.array()),2)*pow(sin(Omega.array()),2))/(1-pow(sin(I.array()),2)*pow(sin(Omega.array()),2))));
        VectorXd T=1./n.array()*2.*((1-pow(ex.array(),2)-pow(ey.array(),2)).pow(3./2))/((1+ex.array()).pow(2))*asin0.array();
        VectorXd asin2 = asin(sqrt((pow((1+r),2)*(1+ex.array()).pow(2)/(pow(aor.array(),2)*pow((1-pow(ex.array(),2)-pow(ey.array(),2)),2))-pow(sin(I.array()),2)*pow(sin(Omega.array()),2))/(1-pow(sin(I.array()),2)*pow(sin(Omega.array()),2))));
        VectorXd asin1 = asin(sqrt((pow((1-r),2)*(1+ex.array()).pow(2)/(pow(aor.array(),2)*pow((1-pow(ex.array(),2)-pow(ey.array(),2)),2))-pow(sin(I.array()),2)*pow(sin(Omega.array()),2))/(1-pow(sin(I.array()),2)*pow(sin(Omega.array()),2))));
        VectorXd Tau=1./n.array();
        VectorXd Taus2 = Tau.array()*((1-pow(ex.array(),2) - pow(ey.array(),2)).pow(3./2));
        VectorXd Taus3 = Taus2.array() /((1+ex.array()).pow(2));
        Tau = Taus3.array()*(asin2.array() - asin1.array());
        return std::make_tuple(w,d,b,T,Tau);
    }

//OrbitalElements2TransitParams with numerical inputs and outputs
std::tuple<double, double, double, double, double> OrbitalElements2TransitParams(double n, double aor, double ex, double ey, double I, double Omega, double r){
    VectorXd n_vec = VectorXd::Constant(1,n);
    VectorXd aor_vec = VectorXd::Constant(1,aor);
    VectorXd ex_vec = VectorXd::Constant(1,ex);
    VectorXd ey_vec = VectorXd::Constant(1,ey);
    VectorXd I_vec = VectorXd::Constant(1,I);
    VectorXd Omega_vec = VectorXd::Constant(1,Omega);
    auto [w,d,b,T,Tau] = OrbitalElements2TransitParams(n_vec,aor_vec,ex_vec,ey_vec,I_vec,Omega_vec,r);
    return std::make_tuple(w[0],d[0],b[0],T[0],Tau[0]);
}