//
// Created by lisa on 11/28/23.
//

#ifndef ANALYTICLC_ORBITALELEMENTS2TRANSITPARAMS_H
#define ANALYTICLC_ORBITALELEMENTS2TRANSITPARAMS_H


#include <helper_functions.hpp>

std::tuple<VectorXd, VectorXd, VectorXd, VectorXd,VectorXd> OrbitalElements2TransitParams(VectorXd n, VectorXd aor, VectorXd ex, VectorXd ey, VectorXd I, VectorXd Omega, double r);
std::tuple<double, double, double, double, double> OrbitalElements2TransitParams(double n, double aor, double ex, double ey, double I, double Omega, double r);

#endif //ANALYTICLC_ORBITALELEMENTS2TRANSITPARAMS_H
