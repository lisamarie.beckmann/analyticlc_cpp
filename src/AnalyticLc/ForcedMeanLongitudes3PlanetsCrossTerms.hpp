#pragma once

#include <Eigen/Dense>
#include <Eigen/src/Core/Array.h>
#include "laplacecoeffs.hpp"

using namespace Eigen;

MatrixXd ForcedMeanLongitudes3PlanetsCrossTerms(LaplaceCoeff &lc, VectorXd P, MatrixXd Lambda, VectorXd mu,
                                                MatrixXcd z = MatrixXd::Zero(1, 1));