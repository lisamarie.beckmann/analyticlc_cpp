#include <Eigen/Dense>
#include <Eigen/src/Core/Array.h>
#include <iostream>
#include "laplacecoeffs.hpp"
#include "analyticlc.hpp"
#include <math.h>       /* atan2 */
#include "helper_functions.hpp"

using namespace Eigen;

VectorXd ForcedMeanLongitudes3PlanetsCrossTerms(LaplaceCoeff &lc, VectorXd P, VectorXd Lambda, VectorXd mu,
                                                VectorXcd z) {
    // ForcedMeanLongitudes3Planets: calculate the forced mean longitudes of a set of
    // planets due to the cross-terms among different resonances.

    //Inputs: P - orbital periods, Lambda - mean longitudes, mu - planets/star
    //mass ratio, z - free eccentricities (if they are not given, zero values
    //are assumed). Labmda and z can be given as columns of multiple values.

    //Output: dLambda - forced mean longitudes

    if (P.size() < 3){
        return VectorXd::Zero(P.size());
    }

    if (z.size() == 1 && P.size() > 1) {
        z = z[0] * VectorXd::Zero(P.size());
    }
    // mean motions
    VectorXd n = 2 * M_PI / P.array();

    // absolute values and angles of eccentricities
    VectorXd e = z.array().abs();

    VectorXd zx = z.array().real();
    VectorXd zy = z.array().imag();

    VectorXd pom(P.size());

    for (int i = 0; i < P.size(); i++) {
        pom(i) = atan2(zy(i), zx(i));
    }

    //allocate space for 3d matrices that will hold the values of dLambda
    //according to these dimensios: orders, data points, number of planets
    VectorXd dLambda = VectorXd::Zero(P.size());

    //go over all possible planets triplets
    for (int j1 = 0; j1 < P.size(); j1++) {
        for (int j2 = j1 + 1; j2 < P.size(); j2++) {
            for (int j3 = j2 + 1; j3 < P.size(); j3++) {
                // calculate the nearest 1st order MMR for each pair in the
                // triplet
                int j = round(P[j2] / (P[j2] - P[j1]));
                int k = round(P[j3] / (P[j3] - P[j2]));
                double nj = j * n[j2] + (1 - j) * n[j1];
                double nk = k * n[j3] + (1 - k) * n[j2];

                // calculate the alpha parameters
                double alph12 = pow(n[j2] / n[j1], 2.0 / 3.0) * pow((1 + mu[j1]) / (1 + mu[j2]), 1.0 / 3.0);
                double alph23 = pow(n[j3] / n[j2], 2.0 / 3.0) * pow((1 + mu[j2]) / (1 + mu[j3]), 1.0 / 3.0);

                // calculate the longitudes of conjunction
                auto Lambda_j = j * Lambda[j2] + (1 - j) * Lambda[j1];
                auto Lambda_k = k * Lambda[j3] + (1 - k) * Lambda[j2];

                // calculate the laplace coefficients
                auto Ak = lc.calculate(alph23, 0.5, k, 0);
                auto DAk = lc.calculate(alph23, 0.5, k, 1);
                auto Ajm1 = lc.calculate(alph12, 0.5, j - 1, 0);
                auto DAjm1 = lc.calculate(alph12, 0.5, j - 1, 1);
                auto D2Ajm1 = lc.calculate(alph12, 0.5, j - 1, 2);
                auto f27k = -k * Ak - 0.5 * alph23 * DAk;
                auto f31j = (j - 0.5) * Ajm1 + 0.5 * alph12 * DAjm1;


                auto Df31j = (j - 0.5) * DAjm1 + 0.5 * DAjm1 + 0.5 * alph12 * D2Ajm1;

                // calculate the factor F
                double F = sqrt(1.0);

                //calculate and add to the dlambda of the intermediate planet
                auto AlphaFactor = -3. / 2. * f27k * (f31j - (j == 2) / (2. * alph12 * alph12)) * alph23;
                auto MassFactor = mu[j1] * mu[j3] / pow(1 + mu[j2], 2.0);
                auto SinTerm1 = (j / nk + (1 - k) / nj) * sin(Lambda_j + Lambda_k - 2 * pom[j2]) / ((nj + nk) *
                                                                                                    (nj +
                                                                                                     nk)); //check for pom(:,j2) dimensions
                auto SinTerm2 = (j / nk - (1 - k) / nj) * sin(Lambda_j - Lambda_k) / ((nj - nk) * (nj - nk));
                dLambda(j2) += F * AlphaFactor * MassFactor * n[j2] * n[j2] * n[j2] * (SinTerm1 + SinTerm2);

                //calculate and add to the dlambda of the innermost planet
                AlphaFactor = 3. / 2. * f27k * (f31j - 2. * alph12 * (j == 2)) * alph12 * alph23;
                MassFactor = mu[j2] * mu[j3] / (1 + mu[j1]) / (1 + mu[j2]);
                SinTerm1 = sin(Lambda_j + Lambda_k - 2 * pom[j2]) / ((nj + nk) * (nj + nk)); //TODO: Check pom
                SinTerm2 = sin(Lambda_j - Lambda_k) / ((nj - nk) * (nj - nk));
                dLambda(j1) = dLambda(j1) +
                              F * AlphaFactor * MassFactor * n[j1] * n[j1] * n[j2] * (j - 1) / nk *
                              (SinTerm1 + SinTerm2);

                //calculate and add to the dlambda of the outermost planet
                AlphaFactor = -3. / 2. * f27k * (f31j - (j == 2) / (2. * alph12 * alph12));
                MassFactor = mu[j1] * mu[j2] / (1 + mu[j2]) / (1 + mu[j3]);
                SinTerm1 = sin(Lambda_j + Lambda_k - 2 * pom[j2]) / ((nj + nk) * (nj + nk)); //TODO: Check pom
                SinTerm2 = -sin(Lambda_j - Lambda_k) / ((nj - nk) * (nj - nk));
                dLambda(j3) = dLambda(j3) +
                              F * AlphaFactor * MassFactor * n[j3] * n[j3] * n[j2] * k / nj * (SinTerm1 + SinTerm2);
            }
        }
    }
    return dLambda;
}

MatrixXd
ForcedMeanLongitudes3PlanetsCrossTerms(LaplaceCoeff &lc, VectorXd P, MatrixXd Lambda, VectorXd mu, MatrixXcd z) {
    //prepare output Matrix
    MatrixXd dLambda = MatrixXd::Zero(z.rows(), P.size()); //warn: may be z.rows()
    for (int i = 0; i < z.rows(); i++) {
        VectorXd LambdaRow = Lambda.row(i);
        VectorXcd zRow = z.row(i);
        dLambda.row(i) = ForcedMeanLongitudes3PlanetsCrossTerms(lc, P, LambdaRow, mu, zRow);
    }
    return dLambda;
}