#pragma once

//#include <cpptrace/cpptrace.hpp>

//#define JSON_TRY_USER if(true)
//#define JSON_CATCH_USER(exception) if(false)
//#define JSON_THROW_USER(e)                           \
//    { throw cpptrace::runtime_error(e.what()); }

#include <nlohmann/json.hpp>

#include <Eigen/Core>

NLOHMANN_JSON_NAMESPACE_BEGIN
        template<>
        struct adl_serializer<Eigen::VectorXd> {
            static void to_json(json &j, const Eigen::VectorXd &opt) {
                assert(false);
            }

            static void from_json(const json &j, Eigen::VectorXd &opt) {
                if (j.is_number() && !j.is_array()) {
                    opt.resize(1);
                    opt(0) = j.get<double>();
                    return;
                } else if (!j.is_array()) {
                    throw std::runtime_error("Expected array");
                }
                opt.resize(j.size());
                for (size_t i = 0; i < j.size(); ++i) {
                    if (j[i].is_null()) {
                        opt(i) = 0;
                    } else {
                        opt(i) = j[i].get<double>();
                    }
                }
            }
        };

        template<>
        struct adl_serializer<Eigen::VectorXcd> {
            static void to_json(json &j, const Eigen::VectorXcd &opt) {
                assert(false);
            }

            static void from_json(const json &j, Eigen::VectorXcd &opt) {
                if (j.is_number() && !j.is_array()) {
                    opt.resize(1);
                    opt(0) = j.get<double>();
                    return;
                } else if (!j.is_array()) {
                    throw std::runtime_error("Expected array");
                }
                opt.resize(j.size());
                for (size_t i = 0; i < j.size(); ++i) {
                    if (j[i].is_null()) {
                        opt(i) = 0;
                    } else {
                        opt(i) = j[i].get<double>();
                    }
                }
            }
        };

        template<>
        struct adl_serializer<Eigen::VectorXi> {
            static void to_json(json &j, const Eigen::VectorXi &opt) {
                assert(false);
            }

            static void from_json(const json &j, Eigen::VectorXi &opt) {
                if (j.is_number() && !j.is_array()) {
                    opt.resize(1);
                    opt(0) = j.get<double>();
                    return;
                } else if (!j.is_array()) {
                    throw std::runtime_error("Expected array");
                }
                opt.resize(j.size());
                for (size_t i = 0; i < j.size(); ++i) {
                    if (j[i].is_null()) {
                        opt(i) = 0;
                    } else {
                        opt(i) = j[i].get<int>();
                    }
                }
            }
        };

        template<>
        struct adl_serializer<Eigen::ArrayXd> {
            static void to_json(json &j, const Eigen::ArrayXd &opt) {
                assert(false);
            }

            static void from_json(const json &j, Eigen::ArrayXd &opt) {
                if (j.is_number()) {
                    opt.resize(1);
                    opt(0) = j.get<double>();
                    return;
                } else if (!j.is_array()) {
                    throw std::runtime_error("Expected array");
                }

                opt.resize(j.size());
                for (size_t i = 0; i < j.size(); ++i) {
                    if (j[i].is_null()) {
                        opt(i) = 0;
                    } else {
                        opt(i) = j[i].get<double>();
                    }
                }
            }
        };

        template<>
        struct adl_serializer<Eigen::ArrayXi> {
            static void to_json(json &j, const Eigen::ArrayXi &opt) {
                assert(false);
            }

            static void from_json(const json &j, Eigen::ArrayXi &opt) {
                if (j.is_number()) {
                    opt.resize(1);
                    opt(0) = j.get<double>();
                    return;
                } else if (!j.is_array()) {
                    throw std::runtime_error("Expected array");
                }

                opt.resize(j.size());
                for (size_t i = 0; i < j.size(); ++i) {
                    if (j[i].is_null()) {
                        opt(i) = 0;
                    } else {
                        opt(i) = j[i].get<int>();
                    }
                }
            }
        };

        template<>
        struct adl_serializer<Eigen::MatrixXd> {
            static void to_json(json &j, const Eigen::MatrixXd &opt) {
                assert(false);
            }

            static void from_json(const json &j, Eigen::MatrixXd &opt) {
                if (!j.is_array()) {
                    throw std::runtime_error("Expected array");
                }
                if (j[0].is_number()) {
                    opt.resize(1, j.size());
                    for (size_t c = 0; c < j.size(); ++c) {
                        opt(0, c) = j[c].get<double>();
                    }
                    return;
                } else if (!j[0].is_array()) {
                    throw std::runtime_error("Expected array of arrays");
                }

                size_t rows = j.size();
                size_t cols = j[0].size();

                opt.resize(rows, cols);
                for (size_t i = 0; i < rows; ++i) {
                    for (size_t c = 0; c < cols; ++c) {
                        opt(i, c) = j[i][c].get<double>();
                    }
                }
            }
        };

        template<>
        struct adl_serializer<Eigen::MatrixXi> {
            static void to_json(json &j, const Eigen::MatrixXi &opt) {
                assert(false);
            }

            static void from_json(const json &j, Eigen::MatrixXi &opt) {
                if (!j.is_array()) {
                    throw std::runtime_error("Expected array");
                }
                if (j[0].is_number()) {
                    opt.resize(1, j.size());
                    for (size_t c = 0; c < j.size(); ++c) {
                        opt(0, c) = j[c].get<int>();
                    }
                    return;
                } else if (!j[0].is_array()) {
                    throw std::runtime_error("Expected array of arrays");
                }

                size_t rows = j.size();
                size_t cols = j[0].size();

                opt.resize(rows, cols);
                for (size_t i = 0; i < rows; ++i) {
                    for (size_t c = 0; c < cols; ++c) {
                        opt(i, c) = j[i][c].get<int>();
                    }
                }
            }
        };
NLOHMANN_JSON_NAMESPACE_END
