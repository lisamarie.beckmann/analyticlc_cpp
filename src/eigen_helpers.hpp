#pragma once
#include <iostream>
#include <Eigen/Core>

using namespace std::complex_literals;

namespace alc {
    std::string to_string(const Eigen::VectorXd &v);

    std::string to_string(const Eigen::VectorXi &v);

    std::string to_string(const Eigen::ArrayXd &v);

    std::string to_string(const Eigen::ArrayXi &v);

    std::string to_string(const Eigen::MatrixXd &v);

    std::string to_string(const Eigen::MatrixXi &v);

    std::string print_diff(const Eigen::VectorXd &v1, const Eigen::VectorXd &v2);

    std::string print_diff(const Eigen::VectorXi &v1, const Eigen::VectorXi &v2);

    std::string print_diff(const Eigen::ArrayXd &v1, const Eigen::ArrayXd &v2);

    std::string print_diff(const Eigen::ArrayXi &v1, const Eigen::ArrayXi &v2);

    std::string print_diff(const Eigen::MatrixXd &v1, const Eigen::MatrixXd &v2);

    std::string print_diff(const Eigen::MatrixXi &v1, const Eigen::MatrixXi &v2);

    std::string print_diff(const Eigen::VectorXcd &v1, const Eigen::VectorXcd &v2);

    std::string print_diff(const Eigen::MatrixXcd &v1, const Eigen::MatrixXcd &v2);
}

std::vector<Eigen::MatrixXcd> reshape(const Eigen::MatrixXcd& input, const std::vector<int>& new_shape);
Eigen::MatrixXcd reshape(std::vector<Eigen::MatrixXcd> input, const std::vector<int>& new_shape);
Eigen::MatrixXcd reshape(Eigen::MatrixXcd input, int new_rows, int new_cols);
std::vector<Eigen::MatrixXcd> multiplyElementWise(const std::vector<Eigen::MatrixXcd>& vec1, const std::vector<Eigen::MatrixXcd>& vec2);
template<typename DerivedA, typename DerivedB>
bool allclose(const Eigen::DenseBase<DerivedA> &a,
              const Eigen::DenseBase<DerivedB> &b,
              const typename DerivedA::RealScalar &rtol
              = Eigen::NumTraits<typename DerivedA::RealScalar>::dummy_precision(),
              const typename DerivedA::RealScalar &atol
              = Eigen::NumTraits<typename DerivedA::RealScalar>::epsilon()) {
    bool result = false;
    try {
        result = ((a.derived() - b.derived()).array().abs()
                  <= (atol + rtol * b.derived().array().abs())).all();
    } catch(...) {
        result = false;
    };

    if(!result) {
        std::cout << std::endl << "allclose:" << std::endl;
        std::cout << a.derived() << std::endl;
        std::cout << "differs from" << std::endl;
        std::cout << b.derived() << std::endl;
        std::cout << std::endl;
    }
    return result;
}
