#pragma once
#include <Eigen/Dense>
#include <Eigen/src/Core/Array.h>
#include <Eigen/src/Core/util/Constants.h>
#include <numbers>
#include <cmath>
#include <tuple>
#include "helper_functions.hpp"
#include "laplacecoeffs.hpp"
#include <igl/repmat.h>
#include <json_helpers.h>

using namespace Eigen;
using nlohmann::json;


// Input used by AnalyticLC
struct PlanetSystem {
    // vector of orbital periods
	ArrayXd P;
    // vector of refernce times-of-mid-transit 
	ArrayXd Tmid0;
    // vector of planets/star radii ratio
	ArrayXd ror;
    // vector of planets/star mass ratio
	ArrayXd mu; 
    // vectors of initial FREE eccentricities components of planets
	ArrayXd ex0;
    // vectors of initial FREE eccentricities components of planets
	ArrayXd ey0;
    // vector of initial FREE inclinations (radians)
	ArrayXd I0;
    // vector of initial FREE longitudes of ascending nodes (radians) 
	ArrayXd Omega0; 
};

template <typename T>
void printEigen(const std::string& name, const T& matrix) {
    std::cout << name << ":\n" << matrix << "\n\n";
}

namespace alc{
    struct AnalyticLCparams{
        VectorXd P;
        VectorXd Tmid0;
        VectorXd ror;
        VectorXd aor;
        VectorXd mu;
        VectorXd ex0;
        VectorXd ey0;
        VectorXd I0;
        VectorXd Omega0;
        VectorXd t;
        double u1;
        double u2;
        VectorXd BinningTime;
        VectorXi CadenceType;
        VectorXd MaxTTV;
        VectorXd tRV;
        double ExtraIters;
        VectorXd OrdersAll;
        bool Calculate3PlanetsCrossTerms;
        int MaxPower;
        bool OrbitalElementsAsCellArrays;
        double J2;
        bool CalcDurations;
        bool CalcAstrometry;
        AnalyticLCparams() {};
	/*
        AnalyticLCparams(const VectorXd &P, const VectorXd &Tmid0, const VectorXd &ror, const VectorXd &aor, const VectorXd &mu, const VectorXd &ex0, const VectorXd &ey0, const VectorXd &I0, const VectorXd &Omega0, const VectorXd &t, double u1, double u2):
            P(P), Tmid0(Tmid0), ror(ror), aor(aor), mu(mu), ex0(ex0), ey0(ey0), I0(I0), Omega0(Omega0), t(t), u1(u1), u2(u2) {
            BinningTime = VectorXd::Zero(1);
            BinningTime[0] = matlabrange(t);
            CadenceType = VectorXi::Ones(t.size());
            MaxTTV = VectorXd::Ones(P.size()) * Infinity;
            tRV = VectorXd::Zero(0);
            ExtraIters = 0;
            OrdersAll = VectorXd::Zero(0);
            Calculate3PlanetsCrossTerms = false;
            MaxPower = 4;
            OrbitalElementsAsCellArrays = false;
            J2 = 0;
            CalcDurations = true;
            CalcAstrometry = true;
        };*/
	AnalyticLCparams(Eigen::Ref<VectorXd> P, Eigen::Ref<VectorXd> Tmid0, Eigen::Ref<VectorXd> ror, Eigen::Ref<VectorXd> aor, Eigen::Ref<VectorXd> mu, Eigen::Ref<VectorXd> ex0, Eigen::Ref<VectorXd> ey0, Eigen::Ref<VectorXd> I0, Eigen::Ref<VectorXd> Omega0, Eigen::Ref<VectorXd> t, double u1, double u2){
		this->P = P;
		this->Tmid0 = Tmid0;
		this->ror = ror;
		this->aor = aor;
		this->mu = mu;
		this->ex0 = ex0;
		this->ey0 = ey0;
		this->I0 = I0;
		this->Omega0 = Omega0;
		this->t = t;
		this->u1 = u1;
		this->u2 = u2;
		this->BinningTime = VectorXd::Zero(1);
		this->BinningTime[0] = matlabrange(t);
		this->CadenceType = VectorXi::Ones(t.size());
		this->MaxTTV = VectorXd::Ones(P.size()) * Infinity;
		this->tRV = VectorXd::Zero(0);
		this->ExtraIters = 0;
		this->OrdersAll = VectorXd::Zero(0);
		this->Calculate3PlanetsCrossTerms = false;
		this->MaxPower = 4;
		this->OrbitalElementsAsCellArrays = false;
		this->J2 = 0;
		this->CalcDurations = true;
		this->CalcAstrometry = true;
	}
        void set_P(Eigen::Ref<VectorXd> P) {this->P = P;};
        VectorXd get_P() {return P;};
        void set_Tmid0(Eigen::Ref<VectorXd> Tmid0) {this->Tmid0 = Tmid0;};
        void set_ror(Eigen::Ref<VectorXd> ror) {this->ror = ror;};
        void set_aor(Eigen::Ref<VectorXd> aor) {this->aor = aor;};
        void set_mu(Eigen::Ref<VectorXd> mu) {this->mu = mu;};
        void set_ex0(Eigen::Ref<VectorXd> ex0) {this->ex0 = ex0;};
        void set_ey0(Eigen::Ref<VectorXd> ey0) {this->ey0 = ey0;};
        void set_I0(Eigen::Ref<VectorXd> I0) {this->I0 = I0;};
        void set_Omega0(Eigen::Ref<VectorXd> Omega0) {this->Omega0 = Omega0;};
        void set_t(Eigen::Ref<VectorXd> t) {this->t = t;};
        void set_u1(double u1) {this->u1 = u1;};
        void set_u2(double u2) {this->u2 = u2;};
        void set_BinningTime(Eigen::Ref<VectorXd> BinningTime) {this->BinningTime = BinningTime;};
        void set_CadenceType(Eigen::Ref<VectorXi> CadenceType) {this->CadenceType = CadenceType;};
        void set_MaxTTV(Eigen::Ref<VectorXd> MaxTTV) {this->MaxTTV = MaxTTV;};
        void set_tRV(Eigen::Ref<VectorXd> tRV) {this->tRV = tRV;};
        void set_ExtraIters(double ExtraIters) {this->ExtraIters = ExtraIters;};
        void set_OrdersAll(Eigen::Ref<VectorXd> OrdersAll) {this->OrdersAll = OrdersAll;};
        void set_Calculate3PlanetsCrossTerms(bool Calculate3PlanetsCrossTerms) {this->Calculate3PlanetsCrossTerms = Calculate3PlanetsCrossTerms;};
        void set_MaxPower(int MaxPower) {this->MaxPower = MaxPower;};
        void set_OrbitalElementsAsCellArrays(bool OrbitalElementsAsCellArrays) {this->OrbitalElementsAsCellArrays = OrbitalElementsAsCellArrays;};
        void set_J2(double J2) {this->J2 = J2;};
        void set_CalcDurations(bool CalcDurations) {this->CalcDurations = CalcDurations;};
        void set_CalcAstrometry(bool CalcAstrometry) {this->CalcAstrometry = CalcAstrometry;};
    };

    struct calcforcedin {
        VectorXd P;
        VectorXd Tmid0;
        VectorXd aor;
        VectorXd mu;
        VectorXd ex0;
        VectorXd ey0;
        VectorXd I0;
        VectorXd Omega0;
        VectorXd t;
        VectorXd tRV;
        VectorXd OrdersAll;
        bool Calculate3PlanetsCrossTerms;
        int MaxPower;
        double J2;

        calcforcedin(Eigen::Ref<VectorXd> P, Eigen::Ref<VectorXd> Tmid0, Eigen::Ref<VectorXd> aor, Eigen::Ref<VectorXd> mu, Eigen::Ref<VectorXd> ex0, Eigen::Ref<VectorXd> ey0,
                    Eigen::Ref<VectorXd> I0, Eigen::Ref<VectorXd> Omega0, Eigen::Ref<VectorXd> t){
            this->P = P;
            this->Tmid0 = Tmid0;
            this->aor = aor;
            this->mu = mu;
            this->ex0 = ex0;
            this->ey0 = ey0;
            this->I0 = I0;
            this->Omega0 = Omega0;
            this->t = t;
            this->tRV = VectorXd::Zero(0);
            this->OrdersAll = VectorXd::Zero(0);
            this->Calculate3PlanetsCrossTerms = false;
            this->MaxPower = 4;
            this->J2 = 0;
        }
        void set_P(Eigen::Ref<VectorXd> P) {this->P = P;};
        VectorXd get_P() {return P;};
        void set_Tmid0(Eigen::Ref<VectorXd> Tmid0) {this->Tmid0 = Tmid0;};
        void set_aor(Eigen::Ref<VectorXd> aor) {this->aor = aor;};
        void set_mu(Eigen::Ref<VectorXd> mu) {this->mu = mu;};
        void set_ex0(Eigen::Ref<VectorXd> ex0) {this->ex0 = ex0;};
        void set_ey0(Eigen::Ref<VectorXd> ey0) {this->ey0 = ey0;};
        void set_I0(Eigen::Ref<VectorXd> I0) {this->I0 = I0;};
        void set_Omega0(Eigen::Ref<VectorXd> Omega0) {this->Omega0 = Omega0;};
        void set_t(Eigen::Ref<VectorXd> t) {this->t = t;};
        void set_tRV(Eigen::Ref<VectorXd> tRV) {this->tRV = tRV;};
        void set_OrdersAll(Eigen::Ref<VectorXd> OrdersAll) {this->OrdersAll = OrdersAll;};
        void set_Calculate3PlanetsCrossTerms(bool Calculate3PlanetsCrossTerms) {this->Calculate3PlanetsCrossTerms = Calculate3PlanetsCrossTerms;};
        void set_MaxPower(int MaxPower) {this->MaxPower = MaxPower;};
        void set_J2(double J2) {this->J2 = J2;};
    };

    void construct(AnalyticLCparams &params, const VectorXd &P, const VectorXd &Tmid0, const VectorXd &ror, const VectorXd &aor, const VectorXd &mu, const VectorXd &ex0, const VectorXd &ey0, const VectorXd &I0, const VectorXd &Omega0, const VectorXd &t, double u1, double u2);

    void from_json(const json &j, AnalyticLCparams &opt);

    struct AnalyticLCout{
        VectorXd LC;
        VectorXd RV_o_r;
        VectorXd Y_o_r;
        VectorXd Z_o_r;

        VectorXd get_LC() {return LC;};
        VectorXd get_RV_o_r() {return RV_o_r;};
        VectorXd get_Y_o_r() {return Y_o_r;};
        VectorXd get_Z_o_r() {return Z_o_r;};
    };

    inline bool operator ==(const AnalyticLCout &lhs, const AnalyticLCout &rhs){
        return lhs.LC == rhs.LC &&
               lhs.RV_o_r == rhs.RV_o_r &&
               lhs.Y_o_r == rhs.Y_o_r &&
               lhs.Z_o_r == rhs.Z_o_r;
    }

    void from_json(const json &j, AnalyticLCout &opt);

    struct OutputList{
        VectorXd tT;
        VectorXd tRV;
        MatrixXcd zT;
        MatrixXcd zRV;
        MatrixXcd uT;
        MatrixXcd uRV;
        MatrixXd D;
        MatrixXd Tau;
        MatrixXd w;
        MatrixXd d;
        MatrixXd b;
        MatrixXd AssociatedTmid;
        MatrixXi AssociatedInd;
        MatrixXd AllLC;
        std::vector<Eigen::VectorXd> TmidActualCell;
        MatrixXcd free_e_T;
        MatrixXcd free_I_T;
        MatrixXcd free_e_RV;
        MatrixXcd free_I_RV;
        std::vector<Eigen::VectorXd> TTVCell;
        std::vector<Eigen::VectorXcd> dzCell;
        std::vector<Eigen::VectorXcd> duCell;
        std::vector<Eigen::VectorXd> dLambdaCell;
        std::vector<Eigen::VectorXcd> zfreeCell;
        VectorXd dbdt;
        VectorXd b0;
        MatrixXd Lambda_T;
        VectorXd P;
        VectorXi BeginIdx;
        VectorXi EndIdx;
        VectorXi Ntr;

        OutputList() {};

        VectorXd get_tT() {return tT;};
        VectorXd get_tRV() {return tRV;};
        MatrixXcd get_zT() {return zT;};
        MatrixXcd get_zRV() {return zRV;};
        MatrixXcd get_uT() {return uT;};
        MatrixXcd get_uRV() {return uRV;};
        MatrixXd get_D() {return D;};
        MatrixXd get_Tau() {return Tau;};
        MatrixXd get_w() {return w;};
        MatrixXd get_d() {return d;};
        MatrixXd get_b() {return b;};
        MatrixXd get_AssociatedTmid() {return AssociatedTmid;};
        MatrixXi get_AssociatedInd() {return AssociatedInd;};
        MatrixXd get_AllLC() {return AllLC;};
        std::vector<Eigen::VectorXd> get_TmidActualCell() {return TmidActualCell;};
        MatrixXcd get_free_e_T() {return free_e_T;};
        MatrixXcd get_free_I_T() {return free_I_T;};
        MatrixXcd get_free_e_RV() {return free_e_RV;};
        MatrixXcd get_free_I_RV() {return free_I_RV;};
        std::vector<Eigen::VectorXd> get_TTVCell() {return TTVCell;};
        std::vector<Eigen::VectorXcd> get_dzCell() {return dzCell;};
        std::vector<Eigen::VectorXcd> get_duCell() {return duCell;};
        std::vector<Eigen::VectorXd> get_dLambdaCell() {return dLambdaCell;};
        std::vector<Eigen::VectorXcd> get_zfreeCell() {return zfreeCell;};
        VectorXd get_dbdt() {return dbdt;};
        VectorXd get_b0() {return b0;};
        MatrixXd get_Lambda_T() {return Lambda_T;};
        VectorXd get_P() {return P;};
        VectorXi get_BeginIdx() {return BeginIdx;};
        VectorXi get_EndIdx() {return EndIdx;};
        VectorXi get_Ntr() {return Ntr;};
    };
    void from_json(const json &j, OutputList &opt);
}

// AnalyticLC: generate an analytic light-curve, and optionally RV and astrometry data, from a set of initial (free) orbital
// elements.
//
// The full logic of AnalyticLC and the model derivation are described in the paper "An Accurate 3D
// Analytic Model for Exoplanetary Photometry, Radial Velocity and
// Astrometry" by Judkovsky, Ofir and Aharonson (2021), please cite this
// paper upon usage. Model and enjoy :)
// 
// Eccentricity components and inclination are defined with respect to the system plane, and NOT with respect to the sky plane. The system plane is
// defined such that the x axis points from the star to the observer, and y
// is on the sky plane such that the inclinations are measured with respect
// to the xy plane. Transits are expected to occur for small
// values of sin(I0)*sin(Omega), which is equal to the cosine of the
// sky-plane inclination.
// The time units are to the user's choice, and should be consistent in all
// parameters (t,P,Tmid etc.).
//

typedef std::map<std::pair<size_t, size_t>, size_t> Cell_map;
// OutputList = {'tT','tRV','zT','zRV','uT','uRV','D','Tau','w','d','b','AssociatedTmid','AssociatedInd','AllLC','TmidActualCell','free_e_T','free_I_T','free_e_RV','free_I_RV','TTVCell','dzCell','duCell','dLambdaCell','zfreeCell','dbdt','b0','Lambda_T','P','BeginIdx','EndIdx','Ntr'}; %a list of parameters to give as an additional output, if required

std::tuple<alc::AnalyticLCout, alc::OutputList> AnalyticLCfunc(alc::AnalyticLCparams params);

//std::tuple<VectorXd, VectorXi,VectorXi, VectorXi> DynamicsTimeAxis(const VectorXd& P, const VectorXd& Tmid0, const VectorXd& t);
std::tuple<MatrixXcd, MatrixXcd> SecularInteractionsMatrix(
		VectorXd n,
		VectorXd mu,
		VectorXd aor,
		double J2);
std::tuple<VectorXd, VectorXd, VectorXd> CalcRV(MatrixXd Lambda, MatrixXcd z, MatrixXcd u, VectorXd mu, VectorXd aor, VectorXd n, bool CalcAstrometry = true);
MatrixXd GenerateLightCurve(const int Nt,
                             const int Npl,
                             const int Ncad,
                             const VectorXi& CadenceType,
                             const VectorXd& t,
                             const VectorXd& BinningTime,
                             const MatrixXd& AssociatedTmid_val,
                             const MatrixXd& w,
                             const MatrixXd& d,
                             const MatrixXd& b,
                             const VectorXd& ror,
                             double u1,
                             double u2);
std::tuple<MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, std::vector<Eigen::VectorXd>, MatrixXd, MatrixXi, VectorXd, VectorXd> TransitsProperties(
        int Nt,
        int Npl,
        VectorXi BeginIdx,
        VectorXi EndIdx,
        VectorXd t,
        VectorXd tT,
        MatrixXd TTV,
        VectorXd P,
        VectorXd n,
        MatrixXd da_oa,
        VectorXd aor,
        MatrixXcd zT,
        MatrixXcd uT,
        VectorXd ror,
        bool CalcDurations
);
std::tuple<MatrixXcd, MatrixXcd, MatrixXcd, MatrixXd, MatrixXcd, MatrixXcd, MatrixXcd, MatrixXd> ResonantInteractions2(
        VectorXd P,
        VectorXd mu,
        MatrixXd Lambda,
        MatrixXcd free_all,
        MatrixXcd free_i,
        VectorXd tT,
        VectorXi BeginIdx,
        VectorXi EndIdx,
        int ExtraIters,
        VectorXd OrdersAll,
        bool Calculate3PlanetsCrossTerms,
        int MaxPower
);
class AnalyticLC {
    private:
		LaplaceCoeff lc;

		//calculate the interaction matrix that will describe the equation dz/dt=Az
		//where z is a vector of the complex eccentricities and for the similar
		//equation for I*exp(i*Omega) using the matrix BB

    public:
        PlanetSystem system;
        // semi-major-axis in stellar radius of innermost planet 
	    double aor;
        ArrayXd aor_values;
        // limb darkening coefficient
	    double u1; 
        // limb darkening coefficient 
	    double u2;
        // time vector to calculate light curve model at 
	    ArrayXd t; 
	    //rough time steps for performing the binning to construct the light curve. Each cadence type needs a BinningTime.
	    ArrayXd BinningTime;
	    //cadence type should label each time stamp. values must be consecutive natural integers: 1, 2,...
	    ArrayXd CadenceType;
	    //maximal TTV amplitude allowed for each planet.
    	ArrayXd MaxTTV; 
	    //times to evaluate radial velocity at
	    ArrayXd tRV;
	    //extra iterations for the TTV calculations
	    size_t ExtraIters = 0;
	    //orders for calculating the forced elements; see function ForcedElements1.
	    ArrayXd OrdersAll;
	    //Should the 3-planets cross terms be incorporated.
	    bool Calculate3PlanetsCrossTerms = false;
	    //maximal (joint) power of eccentricities and inclinations used in the expansion
	    size_t MaxPower = 4;

        AnalyticLC(VectorXd P, VectorXd Tmid0, VectorXd ror, VectorXd aor, VectorXd mu, VectorXd ex0, VectorXd ey0, VectorXd I0, VectorXd Omega0, VectorXd t, double u1, double u2, VectorXd BinningTime = VectorXd::Zero(1), VectorXd CadenceType = VectorXd::Zero(0), VectorXd MaxTTV = VectorXd::Zero(0), VectorXd tRV = VectorXd::Zero(1),double ExtraIters=0, VectorXd OrdersAll = VectorXd::Zero(0),bool Calculate3PlanetsCrossTerms = false, int MaxPower = 4, bool OrbitalElementsAsCellArrays = false, double J2=0);
    };


