#include "helper_functions.hpp"
#include <functional>
#include <iostream>
#include <vector>
#include <cmath>
#include <Eigen/Dense>
//#include <Eigen/MatrixFunctions>

#include "octave_convert.hpp"
#include "eigen_helpers.hpp"

double matlabrange(ArrayXd x) {
    return x.maxCoeff() - x.minCoeff();
}

//generate a time axis that includes one point per transit event
/*
std::tuple<ArrayXd, ArrayXd, ArrayXd, ArrayXd> DynamicsTimeAxis(ArrayXd P, ArrayXd Tmid0, ArrayXd t) {
    ArrayXd td;
    ArrayXd EndIdx(P.size());
    ArrayXd BeginIdx(P.size());
    ArrayXd Ntr(P.size());
    for (size_t j = 0; j < P.size(); j++) {
        auto low = t.minCoeff() + fmod(Tmid0[j] - t.minCoeff(), P[j]);
        auto high = t.maxCoeff();
        auto step = P[j];
        ArrayXd td0;
        if (matlabrange(t) > P[j]) {
            td0 = oct_colon_operator(low, high, step);
        } else {
            td0 = t.minCoeff() + fmod(Tmid0[j] - t.minCoeff(), P[j]);
        }
        auto td_old = td;
        td.resize(td.size() + td0.size());
        td << td_old, td0;
        EndIdx[j] = td.size() - 1;
        if (j == 0) {
            BeginIdx[j] = 0;
        } else {
            BeginIdx[j] = EndIdx[j - 1] + 1;
        }
        Ntr[j] = td0.size();
    }
    return std::make_tuple(td, BeginIdx, EndIdx, Ntr);
}
 */
std::tuple<VectorXd, VectorXi, VectorXi, VectorXi> DynamicsTimeAxis(VectorXd P, VectorXd Tmid0, VectorXd t){
    VectorXd td;
    VectorXi EndIdx(P.size());
    VectorXi BeginIdx(P.size());
    VectorXi Ntr(P.size());
    for (size_t j = 0; j < P.size(); j++) {
        auto low = t.minCoeff() + fmod(Tmid0[j] - t.minCoeff(), P[j]);
        auto high = t.maxCoeff();
        auto step = P[j];
        VectorXd td0;
        if (matlabrange(t) > P[j]) {
            td0 = oct_colon_operator(low, high, step);
        } else {
            td0.resize(1);
            td0[0] = t.minCoeff() + fmod(Tmid0[j] - t.minCoeff(), P[j]);
        }
        auto td_old = td;
        td.resize(td.size() + td0.size());
        td << td_old, td0;
        EndIdx[j] = td.size() - 1;
        if (j == 0) {
            BeginIdx[j] = 0;
        } else {
            BeginIdx[j] = EndIdx[j - 1] + 1;
        }
        Ntr[j] = td0.size();
    }
    return std::make_tuple(td, BeginIdx, EndIdx, Ntr);
}
//
//Vector1stOrderDE: - solves the vector equation dx/dt=Ax
//
//inputs: A - matrix of the equation, x0 - x for t=0, t - time axis to
//evaluate values at. derivation is given at:
//https://www.unf.edu/~mzhan/chapter4.pdf
//


MatrixXcd Vector1stOrderDE(MatrixXcd A, VectorXcd x0, VectorXd t) {
    int nt = t.size();

    /*A << std::complex<double>(0, 0.000004727356679), std::complex<double>(0, -0.000002797378491), std::complex<double>(
            0, -0.000000203095033),
            std::complex<double>(0, -0.000000866818343), std::complex<double>(0,
                                                                              0.000003335222453), std::complex<double>(
            0, -0.000001279225773),
            std::complex<double>(0, -0.000000027221282), std::complex<double>(0,
                                                                              -0.000000553324138), std::complex<double>(
            0, 0.000000959778310);
            */
    if (!(A.array().isFinite()).any()) {
        std::cout << "Vector1stOrderDE A is not finite!" << std::endl;
        return MatrixXcd::Ones(1, 1) * NAN;
    }

    // Compute the eigen decomposition
    ComplexEigenSolver<MatrixXcd> eigSolver(A);
    VectorXcd lam = eigSolver.eigenvalues();
    MatrixXcd ev = eigSolver.eigenvectors();

    int nev = lam.size();

    // Compute phi0i
    MatrixXcd phi0i = ev.inverse();

    // Compute evp
    // Create a 3D vector to hold the data
    //std::vector<Eigen::MatrixXcd> evp(nev, Eigen::MatrixXcd::Zero(t.size(), nev));
    std::vector<Eigen::MatrixXcd> evp_1 = reshape(ev, {1, nev,nev});
    std::vector<Eigen::MatrixXcd> evp(nev, Eigen::MatrixXcd::Zero(t.size(), nev));
    MatrixXcd foo;
    for (int i=0; i<nev; i++){
        evp[i] = evp_1[i].replicate(t.size(),1);
        foo = evp[i];
    }

    Eigen::VectorXcd lam_diag = lam;
    Eigen::VectorXcd t_complex = t.cast<std::complex<double>>();
    Eigen::MatrixXcd exp_lamt = multiplyVectorstoMatrix(t_complex,lam); //.transpose();
    for (int i = 0; i < exp_lamt.rows(); ++i) {
        for (int j = 0; j < exp_lamt.cols(); ++j) {
            exp_lamt(i, j) = exp(exp_lamt(i, j));
        }
    }
    exp_lamt = exp_lamt; //.transpose();

    std::vector<Eigen::MatrixXcd> lamt = reshape(exp_lamt, {static_cast<int>(t.size()),1,nev});
    std::vector<Eigen::MatrixXcd> lamt_3d(nev, Eigen::MatrixXcd::Zero(t.size(), nev));
    for (int i = 0; i<nev; ++i) {
        for (int j = 0; j < lamt_3d[i].rows(); ++j) {
            for (int k = 0; k < lamt_3d[i].cols(); ++k) {
                lamt_3d[i](j,k) = lamt[i](j, 0);
            }
        }
    }

    std::vector<Eigen::MatrixXcd> phit = multiplyElementWise(evp,lamt_3d);

    Eigen::MatrixXcd phi0x0 = phi0i * x0;
    Eigen::MatrixXcd reshaped_phit = reshape(phit, {nt*nev,nev});
    Eigen::MatrixXcd new_phi = reshaped_phit * phi0x0;
    Eigen::MatrixXcd xt = reshape(new_phi,nt,nev);

    return xt;
}



std::tuple<double, double, double, double> fitLinear(VectorXd x, VectorXd y, VectorXd dy) {
    if (dy.size() == 0) {
        dy = VectorXd::Ones(y.size());
    }

    int n = x.size();

    double S = 0.0, Sx = 0.0, Sy = 0.0, Sxx = 0.0, Sxy = 0.0, D = 0.0;

    for (int i = 0; i < n; ++i) {
        double weight = 1.0 / (dy[i] * dy[i]);
        S += weight;
        Sx += x[i] * weight;
        Sy += y[i] * weight;
        Sxx += x[i] * x[i] * weight;
        Sxy += x[i] * y[i] * weight;
    }

    D = S * Sxx - Sx * Sx;
    double a = (S * Sxy - Sx * Sy) / D;
    double b = (Sxx * Sy - Sxy * Sx) / D;

    double sumSquaredResiduals = 0.0;
    for (int i = 0; i < n; ++i) {
        double Ytheo = a * x[i] + b;
        sumSquaredResiduals += (Ytheo - y[i]) * (Ytheo - y[i]);
    }

    double meanSquaredResiduals = sumSquaredResiduals / (n - 2);
    double S2 = 0.0, Sx2 = 0.0;

    for (int i = 0; i < n; ++i) {
        S2 += 1.0 / (dy[i] * dy[i]);
        Sx2 += x[i] * x[i] / (dy[i] * dy[i]);
    }

    D = S2 * Sx2 - Sx * Sx;
    double da = sqrt(S2 / D * meanSquaredResiduals);
    double db = sqrt(Sx2 / D * meanSquaredResiduals);

    return std::make_tuple(a, b, da, db);
}

double calculateMedian(const VectorXd &vec) {
    // Copy the vector to sort it
    VectorXd sortedVec = vec;
    std::sort(sortedVec.data(), sortedVec.data() + sortedVec.size());

    int n = sortedVec.size();

    if (n % 2 == 0) {
        // If the number of elements is even, return the average of the two middle values
        int midIndex1 = n / 2 - 1;
        int midIndex2 = n / 2;
        return (sortedVec(midIndex1) + sortedVec(midIndex2)) / 2.0;
    } else {
        // If the number of elements is odd, return the middle value
        int midIndex = n / 2;
        return sortedVec(midIndex);
    }
}

std::tuple<VectorXd, VectorXi> AssociateTmid(VectorXd t, VectorXd Tmid, double P) {
    // AssociateTmid: Associate a time-of-mid-transit to each time stamp t.
    //
    // Inputs: t - list of times
    //           Tmid - list of times of mid transit
    //           P - orbital period (optional input)



    if (P == 0.0) {
        VectorXd diffs = Tmid.tail(Tmid.size() - 1) - Tmid.head(Tmid.size() - 1);
        P = calculateMedian(diffs);
    }

    VectorXi Tr_Ind = VectorXi::Zero(Tmid.size());
    for (int i = 0; i < Tmid.size(); i++) {
        Tr_Ind[i] = round((Tmid[i] - Tmid[0]) / P);
    }

    VectorXi AssociatedInd = VectorXi::Zero(t.size());
    // transit indices - the first transit in the data is one, and going up TODO:check if it really is one

    for (int i = 0; i < t.size(); i++) {
        AssociatedInd[i] = round((t[i] - Tmid[0]) / P);
    }

    //associate an index of transit per time stamp. Any event before the first
    // Tmid will be associated with the first Tmid. Any event after the last Tmid
    // will be associated with the last Tmid.
    for (int i = 0; i < AssociatedInd.size(); i++) {
        if (AssociatedInd[i] < 0) {
            AssociatedInd[i] = 0;
        }
        if (AssociatedInd[i] > Tr_Ind[Tr_Ind.size() - 1]) {
            AssociatedInd[i] = Tr_Ind[Tr_Ind.size() - 1];
        }
    }

    VectorXd Tmid_Of_Tr_Ind = VectorXd::Zero(Tr_Ind.maxCoeff() + 1);
    //Tmid_Of_Tr_Ind(Tr_Ind) = Tmid; //TODO: Kaputt
    for (int i = 0; i < Tr_Ind.size(); i++) {
        Tmid_Of_Tr_Ind[Tr_Ind[i]] = Tmid[i];
    }
    // Associated transit index
    VectorXd AssociatedTmid = VectorXd::Zero(t.size());
    //for (int i = 0; i < t.size(); i++) {
    //    AssociatedTmid[i] = Tmid_Of_Tr_Ind[AssociatedInd[i]];
    //}
    AssociatedTmid = Tmid_Of_Tr_Ind(AssociatedInd);

    VectorXd ret1 = AssociatedTmid;
    VectorXi ret2 = AssociatedInd;
    auto ret = std::make_tuple(ret1, ret2);
    return ret;
}

//Associate tmid with Tmid only double
std::tuple<VectorXd, VectorXi> AssociateTmid(VectorXd t, double Tmid, double P) {
    // AssociateTmid: Associate a time-of-mid-transit to each time stamp t.


    VectorXi Tr_Ind = VectorXi::Zero(1);
    Tr_Ind[0] = 0;

    VectorXi AssociatedInd = VectorXi::Zero(t.size());
    // transit indices - the first transit in the data is one, and going up TODO:check if it really is one

    for (int i = 0; i < t.size(); i++) {
        AssociatedInd[i] = round((t[i] - Tmid) / P);
    }

    //associate an index of transit per time stamp. Any event before the first
    // Tmid will be associated with the first Tmid. Any event after the last Tmid
    // will be associated with the last Tmid.

    for (int i = 0; i < AssociatedInd.size(); i++) {
        if (AssociatedInd[i] < 0) {
            AssociatedInd[i] = 0;
        }
        if (AssociatedInd[i] > Tr_Ind[Tr_Ind.size() - 1]) {
            AssociatedInd[i] = Tr_Ind[Tr_Ind.size() - 1];
        }
    }

    VectorXd Tmid_Of_Tr_Ind = VectorXd::Zero(Tr_Ind.maxCoeff() + 1);

    for (int i = 0; i < Tr_Ind.size(); i++) {
        Tmid_Of_Tr_Ind[Tr_Ind[i]] = Tmid;
    }

    VectorXd AssociatedTmid = VectorXd::Zero(t.size());

    AssociatedTmid = Tmid_Of_Tr_Ind(AssociatedInd);

    return std::make_tuple(AssociatedTmid, AssociatedInd);
}