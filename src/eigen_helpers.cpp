#include "eigen_helpers.hpp"
#include <complex>
#include <string>

typedef std::complex<double> Complexd;

std::vector<Eigen::MatrixXcd> reshape(const Eigen::MatrixXcd& input, const std::vector<int>& new_shape) {
    // Calculate the total number of elements in the input matrix
    int total_elements = input.rows() * input.cols();

    // Calculate the total number of elements in the desired shape
    int new_total_elements = 1;
    for (int dim : new_shape) {
        new_total_elements *= dim;
    }

    // Check if the total number of elements matches
    if (total_elements != new_total_elements) {
        throw std::invalid_argument("Total number of elements must remain unchanged after reshape.");
    }

    // Reshape the input matrix
    if (new_shape.size()==3){
        std::vector<Eigen::MatrixXcd> result(new_shape[2], Eigen::MatrixXcd::Zero(new_shape[0],new_shape[1]));
        int idx_row = 0;
        int idx_col = 0;
        for (int outer = 0; outer < new_shape[2]; ++outer){
            for (int cols = 0; cols< new_shape[1]; ++cols){
                for (int rows = 0; rows < new_shape[0]; ++rows){
                    result[outer](rows,cols) = input(idx_row,idx_col);
                    ++idx_row;
                    if (idx_row>=input.rows()){
                        idx_row = 0;
                        ++idx_col;
                    }
                }
            }
        }
        return result;
    }
    else{
        throw std::invalid_argument("Reshape only implemented for 3 dimensional.");
    }
}

Eigen::MatrixXcd reshape(std::vector<Eigen::MatrixXcd> input, const std::vector<int>& new_shape) {
    // Calculate the total number of elements in the input matrix
    int total_elements = 0;
    for (auto& mat : input) {
        total_elements += mat.rows() * mat.cols();
    }

    // Calculate the total number of elements in the desired shape
    int new_total_elements = 1;
    for (int dim : new_shape) {
        new_total_elements *= dim;
    }

    // Check if the total number of elements matches
    if (total_elements != new_total_elements) {
        throw std::invalid_argument("Total number of elements must remain unchanged after reshape.");
    }

    // Reshape the input matrix
    if (new_shape.size()==2){
        Eigen::MatrixXcd result(new_shape[0],new_shape[1]);
        int idx_row = 0;
        int idx_col = 0;
        for (int i = 0; i < input.size(); ++i) {
            for (int cols = 0; cols < input[i].cols(); ++cols) {
                for (int rows = 0; rows < input[i].rows(); ++rows) {
                    result(idx_row, idx_col) = input[i](rows, cols);
                    ++idx_row;
                    if (idx_row >= result.rows()) {
                        idx_row = 0;
                        ++idx_col;
                    }
                }
            }
        }

        return result;
    }
    else{
        throw std::invalid_argument("Reshape only implemented for 2 dimensional.");
    }
}

Eigen::MatrixXcd reshape(Eigen::MatrixXcd input, int new_rows, int new_cols) {
    // Calculate the total number of elements in the input matrix
    int total_elements = input.rows() * input.cols();

    // Calculate the total number of elements in the desired shape
    int new_total_elements = new_rows * new_cols;

    // Check if the total number of elements matches
    if (total_elements != new_total_elements) {
        throw std::invalid_argument("Total number of elements must remain unchanged after reshape.");
    }

    // Reshape the input matrix

        Eigen::MatrixXcd result(new_rows,new_cols);
        int idx_row = 0;
        int idx_col = 0;
        for (int cols = 0; cols < input.cols(); ++cols) {
            for (int rows = 0; rows < input.rows(); ++rows) {
                result(idx_row, idx_col) = input(rows, cols);
                ++idx_row;
                if (idx_row >= result.rows()) {
                    idx_row = 0;
                    ++idx_col;
                }
            }
        }

        return result;

}

std::vector<Eigen::MatrixXcd> multiplyElementWise(const std::vector<Eigen::MatrixXcd>& vec1, const std::vector<Eigen::MatrixXcd>& vec2) {
    if (vec1.size() != vec2.size()) {
        throw std::invalid_argument("Vectors must be the same size for element-wise multiplication");
    }

    std::vector<Eigen::MatrixXcd> result(vec1.size());
    for (size_t i = 0; i < vec1.size(); ++i) {
        if (vec1[i].rows() != vec2[i].rows() || vec1[i].cols() != vec2[i].cols()) {
            throw std::invalid_argument("Matrices must be the same size for element-wise multiplication");
        }
        result[i] = vec1[i].cwiseProduct(vec2[i]);
    }

    return result;
}

std::string to_string(const std::complex<double> &c) {
    return std::to_string(c.real()) + " + " + std::to_string(c.imag()) + "i";
}

std::string alc::to_string(const Eigen::VectorXd &v) {
    std::string result = "[";
    for (int i = 0; i < v.size(); ++i) {
        result += std::to_string(v(i));
        if (i < v.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string alc::to_string(const Eigen::VectorXi &v) {
    std::string result = "[";
    for (int i = 0; i < v.size(); ++i) {
        result += std::to_string(v(i));
        if (i < v.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string to_string(const Eigen::ArrayXd &v) {
    std::string result = "[";
    for (int i = 0; i < v.size(); ++i) {
        result += std::to_string(v(i));
        if (i < v.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string to_string(const Eigen::ArrayXi &v) {
    std::string result = "[";
    for (int i = 0; i < v.size(); ++i) {
        result += std::to_string(v(i));
        if (i < v.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string to_string(const Eigen::MatrixXd &v) {
    std::string result = "[";
    for (int i = 0; i < v.rows(); ++i) {
        result += "[";
        for (int j = 0; j < v.cols(); ++j) {
            result += std::to_string(v(i, j));
            if (j < v.cols() - 1) {
                result += ", ";
            }
        }
        result += "]";
        if (i < v.rows() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string to_string(const Eigen::MatrixXi &v) {
    std::string result = "[";
    for (int i = 0; i < v.rows(); ++i) {
        result += "[";
        for (int j = 0; j < v.cols(); ++j) {
            result += std::to_string(v(i, j));
            if (j < v.cols() - 1) {
                result += ", ";
            }
        }
        result += "]";
        if (i < v.rows() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::VectorXd &v1, const Eigen::VectorXd &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.size(); ++i) {
        result += std::to_string(v1(i) - v2(i));
        if (i < v1.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::VectorXi &v1, const Eigen::VectorXi &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.size(); ++i) {
        result += std::to_string(v1(i) - v2(i));
        if (i < v1.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::ArrayXd &v1, const Eigen::ArrayXd &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.size(); ++i) {
        result += std::to_string(v1(i) - v2(i));
        if (i < v1.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::ArrayXi &v1, const Eigen::ArrayXi &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.size(); ++i) {
        result += std::to_string(v1(i) - v2(i));
        if (i < v1.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::MatrixXd &v1, const Eigen::MatrixXd &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.rows(); ++i) {
        result += "[";
        for (int j = 0; j < v1.cols(); ++j) {
            result += std::to_string(v1(i, j) - v2(i, j));
            if (j < v1.cols() - 1) {
                result += ", ";
            }
        }
        result += "]";
        if (i < v1.rows() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::MatrixXi &v1, const Eigen::MatrixXi &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.rows(); ++i) {
        result += "[";
        for (int j = 0; j < v1.cols(); ++j) {
            result += std::to_string(v1(i, j) - v2(i, j));
            if (j < v1.cols() - 1) {
                result += ", ";
            }
        }
        result += "]";
        if (i < v1.rows() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::VectorXcd &v1, const Eigen::VectorXcd &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.size(); ++i) {
        result += to_string(v1(i) - v2(i));
        if (i < v1.size() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

std::string print_diff(const Eigen::MatrixXcd &v1, const Eigen::MatrixXcd &v2) {
    std::string result = "[";
    for (int i = 0; i < v1.rows(); ++i) {
        result += "[";
        for (int j = 0; j < v1.cols(); ++j) {
            result += to_string(v1(i, j) - v2(i, j));
            if (j < v1.cols() - 1) {
                result += ", ";
            }
        }
        result += "]";
        if (i < v1.rows() - 1) {
            result += ", ";
        }
    }
    result += "]";
    return result;
}

