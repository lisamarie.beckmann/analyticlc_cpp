#pragma once

#include "octave_convert.hpp"
#include <NumericalIntegration.h>
#include <fstream>

template<typename Scalar>
class LaplaceFunctor
{
public:
    Scalar j1;
    Scalar s1;
    Scalar alph1;
    Scalar operator()(const Scalar phi) const
    {
        //laplace_integrand=@(j1,s1,alph1) (@(phi) (cos(j1.*phi)./(1-2.*alph1.*cos(phi)+alph1.^2).^s1));
        return cos(j1*phi)/pow((1-2*alph1*cos(phi)+pow(alph1, 2)), s1);
    }
};

// LaplaceCoeff: get a Laplace coefficient (definition: Solar System
// Dynamics, eq. 6.67)
//
//Inputs: 'init' - calculate a table of the laplace coefficients and save it
//           in ~/Matlab/Data/LaplaceCoeffs.mat
//           'load' = return all the table and the row/col values in a
//           structure
//           alph,s,j = return the laplace coefficient for the given alph,s,j.
//           alph can be a vector
//           alph,s,j,D = return the D order derivative of the laplace
//           coefficient for the given alph, s, j.
//
class LaplaceCoeff {
    struct CacheItem {
        double j;
        double s;
        double alph;
        double ret;
    };
    struct CacheItems {
        char magic[4] = {'L','A','C','F'};
        unsigned int count;
        std::vector<CacheItem> items;

        void fromCache(const std::map<std::tuple<double, double, double>, double> &cache);
        void toCache(std::map<std::tuple<double, double, double>, double> &cache);
    };

    std::map<std::tuple<double, double, double>, double> cache;
    private:
        typedef double Scalar;
        LaplaceFunctor<Scalar> inFctr;
        Eigen::Integrator<Scalar> eigIntgtor;
        Eigen::Integrator<Scalar>::QuadratureRule quadratureRule;
        double integrate(double j, double s, double alph, double min, double max);
        VectorXd integrateVec(double j, double s, VectorXd alph, double min, double max);

public:
        double AbsTol = 1e-10;
        double RelTol = 1e-6;
        void saveCacheToFile(const std::string &filename);
        void loadCacheFromFile(const std::string &filename);
        double calculateSingleCoeff(double j, double s, double alph, size_t D);
        LaplaceCoeff(size_t Nj = 0, size_t Ns = 0, size_t ND = 0, double dalpha = 0, size_t subdivisions = 200, Eigen::Integrator<Scalar>::QuadratureRule quadrule = Eigen::Integrator<Scalar>::GaussKronrod81);
        double calculate(double alph, double s, double j, size_t D = 0);
        VectorXd calculate(VectorXd alph, double s, double j, size_t D = 0);
        VectorXd calculate(double alph, VectorXd s, double j, size_t D);
        VectorXd calculate(double alph, double s, VectorXd j, size_t D);
        VectorXd calculate(double alph, VectorXd s, VectorXd j, VectorXi D);
        VectorXd calculate(double alph, double s, VectorXi j, size_t D);


    };