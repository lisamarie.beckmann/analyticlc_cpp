#pragma once

#include <Eigen/Core>
#include <tuple>
#include <vector>
#include <complex>

using namespace std::complex_literals;
using namespace Eigen;

/*class DynamicsTimeAxis {
    public:
	    ArrayXd td;
	    std::vector<size_t> BeginIdx;
	    std::vector<size_t> EndIdx;
	    std::vector<size_t> Ntr;
        //generate a time axis that includes one point per transit event
        DynamicsTimeAxis(ArrayXd P, ArrayXd Tmid0, ArrayXd t);
};
 */
//std::tuple<ArrayXd, ArrayXd, ArrayXd, ArrayXd> DynamicsTimeAxis(ArrayXd P, ArrayXd Tmid0, ArrayXd t);
std::tuple<VectorXd, VectorXi, VectorXi, VectorXi> DynamicsTimeAxis(VectorXd P, VectorXd Tmid0, VectorXd t);

std::tuple<VectorXd, VectorXi> AssociateTmid(VectorXd t, VectorXd Tmid, double P = 0.0);

std::tuple<VectorXd, VectorXi> AssociateTmid(VectorXd t, double Tmid, double P = 0.0);

double matlabrange(ArrayXd x);

//VectorXcd Vector1stOrderDE(MatrixXcd A, VectorXcd x0, ArrayXd t);
MatrixXcd Vector1stOrderDE(MatrixXcd A, VectorXcd x0, VectorXd t);
double calculateMedian(const VectorXd &vec);

std::tuple<double, double, double, double> fitLinear(VectorXd x, VectorXd y, VectorXd dy = VectorXd(0));
