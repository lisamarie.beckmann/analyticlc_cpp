#include "laplacecoeffs.hpp"
#include <fstream>

LaplaceCoeff::LaplaceCoeff(size_t Nj, size_t Ns, size_t ND, double dalpha, size_t subdivisions, Eigen::Integrator<Scalar>::QuadratureRule quadrule) :
    eigIntgtor(subdivisions),
    quadratureRule(quadrule)
{
}

double LaplaceCoeff::integrate(double j, double s, double alph, double min, double max) {
    inFctr.j1 = j;
    inFctr.s1 = s;
    inFctr.alph1 = alph;

    Scalar desAbsErr = AbsTol;
    Scalar desRelErr = RelTol;

    return eigIntgtor.quadratureAdaptive(inFctr, min, max, desAbsErr, desRelErr, quadratureRule);
}

VectorXd LaplaceCoeff::integrateVec(double j, double s, VectorXd alph, double min, double max) {
    VectorXd ret;
    for(auto alpha : alph) {
        ret << integrate(j,s,alpha,min,max);
    }
    return ret;
}


double LaplaceCoeff::calculateSingleCoeff(double j, double s, double alph, size_t D) {

    if(D==0) {
        auto key = std::make_tuple(j, s, alph);

        if(cache.find(key) != cache.end()) {
            return cache[key];
        }

        double ret = integrate(j,s,alph,0,2*M_PI);

        cache[key] = 1 / M_PI * ret;


        return 1/M_PI * ret;
    } else if (D==1) {
        return s*(calculateSingleCoeff(j-1,s+1,alph,D-1)
            -2*alph*calculateSingleCoeff(j,s+1,alph,D-1)
            +calculateSingleCoeff(j+1,s+1,alph,D-1));
    }
    return s*(calculateSingleCoeff(j-1,s+1,alph,D-1)
            -2*alph*calculateSingleCoeff(j,s+1,alph,D-1)
            +calculateSingleCoeff(j+1,s+1,alph,D-1)
            -2*(D-1)*calculateSingleCoeff(j,s+1,alph,D-2));
}
void LaplaceCoeff::CacheItems::fromCache(const std::map<std::tuple<double, double, double>, double> &cache) {
    count = cache.size();
    items.clear();
    for(auto &[key, value] : cache) {
        CacheItem ci;
        ci.j = std::get<0>(key);
        ci.s = std::get<1>(key);
        ci.alph = std::get<2>(key);
        ci.ret = value;
        items.push_back(ci);
    }
}

void LaplaceCoeff::CacheItems::toCache(std::map<std::tuple<double, double, double>, double> &cache) {
    cache.clear();
    for(auto &item : items) {
        cache.insert({std::make_tuple(item.j, item.s, item.alph), item.ret});
    }
}

void LaplaceCoeff::saveCacheToFile(const std::string &filename) {
    CacheItems cis;
    cis.fromCache(cache);
    std::ofstream file(filename, std::ios::binary);
    file.write((char*)&cis.magic, sizeof(cis.magic));
    file.write((char*)&cis.count, sizeof(cis.count));
    for(auto &item : cis.items) {
        file.write((char*)&item, sizeof(item));
    }
    file.close();
}
void LaplaceCoeff::loadCacheFromFile(const std::string &filename) {
    CacheItems cis;
    std::ifstream file(filename, std::ios::binary | std::ios::in);
    if(!file.is_open()) {
        return;
    }
    char magic[4];
    file.read((char*)&magic, sizeof(magic));
    if(magic[0] != 'L' || magic[1] != 'A' || magic[2] != 'C' || magic[3] != 'F') {
        return;
    }
    unsigned int count;
    file.read((char*)&count, sizeof(count));


    for(unsigned int i = 0; i < count; ++i) {
        CacheItem ci;
        file.read((char*)&ci, sizeof(ci));
        cis.items.push_back(ci);
    }
    cis.count = count;
    assert (cis.items.size() == count);
    file.close();
    cis.toCache(cache);
}

double LaplaceCoeff::calculate(double alph, double s, double j, size_t D) {
    return calculateSingleCoeff(j,s,alph,D);
}

VectorXd LaplaceCoeff::calculate(VectorXd alph, double s, double j, size_t D) {
    VectorXd ret(alph.size());
    for (size_t i = 0; i < alph.size(); ++i) {
        ret(i) = calculate(alph[i], s, j, D);
    }
    return ret;
}

VectorXd LaplaceCoeff::calculate(double alph, VectorXd s, double j, size_t D) {
    VectorXd ret(s.size());
    for (size_t i = 0; i < s.size(); ++i) {
        ret(i) = calculate(alph, s[i], j, D);
    }
    return ret;
}

VectorXd LaplaceCoeff::calculate(double alph, double s, VectorXd j, size_t D) {
    VectorXd ret(j.size());
    for (size_t i = 0; i < j.size(); ++i) {
        ret(i) = calculate(alph, s, j[i], D);
    }
    return ret;
}
VectorXd LaplaceCoeff::calculate(double alph, double s, VectorXi j, size_t D) {
    VectorXd ret(j.size());
    for (size_t i = 0; i < j.size(); ++i) {
        double jdouble = j[i];
        ret(i) = calculate(alph, s, jdouble, D);
    }
    return ret;
}


VectorXd LaplaceCoeff::calculate(double alph, VectorXd s, VectorXd j, VectorXi D){
    VectorXd ret(s.size());
    for (size_t i=0; i< s.size(); ++i){
        ret(i) = calculate(alph, s[i], j[i], D[i]);
    }
    return ret;
}