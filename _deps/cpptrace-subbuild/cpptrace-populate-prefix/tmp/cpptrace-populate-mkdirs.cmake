# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-src"
  "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-build"
  "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-subbuild/cpptrace-populate-prefix"
  "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-subbuild/cpptrace-populate-prefix/tmp"
  "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-subbuild/cpptrace-populate-prefix/src/cpptrace-populate-stamp"
  "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-subbuild/cpptrace-populate-prefix/src"
  "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-subbuild/cpptrace-populate-prefix/src/cpptrace-populate-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-subbuild/cpptrace-populate-prefix/src/cpptrace-populate-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "/home/lisa/Master/analyticlc_cpp/_deps/cpptrace-subbuild/cpptrace-populate-prefix/src/cpptrace-populate-stamp${cfgdir}") # cfgdir has leading slash
endif()
