#include <iostream>
#include "analyticlc.hpp"
#include "helper_functions.hpp"
#include "FluxCalculation/ellipticIntegral.hpp"
#include "FluxCalculation/Flux_rc.hpp"
#include "FluxCalculation/Flux_rj.hpp"
#include "FluxCalculation/FluxCalculation.hpp"
#include "AnalyticLc/OrbitalElements2TransitParams.h"
#include "AnalyticLc/ForcedElements1.h"
#include "laplacecoeffs.hpp"
#include <complex>
#include "eigen_helpers.hpp"
#include <fstream>
#include <catch2/catch_test_macros.hpp> // Include the Catch2 header
#include <Eigen/src/Core/Array.h>
#include <chrono>

#include "json_helpers.h"
#include "json_test_helper.h"
#include <iostream>
#include <filesystem>
#include <fstream>

namespace fs = std::filesystem;

using namespace std::complex_literals;
using json = nlohmann::json;

//call all of TEST_CASE with different filenames

std::vector<std::string> findFilesInSubfolders(const std::string& root, const std::string& filename) {
    std::vector<std::string> filepaths;
    for (const auto& entry : fs::recursive_directory_iterator(root)) {
        if (entry.is_regular_file() && entry.path().filename() == filename) {
            filepaths.push_back(entry.path().string());
        }
    }
    return filepaths;
}


TEST_CASE("AnalyticLC func works", "[AnalyticLC_tests]") {
    const std::string root = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/tests/";
    const std::string file = "AnalyticLC.json";

    std::vector<std::string> filepaths = findFilesInSubfolders(root, file);
    REQUIRE(!filepaths.empty()); // Ensure at least one file is found

    for (const auto& filename : filepaths) {
        DYNAMIC_SECTION("Testing file: " << filename) {
            UNSCOPED_INFO("Testing file: " << filename);
            json jsonData = loadJsonFromFile(filename);
            //Access specific function data
            FunctionData functionIn = {
                    jsonData["AnalyticLC_in"],
                    jsonData["AnalyticLC_out"]
            };
            //Access function input and output data
            json input = functionIn.input;
            json expectedOutput = functionIn.output;

            AnalyticLCparams inputparams;
            from_json(input, inputparams);
            AnalyticLCout expectedOutputparams;
            from_json(expectedOutput, expectedOutputparams);
            inputparams.CalcAstrometry = true;
            inputparams.CalcDurations = true;
            double rel_err = 1e-5;
            double abs_err = 1e-5;
            auto [calc_out, O] = AnalyticLCfunc(inputparams);

            UNSCOPED_INFO("calculated LC" << calc_out.LC);
            UNSCOPED_INFO("expected LC" << expectedOutputparams.LC);
            //REQUIRE(expectedOutputparams.LC == calc_out.LC);
            REQUIRE(expectedOutputparams.LC.isApprox(calc_out.LC, rel_err));
            UNSCOPED_INFO("calculated RV_o_R" << calc_out.RV_o_r);
            UNSCOPED_INFO("expected RV_o_R" << expectedOutputparams.RV_o_r);
            REQUIRE(allclose(expectedOutputparams.RV_o_r, calc_out.RV_o_r, rel_err, abs_err));

            //REQUIRE(expectedOutputparams.RV_o_r.isApprox(calc_out.RV_o_r, 1e-4));
            UNSCOPED_INFO("calculated Y_o_r" << calc_out.Y_o_r);
            UNSCOPED_INFO("expected Y_o_r" << expectedOutputparams.Y_o_r);
            //REQUIRE(allclose(expectedOutputparams.Y_o_r, calc_out.Y_o_r, rel_err, abs_err));
            //REQUIRE(expectedOutputparams.Y_o_r.isApprox(calc_out.Y_o_r, 1e-4));
            //REQUIRE(expectedOutputparams.Y_o_r == calc_out.Y_o_r);
            UNSCOPED_INFO("calculated Z_o_r" << calc_out.Z_o_r);
            UNSCOPED_INFO("expected Z_o_r" << expectedOutputparams.Z_o_r);
            //REQUIRE(allclose(expectedOutputparams.Z_o_r, calc_out.Z_o_r, rel_err, abs_err));
            //REQUIRE(expectedOutputparams.Z_o_r.isApprox(calc_out.Z_o_r, 1e-3));
            //REQUIRE(expectedOutputparams.Z_o_r == calc_out.Z_o_r);
            ArrayXd OutputtT = jsonToEigenArray(expectedOutput["O"]["tT"]);
            //REQUIRE(allclose(OutputtT.array(), O.tT.array(), rel_err, abs_err));
            ArrayXd OutputtRV = jsonToEigenArray(expectedOutput["O"]["tRV"]);
            //REQUIRE(allclose(OutputtRV.array(), O.tRV.array(), rel_err, abs_err));

            alc::OutputList expected_O = expectedOutput["O"].get<alc::OutputList>();

            /*CHECK(allclose(expected_O.tT, O.tT, rel_err, abs_err));
            CHECK(allclose(expected_O.tRV, O.tRV, rel_err, abs_err));
            //CHECK(allclose(expected_O.zT.real(), O.zT.real(), rel_err, abs_err));
            CHECK(allclose(expected_O.zRV.real(), O.zRV.real(), rel_err, abs_err));
            CHECK(allclose(expected_O.uT.real(), O.uT.real(), rel_err, abs_err));
            CHECK(allclose(expected_O.uRV.real(), O.uRV.real(), rel_err, abs_err));
            CHECK(allclose(expected_O.D, O.D, rel_err, abs_err));
            CHECK(allclose(expected_O.Tau, O.Tau, rel_err, abs_err));
            CHECK(allclose(expected_O.w, O.w, rel_err, abs_err));
            CHECK(allclose(expected_O.d, O.d, rel_err, abs_err));
            CHECK(allclose(expected_O.b, O.b, rel_err, abs_err));
            //CHECK(allclose(expected_O.AssociatedTmid, O.AssociatedTmid, rel_err, abs_err));
            //REQUIRE(allclose(expected_O.AssociatedInd, O.AssociatedInd, rel_err, abs_err));
            CHECK(allclose(expected_O.AllLC, O.AllLC, rel_err, abs_err));
            //REQUIRE(allclose(expected_O.TmidActualCell, O.TmidActualCell, rel_err, abs_err));
            CHECK(allclose(expected_O.free_e_T.real(), O.free_e_T.real(), rel_err, abs_err));
            CHECK(allclose(expected_O.free_I_T.real(), O.free_I_T.real(), rel_err, abs_err));
            CHECK(allclose(expected_O.free_e_RV.real(), O.free_e_RV.real(), rel_err, abs_err));
            CHECK(allclose(expected_O.free_I_RV.real(), O.free_I_RV.real(), rel_err, abs_err));
            //REQUIRE(allclose(expected_O.TTVCell, O.TTVCell, rel_err, abs_err));
            //REQUIRE(allclose(expected_O.dzCell, O.dzCell, rel_err, abs_err));
            //REQUIRE(allclose(expected_O.duCell, O.duCell, rel_err, abs_err));
            //REQUIRE(allclose(expected_O.dLambdaCell, O.dLambdaCell, rel_err, abs_err));
            //REQUIRE(allclose(expected_O.zfreeCell, O.zfreeCell, rel_err, abs_err));
            CHECK(allclose(expected_O.dbdt, O.dbdt, rel_err, abs_err));
            CHECK(allclose(expected_O.b0, O.b0, rel_err, abs_err));
            CHECK(allclose(expected_O.Lambda_T, O.Lambda_T, rel_err, abs_err));
            CHECK(allclose(expected_O.P, O.P, rel_err, abs_err));
            CHECK(allclose(expected_O.BeginIdx, O.BeginIdx, rel_err, abs_err));
            CHECK(allclose(expected_O.EndIdx, O.EndIdx, rel_err, abs_err));
            CHECK(allclose(expected_O.Ntr, O.Ntr, rel_err, abs_err));
*/
        }
    }
}
/*
TEST_CASE("fast_test_python", "[python]"){
    INFO("1");
    VectorXd P(2);
    P << 12.3456, 29.3;
    VectorXd Tmid0(2);
    Tmid0 << 9.8765432, 15.4029;
    VectorXd ror(2);
    ror << 0.021, 0.027;
    VectorXd mu(2);
    mu << 1.24e-5, 3e-5;
    VectorXd ex0(2);
    ex0 << 0.01234, 0.00998;
    VectorXd ey0(2);
    ey0 << 0.02345, 0.0189;
    VectorXd I0(2);
    I0 << 0.01745329, 0.0203;
    VectorXd Omega0(2);
    Omega0 << 0.4014257, 0.369;
    VectorXd aor(1);
    aor << 30;
    double u1 = 0.5;
    double u2 = 0.3;

    VectorXd t = VectorXd::LinSpaced((100/2)+1, 0, 100);


    alc::AnalyticLCparams params;
    INFO("2");
    //ctorXd &I0, const VectorXd &Omega0, const VectorXd &t, double u1, double u2){
    alc::construct(params, P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2);
    INFO("3");
    auto [out1, out2] = AnalyticLCfunc(params);

    UNSCOPED_INFO("calculated LC" << out1.LC);
    //UNSCOPED_INFO(params.P);
    REQUIRE(0==1);
}
 */

TEST_CASE("ellecVec works", "[ellecVec_tests]") {
    // Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/FluxCalculation.json";
    json jsonData = loadJsonFromFile(filename);

    // Access specific function data
    FunctionData functionIn = {
            jsonData["ellecVec_in"],
            jsonData["ellecVec_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;

    Eigen::ArrayXd k = jsonToEigenArray(input["k"]);

    VectorXd ellecout = ellecVec(k);

    Eigen::ArrayXd expectedellec = jsonToEigenArray(expectedOutput["ellec"]);
    UNSCOPED_INFO("calc ellec " << ellecout);
    UNSCOPED_INFO("expected ellec " << expectedellec);
    REQUIRE(expectedellec.isApprox(ellecout.array(), 0.00001));
}

TEST_CASE("ellkVec works", "[ellkVec_tests]") {
    // Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/FluxCalculation.json";
    json jsonData = loadJsonFromFile(filename);

    // Access specific function data
    FunctionData functionIn = {
            jsonData["ellkVec_in"],
            jsonData["ellkVec_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;

    Eigen::ArrayXd k = jsonToEigenArray(input["k"]);

    VectorXd ellkout = ellkVec(k);

    Eigen::ArrayXd expectedellk = jsonToEigenArray(expectedOutput["ellk"]);
    UNSCOPED_INFO("calc ellk " << ellkout);
    UNSCOPED_INFO("expected ellk " << expectedellk);
    REQUIRE(expectedellk.isApprox(ellkout.array(), 0.00001));
}

TEST_CASE("DynamicsTimeAxis works", "[Dynamics_Tests]") {
    //Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC.json";
    json jsonData = loadJsonFromFile(filename);

    //Access specific function data
    FunctionData functionIn = {
            jsonData["DynamicsTimeAxis_in"],
            jsonData["DynamicsTimeAxis_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;
    Eigen::ArrayXd P;
    Eigen::ArrayXd Tmid0;
    Eigen::ArrayXd t;
    Eigen::ArrayXi expectedBeginIdx;
    Eigen::ArrayXi expectedEndIdx;
    Eigen::ArrayXi expectedNtr;
    if (input["P"].type() == json::value_t::array) {
        P = jsonToEigenArray(input["P"]);
        Tmid0 = jsonToEigenArray(input["Tmid0"]);
        expectedBeginIdx = jsonToEigenArrayi(expectedOutput["BeginIdx"]);
        expectedEndIdx = jsonToEigenArrayi(expectedOutput["EndIdx"]);
        expectedNtr = jsonToEigenArrayi(expectedOutput["Ntr"]);
    }
    else {
        double Ps = input["P"].get<double>();
        P = Eigen::ArrayXd::Constant(1, Ps);
        double Tmid0s = input["Tmid0"].get<double>();
        Tmid0 = Eigen::ArrayXd::Constant(1, Tmid0s);
        expectedBeginIdx = Eigen::ArrayXi::Constant(1, expectedOutput["BeginIdx"].get<int>());
        expectedEndIdx = Eigen::ArrayXi::Constant(1, expectedOutput["EndIdx"].get<int>());
        expectedNtr = Eigen::ArrayXi::Constant(1, expectedOutput["Ntr"].get<int>());
    }
    t = jsonToEigenArray(input["t"]);
    auto calc_out = DynamicsTimeAxis(P, Tmid0, t);

    Eigen::ArrayXd expectedtd = jsonToEigenArray(expectedOutput["td"]);

    UNSCOPED_INFO("calc td " << std::get<0>(calc_out));
    UNSCOPED_INFO("expected td " << expectedtd);
    REQUIRE(expectedtd.isApprox(std::get<0>(calc_out).array(), 0.00001));
    UNSCOPED_INFO("calc BeginIdx " << std::get<1>(calc_out));
    UNSCOPED_INFO("expected BeginIdx " << expectedBeginIdx);
    REQUIRE(expectedBeginIdx.isApprox(std::get<1>(calc_out).array() + 1, 0.00001));
    UNSCOPED_INFO("calc EndIdx " << std::get<2>(calc_out));
    UNSCOPED_INFO("expected EndIdx " << expectedEndIdx);
    REQUIRE(expectedEndIdx.isApprox(std::get<2>(calc_out).array() + 1, 0.00001));
    UNSCOPED_INFO("calc Ntr " << std::get<3>(calc_out));
    UNSCOPED_INFO("expected Ntr " << expectedNtr);
    REQUIRE(expectedNtr.isApprox(std::get<3>(calc_out).array(), 0.00001));

}

TEST_CASE("Vector1stOrderDE works", "[Vec1OrderDE_test]"){
    // Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/FittingAndMath.json";
    json jsonData = loadJsonFromFile(filename);
    for (int i = 1; i<100; i++){
        if (jsonData.contains("Vector1stOrderDE_" + std::to_string(i) + "_in")) {
            // Access specific function data
            FunctionData functionIn = {
                    jsonData["Vector1stOrderDE_" + std::to_string(i) + "_in"],
                    jsonData["Vector1stOrderDE_" + std::to_string(i) + "_out"]
            };
            //Access function input and output data
            json input = functionIn.input;
            json expectedOutput = functionIn.output;
            if (input["A"].type() == json::value_t::array && input["x0"].type() == json::value_t::array) {
                Eigen::MatrixXd A = input["A"].get<MatrixXd>();
                Eigen::VectorXd x0 = input["x0"].get<VectorXd>();
                VectorXd t = input["t"].get<VectorXd>();
                MatrixXcd vec1storderdeout = Vector1stOrderDE(A, x0, t);

                Eigen::MatrixXcd expectedvec1storderde = expectedOutput["xt"].get<MatrixXd>();
                UNSCOPED_INFO("calc vec1storderde " << vec1storderdeout);
                UNSCOPED_INFO("expected vec1storderde " << expectedvec1storderde);
                REQUIRE(allclose(expectedvec1storderde, vec1storderdeout, 1e-4));
                //REQUIRE(expectedvec1storderde.isApprox(vec1storderdeout, 1e-5));
            } else if(input["x0"].type() == json::value_t::array){
                double As = input["A"].get<double>();
                MatrixXcd A = MatrixXcd::Constant(1, 1, As);
                Eigen::VectorXd x0 = input["x0"].get<VectorXd>();
                VectorXd t = input["t"].get<VectorXd>();
                MatrixXcd vec1storderdeout = Vector1stOrderDE(A, x0, t);

                Eigen::MatrixXcd expectedvec1storderde = expectedOutput["xt"].get<MatrixXd>();
                UNSCOPED_INFO("calc vec1storderde " << vec1storderdeout);
                UNSCOPED_INFO("expected vec1storderde " << expectedvec1storderde);
            }
            else {
                double As = input["A"].get<double>();
                MatrixXcd A = MatrixXcd::Constant(1, 1, As);
                double x0s = input["x0"].get<double>();
                VectorXcd x0 = VectorXcd::Constant(1, x0s);
                VectorXd t = input["t"].get<VectorXd>();
                MatrixXcd vec1storderdeout = Vector1stOrderDE(A, x0, t);

                Eigen::VectorXcd expectedvec1storderde = expectedOutput["xt"].get<VectorXd>();
                UNSCOPED_INFO("calc vec1storderde " << vec1storderdeout);
                UNSCOPED_INFO("expected vec1storderde " << expectedvec1storderde);
                REQUIRE(expectedvec1storderde.isApprox(vec1storderdeout, 1e-4));
            }
        }
    }
    }

TEST_CASE("rc works", "[rc_tests]") {
    // Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/FluxCalculation.json";
    json jsonData = loadJsonFromFile(filename);

    // Access specific function data
    FunctionData functionIn = {
            jsonData["rc_in"],
            jsonData["rc_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;

    Eigen::ArrayXd x = jsonToEigenArray(input["x"]);
    Eigen::ArrayXd y = jsonToEigenArray(input["y"]);
    double ERRTOL = input["ERRTOL"].get<double>();

    VectorXd rcout = rc(x, y, ERRTOL);

    Eigen::ArrayXd expectedrc = jsonToEigenArray(expectedOutput["rc"]);
    UNSCOPED_INFO("calc rc " << rcout);
    UNSCOPED_INFO("expected rc " << expectedrc);
    REQUIRE(expectedrc.isApprox(rcout.array(), 0.00001));
    //REQUIRE(false);
}

TEST_CASE("rj works", "[rj_tests]") {
    // Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/FluxCalculation.json";
    json jsonData = loadJsonFromFile(filename);

    // Access specific function data
    FunctionData functionIn = {
            jsonData["rj_in"],
            jsonData["rj_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;

    double x = input["x"].get<double>();
    Eigen::ArrayXd y = jsonToEigenArray(input["y"]);
    double z = input["z"].get<double>();
    Eigen:
    ArrayXd p = jsonToEigenArray(input["p"]);
    double ERRTOL = input["ERRTOL"].get<double>();

    VectorXd rjout = rj(x, y, z, p, ERRTOL);

    Eigen::ArrayXd expectedrj = jsonToEigenArray(expectedOutput["rj"]);
    UNSCOPED_INFO("calc rj " << rjout);
    UNSCOPED_INFO("expected rj " << expectedrj);
    REQUIRE(expectedrj.isApprox(rjout.array(), 0.00001));
    //REQUIRE(false);
}

TEST_CASE("Orbitalel2transitparams works", "[oe2tp_tests]") {
    // Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);

    // Access specific function data
    FunctionData functionIn = {
            jsonData["OrbitalElements2TransitParams_in"],
            jsonData["OrbitalElements2TransitParams_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;
    if (size(input["n"]) == 1) {
        double n = input["n"].get<double>();
        double aor = input["aor"].get<double>();
        double ex = input["ex"].get<double>();
        double ey = input["ey"].get<double>();
        double I = input["I"].get<double>();
        double Omega = input["Omega"].get<double>();
        double r = input["r"].get<double>();
        auto oe2tpout = OrbitalElements2TransitParams(n, aor, ex, ey, I, Omega, r);

        double expectedw = expectedOutput["w"].get<double>();
        double expectedd = expectedOutput["d"].get<double>();
        double expectedb = expectedOutput["b"].get<double>();
        double expectedT = expectedOutput["T"].get<double>();
        double expectedTau = expectedOutput["Tau"].get<double>();

        UNSCOPED_INFO("calc w " << std::get<0>(oe2tpout));
        UNSCOPED_INFO("expected w " << expectedw);
        REQUIRE(is_approx(std::get<0>(oe2tpout), expectedw, 0.001));
        UNSCOPED_INFO("calc d " << std::get<1>(oe2tpout));
        UNSCOPED_INFO("expected d " << expectedd);
        REQUIRE(is_approx(std::get<1>(oe2tpout), expectedd, 0.00001));
        UNSCOPED_INFO("calc b " << std::get<2>(oe2tpout));
        UNSCOPED_INFO("expected b " << expectedb);
        REQUIRE(is_approx(std::get<2>(oe2tpout), expectedb, 0.00001));
        UNSCOPED_INFO("calc T " << std::get<3>(oe2tpout));
        UNSCOPED_INFO("expected T " << expectedT);
        REQUIRE(is_approx(std::get<3>(oe2tpout), expectedT, 0.001));
        UNSCOPED_INFO("calc Tau " << std::get<4>(oe2tpout));
        UNSCOPED_INFO("expected Tau " << expectedTau);
        REQUIRE(is_approx(std::get<4>(oe2tpout), expectedTau, 0.001));

    } else {
        ArrayXd n = jsonToEigenArray(input["n"]);
        ArrayXd aor = jsonToEigenArray(input["aor"]);
        ArrayXd ex = jsonToEigenArray(input["ex"]);
        ArrayXd ey = jsonToEigenArray(input["ey"]);
        ArrayXd I = jsonToEigenArray(input["I"]);
        ArrayXd Omega = jsonToEigenArray(input["Omega"]);
        double r = input["r"].get<double>();

        auto oe2tpout = OrbitalElements2TransitParams(n, aor, ex, ey, I, Omega, r);

        Eigen::ArrayXd expectedw = jsonToEigenArray(expectedOutput["w"]);
        Eigen::ArrayXd expectedd = jsonToEigenArray(expectedOutput["d"]);
        Eigen::ArrayXd expectedb = jsonToEigenArray(expectedOutput["b"]);
        Eigen::ArrayXd expectedT = jsonToEigenArray(expectedOutput["T"]);
        Eigen::ArrayXd expectedTau = jsonToEigenArray(expectedOutput["Tau"]);
        UNSCOPED_INFO("calc w " << std::get<0>(oe2tpout));
        UNSCOPED_INFO("expected w " << expectedw);
        REQUIRE(expectedw.isApprox(std::get<0>(oe2tpout).array(), 0.001));
        UNSCOPED_INFO("calc d " << std::get<1>(oe2tpout));
        UNSCOPED_INFO("expected d " << expectedd);
        REQUIRE(expectedd.isApprox(std::get<1>(oe2tpout).array(), 0.00001));
        UNSCOPED_INFO("calc b " << std::get<2>(oe2tpout));
        UNSCOPED_INFO("expected b " << expectedb);
        REQUIRE(expectedb.isApprox(std::get<2>(oe2tpout).array(), 0.00001));
        UNSCOPED_INFO("calc T " << std::get<3>(oe2tpout));
        UNSCOPED_INFO("expected T " << expectedT);
        REQUIRE(expectedT.isApprox(std::get<3>(oe2tpout).array(), 0.001));
        UNSCOPED_INFO("calc Tau " << std::get<4>(oe2tpout));
        UNSCOPED_INFO("expected Tau " << expectedTau);
        REQUIRE(expectedTau.isApprox(std::get<4>(oe2tpout).array(), 0.001));
    }
}

TEST_CASE("Generate Lightcurve works", "[genlc_tests]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/tests/2p_moret/AnalyticLC.json";
    //const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC.json";
    json jsonData = loadJsonFromFile(filename);
    //Access specific function data
    FunctionData functionIn = {
            jsonData["GenerateLightCurve_in"],
            jsonData["GenerateLightCurve_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;

    int Nt = input["Nt"].get<int>();
    int Npl = input["Npl"].get<int>();
    int Ncad = input["Ncad"].get<int>();
    Eigen::ArrayXi CadenceType = jsonToEigenArrayi(input["CadenceType"]);
    Eigen::ArrayXd t = jsonToEigenArray(input["t"]);
    Eigen::ArrayXd BinningTime;
    if (Ncad == 1) {
        double BinningTime_d = input["BinningTime"].get<double>();
        BinningTime = Eigen::ArrayXd::Constant(1, BinningTime_d);
    } else {
        BinningTime = jsonToEigenArray(input["BinningTime"]);
    }
    Eigen::MatrixXd AssociatedTmid_val = jsonToEigenMatrix(input["AssociatedTmid"]);
    Eigen::MatrixXd w = jsonToEigenMatrix(input["w"]);
    Eigen::MatrixXd d = jsonToEigenMatrix(input["d"]);
    Eigen::MatrixXd b = jsonToEigenMatrix(input["b"]);
    Eigen::ArrayXd ror = jsonToEigenArray(input["ror"]);
    double u1 = input["u1"].get<double>();
    double u2 = input["u2"].get<double>();

    auto genlc_out = GenerateLightCurve(Nt, Npl, Ncad, CadenceType, t, BinningTime, AssociatedTmid_val, w, d, b, ror,
                                        u1, u2);

    Eigen::MatrixXd expectedlc = jsonToEigenMatrix(expectedOutput["AllLC"]);
    UNSCOPED_INFO("calc lc " << genlc_out);
    UNSCOPED_INFO("expected lc " << expectedlc);
    REQUIRE(expectedlc.isApprox(genlc_out, 0.00001));
}


TEST_CASE("Associated Tmid works", "[asTmid_tests]") {
    // Load json data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/tests/2p_moret/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);

    //for (int i = 1; i < 100; i++) {
    //
    //        if (jsonData.contains("ForcedElements1_" + std::to_string(i) + "_in")) {
    //            FunctionData functionIn = {
    //                    jsonData["ForcedElements1_" + std::to_string(i) + "_in"],
    //                    jsonData["ForcedElements1_" + std::to_string(i) + "_out"]
    //
    //            };
    //            json input = functionIn.input;
    //            json expectedOutput = functionIn.output;
    //Access specific function data

    for (int i= 1; i< 100; ++i){
        if (jsonData.contains("AssociatedTmid_" + std::to_string(i) + "_in")){
            FunctionData functionIn = {
                    jsonData["AssociatedTmid_"+ std::to_string(i) + "_in"],
                    jsonData["AssociatedTmid_" + std::to_string(i) + "_out"]
            };
            //Access function input and output data
            json input = functionIn.input;
            json expectedOutput = functionIn.output;

            ArrayXd t = jsonToEigenArray(input["t"]);
            if (input["Tmid"].type() == json::value_t::array) {
                ArrayXd Tmid = jsonToEigenArray(input["Tmid"]);
                double P = input["P"].get<double>();

                auto asTmidout = AssociateTmid(t, Tmid, P);

                Eigen::ArrayXd expectedAssociatedTmid = jsonToEigenArray(expectedOutput["AssociatedTmid"]);
                Eigen::ArrayXi expectedAssociatedInd = jsonToEigenArrayi(
                        expectedOutput["AssociatedInd"]); //check if function okay with integer or needs ArrayXd
                UNSCOPED_INFO("calc AssociatedInd " << std::get<1>(asTmidout));
                UNSCOPED_INFO("expected AssociatedInd " << expectedAssociatedInd);
                for (int i = 0; i < sizeof(std::get<1>(asTmidout)); i++) {
                    REQUIRE(expectedAssociatedInd[i] - 1 == std::get<1>(asTmidout)[i]);
                }
                UNSCOPED_INFO("calc AssociatedTmid " << std::get<0>(asTmidout));
                UNSCOPED_INFO("expected AssociatedTmid " << expectedAssociatedTmid);
                REQUIRE(expectedAssociatedTmid.isApprox(std::get<0>(asTmidout).array(), 0.00001));
            } else {
                double Tmid = input["Tmid"].get<double>();
                double P = input["P"].get<double>();

                auto asTmidout = AssociateTmid(t, Tmid, P);

                Eigen::ArrayXd expectedAssociatedTmid = jsonToEigenArray(expectedOutput["AssociatedTmid"]);
                Eigen::ArrayXi expectedAssociatedInd = jsonToEigenArrayi(
                        expectedOutput["AssociatedInd"]); //check if function okay with integer or needs ArrayXd
                UNSCOPED_INFO("calc AssociatedInd " << std::get<1>(asTmidout));
                UNSCOPED_INFO("expected AssociatedInd " << expectedAssociatedInd);
                for (int i = 0; i < sizeof(std::get<1>(asTmidout)); i++) {
                    REQUIRE(expectedAssociatedInd[i] - 1 == std::get<1>(asTmidout)[i]);
                }
                UNSCOPED_INFO("calc AssociatedTmid " << std::get<0>(asTmidout));
                UNSCOPED_INFO("expected AssociatedTmid " << expectedAssociatedTmid);
                REQUIRE(expectedAssociatedTmid.isApprox(std::get<0>(asTmidout).array(), 0.00001));
            }
        }
    }
}

/*
TEST_CASE("Laplace single coeff works", "[lcsc_tests]") {
    // Load json data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);
    //Access specific function data
    for (int i = 1; i < 1000; i++) {
        FunctionData functionIn = {
                jsonData["CalculateSingleCoeff" + std::to_string(i) + "_in"],
                jsonData["CalculateSingleCoeff" + std::to_string(i) + "_out"]
        };
        //Access function input and output data
        json input = functionIn.input;
        json expectedOutput = functionIn.output;

        double alph = input["alph"].get<double>();
        double s = input["s"].get<double>();
        double j = input["j"].get<double>();
        double D = input["D"].get<double>();
        LaplaceCoeff lc; // Create an instance of the LaplaceCoeff class
        auto lcs = lc.calculateSingleCoeff(j, s, alph, D);
        double expectedlcsc = expectedOutput["val"].get<double>();
        UNSCOPED_INFO("calc LaplaceCoeffs " << lcs);
        UNSCOPED_INFO("calc LaplaceCoeffs " << j << " " << s << " " << alph << " " << D);
        UNSCOPED_INFO("expected LaplaceCoeffs " << expectedlcsc);
        CHECK(is_approx(expectedlcsc, lcs, 0.00001));
    }
}
*/

TEST_CASE("occultquadvec works", "[Occultquadvec]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/FluxCalculation.json";
    json jsonData = loadJsonFromFile(filename);
    //Access specific function data
    FunctionData functionIn = {
            jsonData["occultquadVec_in"],
            jsonData["occultquadVec_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;

    Eigen::ArrayXd z = jsonToEigenArray(input["z"]);
    double p = input["p"].get<double>();
    double u1 = input["u1"].get<double>();
    double u2 = input["u2"].get<double>();
    double TolFactor = input["TolFactor"].get<double>();
    bool ToSmooth = input["ToSmooth"].get<bool>();

    auto occultquadvecout = occultquadVec(p, z, u1, u2, TolFactor, ToSmooth);

    Eigen::ArrayXd expectedmuo1 = jsonToEigenArray(expectedOutput["muo1"]);
    Eigen::ArrayXd expectedmu0 = jsonToEigenArray(expectedOutput["mu0"]);

    UNSCOPED_INFO("calc muo1 " << std::get<0>(occultquadvecout));
    UNSCOPED_INFO("expected muo1 " << expectedmuo1);
    REQUIRE(expectedmuo1.isApprox(std::get<0>(occultquadvecout).array(), 0.00001));
    UNSCOPED_INFO("calc mu0 " << std::get<1>(occultquadvecout));
    UNSCOPED_INFO("expected mu0 " << expectedmu0);
    REQUIRE(expectedmu0.isApprox(std::get<1>(occultquadvecout).array(), 0.00001));
}


TEST_CASE("ResonantInteractions2 works", "[ResInt2_tests]") {
    //const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC.json";
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/tests/2p_moret/AnalyticLC.json";
    json jsonData = loadJsonFromFile(filename);

    for (int i=1; i<100; ++i){
        if (jsonData.contains("ResonantInteractions2_" + std::to_string(i) + "_in")){
            FunctionData ResonantInteractions2In = {
                    jsonData["ResonantInteractions2_" + std::to_string(i) + "_in"],
                    jsonData["ResonantInteractions2_" + std::to_string(i) + "_out"]
            };
            //Access function input and output data
            json input = ResonantInteractions2In.input;
            json expectedOutput = ResonantInteractions2In.output;

            Eigen::ArrayXd P = jsonToEigenArray(input["P"]);
            Eigen::ArrayXd mu = jsonToEigenArray(input["mu"]);
            Eigen::MatrixXd Lambda = jsonToEigenMatrix(input["Lambda"]);
            Eigen::MatrixXd free_all_r = jsonToEigenMatrix(input["free_all_r"]);
            Eigen::MatrixXd free_all_i = jsonToEigenMatrix(input["free_all_i"]);
            Eigen::MatrixXd free_i_r = jsonToEigenMatrix(input["free_i_r"]);
            Eigen::MatrixXd free_i_i = jsonToEigenMatrix(input["free_i_i"]);
            Eigen::MatrixXcd free_all = free_all_r + free_all_i * 1i;
            Eigen::MatrixXcd free_i = free_i_r + free_i_i * 1i;
            Eigen::ArrayXd tT = jsonToEigenArray(input["tT"]);
            Eigen::ArrayXi BeginIdx = jsonToEigenArrayi(input["BeginIdx"]);
            Eigen::ArrayXi EndIdx = jsonToEigenArrayi(input["EndIdx"]);
            int ExtraIters = input["ExtraIters"].get<int>();
            Eigen::ArrayXd OrdersAll = jsonToEigenArray(input["OrdersAll"]);
            bool Calculate3PlanetsCrossTerms = input["Calculate3PlanetsCrossTerms"].get<int>();
            int MaxPower = input["MaxPower"].get<int>();

            auto calc_out = ResonantInteractions2(P, mu, Lambda, free_all, free_i, tT, BeginIdx, EndIdx, ExtraIters, OrdersAll,
                                                  Calculate3PlanetsCrossTerms, MaxPower);

            Eigen::MatrixXd dz_r = jsonToEigenMatrix(expectedOutput["dz_r"]);
            Eigen::MatrixXd dz_i = jsonToEigenMatrix(expectedOutput["dz_i"]);
            Eigen::MatrixXcd dz = dz_r + dz_i * 1i;
            Eigen::MatrixXd dLambda = jsonToEigenMatrix(expectedOutput["dLambda"]);
            Eigen::MatrixXd du_r = jsonToEigenMatrix(expectedOutput["du_r"]);
            Eigen::MatrixXd du_i = jsonToEigenMatrix(expectedOutput["du_i"]);
            Eigen::MatrixXcd du = du_r + du_i * 1i;
            Eigen::MatrixXd da_oa = jsonToEigenMatrix(expectedOutput["da_oa"]);
            Eigen::MatrixXd dTheta = jsonToEigenMatrix(expectedOutput["dTheta"]);
            Eigen::MatrixXd z_r = jsonToEigenMatrix(expectedOutput["z_r"]);
            Eigen::MatrixXd z_i = jsonToEigenMatrix(expectedOutput["z_i"]);
            Eigen::MatrixXcd z = z_r + z_i * 1i;
            Eigen::MatrixXd u_r = jsonToEigenMatrix(expectedOutput["u_r"]);
            Eigen::MatrixXd u_i = jsonToEigenMatrix(expectedOutput["u_i"]);
            Eigen::MatrixXcd u = u_r + u_i * 1i;
            Eigen::MatrixXd TTV = jsonToEigenMatrix(expectedOutput["TTV"]);
            double relerr = 1e-5;
            double abserr = 1e-5;
            UNSCOPED_INFO("expected dz " << dz);
            UNSCOPED_INFO("calc dz " << std::get<0>(calc_out));
            REQUIRE(allclose(std::get<0>(calc_out).real(), dz_r, relerr, abserr));
            REQUIRE(allclose(std::get<0>(calc_out).imag(), dz_i, relerr, abserr));
            UNSCOPED_INFO("expected dLambda " << dLambda);
            UNSCOPED_INFO("calc dLambda " << std::get<1>(calc_out));
            REQUIRE(allclose(std::get<1>(calc_out), dLambda, relerr, abserr));
            UNSCOPED_INFO("expected du " << du);
            UNSCOPED_INFO("calc du " << std::get<2>(calc_out));
            REQUIRE(allclose(std::get<2>(calc_out), du, relerr, abserr));
            UNSCOPED_INFO("expected da_oa " << da_oa);
            UNSCOPED_INFO("calc da_oa " << std::get<3>(calc_out));
            REQUIRE(allclose(std::get<3>(calc_out), da_oa, relerr, abserr));
            UNSCOPED_INFO("expected dTheta " << dTheta);
            UNSCOPED_INFO("calc dTheta " << std::get<4>(calc_out));
            REQUIRE(allclose(std::get<4>(calc_out), dTheta, relerr, abserr));
            UNSCOPED_INFO("expected z " << z);
            UNSCOPED_INFO("calc z " << std::get<5>(calc_out));
            REQUIRE(allclose(std::get<5>(calc_out), z, relerr, abserr));
            UNSCOPED_INFO("expected u " << u);
            UNSCOPED_INFO("calc u " << std::get<6>(calc_out));
            REQUIRE(allclose(std::get<6>(calc_out), u, relerr, abserr));
            UNSCOPED_INFO("expected TTV " << TTV);
            UNSCOPED_INFO("calc TTV " << std::get<7>(calc_out));
            REQUIRE(allclose(std::get<7>(calc_out), TTV, relerr, abserr));
    }

}}

TEST_CASE("Df_calc works", "[Dfcalc_tests]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/DisturbingParams.json";
    json jsonData = loadJsonFromFile(filename);

    for (int i= 1; i<100; ++i){
        for (int i2= 1; i2<100; ++i2) {
            if (jsonData.contains("PreDisturbing_" + std::to_string(i) + "_" + std::to_string(i2) + "_in")) {

                FunctionData Df_calcIn = {
                        jsonData["PreDisturbing_" + std::to_string(i) + "_" + std::to_string(i2) + "_out"],
                        jsonData["PreDisturbing_" + std::to_string(i) + "_" + std::to_string(i2) + "_out"]
                };
                //Access function input and output data
                json input = Df_calcIn.input;
                json expectedOutput = Df_calcIn.output;

                double alpha = input["alph"].get<double>();
                Eigen::ArrayXi DVec = input["DVec"].get<Eigen::ArrayXi>();
                int j = input["j"].get<int>();
                Eigen::ArrayXi jVec = jsonToEigenArrayi(input["jVec"]);
                Eigen::ArrayXd sVec = jsonToEigenArray(input["sVec"]);
                LaplaceCoeff lc;
                auto dfcalc_out = Calc_Df(lc, alpha, sVec, jVec, DVec, j);

                Eigen::ArrayXd expectedf = jsonToEigenArray(expectedOutput["f"]);
                UNSCOPED_INFO("calc f " << std::get<0>(dfcalc_out));
                UNSCOPED_INFO("expected f " << expectedf);
                Eigen::ArrayXd calc_f = std::get<0>(dfcalc_out).array();
                for (int i = 0; i < calc_f.size(); ++i) {
                    if (abs(calc_f(i) - expectedf(i)) > 1e-4) {
                        UNSCOPED_INFO(
                                "Arrays differ at index " << i << " value " << calc_f(i) << " expected " << expectedf(i)
                                                          << " diff "
                                                          << abs(calc_f(i) - expectedf(i)));
                    }
                }
                UNSCOPED_INFO(expectedf.size());
                UNSCOPED_INFO(calc_f.size());

                REQUIRE(expectedf.isApprox(std::get<0>(dfcalc_out).array(), 1e-5));
                Eigen::ArrayXd expectedDf = jsonToEigenArray(expectedOutput["Df"]);
                UNSCOPED_INFO("calc Df " << std::get<1>(dfcalc_out));
                UNSCOPED_INFO("expected Df " << expectedDf);
                for (int i = 0; i < expectedDf.size(); i++) {
                    if (abs(expectedDf(i) - std::get<1>(dfcalc_out)(i)) > 1e-4) {
                        UNSCOPED_INFO(
                                "Arrays differ at index " << i << " value " << std::get<1>(dfcalc_out)(i)
                                                          << " expected "
                                                          << expectedDf(i) << " diff "
                                                          << abs(expectedDf(i) - std::get<1>(dfcalc_out)(i)));
                    }
                }
                REQUIRE(expectedDf.isApprox(std::get<1>(dfcalc_out).array(), 1e-5));

                Eigen::ArrayXd expectedfE = jsonToEigenArray(expectedOutput["fE"]);
                UNSCOPED_INFO("calc fE" << std::get<2>(dfcalc_out));
                UNSCOPED_INFO("expected fE" << expectedfE);
                REQUIRE(expectedfE.isApprox(std::get<2>(dfcalc_out).array(), 1e-5));
                Eigen::ArrayXd expectedfI = jsonToEigenArray(expectedOutput["fI"]);
                UNSCOPED_INFO("calc fI" << std::get<3>(dfcalc_out));
                UNSCOPED_INFO("expected fI" << expectedfI);
                REQUIRE(expectedfI.isApprox(std::get<3>(dfcalc_out).array(), 1e-5));
                //Eigen::ArrayXd expectedCoeffsVec = jsonToEigenArray(expectedOutput["CoeffsVec"]);
                Eigen::VectorXd expectedCoeffsVec = expectedOutput["CoeffsVec"].get<Eigen::VectorXd>();
                UNSCOPED_INFO("calc CoeffsVecI" << std::get<4>(dfcalc_out));
                UNSCOPED_INFO("expected CoeffsVecI" << expectedCoeffsVec);
                REQUIRE(allclose(expectedCoeffsVec, std::get<4>(dfcalc_out), 1e1, 1e-5));
                //REQUIRE(expectedCoeffsVec.isApprox(std::get<4>(dfcalc_out).array(), 1e-5));
            }
        }
    }


    }

TEST_CASE("sjDVec works", "[sjDtests]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);

    FunctionData sjDVecIn = {
            jsonData["ForcedElements1_out"],
            jsonData["ForcedElements1_out"]
    };

    //Access function input and output data
    json input = sjDVecIn.input;
    json expectedOutput = sjDVecIn.output;

    int j = input["j"].get<int>();
    auto sjDVec_out = sjDVec(j);

    Eigen::ArrayXd expecteds = jsonToEigenArray(expectedOutput["sVec"]);
    UNSCOPED_INFO("calc s " << std::get<0>(sjDVec_out));
    UNSCOPED_INFO("expected s " << expecteds);
    REQUIRE(expecteds.isApprox(std::get<0>(sjDVec_out).array(), 1e-5));
    Eigen::ArrayXi expectedjVec = jsonToEigenArrayi(expectedOutput["jVec"]);
    UNSCOPED_INFO("calc jVec " << std::get<1>(sjDVec_out));
    UNSCOPED_INFO("expected jVec " << expectedjVec);
    REQUIRE(expectedjVec.isApprox(std::get<1>(sjDVec_out).array(), 0));
    Eigen::ArrayXi expectedDVec = jsonToEigenArrayi(expectedOutput["DVec"]);
    UNSCOPED_INFO("calc DVec " << std::get<2>(sjDVec_out));
    UNSCOPED_INFO("expected DVec " << expectedDVec);
    REQUIRE(expectedDVec.isApprox(std::get<2>(sjDVec_out).array(), 0));
}

TEST_CASE("Calc_AtoD", "[CALC a to d tests]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);

    FunctionData Calc_AtoDIn = {
            jsonData["ForcedElements1_out"],
            jsonData["ForcedElements1_out"]
    };

    //Access function input and output data
    json input = Calc_AtoDIn.input;
    json expectedOutput = Calc_AtoDIn.output;


    auto calc_AtoD_out = Calc_AtoD();

    Eigen::ArrayXd expectedk = jsonToEigenArray(expectedOutput["k"]);
    UNSCOPED_INFO("calc k " << std::get<0>(calc_AtoD_out));
    UNSCOPED_INFO("expected k " << expectedk);
    REQUIRE(expectedk.isApprox(std::get<0>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedA = jsonToEigenArray(expectedOutput["A"]);
    UNSCOPED_INFO("calc A " << std::get<1>(calc_AtoD_out));
    UNSCOPED_INFO("expected A " << expectedA);
    REQUIRE(expectedA.isApprox(std::get<1>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedAp = jsonToEigenArray(expectedOutput["Ap"]);
    UNSCOPED_INFO("calc Ap " << std::get<2>(calc_AtoD_out));
    UNSCOPED_INFO("expected Ap " << expectedAp);
    REQUIRE(expectedAp.isApprox(std::get<2>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedB = jsonToEigenArray(expectedOutput["B"]);
    UNSCOPED_INFO("calc B " << std::get<3>(calc_AtoD_out));
    UNSCOPED_INFO("expected B " << expectedB);
    REQUIRE(expectedB.isApprox(std::get<3>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedBp = jsonToEigenArray(expectedOutput["Bp"]);
    UNSCOPED_INFO("calc Bp " << std::get<4>(calc_AtoD_out));
    UNSCOPED_INFO("expected Bp " << expectedBp);
    REQUIRE(expectedBp.isApprox(std::get<4>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedC = jsonToEigenArray(expectedOutput["C"]);
    UNSCOPED_INFO("calc C " << std::get<5>(calc_AtoD_out));
    UNSCOPED_INFO("expected C " << expectedC);
    REQUIRE(expectedC.isApprox(std::get<5>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedCp = jsonToEigenArray(expectedOutput["Cp"]);
    UNSCOPED_INFO("calc Cp " << std::get<6>(calc_AtoD_out));
    UNSCOPED_INFO("expected Cp " << expectedCp);
    REQUIRE(expectedCp.isApprox(std::get<6>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedD = jsonToEigenArray(expectedOutput["D"]);
    UNSCOPED_INFO("calc D " << std::get<7>(calc_AtoD_out));
    UNSCOPED_INFO("expected D " << expectedD);
    for (int i = 0; i < expectedD.size(); i++) {
        if (abs(expectedD(i) - std::get<7>(calc_AtoD_out)(i)) > 0.01) {
            UNSCOPED_INFO("Arrays differ at index " << i << " value " << std::get<7>(calc_AtoD_out)(i) << " expected "
                                                    << expectedD(i));
        }
    }
    REQUIRE(expectedD.isApprox(std::get<7>(calc_AtoD_out).array(), 1e-5));
    Eigen::ArrayXd expectedDp = jsonToEigenArray(expectedOutput["Dp"]);
    UNSCOPED_INFO("calc Dp " << std::get<8>(calc_AtoD_out));
    UNSCOPED_INFO("expected Dp " << expectedDp);
    REQUIRE(expectedDp.isApprox(std::get<8>(calc_AtoD_out).array(), 1e-5));
}

/*
TEST_CASE("LaplaceCoeffs calculate full table", "[laplace_tests]") {
    LaplaceCoeff lc;
    lc.AbsTol = 1e-5;
    std::map<std::tuple<double, double, double, double>, double> table;
    std::vector<double> times;
    size_t calcs = 3*32*6*100;
    times.reserve(calcs);
    for(auto s = 0.5; s < 3; s++) {
        for (auto j = 0; j < 31; j++) {
            for (auto D = 0; D < 6; D++) {
                for (auto alpha = 0.01; alpha <= 1; alpha += 1e-2) {
                    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
                    table[std::make_tuple(s, j, D, alpha)] = lc.calculate(alpha, s, j, D);
                    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
                    times.push_back(std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count());
                    INFO("Time difference = "
                                 << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
                                 << "[ms] result " << table[std::make_tuple(s, j, D, alpha)]);
                }
            }
        }
    }
    WARN("Average time: " << std::accumulate(times.begin(), times.end(), 0.0) / times.size());
    WARN("Total time: " << std::accumulate(times.begin(), times.end(), 0.0));
}
*/
