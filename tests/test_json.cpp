//#include "analyticlc.hpp"
#include <iostream>
#include <fstream>
#include <catch2/catch_test_macros.hpp> // Include the Catch2 header
#include "json_helpers.h"
//#include <Eigen/src/Core/Array.h>

using json = nlohmann::json;

TEST_CASE("test json to VectorXd") {
    json test_json = json::parse(R"({"a": [1.1, 2.2, 3.0]})");
    auto test_vec = test_json["a"].template get<Eigen::VectorXd>();
    REQUIRE(test_vec(0) == 1.1);
    REQUIRE(test_vec(1) == 2.2);
    REQUIRE(test_vec(2) == 3.0);
}

TEST_CASE("test json to VectorXd with null values") {
    json test_json = json::parse(R"({"a": [1.1, 2.2, 3.0, null]})");
    auto test_vec = test_json["a"].template get<Eigen::VectorXd>();
    REQUIRE(test_vec(0) == 1.1);
    REQUIRE(test_vec(1) == 2.2);
    REQUIRE(test_vec(2) == 3.0);
    REQUIRE(test_vec(3) == 0);
}

TEST_CASE("test number to VectorXd") {
    json test_json = json::parse(R"({"a": 1.1})");
    auto test_vec = test_json["a"].template get<Eigen::VectorXd>();
    REQUIRE(test_vec(0) == 1.1);
}

TEST_CASE("test json to ArrayXd") {
    json test_json = json::parse(R"({"a": [1.1, 2.2, 3.0]})");
    Eigen::ArrayXd test_array = test_json["a"].template get<Eigen::ArrayXd>();
    REQUIRE(test_array(0) == 1.1);
    REQUIRE(test_array(1) == 2.2);
    REQUIRE(test_array(2) == 3.0);
}

TEST_CASE("test json to ArrayXd with null values") {
    json test_json = json::parse(R"({"a": [1.1, 2.2, 3.0, null]})");
    Eigen::ArrayXd test_array = test_json["a"].template get<Eigen::ArrayXd>();
    REQUIRE(test_array(0) == 1.1);
    REQUIRE(test_array(1) == 2.2);
    REQUIRE(test_array(2) == 3.0);
    REQUIRE(test_array(3) == 0);
}

TEST_CASE("test number to ArrayXd") {
    json test_json = json::parse(R"({"a": 1.1})");
    Eigen::ArrayXd test_array = test_json["a"].template get<Eigen::ArrayXd>();
    REQUIRE(test_array(0) == 1.1);
}

TEST_CASE("test json to VectorXi") {
    json test_json = json::parse(R"({"a": [1, 2, 3]})");
    auto test_vec = test_json["a"].template get<Eigen::VectorXi>();
    REQUIRE(test_vec(0) == 1);
    REQUIRE(test_vec(1) == 2);
    REQUIRE(test_vec(2) == 3);
}

TEST_CASE("test json to VectorXi with null values") {
    json test_json = json::parse(R"({"a": [1, 2, 3, null]})");
    auto test_vec = test_json["a"].template get<Eigen::VectorXi>();
    REQUIRE(test_vec(0) == 1);
    REQUIRE(test_vec(1) == 2);
    REQUIRE(test_vec(2) == 3);
    REQUIRE(test_vec(3) == 0);
}

TEST_CASE("test json to ArrayXi") {
    json test_json = json::parse(R"({"a": [1, 2, 3]})");
    auto test_vec = test_json["a"].template get<Eigen::ArrayXi>();
    REQUIRE(test_vec(0) == 1);
    REQUIRE(test_vec(1) == 2);
    REQUIRE(test_vec(2) == 3);
}

TEST_CASE("test number to ArrayXi") {
    json test_json = json::parse(R"({"a": 1})");
    auto test_vec = test_json["a"].template get<Eigen::ArrayXi>();
    REQUIRE(test_vec(0) == 1);
}

TEST_CASE("test json to ArrayXi with null values") {
    json test_json = json::parse(R"({"a": [1, 2, 3, null]})");
    auto test_vec = test_json["a"].template get<Eigen::ArrayXi>();
    REQUIRE(test_vec(0) == 1);
    REQUIRE(test_vec(1) == 2);
    REQUIRE(test_vec(2) == 3);
    REQUIRE(test_vec(3) == 0);
}

TEST_CASE("test json to MatrixXd") {
    json test_json = json::parse(R"({"a": [[1.1, 2.2], [3.0, 4.0]]})");
    auto test_mat = test_json["a"].template get<Eigen::MatrixXd>();
    REQUIRE(test_mat(0, 0) == 1.1);
    REQUIRE(test_mat(0, 1) == 2.2);
    REQUIRE(test_mat(1, 0) == 3.0);
    REQUIRE(test_mat(1, 1) == 4.0);
}

TEST_CASE("test json to MatrixXd with null values") {
    json test_json = json::parse(R"({"a": [[1.1, 2.2], [3.0, null]]})");
    auto test_mat = test_json["a"].template get<Eigen::MatrixXd>();
    REQUIRE(test_mat(0, 0) == 1.1);
    REQUIRE(test_mat(0, 1) == 2.2);
    REQUIRE(test_mat(1, 0) == 3.0);
    REQUIRE(test_mat(1, 1) == 0);
}

TEST_CASE("test simple array to MatrixXd") {
    json test_json = json::parse(R"({"a": [1.1, 2.2, 3.0, 4.0]})");
    auto test_mat = test_json["a"].template get<Eigen::MatrixXd>();
    REQUIRE(test_mat(0, 0) == 1.1);
    REQUIRE(test_mat(1, 0) == 2.2);
    REQUIRE(test_mat(2, 0) == 3.0);
    REQUIRE(test_mat(3, 0) == 4.0);
}

/*
TEST_CASE("SecularInteractionsMatrix Function works", "[secint_tests]") {
// Load the JSON data
    json jsonData = loadJsonFromFile("AnalyticLC.json");

// Access specific function data
    FunctionData functionIn = {
            jsonData["SecularInteractionsMatrix_in"],
            jsonData["SecularInteractionsMatrix_out"]
    };
    FunctionData AnalyticLCIn = {
            jsonData["AnalyticLC_in"],
            jsonData["AnalyticLC_out"]
    };

// Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;
//json expectedOutput = 0;

    json anlcin = AnalyticLCIn.input;
    Eigen::ArrayXd P = jsonToEigenArray(anlcin["P"]);
    Eigen::ArrayXd Tmid0 = jsonToEigenArray(anlcin["Tmid0"]);
    Eigen::ArrayXd ror = jsonToEigenArray(anlcin["ror"]);
    Eigen::ArrayXd mu = jsonToEigenArray(anlcin["mu"]);
    Eigen::ArrayXd ex0 = jsonToEigenArray(anlcin["ex0"]);
    Eigen::ArrayXd ey0 = jsonToEigenArray(anlcin["ey0"]);
    Eigen::ArrayXd I0 = jsonToEigenArray(anlcin["I0"]);
    Eigen::ArrayXd Omega0 = jsonToEigenArray(anlcin["Omega0"]);
    PlanetSystem system = {P, Tmid0, ror, mu, ex0, ey0, I0, Omega0};
    double aor = anlcin["aor"].get<double>();
    double u1 = anlcin["u1"].get<double>();
    double u2 = anlcin["u2"].get<double>();
    Eigen::ArrayXd t = jsonToEigenArray(anlcin["t"]);

    AnalyticLC anlc(system, aor, u1, u2, t);
    Eigen::ArrayXd n = jsonToEigenArray(input["n"]);
    Eigen::ArrayXd mu2 = jsonToEigenArray(input["mu"]);
    Eigen::ArrayXd aor2 = jsonToEigenArray(input["aor"]);
    double J2 = input["J2"].get<double>();
    auto secint = anlc.SecularInteractionsMatrix(n, mu2, aor2, J2);

// Print the contents of the MatrixXcd objects within the tuple
    std::cout << "Matrix A:" << std::endl << std::get<0>(secint) << std::endl;
    std::cout << "Matrix B:" << std::endl << std::get<1>(secint) << std::endl;

    Eigen::MatrixXd expectedAA = jsonToEigenMatrix(expectedOutput["AA"]);
    Eigen::MatrixXd expectedBB = jsonToEigenMatrix(expectedOutput["BB"]);
    REQUIRE((std::get<0>(secint)) == expectedAA);
    REQUIRE((std::get<1>(secint)) == expectedBB);
}


// ... Call your C++ function with 'input' and compare output with 'expectedOutput'
// REQUIRE(actualOutput == expectedOutput);
*/