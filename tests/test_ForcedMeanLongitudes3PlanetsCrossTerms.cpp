#include "catch2/catch_test_macros.hpp"
#include "AnalyticLc/ForcedMeanLongitudes3PlanetsCrossTerms.hpp"
#include "laplacecoeffs.hpp"
#include "json_helpers.h"
#include <complex>
#include "Eigen/Dense"
#include "json_test_helper.h"

using namespace std::complex_literals;
using json = nlohmann::json;

TEST_CASE("ForcedMeanLongitudes3PlanetsCrossTerms works", "[FML3PCT tests]") {
    LaplaceCoeff lc;
// Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);

// Access specific function data
    FunctionData functionIn = {
            jsonData["ForcedMeanLongitudes3PlanetsCrossTerms_in"],
            jsonData["ForcedMeanLongitudes3PlanetsCrossTerms_out"]
    };

// Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;

    Eigen::VectorXd P = jsonToEigenArray(input["P"]);
    Eigen::VectorXd mu = jsonToEigenArray(input["mu"]);
    if (input["Lambda"][0].type() == json::value_t::array) {

        Eigen::MatrixXd Lambda = jsonToEigenMatrix(input["Lambda"]);
        Eigen::MatrixXd z_a = jsonToEigenMatrix(input["z_i"]);
        Eigen::MatrixXd z_b = jsonToEigenMatrix(input["z_r"]);
        Eigen::MatrixXcd z_ges = z_b + 1i * z_a;
        Eigen::MatrixXd fml3pi_out = ForcedMeanLongitudes3PlanetsCrossTerms(lc, P, Lambda, mu, z_ges);

        Eigen::MatrixXd expectedfml3pi = jsonToEigenMatrix(expectedOutput["dLambda"]);
        UNSCOPED_INFO("calc fml3pi " << fml3pi_out);
        UNSCOPED_INFO("expected fml3pi " << expectedfml3pi);
        REQUIRE(expectedfml3pi.isApprox(fml3pi_out, 0.001));
    } else {
        Eigen::VectorXd Lambda = jsonToEigenArray(input["Lambda"]);
        Eigen::ArrayXcd z_a = jsonToEigenArray(input["z_i"]);
        Eigen::ArrayXcd z_b = jsonToEigenArray(input["z_r"]);
        Eigen::VectorXcd z_ges = z_b + 1i * z_a;
        Eigen::MatrixXd fml3pi_out = ForcedMeanLongitudes3PlanetsCrossTerms(lc, P, Lambda.transpose(), mu, z_ges.transpose());

        Eigen::ArrayXd expectedfml3pi = jsonToEigenArray(expectedOutput["dLambda"]);
        UNSCOPED_INFO("calc fml3pi " << fml3pi_out);
        UNSCOPED_INFO("expected fml3pi " << expectedfml3pi);
        REQUIRE(expectedfml3pi.isApprox(fml3pi_out.row(0).transpose().array(), 0.001));
    }
}
