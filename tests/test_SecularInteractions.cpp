#include "catch2/catch_test_macros.hpp"
#include "json_helpers.h"
#include "json_test_helper.h"
#include "analyticlc.hpp"

using json = nlohmann::json;

TEST_CASE("SecularInteractionsMatrix Function works", "[secint_tests]") {
// Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC.json";
    json jsonData = loadJsonFromFile(filename);

// Access specific function data
    FunctionData functionIn = {
            jsonData["SecularInteractionsMatrix_in"],
            jsonData["SecularInteractionsMatrix_out"]
    };

// Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;
//auto anlc = anlcin(filename);
//auto input = anlc["SecularInteractionsMatrix_in"]
    if (input["n"].type() == json::value_t::array) {
        Eigen::ArrayXd n = jsonToEigenArray(input["n"]);
        Eigen::ArrayXd mu2 = jsonToEigenArray(input["mu"]);
        Eigen::ArrayXd aor2 = jsonToEigenArray(input["aor"]);
        double J2 = input["J2"].get<double>();

        auto secint = SecularInteractionsMatrix(n, mu2, aor2, J2);

// Print the contents of the MatrixXcd objects within the tuple
//std::cout << "Matrix A:" << std::endl << std::get<0>(secint) << std::endl;
//std::cout << "Matrix B:" << std::endl << std::get<1>(secint) << std::endl;
        UNSCOPED_INFO("calc A " << std::get<0>(secint));
        UNSCOPED_INFO("calc B" << std::get<1>(secint));

        Eigen::MatrixXd expectedAA_r = jsonToEigenMatrix(expectedOutput["AA_r"]);
        Eigen::MatrixXd expectedAA_i = jsonToEigenMatrix(expectedOutput["AA_i"]);
        Eigen::MatrixXd expectedBB_r = jsonToEigenMatrix(expectedOutput["BB_r"]);
        Eigen::MatrixXd expectedBB_i = jsonToEigenMatrix(expectedOutput["BB_i"]);
        Eigen::MatrixXcd expectedAA = expectedAA_r.array() + expectedAA_i.array() * 1i;
        Eigen::MatrixXcd expectedBB = expectedBB_r.array() + expectedBB_i.array() * 1i;
        UNSCOPED_INFO("expected AA: " << expectedAA);
        UNSCOPED_INFO("expected BB: " << expectedBB);

        REQUIRE(expectedAA.isApprox(std::get<0>(secint), 0.001));
        REQUIRE(expectedBB.isApprox(std::get<1>(secint), 0.001));
    } else {
        double ns = input["n"].get<double>();
        double mu2s = input["mu"].get<double>();
        double aor2s = input["aor"].get<double>();
        double J2 = input["J2"].get<double>();
        Eigen::ArrayXd n = Eigen::ArrayXd::Constant(1, ns);
        Eigen::ArrayXd mu2 = Eigen::ArrayXd::Constant(1, mu2s);
        Eigen::ArrayXd aor2 = Eigen::ArrayXd::Constant(1, aor2s);
        auto secint = SecularInteractionsMatrix(n, mu2, aor2, J2);

        UNSCOPED_INFO("calc A " << std::get<0>(secint));
        UNSCOPED_INFO("calc B" << std::get<1>(secint));

        std::complex<double> expectedAAs = expectedOutput["AA_r"].get<double>() + std::complex<double>(0, 1)*expectedOutput["AA_i"].get<double>();
        std::complex<double> expectedBBs = expectedOutput["BB_r"].get<double>() + std::complex<double>(0, 1)*expectedOutput["BB_i"].get<double>();
        Eigen::MatrixXcd expectedAA = Eigen::MatrixXcd::Constant(1, 1, expectedAAs);
        Eigen::MatrixXcd expectedBB = Eigen::MatrixXcd::Constant(1, 1, expectedBBs);

UNSCOPED_INFO("expected AA: " << expectedAA);
UNSCOPED_INFO("expected BB: " << expectedBB);

//REQUIRE((std::get<0>(secint)) == expectedAA);
//REQUIRE((std::get<1>(secint)) == expectedBB);


    }


//REQUIRE((std::get<0>(secint)) == expectedAA);
//REQUIRE((std::get<1>(secint)) == expectedBB);
}
