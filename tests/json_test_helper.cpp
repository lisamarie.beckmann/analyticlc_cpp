#include "json_test_helper.h"

// Load JSON from a file
json loadJsonFromFile(const std::string &filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        FAIL(("Failed to open file: " + filename));
    } else {
        INFO(("Success open file: " + filename));
    }
    json jsonData;
    file >> jsonData;
    return jsonData;
}

Eigen::ArrayXd jsonToEigenArray(const json &jsonData) {
    size_t size = jsonData.size();
    Eigen::ArrayXd eigenArray(size);
    INFO("JSON data " << jsonData);
    for (size_t i = 0; i < size; ++i) {
        if (jsonData[i].is_null()) {
            eigenArray(i) = 0;
        } else {
            eigenArray(i) = jsonData[i].get<double>();
        }
    }

    return eigenArray;
}

Eigen::ArrayXi jsonToEigenArrayi(const json &jsonData) {
    size_t size = jsonData.size();
    Eigen::ArrayXi eigenArray(size);
    INFO("JSON data " << jsonData);
    for (size_t i = 0; i < size; ++i) {
        eigenArray(i) = jsonData[i].get<int>();
    }

    return eigenArray;
}

Eigen::MatrixXd jsonToEigenMatrix(const json &jsonData) {
    size_t rows = jsonData.size();
    size_t cols = jsonData[0].size();

    Eigen::MatrixXd eigenMatrix(rows, cols);

    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < cols; ++j) {
            eigenMatrix(i, j) = jsonData[i][j].get<double>();
        }
    }

    return eigenMatrix;
}

Eigen::MatrixXi jsonToEigenMatrixi(const json &jsonData) {
    size_t rows = jsonData.size();
    size_t cols = jsonData[0].size();

    Eigen::MatrixXi eigenMatrix(rows, cols);

    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < cols; ++j) {
            eigenMatrix(i, j) = jsonData[i][j].get<int>();
        }
    }

    return eigenMatrix;
}

