#include "catch2/catch_test_macros.hpp"
#include "json_helpers.h"
#include "AnalyticLc/DisturbingFunctionMultTermVariations.hpp"
#include "eigen_helpers.hpp"
#include "json_test_helper.h"

TEST_CASE("calculate_disturbing params works", "[Calcdisparma]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/DisturbingParams.json";
    json jsonData = loadJsonFromFile(filename);
    LaplaceCoeff lc;

    for (int i = 1; i < 100; i++) {
        for (int j = 1; j < 100; j++) {
            if (jsonData.contains("PreDisturbing_" + std::to_string(i) + "_" + std::to_string(j) + "_in")) {
                FunctionData functionIn = {
                        jsonData["PreDisturbing_" + std::to_string(i) + "_" + std::to_string(j) + "_in"],
                        jsonData["PreDisturbing_" + std::to_string(i) + "_" + std::to_string(j) + "_out"]
                };
                //Access function input and output data
                json input = functionIn.input;
                json expectedOutput = functionIn.output;

                CalculateDisturbingFunctionParams calc_params;
                ForcedElementsParams fe_params{lc};
                input.get_to(fe_params);
                expectedOutput.get_to(calc_params);

                int j1 = expectedOutput["j1"].get<int>();
                j1 -= 1;
                int j2 = j1 + 1;
                auto disparams = calculate_disturbing_params(j1, j2, fe_params, calc_params);

                DisturbingFunctionParams expected_disparams;

                from_json(expectedOutput, expected_disparams);

                //UNSCOPED_INFO("calc disparams " << to_string(disparams));
                //UNSCOPED_INFO("expected disparams " << to_string(expected_disparams));
                UNSCOPED_INFO(print_diff_error(disparams, expected_disparams,1e-5));
                REQUIRE(disparams == expected_disparams);

            } else {
                break;
            }
        }
    }
}

TEST_CASE("DisturbingFunctionMultTermVariants works", "[DisFunc_FE]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);
    double error = 1e-9;
    for (int i = 1; i < 100; i++) {

        if (jsonData.contains("ForcedElements1_" + std::to_string(i) + "_in")) {
            FunctionData functionIn = {
                    jsonData["ForcedElements1_" + std::to_string(i) + "_in"],
                    jsonData["ForcedElements1_" + std::to_string(i) + "_out"]

            };
            json input = functionIn.output;
            json expectedOutput = functionIn.output;

            DisturbingFunctionParams params;
            from_json(input, params);
            auto dfmtv = DisturbingFunctionMultTermVariations(params);

            DisturbingFunctionOutput expected_dfmtv;
            from_json(expectedOutput, expected_dfmtv);

            UNSCOPED_INFO("calc dz1 " << dfmtv.dz1);
            UNSCOPED_INFO("expected dz1 " << expected_dfmtv.dz1);
            REQUIRE(allclose(dfmtv.dz1.real(), expected_dfmtv.dz1.real(), error));
            REQUIRE(allclose(dfmtv.dz1.imag(), expected_dfmtv.dz1.imag(), error));
            UNSCOPED_INFO("calc dz2 " << dfmtv.dz2);
            UNSCOPED_INFO("expected dz2 " << expected_dfmtv.dz2);
            REQUIRE(allclose(dfmtv.dz2.real(), expected_dfmtv.dz2.real(), error));
            REQUIRE(allclose(dfmtv.dz2.imag(), expected_dfmtv.dz2.imag(), error));
            UNSCOPED_INFO("calc du1 " << dfmtv.du1);
            UNSCOPED_INFO("expected du1 " << expected_dfmtv.du1);
            REQUIRE(allclose(dfmtv.du1.real(), expected_dfmtv.du1.real(), error));
            REQUIRE(allclose(dfmtv.du1.imag(), expected_dfmtv.du1.imag(), error));
            UNSCOPED_INFO("calc du2 " << dfmtv.du2);
            UNSCOPED_INFO("expected du2 " << expected_dfmtv.du2);
            REQUIRE(allclose(dfmtv.du2.real(), expected_dfmtv.du2.real(), error));
            REQUIRE(allclose(dfmtv.du2.imag(), expected_dfmtv.du2.imag(), error));
            UNSCOPED_INFO("calc da_oa1 " << dfmtv.da_oa1);
            UNSCOPED_INFO("expected da_oa1 " << expected_dfmtv.da_oa1);
            REQUIRE(allclose(dfmtv.da_oa1, expected_dfmtv.da_oa1, error));
            UNSCOPED_INFO("calc da_oa2 " << dfmtv.da_oa2);
            UNSCOPED_INFO("expected da_oa2 " << expected_dfmtv.da_oa2);
            REQUIRE(allclose(dfmtv.da_oa2, expected_dfmtv.da_oa2, error));
            UNSCOPED_INFO("calc dLambda1 " << dfmtv.dLambda1);
            UNSCOPED_INFO("expected dLambda1 " << expected_dfmtv.dLambda1);
            REQUIRE(allclose(dfmtv.dLambda1, expected_dfmtv.dLambda1, error));
            UNSCOPED_INFO("calc dLambda2 " << dfmtv.dLambda2);
            UNSCOPED_INFO("expected dLambda2 " << expected_dfmtv.dLambda2);
            REQUIRE(allclose(dfmtv.dLambda2, expected_dfmtv.dLambda2, error));
        }
    }
}

TEST_CASE("ForcedElements1 works", "[ForcedElements]") {
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC_f.json";
    json jsonData = loadJsonFromFile(filename);
    double error = 1e-5;
    LaplaceCoeff lc;
    for (int i = 1; i < 100; i++) {
        if (jsonData.contains("ForcedElements1_" + std::to_string(i) + "_in")) {
            FunctionData functionIn = {
                    jsonData["ForcedElements1_" + std::to_string(i) + "_in"],
                    jsonData["ForcedElements1_" + std::to_string(i) + "_out"]
            };
            json input = functionIn.input;
            json expectedOutput = functionIn.output;
            //if (input["Lambda"][0].type() != json::value_t::array) {
            //  continue;
            //}

            ForcedElementsParams params(lc);
            from_json(input, params);
            auto ForcedElements1out = ForcedElements1(params);
            MatrixXd expecteddz_r = expectedOutput.at("dz_r").get<MatrixXd>();
            MatrixXd expecteddz_i = expectedOutput.at("dz_i").get<MatrixXd>();
            MatrixXd expecteddLambda_r = expectedOutput.at("dLambda_r").get<MatrixXd>();
            MatrixXd expecteddu_r = expectedOutput.at("du_r").get<MatrixXd>();
            MatrixXd expecteddu_i = expectedOutput.at("du_i").get<MatrixXd>();
            MatrixXd expectedda_oa_r = expectedOutput.at("da_oa_r").get<MatrixXd>();

            double rel_err = 1e-5;
            double abs_err = 1e-7;
            UNSCOPED_INFO("calc dz " << std::get<0>(ForcedElements1out));
            UNSCOPED_INFO("expected dz_r" << expecteddz_r);
            UNSCOPED_INFO("expected dz_i" << expecteddz_i);
            REQUIRE(allclose(std::get<0>(ForcedElements1out).real(), expecteddz_r, rel_err, abs_err));
            REQUIRE(allclose(std::get<0>(ForcedElements1out).imag(), expecteddz_i, rel_err, abs_err));

            UNSCOPED_INFO("calc dLambda " << std::get<1>(ForcedElements1out));
            UNSCOPED_INFO("expected dLambda " << expecteddLambda_r);
            REQUIRE(allclose(std::get<1>(ForcedElements1out).real(), expecteddLambda_r, rel_err, abs_err));

            UNSCOPED_INFO("calc du " << std::get<2>(ForcedElements1out));
            UNSCOPED_INFO("expected du " << expecteddu_r << " " << expecteddu_i);
            REQUIRE(allclose(std::get<2>(ForcedElements1out).real(), expecteddu_r, rel_err, abs_err));
            REQUIRE(allclose(std::get<2>(ForcedElements1out).imag(), expecteddu_i, rel_err, abs_err));

            UNSCOPED_INFO("calc da_oa " << std::get<3>(ForcedElements1out));
            UNSCOPED_INFO("expected da_oa " << expectedda_oa_r);
            REQUIRE(allclose(std::get<3>(ForcedElements1out).real(), expectedda_oa_r, rel_err, abs_err));
        }
    }
}

