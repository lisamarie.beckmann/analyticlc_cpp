#pragma once

#include "json_helpers.h"
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include <fstream>
#include <Eigen/Core>

using json = nlohmann::json;

// Define a struct to hold function input and output data
struct FunctionData {
    json input;
    json output;
};

json loadJsonFromFile(const std::string &filename);

Eigen::ArrayXd jsonToEigenArray(const json &jsonData);

Eigen::ArrayXi jsonToEigenArrayi(const json &jsonData);

Eigen::MatrixXd jsonToEigenMatrix(const json &jsonData);

Eigen::MatrixXi jsonToEigenMatrixi(const json &jsonData);
