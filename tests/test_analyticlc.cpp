#include "catch2/catch_test_macros.hpp"
#include "json_helpers.h"
#include "json_test_helper.h"
#include "eigen_helpers.hpp"
#include "analyticlc.hpp"

TEST_CASE("CalcRV works", "[calcRV tests]") {
    // Load the JSON data
    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/AnalyticLC.json";
    json jsonData = loadJsonFromFile(filename);

    // Access specific function data
    FunctionData functionIn = {
            jsonData["CalcRV_in"],
            jsonData["CalcRV_out"]
    };
    //Access function input and output data
    json input = functionIn.input;
    json expectedOutput = functionIn.output;
    Eigen::MatrixXd Lambda = input["Lambda"].get<Eigen::MatrixXd>();
    Eigen::MatrixXd z_pre = input["z"].get<Eigen::MatrixXd>();
    Eigen::MatrixXd u_pre = input["u"].get<Eigen::MatrixXd>();

    Eigen::MatrixXcd z(Lambda.rows(), Lambda.cols());
    for (int i = 0; i < Lambda.rows(); i++) {
        for (int j = 0; j < Lambda.cols(); j++) {
            z(i, j) = z_pre(i, j) + z_pre(i, j + Lambda.cols()) * 1i;
        }
    }
    Eigen::MatrixXcd u(Lambda.rows(), Lambda.cols());
    for (int i = 0; i < Lambda.rows(); i++) {
        for (int j = 0; j < Lambda.cols(); j++) {
            u(i, j) = u_pre(i, j) + u_pre(i, j + Lambda.cols()) * 1i;
        }
    }
    Eigen::ArrayXd mu = jsonToEigenArray(input["mu"]);
    Eigen::ArrayXd aor = jsonToEigenArray(input["aor"]);
    Eigen::ArrayXd n = jsonToEigenArray(input["n"]); //problem could be that not complex
    bool CalcAstrometry = input["CalcAstrometry"].get<bool>();
    auto calc_out = CalcRV(Lambda, z, u, mu, aor, n, CalcAstrometry);

    Eigen::MatrixXd exp_RV_o_r_pre = expectedOutput["RV_o_r"].get<Eigen::MatrixXd>();
    Eigen::VectorXd exp_RV_o_r = exp_RV_o_r_pre.col(0);
    Eigen::MatrixXd exp_Y_o_r_pre = expectedOutput["Y_o_r"].get<Eigen::MatrixXd>();
    Eigen::VectorXd exp_Y_o_r = exp_Y_o_r_pre.col(0);
    Eigen::MatrixXd exp_Z_o_r_pre = expectedOutput["Z_o_r"].get<Eigen::MatrixXd>();
    Eigen::VectorXd exp_Z_o_r = exp_Z_o_r_pre.col(0);

    double rtol = 1e-5;
    double atol = 1e-5;
    UNSCOPED_INFO("calc RV_o_r " << std::get<0>(calc_out));
    UNSCOPED_INFO("expected RV_o_r " << exp_RV_o_r);
    REQUIRE(allclose(std::get<0>(calc_out), exp_RV_o_r, rtol, atol));
    UNSCOPED_INFO("calc Y_o_r " << std::get<1>(calc_out));
    UNSCOPED_INFO("expected Y_o_r " << exp_Y_o_r);
    REQUIRE(allclose(std::get<1>(calc_out), exp_Y_o_r, rtol, atol));
    UNSCOPED_INFO("calc Z_o_r " << std::get<2>(calc_out));
    UNSCOPED_INFO("expected Z_o_r " << exp_Z_o_r);
    REQUIRE(allclose(std::get<2>(calc_out), exp_Z_o_r, rtol, atol));
}


