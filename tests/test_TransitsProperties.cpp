#include "catch2/catch_test_macros.hpp"
#include "json_helpers.h"
#include "json_test_helper.h"

#include "analyticlc.hpp"

// Helper for the TransitsProperties tests
Eigen::VectorXd Celltovec(std::vector<Eigen::VectorXd> cell) {
    int size = cell.size();
    Eigen::VectorXd vec(0);
    for (int i = 0; i < size; i++) {
        int size2 = cell[i].size();
        for (int j = 0; j < size2; j++) {
            vec.conservativeResize(vec.size() + 1);
            vec[vec.size() - 1] = cell[i][j];
        }
    }
    return vec;
}

TEST_CASE("TransitsProperties works", "[Trprob_tests]") {

    const std::string filename = "/home/lisa/Documents/Uni/Master/AnalyticLCFiles/tests/2p_moret/AnalyticLC.json";
    json jsonData = loadJsonFromFile(filename);
//for (int i= 1; i< 100; ++i){
//        if (jsonData.contains("AssociatedTmid_" + std::to_string(i) + "_in")){
//            FunctionData functionIn = {
//                    jsonData["AssociatedTmid_"+ std::to_string(i) + "_in"],
//                    jsonData["AssociatedTmid_" + std::to_string(i) + "_out"]
//            };
    for (int i= 1; i< 100; ++i){
        if (jsonData.contains("TransitsProperties_" + std::to_string(i) + "_in")){
            FunctionData functionIn = {
                jsonData["TransitsProperties_" + std::to_string(i) + "_in"],
                jsonData["TransitsProperties_" + std::to_string(i) + "_out"]
            };
            //Access function input and output data
            json input = functionIn.input;
            json expectedOutput = functionIn.output;

            int Nt = input["Nt"].get<int>();
            int Npl = input["Npl"].get<int>();
            ArrayXi BeginIdx = jsonToEigenArrayi(input["BeginIdx"]);
            BeginIdx = BeginIdx.array() - 1;
            ArrayXi EndIdx = jsonToEigenArrayi(input["EndIdx"]);
            EndIdx = EndIdx.array() - 1;
            ArrayXd t = jsonToEigenArray(input["t"]);
            ArrayXd tT = jsonToEigenArray(input["tT"]);
            MatrixXd TTV = jsonToEigenMatrix(input["TTV"]);
            ArrayXd P = jsonToEigenArray(input["P"]);
            ArrayXd n = jsonToEigenArray(input["n"]);
            MatrixXd da_oa = jsonToEigenMatrix(input["da_oa"]);
            ArrayXd aor = jsonToEigenArray(input["aor"]);
            MatrixXd zT_real = jsonToEigenMatrix(input["zT_real"]);
            MatrixXd zT_imag = jsonToEigenMatrix(input["zT_imag"]);
            MatrixXcd zT = zT_real.array() + zT_imag.array() * 1i;
            MatrixXd uT_real = jsonToEigenMatrix(input["uT_real"]);
            MatrixXd uT_imag = jsonToEigenMatrix(input["uT_imag"]);
            MatrixXcd uT = uT_real.array() + uT_imag.array() * 1i;
            ArrayXd ror = jsonToEigenArray(input["ror"]);
            bool CalcDurations = input["CalcDurations"].get<bool>();

            auto calc_out = TransitsProperties(Nt, Npl, BeginIdx, EndIdx, t, tT, TTV, P, n, da_oa, aor, zT, uT, ror,
                                               CalcDurations);
            expectedOutput = expectedOutput;
            MatrixXd expected_D = jsonToEigenMatrix(expectedOutput["D"]);
            ArrayXd expected_D1 = jsonToEigenArray(expectedOutput["D1"]);
            MatrixXd expected_Tau = jsonToEigenMatrix(expectedOutput["Tau"]);
            ArrayXd expected_Tau1 = jsonToEigenArray(expectedOutput["Tau1"]);
            MatrixXd expected_w = jsonToEigenMatrix(expectedOutput["w"]);
            ArrayXd expected_w1 = jsonToEigenArray(expectedOutput["w1"]);
            MatrixXd expected_d = jsonToEigenMatrix(expectedOutput["d"]);
            ArrayXd expected_d1 = jsonToEigenArray(expectedOutput["d1"]);
            MatrixXd expected_b = jsonToEigenMatrix(expectedOutput["b"]);
            ArrayXd expected_b1 = jsonToEigenArray(expectedOutput["b1"]);
            MatrixXd expected_AssociatedTmid = jsonToEigenMatrix(expectedOutput["AssociatedTmid"]);
            MatrixXi expected_AssociatedInd = jsonToEigenMatrixi(expectedOutput["AssociatedInd"]);
            VectorXd dbdt = jsonToEigenArray(expectedOutput["dbdt"]);
            VectorXd b0 = jsonToEigenArray(expectedOutput["b0"]);

            UNSCOPED_INFO("calc D " << std::get<0>(calc_out));
            UNSCOPED_INFO("expected D " << expected_D);
            REQUIRE(expected_D.isApprox(std::get<0>(calc_out), 0.001));
            std::vector<Eigen::VectorXd> D1 = std::get<1>(calc_out);
            ArrayXd D1_calc = Celltovec(D1);
            UNSCOPED_INFO("calc D1 " << D1_calc);
            UNSCOPED_INFO("expected D1 " << expected_D1);
            REQUIRE(expected_D1.isApprox(D1_calc, 0.001));
            UNSCOPED_INFO("calc Tau " << std::get<2>(calc_out));
            UNSCOPED_INFO("expected Tau " << expected_Tau);
            REQUIRE(expected_Tau.isApprox(std::get<2>(calc_out), 0.001));
            std::vector<Eigen::VectorXd> Tau1 = std::get<3>(calc_out);
            ArrayXd Tau1_calc = Celltovec(Tau1);
            UNSCOPED_INFO("calc Tau1 " << Tau1_calc);
            UNSCOPED_INFO("expected Tau1 " << expected_Tau1);
            REQUIRE(expected_Tau1.isApprox(Tau1_calc, 0.001));
            UNSCOPED_INFO("calc w " << std::get<4>(calc_out));
            UNSCOPED_INFO("expected w " << expected_w);
            REQUIRE(expected_w.isApprox(std::get<4>(calc_out), 0.001));
            std::vector<Eigen::VectorXd> w1 = std::get<5>(calc_out);
            ArrayXd w1_calc = Celltovec(w1);
            UNSCOPED_INFO("calc w1 " << w1_calc);
            UNSCOPED_INFO("expected w1 " << expected_w1);
            REQUIRE(expected_w1.isApprox(w1_calc, 0.001));
            UNSCOPED_INFO("calc d " << std::get<6>(calc_out));
            UNSCOPED_INFO("expected d " << expected_d);
            REQUIRE(expected_d.isApprox(std::get<6>(calc_out), 0.001));
            std::vector<Eigen::VectorXd> d1 = std::get<7>(calc_out);
            ArrayXd d1_calc = Celltovec(d1);
            UNSCOPED_INFO("calc d1 " << d1_calc);
            UNSCOPED_INFO("expected d1 " << expected_d1);
            REQUIRE(expected_d1.isApprox(d1_calc, 0.001));
            UNSCOPED_INFO("calc b " << std::get<8>(calc_out));
            UNSCOPED_INFO("expected b " << expected_b);
            REQUIRE(expected_b.isApprox(std::get<8>(calc_out), 0.001));
            std::vector<Eigen::VectorXd> b1 = std::get<9>(calc_out);
            ArrayXd b1_calc = Celltovec(b1);
            UNSCOPED_INFO("calc b1 " << b1_calc);
            UNSCOPED_INFO("expected b1 " << expected_b1);
            REQUIRE(expected_b1.isApprox(b1_calc, 0.001));
            UNSCOPED_INFO("calc AssociatedTmid " << std::get<10>(calc_out));
            UNSCOPED_INFO("expected AssociatedTmid " << expected_AssociatedTmid);
            REQUIRE(expected_AssociatedTmid.isApprox(std::get<10>(calc_out), 0.001));
            UNSCOPED_INFO("calc AssociatedInd " << std::get<11>(calc_out));
            UNSCOPED_INFO("expected AssociatedInd " << expected_AssociatedInd);
            REQUIRE(expected_AssociatedInd.isApprox(std::get<11>(calc_out).array() + 1, 0));
            UNSCOPED_INFO("calc dbdt " << std::get<12>(calc_out));
            UNSCOPED_INFO("expected dbdt " << dbdt);
            REQUIRE(dbdt.isApprox(std::get<12>(calc_out), 0.001));
            UNSCOPED_INFO("calc b0 " << std::get<13>(calc_out));
            UNSCOPED_INFO("expected b0 " << b0);
            REQUIRE(b0.isApprox(std::get<13>(calc_out), 0.001));
        }
        }


}

