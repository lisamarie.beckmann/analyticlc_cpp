#!/usr/bin/env bash

./build/tests/catch_test "AnalyticLC func works" -c "Testing file: /home/lisa/Documents/Uni/Master/AnalyticLCFiles/tests/4p_manyt/AnalyticLC.json" &
perf record -F 99 -p $! -g --call-graph dwarf 
perf script report flamegraph
