import pythonapi
import numpy as np

import threading

# Define a worker function to call the C++ function
def worker(x):
    pa = pythonapi.AnalyticLCparams()
    
    P_true=[17.1,34.51]
    Tmid0_true=[1325.328,1331.2851]
    ror_true=[0.11, 0.1236]
    aor_true=[29.1]
    mu_true=[6.2e-5, 3.22e-4]
    ex0_true=[0.1234, 0.00998]
    ey0_true=[0.2345, 0.0189]
    I0_true=[0.0001, 0.0315]
    Omega0_true=[0.0001, 0.0001]
    
    u1=0.33;
    u2=0.32;
    #t=np.linspace(0,50,100) #0:0.002:100;
    #t = np.sort(ran2[0]) #sorted(ran2)
    a = np.linspace(9.4,10.4,50)
    b = np.linspace(15, 15.8,50)
    t = np.append(a,b)
    
    t = np.linspace(1324, 1339, 100)
    
    
    pythonapi.construct(pa, P_true, Tmid0_true, ror_true, aor_true, mu_true, ex0_true, ey0_true, I0_true, Omega0_true, t, u1, u2)
    
    print(pa)
    
    out = pythonapi.AnalyticLCfunc(pa)
    
    print(out)
    
    out1 = out[0]
    out2 = out[1]
    
    print(out1.get_LC())
    print(out1.get_RV_o_r())


# Create multiple threads
threads = []
for i in range(5):
    t = threading.Thread(target=worker, args=(i,))
    threads.append(t)
    t.start()

# Wait for all threads to complete
for t in threads:
    t.join()

print("All threads have finished execution.")


