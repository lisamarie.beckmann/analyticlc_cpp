#include "analyticlcpp.h"
#include <iostream>
#include <Eigen/Dense>
#include "analyticlc.hpp"

static alc::AnalyticLCparams params;

void test() {
    std::cout << "Hello from C++!" << std::endl;
}

void set_matrix(double* data, int rows, int cols) {
    // Convert the array of arrays to an Eigen matrix
    Eigen::Map<Eigen::MatrixXd> matrix(data, rows, cols);
    //std::cout << "Eigen Matrix:" << std::endl << matrix << std::endl;
}

void set_vector(double* data, int size) {
    // Convert the array to an Eigen vector
    Eigen::Map<Eigen::VectorXd> vector(data, size);
    //std::cout << "Eigen Vector:" << std::endl << vector << std::endl;
}

//main func to test set_matrix
int main() {
    double data[2][3] = {{1, 2, 3}, {4, 5, 6}};
    set_matrix((double*)data, 2,3);
    return 0;
}

void init(double numP, double numt, double* P, double* Tmid0, double* ror, double* mu, double* ex0, double* ey0, double* I0, double* Omega0, double aor, double u1, double u2, double* t) {
    VectorXd P_arr = Eigen::Map<VectorXd>(P, numP);
    VectorXd Tmid0_arr = Eigen::Map<VectorXd>(Tmid0, numP);
    VectorXd ror_arr = Eigen::Map<VectorXd>(ror, numP);
    VectorXd mu_arr = Eigen::Map<VectorXd>(mu, numP);
    VectorXd ex0_arr = Eigen::Map<VectorXd>(ex0, numP);
    VectorXd ey0_arr = Eigen::Map<VectorXd>(ey0, numP);
    VectorXd I0_arr = Eigen::Map<VectorXd>(I0, numP);
    VectorXd Omega0_arr = Eigen::Map<VectorXd>(Omega0, numP);

    params(P_arr, Tmid0_arr, ror_arr, aor, mu_arr, ex0_arr, ey0_arr, I0_arr, Omega0_arr, t, u1, u2);
    )

void set_BinningTime(double* BinningTime, double numt) {
    VectorXd BinningTime_arr = Eigen::Map<VectorXd>(BinningTime, numt);
    params.BinningTime = BinningTime_arr;
}

void set_CadenceType(double* CadenceType, double numt) {
    VectorXi CadenceType_arr = Eigen::Map<VectorXi>(CadenceType, numt);
    params.CadenceType = CadenceType_arr;
}

void set_MaxTTV(double* MaxTTV, double numP) {
    VectorXd MaxTTV_arr = Eigen::Map<VectorXd>(MaxTTV, numP);
    params.MaxTTV = MaxTTV_arr;
}

void set_tRV(double* tRV, double numtRV) {
    VectorXd tRV_arr = Eigen::Map<VectorXd>(tRV, numtRV);
    params.tRV = tRV_arr;
}

void set_ExtraIters(double ExtraIters) {
    params.ExtraIters = ExtraIters;
}

void set_OrdersAll(double* OrdersAll, double numP) {
    VectorXd OrdersAll_arr = Eigen::Map<VectorXd>(OrdersAll, numP);
    params.OrdersAll = OrdersAll_arr;
}

void set_Calculate3PlanetsCrossTerms(bool Calculate3PlanetsCrossTerms) {
    params.Calculate3PlanetsCrossTerms = Calculate3PlanetsCrossTerms;
}

void set_MaxPower(int MaxPower) {
    params.MaxPower = MaxPower;
}

void set_OrbitalElementsAsCellArrays(bool OrbitalElementsAsCellArrays) {
    params.OrbitalElementsAsCellArrays = OrbitalElementsAsCellArrays;
}

void set_J2(double J2) {
    params.J2 = J2;
}

void calc(bool CalcDurations, bool CalcAstrometry) {
    auto[analyticout, others] = AnalyticLC(params, CalcDurations, CalcAstrometry);
    LC = analyticout.LC;
    RV_o_r = analyticout.RV_o_r;
}
