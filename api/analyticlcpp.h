#pragma once
//#include <Eigen/Dense>

#ifdef __cplusplus
extern "C" {
#endif

struct Matrix {
    double** data;
    int rows;
    int cols;
};

struct Vector {
    double* data;
    int size;
};

struct OutputList{
    VectorXd tT;
    VectorXd tRV;
    MatrixXcd zT;
    MatrixXcd zRV;
    MatrixXcd uT;
    MatrixXcd uRV;
    MatrixXd D;
    MatrixXd Tau;
    MatrixXd w;
    MatrixXd d;
    MatrixXd b;
    MatrixXd AssociatedTmid;
    MatrixXi AssociatedInd;
    MatrixXd AllLC;
    std::vector<Eigen::VectorXd> TmidActualCell;
    MatrixXcd free_e_T;
    MatrixXcd free_I_T;
    MatrixXcd free_e_RV;
    MatrixXcd free_I_RV;
    std::vector<Eigen::VectorXd> TTVCell;
    std::vector<Eigen::VectorXcd> dzCell;
    std::vector<Eigen::VectorXcd> duCell;
    std::vector<Eigen::VectorXd> dLambdaCell;
    std::vector<Eigen::VectorXcd> zfreeCell;
    VectorXd dbdt;
    VectorXd b0;
    MatrixXd Lambda_T;
    VectorXd P;
    VectorXi BeginIdx;
    VectorXi EndIdx;
    VectorXi Ntr;
};

void test();

#ifdef __cplusplus
};
#endif

void set_matrix(double** data, int rows, int cols);