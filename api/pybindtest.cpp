#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>

#include <Eigen/Dense>
#include "analyticlc.hpp"
#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

PYBIND11_MODULE(pythonapi, m) {
    m.doc() = R"pbdoc(
        Pybind11 example plugin
        -----------------------

        .. currentmodule:: cmake_example

        .. autosummary::
           :toctree: _generate

    )pbdoc";


    m.def("AnalyticLCfunc", &AnalyticLCfunc, R"pbdoc(
        AnalyticLC: generate an analytic light-curve, and optionally RV and
        astrometry data, from a set of initial (free) orbital elements.
   )pbdoc");

m.def("construct", &alc::construct, py::call_guard<py::gil_scoped_release>(), R"pbdoc(
        set inital values for AnalyticLCparams
    )pbdoc");


py::class_<alc::AnalyticLCout>(m,"AnalyticLCout")
.def(py::init<>())
.def("get_LC", &alc::AnalyticLCout::get_LC, py::return_value_policy::reference_internal)
.def("get_RV_o_r", &alc::AnalyticLCout::get_RV_o_r, py::return_value_policy::reference_internal)
.def("get_Y_o_r", &alc::AnalyticLCout::get_Y_o_r, py::return_value_policy::reference_internal)
.def("get_Z_o_r", &alc::AnalyticLCout::get_Z_o_r, py::return_value_policy::reference_internal)
;

py::class_<alc::AnalyticLCparams>(m, "AnalyticLCparams")
.def(py::init<>())
.def(py::init<Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>, double, double>())
.def("set_P", &alc::AnalyticLCparams::set_P)
.def("get_P", &alc::AnalyticLCparams::get_P)
.def("set_Tmid0", &alc::AnalyticLCparams::set_Tmid0)
.def("set_ror", &alc::AnalyticLCparams::set_ror)
.def("set_aor", &alc::AnalyticLCparams::set_aor)
.def("set_mu", &alc::AnalyticLCparams::set_mu)
.def("set_ex0", &alc::AnalyticLCparams::set_ex0)
.def("set_ey0", &alc::AnalyticLCparams::set_ey0)
.def("set_I0", &alc::AnalyticLCparams::set_I0)
.def("set_Omega0", &alc::AnalyticLCparams::set_Omega0)
.def("set_t", &alc::AnalyticLCparams::set_t)
.def("set_u1", &alc::AnalyticLCparams::set_u1)
.def("set_u2", &alc::AnalyticLCparams::set_u2)
.def("set_BinningTime", &alc::AnalyticLCparams::set_BinningTime)
.def("set_CadenceType", &alc::AnalyticLCparams::set_CadenceType)
.def("set_MaxTTV", &alc::AnalyticLCparams::set_MaxTTV)
.def("set_tRV", &alc::AnalyticLCparams::set_tRV)
.def("set_ExtraIters", &alc::AnalyticLCparams::set_ExtraIters)
.def("set_OrdersAll", &alc::AnalyticLCparams::set_OrdersAll)
.def("set_Calculate3PlanetsCrossTerms", &alc::AnalyticLCparams::set_Calculate3PlanetsCrossTerms)
.def("set_MaxPower", &alc::AnalyticLCparams::set_MaxPower)
.def("set_OrbitalElementsAsCellArrays", &alc::AnalyticLCparams::set_OrbitalElementsAsCellArrays)
.def("set_J2", &alc::AnalyticLCparams::set_J2)
.def("set_CalcDurations", &alc::AnalyticLCparams::set_CalcDurations)
.def("set_CalcAstrometry", &alc::AnalyticLCparams::set_CalcAstrometry)
;


py::class_<alc::OutputList>(m, "OutputList")
.def(py::init<>())
.def("get_tT", &alc::OutputList::get_tT)
.def("get_tRV", &alc::OutputList::get_tRV)
.def("get_zT", &alc::OutputList::get_zT)
.def("get_zRV", &alc::OutputList::get_zRV)
.def("get_uT", &alc::OutputList::get_uT)
.def("get_uRV", &alc::OutputList::get_uRV)
.def("get_D", &alc::OutputList::get_D)
.def("get_Tau", &alc::OutputList::get_Tau)
.def("get_w", &alc::OutputList::get_w)
.def("get_d", &alc::OutputList::get_d)
.def("get_b", &alc::OutputList::get_b)
.def("get_AssociatedTmid", &alc::OutputList::get_AssociatedTmid)
.def("get_AssociatedInd", &alc::OutputList::get_AssociatedInd)
.def("get_AllLC", &alc::OutputList::get_AllLC)
.def("get_TmidActualCell", &alc::OutputList::get_TmidActualCell)
.def("get_free_e_T", &alc::OutputList::get_free_e_T)
.def("get_free_I_T", &alc::OutputList::get_free_I_T)
.def("get_free_e_RV", &alc::OutputList::get_free_e_RV)
.def("get_free_I_RV", &alc::OutputList::get_free_I_RV)
.def("get_TTVCell", &alc::OutputList::get_TTVCell)
.def("get_dzCell", &alc::OutputList::get_dzCell)
.def("get_duCell", &alc::OutputList::get_duCell)
.def("get_dLambdaCell", &alc::OutputList::get_dLambdaCell)
.def("get_zfreeCell", &alc::OutputList::get_zfreeCell)
.def("get_dbdt", &alc::OutputList::get_dbdt)
.def("get_b0", &alc::OutputList::get_b0)
.def("get_Lambda_T", &alc::OutputList::get_Lambda_T)
.def("get_P", &alc::OutputList::get_P)
.def("get_BeginIdx", &alc::OutputList::get_BeginIdx)
.def("get_EndIdx", &alc::OutputList::get_EndIdx)
.def("get_Ntr", &alc::OutputList::get_Ntr)
;

#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
