#0!/usr/bin/env python3
# -*- coding: utf-8 -*-
from scipy.stats import gamma,norm,beta,truncnorm
import scipy.linalg as sl
from numpy.matlib import repmat
import numpy as np
import math
from types import SimpleNamespace

# nested sampling and multiprocessing tools
import dynesty
from dynesty.utils import quantile as _quantile

from dynesty.utils import resample_equal
from dynesty import plotting as dyplot
from multiprocessing import Pool

import copy

# system functions that are always useful to have
import time, sys, os

# basic numeric setup
import numpy as np
from numpy import random
# inline plotting
#%matplotlib inline

# plotting
import matplotlib
from matplotlib import pyplot as plt
import rebound
import csv
import bisect

# for comparison: other lc modeling tool 
#import planetplanet as pp

# managing data
import json
import pickle
import sys
import os
import time


# Calculate the path to the 'pythonapi' directory
# This example assumes 'test.py' is in 'path/analyticpy' and 'pythonapi' is in 'path/build'
path_to_pythonapi = os.path.abspath(os.path.join(os.path.dirname(__file__), '../build'))

# Add the path to sys.path
sys.path.append(path_to_pythonapi)

import pythonapi
global_start_time = time.time()

def print_time(message):
    elapsed_time = time.time() - global_start_time
    print(f"{message} - Elapsed time: {elapsed_time:.2f} seconds")

# Now you can import your library
################################################################################
##########################Import data###########################################
################################################################################

def parse_parameters(settings):
    """
    reads from dictionaries in settings to get which parameters shall be fixed/determined with dynesty
    returns:
    p: list with all parameters that should be modeled around with ranges
    fix: dictionary with fixed values 
    """
    p = []
    fix = {}
    for planet in settings["planets"]:
        for key, value in planet.items():
            fix[key] = [(None, None)] * len(settings["planets"])

    i = 0
    for planet in settings["planets"]:
        for key, value in planet.items():
            if value["type"] != "fixed":
                p += [(i, key, value["type"], value["values"])]
            else:
                fix[key][i] = value["values"]
        i += 1

    for key, value in settings["parameters"].items():
        if value["type"] != "fixed":
            p += [(-1, key, value["type"], value["values"])]
        else:
            fix[key] = value["values"]
    for lc in settings["TR_data"]:
        for key, value in lc.items():
            fix["sigTR"] = [(None, None)] * len(settings["TR_data"])
            fix["u1"] = [(None, None)] * len(settings["TR_data"])
            fix["u2"] = [(None, None)] * len(settings["TR_data"])

    j = 0
    for lc in settings["TR_data"]:
        for key, value in lc.items():
            if key == "sigTR":
                if value["type"]!= "fixed":
                    p += [(j, key, value["type"], value["values"])]
                else:
                    fix[key][j] = value["values"]
            elif key == "u1":
                if value["type"]!= "fixed":
                    p += [(j, key, value["type"], value["values"])]
                else:
                    fix[key][j] = value["values"]
            elif key == "u2":
                if value["type"]!= "fixed":
                    p += [(j, key, value["type"], value["values"])]
                else:
                    fix[key][j] = value["values"]
                j += 1
    for rv in settings["RV_data"]:
        for key, value in rv.items():
            fix["sigRV"] = [(None, None)] * len(settings["RV_data"])
    j = 0 
    for rv in settings["RV_data"]:
        for key, value in rv.items():
            if key == "sigRV":
                if value["type"]!= "fixed":
                    p += [(j, key, value["type"], value["values"])]
                else:
                    fix[key][j] = value["values"]
                j += 1
    return p, fix

def parse_settings_init():
    settings = json.load(open("gj357_parameter.json"))

    p, fix = parse_parameters(settings)
    
    rvdatalist, trdatalist = import_data(settings["RV_data"], settings["TR_data"])

    tRV = []
    RV = []
    for rvdata in rvdatalist:
        tRV.extend(rvdata[0])
        RV.extend(rvdata[1])
    t = []
    TR = []
    for trdata in trdatalist:
        t.extend(trdata[0])
        TR.extend(trdata[1])

    fix["RV"] = RV
    fix["tRV"] = tRV
    fix["TR"] = TR
    fix["t"] = t

    return p,fix, rvdatalist, trdatalist
 

def parse_settings():
    """
    read in json with parameters to be fixated or modeled around
    returns the ranges to model, fixed parameters and settings like number of livepoints and datapaths
    """
    settings = json.load(open("gj357_parameter.json"))

    p, fix = parse_parameters(settings)
    
    rvdatalist, trdatalist = import_data(settings["RV_data"], settings["TR_data"])

    return p, fix, rvdatalist, trdatalist, settings["nkern"], settings["nlivepoints"],settings["save_datapath"] 

def import_data(RV_data, TR_data):
    """
    reads in RV and TR data if a datapath is given. Standard data path with no data of that type: "/dev/null"
    returns list with time, data and err of each
    """
    rvdatalist = []
    for rv in RV_data:
        RV_datapath = rv["RV_datapath"]
        instrument = rv["instrument"]
        delimiter = rv["delimiter"]
        if RV_datapath == "/dev/null":
            tRV = []
            RV = []
            RVerr = []
        else:
           tRV, RV, RVerr = np.loadtxt(RV_datapath, delimiter = delimiter, unpack=True, usecols=(0,1,2))
           if rv["bin"] == True:
               bin_size = rv["binsize"]
               tRV, RV, RVerr = bin_data(tRV, RV, RVerr, bin_size)
        rvdatalist.append((tRV, RV, RVerr,instrument))
    trdatalist = []   
    for tr in TR_data:
        TR_datapath = tr["TR_datapath"]
        instrument = tr["instrument"]
        delimiter = tr["delimiter"]
        if TR_datapath == "/dev/null":
            t = []
            TR = []
            TRerr = []
        else:
            t, TR, TRerr = np.loadtxt(TR_datapath, delimiter = delimiter, unpack=True, usecols=(0, 1, 2))
            if tr["bin"] == "True":
                bin_size = tr["binsize"]
                print("is binning")
                t, TR, TRerr = bin_data(t, TR, TRerr, bin_size)
            if tr["snip_around_transit"] == "True":
                print("is snipping")
                snip_mask = np.zeros_like(t, dtype=bool)  # Initialize a mask of False values
                P_list = tr["P_snip"]
                Tmid0_list = tr["Tmid_snip"]
                snip_width = tr["snip_width"]
                for P, Tmid0 in zip(P_list, Tmid0_list):
                    # Calculate all possible transit times within the range of t
                    transit_times = Tmid0 + P * np.arange(-1e6, 1e6)  # Arbitrary large range
                    transit_times = transit_times[(transit_times > t.min() - snip_width/2) & 
                                      (transit_times < t.max() + snip_width/2)]

                    # Create a mask for each transit
                    for T in transit_times:
                        transit_mask = (t > T - snip_width/2) & (t < T + snip_width/2)
                        snip_mask |= transit_mask  # Combine the masks

                # Snip the data
                t = t[snip_mask]
                TR = TR[snip_mask]
                TRerr = TRerr[snip_mask]
                print(t)
                print(len(t))
        trdatalist.append((t, TR, TRerr,instrument))

    return rvdatalist, trdatalist

def bin_data(t, flux, flux_err, bin_size):
    # Create bins
    bins = np.arange(t.min(), t.max(), bin_size)
    binned_t = []
    binned_flux = []
    binned_flux_err = []

    # Bin data
    for i in range(len(bins) - 1):
        bin_mask = (t >= bins[i]) & (t < bins[i + 1])
        if np.any(bin_mask):
            binned_t.append(np.mean(t[bin_mask]))
            binned_flux.append(np.mean(flux[bin_mask]))
            binned_flux_err.append(np.sqrt(np.sum(flux_err[bin_mask]**2)) / np.sum(bin_mask))

    return np.array(binned_t), np.array(binned_flux), np.array(binned_flux_err)


###########################################################################
#################Translate parameters###########################################
################################################################################

def z_to_eom(z): 
    """
    takes the complex eccentricity z of format z=e*exp(i*omega) with forced parts
    (so after using function geteccRV/geteccT)
    and returns omega and e
    
    attention: the complex ecc are given for every point in t/tRV,
    I decided for my use-cases that it is sufficent to take z at the first time point,
    it may be different for yours
    """
    elist = []
    omlist = []
    for single_z in z[0]:
        h = np.real(single_z) #z is a list, given for every evaluated time point
        k = np.imag(single_z) #decided to take the configuration at the beginning
        hksq = np.sqrt(h**2+k**2)
        if k==0:
            e = -h
            om = np.pi
        elif(hksq != 0 and h*hksq != h**2+k**2):
            e = hksq
            om = 2*(np.arctan(k/(h-hksq)))
        else:
            e = hksq
            om = 2*(np.arctan(k/(hksq+h)))
        elist.append(e)
        omlist.append(om)

    return elist,omlist


def calc_peri(Tmid, P, ecc, omega):
    """
    caclulates the periastron time from time of mid transit, period,
    eccentricity and argument of periapsis
    """
    f = np.pi/2 -omega
    ee = 2 * np.arctan(np.tan(f/2) * np.sqrt((1-ecc)/(1+ecc)))
    tperi = Tmid - P/(2*np.pi)*(ee-ecc*np.sin(ee))
    return tperi-Tmid

def getecc(P,Tmid0,mu,ex0,ey0,I0,Omega0,Lambda,tToRV,t,MaxPower=4):
    """
    help function that calculates the complex eccentricty and inclination
    including free and forced elements
    """
    OrdersAll=len(P)
    '''
    free_e_T,free_I_T  = octave.feval("PreForcedElements",P,mu,ex0,ey0,I0,Omega0,tToRV,t,nout=2) 
    print("free_e_T {}", free_e_T)
    print("free_I_T {}", free_I_T)
    #todo: more than 2 planets
    #dz = forced eccentricities, dLambda=forced mean longitudes, du = forced inclinations, da_oa = 
    dz,dLambda,du,da_oa = octave.feval("ForcedElements1",P, Lambda, mu, free_e_T, free_I_T,OrdersAll,MaxPower,nout=4)
    z = free_e_T + dz
    u = free_I_T +du
    #Lambda = free lambda +dLambda

    return (z,u)
    '''
    return()

def geteccRV(P, Tmid0, mu, ex0,ey0,I0,Omega0,tRV,t,MaxPower=4):
    """
    calculates the complex eccentricty and inclination including free and forced
    elements for every point in tRV
    uses function getecc
    """
    n = 2*np.pi/np.asarray(P)
    NRV = len(tRV)
    t0_RV = np.zeros((len(tRV),len(Tmid0)))
    for i in range(len(Tmid0)):
        t0_RV[:,i] = tRV-Tmid0[i]
    Lambda_RV = t0_RV*repmat(n,NRV,1)
    Lambda_RV.reshape(-1)
    z,u = getecc(P,Tmid0,mu,ex0,ey0,I0,Omega0,Lambda_RV,tRV,t,MaxPower)
    return (z,u)

def geteccT(P,Tmid0,mu,ex0,ey0,I0,Omega0,tT,t,MaxPower=4):
    """
    calculates the complex eccentricty and inclination including free and forced
    elements for every point in tRV
    uses function getecc
    """
    n = 2*np.pi/np.asarray(P)
    NTr=len(tT)
    t0_T=np.zeros((len(tT),len(Tmid0)))
    for i in range(len(Tmid0)):
        t0_T[:,i]=tT[i]-Tmid0[i]
    Lambda_T=t0_T*repmat(n,NTr,1)
    Lambda_T.reshape(-1)
    z,u = getecc(P,Tmid0,mu,ex0,ey0,I0,Omega0,Lambda_T,tT,t,MaxPower)
    return (z,u)

#######################################################################################################################################################
################Dynesty and Octave enviroment###################################
################################################################################
def calculate(nkern, nlivepoints, parameters, fixed_parameters, savedatapath):
    #print_time("Starting calculate function")
    ndim = len(parameters)

    p = Pool(nkern, init, [parameters, fixed_parameters]) 
    # NestedSampler initialization
    #print_time("Starting the NestedSampler initialization")
    dsampler = dynesty.NestedSampler(loglike, prior_transform, ndim=ndim, periodic=None,
            bound='multi', sample='rwalk',nlive=nlivepoints,pool=p, queue_size=nkern) #n*n/2 live points mind.
    #print_time("NestedSampler initialization completed")
        # Sampling process
    #print_time("Starting sampling")
    os.makedirs(savedatapath, exist_ok = True)

    try:
        dsampler.run_nested(dlogz=0.1)
    except KeyboardInterrupt:
        print("Saving progress before exiting...")
        # Save the sampler state before exiting
        results = dsampler.results
        with open(f"{savedatapath}/dynesty_sampler_state_interrupt.pkl", 'wb') as f:
            pickle.dump(results, f)
        print("Sampler state saved!")
    #print_time("Sampling completed")
    
    # Processing results
    #print_time("Processing results")
    results = dsampler.results  # or any other processing step
    #print_time("Results processing completed")
    with open(f"{savedatapath}/restart.pickle", 'wb') as f:
        pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)	
    #print_time("Pickle completed")
    return results


def init(parameters, fixed_parameters):
    """
    initalizes global parameter and later octave instances
    for pooling in function calculate()
    """
    global p 
    p = parameters
    global fix
    fix = fixed_parameters


#################### Prior transforms for nested samplers ######################
def transform_uniform(x, hyperparameters):
    a, b = hyperparameters
    return a + (b-a)*x

def transform_loguniform(x, hyperparameters):
    a, b = hyperparameters
    la = np.log(a)
    lb = np.log(b)
    return np.exp(la + x * (lb - la))

def transform_normal(x, hyperparameters):
    mu, sigma = hyperparameters
    return norm.ppf(x, loc=mu, scale=sigma)

def transform_beta(x, hyperparameters):
    a, b = hyperparameters
    return beta.ppf(x, a, b)

def transform_exponential(x, hyperparameters):
    a = hyperparameters
    return gamma.ppf(x, a)

def transform_truncated_normal(x, hyperparameters):
    mu, sigma, a, b = hyperparameters
    ar, br = (a - mu) / sigma, (b - mu) / sigma
    return truncnorm.ppf(x, ar, br, loc=mu, scale=sigma)

def transform_modifiedjeffreys(x, hyperparameters):
    turn, hi = hyperparameters
    return turn * (np.exp( (x + 1e-10) * np.log(hi/turn + 1)) - 1)

def prior_transform(utheta):
    global p;
    global fix;
    #print_time("start prior_transfrom") 
    transforms = [x[2] for x in p]
    hyperparams = [x[3] for x in p]
    transformed_priors = np.zeros(len(utheta))
    for k,param in enumerate(utheta):
        if transforms[k] == 'uniform':
            transformed_priors[k] = transform_uniform(param,hyperparams[k])
        elif transforms[k] == 'loguniform':
            transformed_priors[k] = transform_loguniform(param,hyperparams[k])
        elif transforms[k] == 'normal':
            transformed_priors[k] = transform_normal(param,hyperparams[k])
        elif transforms[k] == 'beta':
            transformed_priors[k] = transform_beta(param,hyperparams[k])
        elif transforms[k] == 'exponential':
            transformed_priors[k] = transform_exponential(param,hyperparams[k])
        elif transforms[k] == 'truncated_normal':
            transformed_priors[k] = transform_truncated_normal(param,hyperparams[k])
        elif transforms[k] == 'modifiedjeffreys':
            transformed_priors[k] = transform_modifiedjeffreys(param,hyperparams[k])
        else:
            raise ValueError("Transform not known")
    #print_time("end piror_transform")
    return transformed_priors


def namespace(theta, p, fix):
    parameters = fix.copy()
    for i in range(len(theta)):
        planetid = p[i][0]
        keyname = p[i][1]
        if planetid == -1:
            parameters[keyname] = theta[i]
        elif planetid > 999:
            parameters[keyname][planetid-1000] = theta[i]
        elif planetid > 99:
            parameters[keyname][planetid-100] = theta[i]
        else:
            parameters[keyname][planetid] = theta[i]

    n = SimpleNamespace(**parameters)

    return n, parameters


def loglike(theta):
    """
    calculates the log-likelihood based on the AnalyticLC model
    returns -inf in case errors occur
    """
    global p;
    global fix;
    print_time("Starting loglike function")
    n, parameters = namespace(theta, p, fix)
    try:
        print_time("Starting all")
        aor = [n.aor]
        P = np.array(n.P, np.float64)
        Tmid0 = np.array(n.Tmid0, np.float64)
        ror = np.array(n.ror, np.float64)
        aor = np.array(aor, np.float64)
        mu = np.array(n.mu, np.float64)
        ex0 = np.array(n.ex0, np.float64)
        ey0 = np.array(n.ey0, np.float64)
        I0 = np.array(n.I0, np.float64)
        Omega0 = np.array(n.Omega0, np.float64)
        t = np.array(n.t, np.float64)
        tRV = np.array(n.tRV, np.float64)
        AnLC_params = pythonapi.AnalyticLCparams(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, n.u1[0], n.u2[0])
        print("after anlc init")
        MaxTTV = np.array([10], np.float64)
        AnLC_params.set_MaxTTV(MaxTTV)
        AnLC_params.set_tRV(tRV)
        AnLC_params.set_Calculate3PlanetsCrossTerms(False)
        print_time("before Anlc")
        output = pythonapi.AnalyticLCfunc(AnLC_params)
        print_time("after Anlc")
        main_out = output[0]
        # other_out = output[1]
        LC = main_out.get_LC()
        RV_o_r = main_out.get_RV_o_r()
        ret =0
        
        ##print_time("AnalyticLCfunc completed")
        if len(n.RV)!= 0:#in case of RV data
            index = 0
            conti = True
            while conti == True:
                print_time("Starting RV data")
                try:
                    tRV_inst = np.array(fix[f"tRV_inst_{index}"], np.float64)
                    RV_inst = np.array(fix[f"RV_inst_{index}"], np.float64)
                    RVerr_inst = np.array(fix[f"RVerr_inst_{index}"], np.float64)
                    RVname_inst = fix[f"RVname_inst_{index}"]
                    tRV_inst_set = set(tRV_inst)
                    indices = [j for j, x in enumerate(tRV) if x in tRV_inst_set] #error if two times match exactly for two instruments
                
                    modelRV = RV_o_r[indices]
                    S_R = n.R_S*6.957e8 #stellar radii in meters
                    d = 86400 #seconds in a days
                    modelRV = modelRV*(S_R/d) #RV_o_R output from AnalyticLC in stellar radii over time unit (days)
                
                    RVerr = RVerr_inst*(S_R/d)
                    inv_sigmaRV = 1.0/(RVerr**2 + np.exp( 2 * n.sigRV[index]))
                    ret += -0.5 * (np.sum( (RV_inst-modelRV)**2 * inv_sigmaRV - np.log(inv_sigmaRV)))
                
                    index += 1
                except KeyError:
                    # If a KeyError is raised, break the loop
                    conti = False 
                           
        if len(n.TR) != 0:  #in case of transit data
            index = 0 
            conti = True
            while (conti == True):
                print_time("Starting transit data")
                try:
                    t_inst = np.array(fix[f"t_inst_{index}"], np.float64)
                    TR_inst = np.array(fix[f"TR_inst_{index}"], np.float64)
                    TRerr_inst = np.array(fix[f"TRerr_inst_{index}"], np.float64)
                    TRname_inst = fix[f"TRname_inst_{index}"]
                    if index == 0:
                        t_inst_set = set(t_inst)
                        indices = [j for j, x in enumerate(t) if x in t_inst_set]
                        modelLC = LC[indices]
                        if len(TR_inst)!= len(modelLC):#error if two times match exactly for two instruments
                            print("WARNING: lengths of model and input don't agree, maybe two times for two instruments match exactly")
                    else:
                        t_inst = np.array(t_inst, np.float64)
                        AnLC_params = pythonapi.AnalyticLCparams(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t_inst, n.u1[index], n.u2[index])
                        AnLC_params.set_MaxTTV(MaxTTV)
                        output = pythonapi.AnalyticLCfunc(AnLC_params)
                        main_out = output[0]
                        # other_out = output[1]
                        modelLC = main_out.get_LC()
                    inv_sigmaLC = 1.0/(TRerr_inst**2 + np.exp( 2 * n.sigTR[index]))
                    ret += -0.5 * (np.sum( (TR_inst-modelLC)**2 * inv_sigmaLC - np.log(inv_sigmaLC)))
                    print(f"ret is {ret}")
                    print(f"index is {index}")
                    index += 1 
                except KeyError:
                    # If a KeyError is raised, break the loop
                    conti = False

        if math.isnan(ret):
            return -np.inf
        #print_time("Loglike function completed")

        return ret
        
    except Exception as e:
        print(P, Tmid0, ror, aor, mu, ex0,ey0, I0, Omega0,t, n.u1, n.u2, tRV)
        print(e)
        return -np.inf
        

        
################################################################################
######################### Plotting of results ##################################
################################################################################

def create_plot(dres,p,fix,rvdatalist, trdatalist, ndim,savedatapath):
    #truths = [P_true[0], P_true[1], sig_true]
    """
    labels = [x[1] for x in p]
    ###################### Trace Plot ###########################################
    fig, axes = dyplot.traceplot(dres, labels=labels, connect=True,
                                 fig=plt.subplots(ndim, 2, figsize=(10, 2*ndim)),
                                 trace_kwargs={'linewidth': 1},
                                 connect_kwargs={'markersize': 2})
    
    for ax in axes.flatten():
        ax.tick_params(axis='both', which='major', labelsize=8)
        ax.set_title(ax.get_title(), fontsize=10)
        ax.set_xlabel(ax.get_xlabel(), fontsize=8)
        ax.set_ylabel(ax.get_ylabel(), fontsize=8)

    fig.tight_layout()
    fig.subplots_adjust(top=0.95, hspace=0.4)
    plt.savefig(f"{savedatapath}/traceplot.png", dpi=300)
    #plt.show()
    plt.close()
    
    ###################### Corner Plot #########################################
    fig, axes = dyplot.cornerplot(dres, show_titles=True,
                                  title_kwargs={'y': 1.04, 'fontsize': 8},
                                  labels=labels,
                                  fig=plt.subplots(ndim, ndim, figsize=(2*ndim, 2*ndim)),
                                  max_n_ticks=3)
    
    for ax in axes.flatten():
        ax.tick_params(axis='both', which='major', labelsize=8)
        if hasattr(ax, 'set_title'):
            ax.set_title(ax.get_title(), fontsize=10)
        if hasattr(ax, 'set_xlabel'):
            ax.set_xlabel(ax.get_xlabel(), fontsize=8)
        if hasattr(ax, 'set_ylabel'):
            ax.set_ylabel(ax.get_ylabel(), fontsize=8)

    fig.tight_layout()
    fig.subplots_adjust(top=0.95, hspace=0.4, wspace=0.4)
    plt.savefig(f"{savedatapath}/cornerplot.png", dpi=300)
    #plt.show()
    plt.close()  
    ##################### lightcurve and RV ####################################
    """
    print(f"dres['samples'] size: {len(dres['samples'])}")
    print(f"dres['niter']: {dres['niter']}")
    solu = dres["samples"][dres["niter"]] #solution with highest likelihood
    n, parameters = namespace(solu, p, fix)
    print(len(n.TR))
    if len(n.TR)!=0:
        t = np.linspace(n.t[0], n.t[-1], max(int(n.t[-1]-n.t[0]),1)*50)
    else:
        t = []
    print(f"t is {t}")
    if len(n.RV)!=0:
        tRV = np.linspace(n.tRV[0], n.tRV[-1], max(int(n.tRV[-1]-n.tRV[0]),1)*10)
    else:
        tRV = []
    print(f"tRV is {tRV}")
    AnLC_params = pythonapi.AnalyticLCparams()
    aor = [n.aor]
    P = np.array(n.P, np.float64)
    Tmid0 = np.array(n.Tmid0, np.float64)
    ror = np.array(n.ror, np.float64)
    aor = np.array(aor, np.float64)
    mu = np.array(n.mu, np.float64)
    ex0 = np.array(n.ex0, np.float64)
    ey0 = np.array(n.ey0, np.float64)
    I0 = np.array(n.I0, np.float64)
    Omega0 = np.array(n.Omega0, np.float64)
    t = np.array(t, np.float64)
    tRV = np.array(tRV, np.float64)
    AnLC_params = pythonapi.AnalyticLCparams(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, n.u1[0], n.u2[0])
    AnLC_params.set_tRV(tRV)
    MaxTTV = np.array([10], np.float64)
    AnLC_params.set_MaxTTV(MaxTTV)
    output = pythonapi.AnalyticLCfunc(AnLC_params)
    main_out = output[0]
    # other_out = output[1]
    LC = main_out.get_LC()
    print(f"LC is {LC}")
    RV_o_r = main_out.get_RV_o_r()
    """f len(n.TR)!=0:
        plt.plot(t, LC, label = 'model', zorder = 2.3)
        for i, tr_inst in enumerate(trdatalist):
            t_inst, TR_inst, TRerr_inst, TRname_inst = tr_inst
            plt.errorbar(t_inst, TR_inst, yerr=TRerr_inst, fmt='o', label = TRname_inst)
        plt.legend()
        plt.xlabel("time [d]")
        plt.ylabel("flux noramlized")
        plt.savefig(f"{savedatapath}/LC_final.png")
        plt.show()
        plt.close()

        for i, tr_inst in enumerate(trdatalist):
            t_inst, TR_inst, TRerr_inst, TRname_inst = tr_inst
            plt.errorbar(t_inst, TR_inst, yerr=TRerr_inst, fmt='o', label = TRname_inst)
        plt.legend()
        plt.xlabel("Time [BTJD]")
        plt.ylabel("Normalized flux")
        plt.savefig(f"{savedatapath}/lc_overview.png")
        plt.show()
        plt.close()
        os.makedirs(savedatapath, exist_ok=True)


        transitplotspath = os.path.join(savedatapath, "transitplots")

        # Create the transitplots subfolder if it doesn't exist
        os.makedirs(transitplotspath, exist_ok=True)
        #phase folded

        y_min = 0.997
        y_max = 1.002
        for count, P in enumerate(n.P):
            closetotransittimes = []
            closetotransittimesphase_mod = []
            closetotransitTR_mod = []
            closetotransitTRerr_mod = []
            
            # Loop through t and calculate the model
            for num, ti in enumerate(t):
                t_phase = (ti - Tmid0[count] + 0.5 * P) % P - 0.5 * P
                if abs(t_phase) <= 0.05:
                    closetotransittimes.append(t_inst)
                    closetotransittimesphase_mod.append(t_phase)
                    closetotransitTR_mod.append(LC[num])
            
            phase_sorted_indices_mod = np.argsort(closetotransittimesphase_mod)
            closetotransittimesphase_mod = np.array(closetotransittimesphase_mod)[phase_sorted_indices_mod]
            closetotransitTR_mod = np.array(closetotransitTR_mod)[phase_sorted_indices_mod]
            
            # Prepare figure with subplots
            fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [3, 1]})
            
            # Plot the model on the first subplot
            ax1.plot(closetotransittimesphase_mod, closetotransitTR_mod, label='model', linewidth=3, zorder=2.3)
            ax2.plot([],[])
            # Loop through the data
            for i, tr_inst in enumerate(trdatalist):
                t_inst, TR_inst, TRerr_inst, TRname_inst = tr_inst
                closetotransittimes = []
                closetotransittimesphase = []
                closetotransitTR = []
                closetotransitTRerr = []
                for num, ti in enumerate(t_inst):
                    t_phase = (ti - Tmid0[count] + 0.5 * P) % P - 0.5 * P
                    if abs(t_phase) <= 0.05:
                        closetotransittimes.append(t_inst)
                        closetotransittimesphase.append(t_phase)
                        closetotransitTR.append(TR_inst[num])
                        closetotransitTRerr.append(TRerr_inst[num])

                # Convert to arrays
                closetotransittimesphase = np.array(closetotransittimesphase)
                closetotransitTR = np.array(closetotransitTR)
                closetotransitTRerr = np.array(closetotransitTRerr)

                # Plot data with error bars on the first subplot
                ax1.errorbar(closetotransittimesphase, closetotransitTR, yerr=closetotransitTRerr, fmt='o', alpha=0.5, markersize=4, label=TRname_inst)
                model_interp = np.interp(closetotransittimesphase, closetotransittimesphase_mod, closetotransitTR_mod)
                residuals = closetotransitTR - model_interp  
                # Calculate and plot residuals on the second subplot
                ax2.errorbar(closetotransittimesphase, residuals, yerr=closetotransitTRerr, fmt='o', alpha=0.5, markersize=4, label=TRname_inst)

            # Plot details for model and data
            ax1.set_ylim(y_min, y_max)
            ax1.set_ylabel("Flux Normalized")
            ax1.legend()

            # Plot residuals subplot
            ax2.axhline(0, color='gray', linewidth=1)
            ax2.set_xlabel("Phase")
            ax2.set_ylabel("Residuals")
            
            # Save and display
            plt.tight_layout()
            plt.savefig(f"{savedatapath}/LC_phase_{P}_with_residuals.png")
            plt.show()
            plt.close()

            # Snip around every transit and save plots
            for i in range(int(np.max(n.t) // P)):  # Loop over each transit
                transit_center = Tmid0[count] + i * P
                window_mask = (n.t >= transit_center - 0.02 * P) & (n.t <= transit_center + 0.02 * P)
                if np.any(window_mask):
                    model_window = LC[(t >= transit_center - 0.02 * P) & (t <= transit_center + 0.02 * P)]
                    t_model_window = t[(t >= transit_center - 0.02 * P) & (t <= transit_center + 0.02 * P)]

                    # Prepare figure with subplots for each transit
                    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [3, 1]})

                    # Plot model on first subplot
                    ax1.plot(t_model_window, model_window, label='model', color='orange', linewidth=3, zorder=2.3)

                    # Loop over the data for each transit
                    for i, tr_inst in enumerate(trdatalist):
                        t_inst, TR_inst, TRerr_inst, TRname_inst = tr_inst
                        window_mask = (t_inst >= transit_center - 0.02 * P) & (t_inst <= transit_center + 0.02 * P)
                        t_window = t_inst[window_mask]
                        TR_window = TR_inst[window_mask]
                        TRerr_window = TRerr_inst[window_mask]

                        # Plot data with error bars on the first subplot
                        ax1.errorbar(t_window, TR_window, yerr=TRerr_window, fmt='o', alpha=0.5, markersize=4, label=TRname_inst)

                        # Calculate and plot residuals on the second subplot
                        model_interp = np.interp(t_window, t_model_window, model_window)
                        residuals = TR_window - model_interp  # observed - model
                        ax2.errorbar(t_window, residuals, yerr=TRerr_window, fmt='o', alpha=0.5, markersize=4, label=TRname_inst)

                    # Plot details for model and data
                    ax1.set_ylabel("Flux Normalized")
                    ax1.legend()
                    ax1.set_ylim(y_min, y_max)
                    # Plot residuals subplot
                    ax2.axhline(0, color='gray', linewidth=1)
                    ax2.set_xlabel("Time [d]")
                    ax2.set_ylabel("Residuals")

                    # Save and display
                    plt.tight_layout()
                    plt.savefig(f"{transitplotspath}/Transit_{transit_center:.2f}_days_with_residuals.png")
                    plt.show()
                    plt.close()
       """
    if len(n.RV)!=0:
        d = 86400 #seconds in a day
        S_R = n.R_S*6.957e8 #stellar radii in meters
        RV_o_r = RV_o_r*(S_R/d)
        plt.plot(tRV, RV_o_r, 'o', label = 'model', zorder = 2.3)
        for i, rv_inst in enumerate(rvdatalist):
            t_inst, RV_inst, RVerr_inst, RVname_inst = rv_inst
            plt.errorbar(t_inst, RV_inst, yerr=RVerr_inst, fmt='o', label = RVname_inst)
        plt.legend()
        plt.xlabel("time [d]")
        plt.ylabel("RV [m/s]")
        plt.savefig(f"{savedatapath}/RV_final.png")
        plt.show()
        plt.close()

        #phase folded
        for P in n.P:
            #make plot bigger
            plt.figure(figsize=(10, 6))
            plt.plot((tRV-tRV[0])%P, RV_o_r, 'o', ms = 0.28, label = 'model', zorder = 2.3)
            for i, rv_inst in enumerate(rvdatalist):
                t_inst, RV_inst, RVerr_inst, RVname_inst = rv_inst
                plt.errorbar((t_inst-tRV[0])%P, RV_inst, yerr = RVerr_inst, fmt='o', label = RVname_inst)
            plt.xlabel("phase")
            plt.ylabel("RV [m/s]")
            plt.legend()
            plt.savefig(f"{savedatapath}/RV_phase_{P}.png")
            plt.show()
            plt.close()

           


def compute_summary_stats(samples):
    """Compute the median and 1-sigma credible interval (16th and 84th percentiles) of samples."""
    medians = np.median(samples, axis=0)
    lower_bounds = np.percentile(samples, 16, axis=0)
    upper_bounds = np.percentile(samples, 84, axis=0)
    return medians, lower_bounds, upper_bounds

def save_results_text(dres, p, savedatapath):
    filepath = f"{savedatapath}/results.txt"
    labels = [x[1] for x in p]
    samples = dres["samples"]
    samples = np.atleast_1d(samples)
    if len(samples.shape) == 1:
        samples = np.atleast_2d(samples)
    else:
        assert len(samples.shape) == 2, "Samples must be 1- or 2-D."
        samples = samples.T
    assert samples.shape[0] <= samples.shape[1], "There are more " \
                                                 "dimensions than samples!"


    len(samples)
    medians, lower_bounds, upper_bounds = compute_summary_stats(samples)
    with open(filepath, 'w') as f:

        f.write("Dynesty Run Results\n")
        f.write("===================\n\n")

        f.write("Log Evidence (logz):\n")
        f.write(f"  {dres.logz[-1]:.4f} ± {dres.logzerr[-1]:.4f}\n\n")
        weights = dres.importance_weights()
        title_quantiles=(0.025, 0.5, 0.975),
        f.write("Parameters:\n")
        for i, a in enumerate(samples):
            qs = _quantile(a, title_quantiles, weights=weights)
            ql = qs[0][0]
            qm = qs[0][1]
            qh = qs[0][2]
            q_minus, q_plus = qm - ql, qh - qm
            f.write(f"   {labels[i]}:\n")
            f.write(f"        {qm} + {q_plus} - {q_minus} \n")
        
        f.write("Additional Information:\n")
        f.write(f"  Number of iterations: {dres.niter}\n")
        f.write(f"  Number of samples: {len(dres['samples'])}\n\n")

    
################################################################################
######################### Testing and trying ###################################
################################################################################

def testeccmain():
    """
    function to test the behaviour of eccentric planets
    """
    P_true=[5.1]
    Tmid0_true=[27.458]
    ror_true=[0.1]
    aor_true=29.6697
    mu_true=[3.8101e-5]
    ex0_true=[0.4655]
    ey0_true=[0.0046]
    I0_true=[0.0]
    Omega0_true=[0.0]
    sig_true = -9 

    #t_mock = np.linspace(0,100,500)
    u1=0.5;
    u2=0.3;
    t=np.linspace(0,50,2) #0:0.002:100;
    tRV=np.linspace(0,50,100)
    AnLC_params = pythonapi.AnalyticLCparams()

    pythonapi.construct(AnLC_params, P_true, Tmid0_true, ror_true, aor_true, mu_true, ex0_true, ey0_true, I0_true,Omega0_true, t, u1, u2)
    AnLCparams.set_tRV(n.tRV)
    output = pythonapi.AnalyticLCfunc(AnLC_params)
    main_out = output[0]
        # other_out = output[1]
    LC = main_out.get_LC()
    RV_o_r = main_out.get_RV_o_r()
        
    plt.plot(tRV, RV_o_r)
    plt.savefig(f"ecc/ex{ex0_true[0]}_ey{ey0_true[0]}.png")
    plt.show()
    plt.close()
    for P in P_true:
        #plt.plot((tRV-tRV[0])%P, RV_o_r, 'o',label = 'model')
        plt.plot((tRV-tRV[0])%P, RV_o_r, 'o',label = 'model')
        plt.savefig(f"ecc/ppex{ex0_true[0]}_ey{ey0_true[0]}.png")
        #print(O.free_e_RV)
    #z = ex0_true[0]+j*ey0_true[0]

    #e = octave.feval("ForcedElements1(P_true, O[Lambda_T], mu_true, 

#def ppcomparison(mstar, rstar, limbdark, mp, rp, Tmid0, P, ex0, ey0, I0, Omega0, dist,t):
    """
    uses planetplanet to compare lightcurves to AnalyticLC
    plots orbits from planetplanet, lightcurve comparison
    and difference between models
    """
    """
    # parameters
    mearth = 5.9722e24 #in kg
    rearth = 6.3781e6 #in m
    msun = 1.98847e30 #in kg
    rsun = 6.957e8 #in m
    pc = 3.086e16 #pc to meters
    
    # converting different input parameters
    ror = (np.array(rp)/rstar)/(rsun/rearth) #rp is in earth radii, rstar in sun
    aor = (dist[0]/rstar)/(rsun/pc) #dist is in pc, rstar in sun radii
    mu = (np.array(mp)/mstar)/(msun/mearth)  #mp in earth masses, mstar in sun
    tRV = []
    AnLC_params = pythonapi.AnalyticLCparams()

    pythonapi.construct(AnLC_params, P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2)
    AnLCparams.set_tRV(n.tRV)
    output = pythonapi.AnalyticLCfunc(AnLC_params)
    main_out = output[0]
        # other_out = output[1]
    LC = main_out.get_LC()
    RV_o_r = main_out.get_RV_o_r()
    z,u = geteccT(P,Tmid0,mu,ex0,ey0,I0,Omega0,O.tT,t)
    inc = np.array(I0)+90
    print(f"z: {z}")
    e,omega = z_to_eom(z) 
    star = pp.Star('star', m=mstar, r = rstar, limbdark = limbdark)
    Np = len(mp)
    planets = [None]*Np
    names = ["a","b","c","d","e"]
    for i in range(Np):
        planets[i] = pp.Planet(names[i], m=mp[i], r=rp[i], per=P[i],inc=inc[i],t0=Tmid0[i], ecc=e[i], w=omega[i],Omega=Omega0[i])
    system = pp.System(star,*planets,distances=dist)
    
    ################################ Plotting #################################
    system.compute(t)
    system.plot_orbits()
    plt.savefig("toi216/late_orbits.png")
    plt.show()
    plt.clf()
    plt.cla()
    plt.close()
    
    # plots AnalyticLC and planetplanet lighcurves in one plot
    system.plot_lightcurve(wavelength=15)
    plt.plot(t,LC)
    plt.savefig("toi216/late_lc_pp.png")
    plt.show()
    plt.clf()
    plt.cla()
    plt.close()
    
    #plots difference between AnalyticLC and planetplanet models
    wavelength = 15
    w = np.argmax(1e-6 * wavelength <= system.bodies[0].wavelength)
    flux = system.flux[:,w] / np.nanmedian(system.flux[:,w])
    diff = flux-LC
    plt.plot(t,diff)
    plt.xlabel("t [d]")
    plt.ylabel("diff norm. flux planetplanet-AnalyticLC")
    plt.savefig("toi216/late_diff.png")
    plt.show()
    plt.clf()
    plt.cla()
    plt.close()
    """
#def testplanetplanet():
    """
    parameters from Dawson et al. (2019) for TOI-216b und TOI-216c to use function
    ppcomparison to compare between planetplanet and AnalyticLC for planets close
    to resonance
    """
    """
    mearth = 5.9722e24 #in kg
    rearth = 6.3781e6 #in m
    msun = 1.98847e30 #in kg
    rsun = 6.957e8 #in m
    pc = 3.086e16 #parsec to meters
    mstar = 0.78
    rstar = 9.765
    rp = [8.6,10.2]
    mp = [15.9,82.6]
    dist = [29.1*rstar*rsun/pc, 53.8*rstar*rsun/pc]
    P=[17.1,34.51]
    Tmid0=[1325.328,1331.2851]
    ex0=[0.1234, 0.00998]
    ey0=[0.2345, 0.0189]
    I0=[0.0001, 0.0315]
    Omega0=[0.0001, 0.0001]
    tf,flux,ferr = np.loadtxt("TOI216_S1_1.csv",delimiter=',',unpack=True)
    t = np.arange(1300,2100,0.1)
    #t_mock = np.linspace(0,100,500)
    limbdark=[0.33,0.32]    
    ppcomparison(mstar, rstar, limbdark, mp, rp, Tmid0, P, ex0, ey0, I0, Omega0, dist,t)
"""
def runsingle(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV):
    """
    runs a single AnalyticLC model with given parameters
    """
    P = np.array(P, np.float64)
    Tmid0 = np.array(Tmid0, np.float64)
    ror = np.array(ror, np.float64)
    aor = np.array(aor, np.float64)
    mu = np.array(mu, np.float64)
    ex0 = np.array(ex0, np.float64)
    ey0 = np.array(ey0, np.float64)
    I0 = np.array(I0, np.float64)
    Omega0 = np.array(Omega0, np.float64)
    t = np.array(t, np.float64)
    tRV = np.array(tRV, np.float64)
    #AnLC_params = pythonapi.AnalyticLCparams(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, n.u1, n.u2)
    

    AnLC_params = pythonapi.AnalyticLCparams(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2)
    AnLC_params.set_MaxTTV(np.array([10], np.float64))
    AnLC_params.set_tRV(tRV)
    AnLC_params.set_Calculate3PlanetsCrossTerms(True)
    output = pythonapi.AnalyticLCfunc(AnLC_params)
    main_out = output[0]
    # other_out = output[1]
    LC = main_out.get_LC()
    RV_o_r = main_out.get_RV_o_r()
    Y_o_r = main_out.get_Y_o_r()
    Z_o_r = main_out.get_Z_o_r()
    return LC, RV_o_r, Y_o_r, Z_o_r 

def plotsingle(LC, RV_o_r, t, tRV, Plist, label):
    for P in Plist:
            phase_sorted_indices = np.argsort((t - t[0] + 0.5 * P) % P + 0.5 * P)
            t_phase_sorted = ((t - t[0] + 0.5 * P) % P + 0.5 * P)[phase_sorted_indices]
            LC_phase_sorted = LC[phase_sorted_indices]
            plt.plot(t_phase_sorted, LC_phase_sorted, label=label, color='orange', linewidth=3, zorder = 2.3)
        
            plt.xlabel("phase")
            plt.ylabel("flux normalized")
            plt.legend()
            plt.show()
            plt.close()

    """
    plots a single AnalyticLC model
    """
    plt.plot(tRV, RV_o_r, 'o', label = 'model', zorder = 2.3)
    plt.xlabel("time [d]")
    plt.ylabel("RV [S_R/d]")
    plt.show()

from mpl_toolkits.mplot3d import Axes3D

def plotmultiple(P, Tmid0, ror, aor, mu, t, u1, u2, tRV):
    """runs multiple single with different I and ecc and plots them in one plot for each """
    
    # Define parameter ranges
    I0 = np.deg2rad(np.array([1e-9,0.5,1,2,3.14, 7, 15, 50, 70, 90]))
    Omega0 = np.deg2rad(np.array([1e-9,0.5,1,2,3.14, 7, 15, 50, 70, 90]))
    ex0 = np.array([1e-9, 0.0001,0.001, 0.005, 0.01,0.05, 0.1,0.3, 0.5,0.9])
    ey0 = np.array([1e-9, 0.0001,0.001, 0.005, 0.01, 0.05, 0.1,0.3, 0.5,0.9])
    
    # Arrays for u1 and u2 to be varied
    u1_vary = np.array([0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5])
    u2_vary = np.array([0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5])
    
    print(tRV)
    print(len(P))

    # Define colormaps for u1 values
    color_maps = ['Blues', 'Reds', 'Greens', 'Purples', 'Oranges', 'Greys', 'YlGn', 'YlOrRd', 'BuPu']
    
    plt.figure(figsize=(10, 8))  # Adjust figure size

    # Loop over u1 and u2 values
    for i in range(9):  # For each u1 value
        for j in range(9):  # For each u2 value
            
            # Set specific parameters for the simulation
            I0_ij = np.array([I0[0], I0[0]], np.float64)
            Omega0_ij = np.array([Omega0[9], Omega0[9]], np.float64)
            ex0_ij = np.array([ex0[0], ex0[0]], np.float64)
            ey0_ij = np.array([ey0[0], ey0[0]], np.float64)
            
            # Current values of u1 and u2
            u1_i = u1_vary[i]
            u2_j = u2_vary[j]
            
            # Call to the function that generates data (you can modify this according to your needs)
            LC, RV, Y, Z = runsingle(P, Tmid0, ror, aor, mu, ex0_ij, ey0_ij, I0_ij, Omega0_ij, t, u1_i, u2_j, tRV)
            
            # Choose a color from the respective colormap for the current (u1, u2) combination
            colormap = plt.get_cmap(color_maps[i])  # Pick colormap based on u1
            color = colormap(j / len(u2_vary))  # Adjust intensity based on u2
            
            # Plot the light curve with the selected color
            plt.plot(t, LC, label=f"u1={u1_i}, u2={u2_j}", color=color)
    
    # Labels and Legend
    plt.xlabel("time [d]")
    plt.ylabel("flux normalized")
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', fontsize='small', ncol=2)  # Adjust legend outside the plot
    plt.tight_layout()
    plt.savefig("fullLC_u1u2_colored.png")
    plt.show()
    plt.close()
def plotmultiple_all(P, Tmid0, ror, aor, mu, t, u1, u2, tRV):
    """runs multiple single with different I and ecc and plots them in one plot for each """
    I0 = np.deg2rad(np.array([1e-9,0.5,1,2,3.14, 7, 15, 50, 70, 90]))
    Omega0 = np.deg2rad(np.array([1e-9,0.5,1,2,3.14, 7, 15, 50, 70, 90]))
    ex0 = np.array([1e-9, 0.0001,0.001, 0.005, 0.01,0.05, 0.1,0.3, 0.5,0.9])
    ey0 = np.array([1e-9, 0.0001,0.001, 0.005, 0.01, 0.05, 0.1,0.3, 0.5,0.9])
    u1_vary = np.array([0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5])
    u2_vary = np.array([0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5])
    print(tRV)
    print(len(P))
    for i in range(9):
        for j in range(9):
            I0 = np.array([I0[0], I0[0]], np.float64)
            Omega0_0 = np.array([Omega0[9], Omega0[9]], np.float64)
            ex0_0 = np.array([ex0[0],ex0[0]], np.float64)
            ey0_0 = np.array([ey0[0],ey0[0]], np.float64)
            u1_i = u1_vary[i]
            u2_j = u2_vary[j]
            LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_0, ey0_0, I0, Omega0_0, t, u1_i, u2_j, tRV)
            plt.plot(t, LC, label = f"u1 is {u1_i}, u2 is {u2_j}")
    plt.xlabel("time [d]")
    plt.ylabel("flux normalized")
    plt.legend()
    plt.savefig("fullLC_u1u2.png")
    plt.show()
    plt.close()
    if len(P)>1:
        fig, axes = plt.subplots(nrows=1, ncols=len(P)*2, figsize=(12, 6))
        for i in range(10):
            try:
                print(i)
                I0_i = np.array([I0[i], I0[0]], np.float64)
                print(I0_i)
                Omega0_0 = np.array([Omega0[9], Omega0[9]], np.float64)
                print(Omega0_0)
                ex0_0 = np.array([ex0[0],ex0[0]], np.float64)
                ey0_0 = np.array([ey0[0],ey0[0]], np.float64)
                LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_0, ey0_0, I0_i, Omega0_0, t, u1, u2, tRV)
                print("here")
                print(RV)
                for j, period in enumerate(P):
                    phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                    t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                    LC_phase_sorted = LC[phase_sorted_indices]
                    lower_bound = -period / 4
                    upper_bound = period / 4
                    """
                    # Filter for the desired range on the x-axis
                    mask = (t_phase_sorted >= lower_bound) & (t_phase_sorted <= upper_bound)
                    t_phase_filtered = t_phase_sorted[mask]
                    LC_phase_filtered = LC_phase_sorted[mask]
                    """
                    axes[j].plot(t_phase_sorted, LC_phase_sorted, label=f"I0 is {I0[i]}")

                    
                """
                for j,period in enumerate(P):
                    phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                    t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                    LC_phase_sorted = LC[phase_sorted_indices]
                    axes[j].plot(t_phase_sorted, LC_phase_sorted, label=f"I0 is {I0[i]}")
                """
                for j,period in enumerate(P):
                    axes[len(P)+j].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = f"I0 is {I0[i]}")

            except Exception as e:
                print(e)
        axes[0].set_xlabel("phase")
        axes[0].set_ylabel("flux normalized")
        axes[0].legend()
        axes[len(P)].set_xlabel("phase")
        axes[len(P)].set_ylabel("RV [R_S/d]")
        axes[len(P)].legend()
        plt.show()
        plt.close()

    for i in range(10):
        I0_i = np.array([I0[i], I0[0]], np.float64)
        Omega0_0 = np.array([Omega0[9], Omega0[9]], np.float64)
        ex0_0 = np.array([ex0[0],ex0[0]], np.float64)
        ey0_0 = np.array([ey0[0],ey0[0]], np.float64)
        LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_0, ey0_0, I0_i, Omega0_0, t, u1, u2, tRV)
        plt.plot(t, LC, label = f"I0 is {I0[i]}")
    plt.xlabel("time [d]")
    plt.ylabel("flux normalized")
    plt.legend()
    plt.savefig("fullLC_i0.png")
    plt.show()
    plt.close()



    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    for i in range(10):
        try:
            print(i)
            I0_i = np.array([I0[i]], np.float64)
            print(I0_i)
            Omega0_0 = np.array([Omega0[9]], np.float64)
            print(Omega0_0)
            ex0_0 = np.array([ex0[0]], np.float64)
            ey0_0 = np.array([ey0[0]], np.float64)
            LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_0, ey0_0, I0_i, Omega0_0, t, u1, u2, tRV)
            print("here")
            print(RV)
            for period in P:
                phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                LC_phase_sorted = LC[phase_sorted_indices]
                axes[0].plot(t_phase_sorted, LC_phase_sorted, label=f"I0 is {I0[i]}")
            for period in P:
                axes[1].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = f"I0 is {I0[i]}")
                plt.pl
        except Exception as e:
            print(e)
    axes[0].set_xlabel("phase")
    axes[0].set_ylabel("flux normalized")
    axes[0].legend()
    axes[1].set_xlabel("phase")
    axes[1].set_ylabel("RV [R_S/d]")
    axes[1].legend()
    plt.savefig("varyi0.png")
    plt.show()
    plt.close()
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    for i in range(10):
        try:
            I0_0 = np.array([I0[0]], np.float64)
            Omega0_i = np.array([Omega0[i]], np.float64)
            ex0_0 = np.array([ex0[0]], np.float64)
            ey0_0 = np.array([ey0[0]], np.float64)
            LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_0, ey0_0, I0_0, Omega0_i, t, u1, u2, tRV)
            for period in P:
                phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                LC_phase_sorted = LC[phase_sorted_indices]
                axes[0].plot(t_phase_sorted, LC_phase_sorted, label=f"Omega0 is {Omega0[i]}")
            for period in P:
                axes[1].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = f"Omega0 is {Omega0[i]}")

        except Exception as e:
            print(e)
    axes[0].set_xlabel("phase")
    axes[0].set_ylabel("flux normalized")
    axes[0].legend()
    axes[1].set_xlabel("phase")
    axes[1].set_ylabel("RV [R_S/d]")
    axes[1].legend()
    plt.savefig("varyomega0.png")
    plt.show()
    plt.close()

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    for i in range(10):
        try:
            I0_i = np.array([I0[i]], np.float64)
            Omega0_i = np.array([Omega0[i]], np.float64)
            ex0_0 = np.array([ex0[0]], np.float64)
            ey0_0 = np.array([ey0[0]], np.float64)
            LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_0, ey0_0, I0_i, Omega0_i, t, u1, u2, tRV)
            for period in P:
                phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                LC_phase_sorted = LC[phase_sorted_indices]
                axes[0].plot(t_phase_sorted, LC_phase_sorted, label=f"I0 is {I0[i]}, Omega0 is {Omega0[i]}")
            for period in P:
                axes[1].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = f"I0 is {I0[i]}, Omega0 is {Omega0[i]}")

        except Exception as e:
            print(e)
    axes[0].set_xlabel("phase")
    axes[0].set_ylabel("flux normalized")
    axes[0].legend()
    axes[1].set_xlabel("phase")
    axes[1].set_ylabel("RV [R_S/d]")
    axes[1].legend()
    plt.savefig("varyiom.png")
    plt.show()
    plt.close()

    from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset


    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    plt.subplots_adjust(wspace=0.3)  # Adjust space between plots


    # Create zoomed-in insets outside the loop
    ax_inset_0 = inset_axes(axes[0], width="40%", height="40%", loc="upper right")
    ax_inset_1 = inset_axes(axes[1], width="40%", height="40%", loc="lower right")
    for i in range(10):
        try:
            I0_0 = np.array([I0[0]], np.float64)
            Omega0_0 = np.array([Omega0[9]], np.float64)
            ex0_i = np.array([ex0[i]], np.float64)
            ey0_0 = np.array([ey0[0]], np.float64)
            LC, RV, Y, Z = runsingle(P, Tmid0, ror, aor, mu, ex0_i, ey0_0, I0_0, Omega0_0, t, u1, u2, tRV)

            # Plot LC
            for period in P:
                phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                LC_phase_sorted = LC[phase_sorted_indices]
                axes[0].plot(t_phase_sorted, LC_phase_sorted, label=f"ex0 is {ex0[i]}")

                # Add zoomed-in inset for axes[0] inside the loop
                ax_inset_0.plot(t_phase_sorted, LC_phase_sorted)
                ax_inset_0.set_xlim(3.715, 3.74)
                ax_inset_0.set_ylim(0.99889, 0.99894)
                ax_inset_0.set_xticks(np.linspace(3.715, 3.74, 5))
                ax_inset_0.set_yticks(np.linspace(0.99889, 0.99894, 3))
                ax_inset_0.tick_params(axis='both', which='both', labelsize=8)

            # Plot RV
            for period in P:
                axes[1].plot((tRV - tRV[0]) % period, RV, 'o', ms=0.22, label=f"ex0 is {ex0[i]}")

                # Add zoomed-in inset for axes[1] inside the loop
                ax_inset_1.plot((tRV - tRV[0]) % period, RV, 'o', ms=0.22)
                ax_inset_1.set_xlim(0.67, 0.8)
                ax_inset_1.set_ylim(-0.0006255, -0.000616)
                ax_inset_1.set_xticks(np.linspace(0.6, 0.8, 4))
                ax_inset_1.set_yticks(np.linspace(-0.0006255, -0.000616, 4))
                ax_inset_1.tick_params(axis='both', which='both', labelsize=8)

        except Exception as e:
            print(e)
    mark_inset(axes[0], ax_inset_0, loc1=2, loc2=4, fc="none", ec="black")
    mark_inset(axes[1], ax_inset_1, loc1=1, loc2=3, fc="none", ec="black")


# Label the main axes
    axes[0].set_xlabel("phase")
    axes[0].set_ylabel("flux normalized")
    axes[0].legend()

    axes[1].set_xlabel("phase")
    axes[1].set_ylabel("RV [R_S/d]")
    axes[1].legend(loc='upper left')

# Save and show the plot
    plt.savefig("vary_ex0_zoomed.png")
    plt.show()
    plt.close()
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))


    for i in range(10):
        try:
            I0_0 = np.array([I0[0]], np.float64)
            Omega0_0 = np.array([Omega0[9]], np.float64)
            ex0_i = np.array([ex0[i]], np.float64)
            ey0_0 = np.array([ey0[0]], np.float64)
            LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_i, ey0_0, I0_0, Omega0_0, t, u1, u2, tRV)
            for period in P:
                phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                LC_phase_sorted = LC[phase_sorted_indices]
                axes[0].plot(t_phase_sorted, LC_phase_sorted, label=f"ex0 is {ex0[i]}")
            for period in P:
                axes[1].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = f"ex0 is {ex0[i]}")

        except Exception as e:
            print(e)
    axes[0].set_xlabel("phase")
    axes[0].set_ylabel("flux normalized")
    axes[0].legend()
    axes[1].set_xlabel("phase")
    axes[1].set_ylabel("RV [R_S/d]")
    axes[1].legend()
    plt.savefig("vary_ex0.png")
    plt.show()
    plt.close()

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))
    for i in range(10):
        try:
            I0_0 = np.array([I0[0]], np.float64)
            Omega0_0 = np.array([Omega0[9]], np.float64)
            ex0_0 = np.array([ex0[0]], np.float64)
            ey0_i = np.array([ey0[i]], np.float64)
            LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_0, ey0_i, I0_0, Omega0_0, t, u1, u2, tRV)
            for period in P:
                phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                LC_phase_sorted = LC[phase_sorted_indices]
                axes[0].plot(t_phase_sorted, LC_phase_sorted, label=f"ey0 is {ey0[i]}")


            for period in P:
                axes[1].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = f"ey0 is {ey0[i]}")

        except Exception as e:
            print(e)
    axes[0].set_xlabel("phase")
    axes[0].set_ylabel("flux normalized")
    axes[0].legend()
    axes[1].set_xlabel("phase")
    axes[1].set_ylabel("RV [R_S/d]")
    axes[1].legend()
    plt.savefig("vary_ey0.png")
    plt.show()
    plt.close()
    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))


    for i in range(10):
        try:
            I0_0 = np.array([I0[0]], np.float64)
            Omega0_0 = np.array([Omega0[9]], np.float64)
            ex0_i = np.array([ex0[i]], np.float64)
            ey0_i = np.array([ey0[i]], np.float64)
            LC, RV,Y,Z = runsingle(P, Tmid0, ror, aor, mu, ex0_i, ey0_i, I0_0, Omega0_0, t, u1, u2, tRV)
            for period in P:
                phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
                t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
                LC_phase_sorted = LC[phase_sorted_indices]
                axes[0].plot(t_phase_sorted, LC_phase_sorted, label=f"ex0 is {ex0[i]}, ey0 is {ey0[i]}")

            for period in P:
                axes[1].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = f"ex0 is {ex0[i]}, ey0 is {ey0[i]}")

        except Exception as e:
            print(e)
    axes[0].set_xlabel("phase")
    axes[0].set_ylabel("flux normalized")
    axes[0].legend()
    axes[1].set_xlabel("phase")
    axes[1].set_ylabel("RV [R_S/d]")
    axes[1].legend()
    plt.savefig("vary_ex0ey0.png")
    plt.show()
    plt.close()
 

"""
    plt.plot(tRV, RV_o_r, 'o', label = 'model', zorder = 2.3)
    plt.xlabel("time [d]")
    plt.ylabel("RV [S_R/d]")
    plt.show()
    for i in range(10):
        LC, RV = runsingle(P, Tmid0, ror, aor, mu, ex0[0], ey0[0], I0[0], Omega0[i], t, u1, u2, tRV)
        for period in P:
            phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
            t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
            LC_phase_sorted = LC[phase_sorted_indices]
            plt.plot(t_phase_sorted, LC_phase_sorted, label=f"Omega0 is {Omega0[i]}")

    plt.xlabel("phase")
    plt.ylabel("flux normalized")
    plt.legend()
    plt.show()
    plt.close()
    
    for i in range(10):
        LC, RV = runsingle(P, Tmid0, ror, aor, mu, ex0[i], ey0[0], I0[0], Omega0[0], t, u1, u2, tRV)
        for period in P:
            phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
            t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
            LC_phase_sorted = LC[phase_sorted_indices]
            plt.plot(t_phase_sorted, LC_phase_sorted, label=f"ex0 is {ex0[i]}")

    plt.xlabel("phase")
    plt.ylabel("flux normalized")
    plt.legend()
    plt.show()
    plt.close()

    for i in range(10):
        LC, RV = runsingle(P, Tmid0, ror, aor, mu, ex0[0], ey0[i], I0[0], Omega0[0], t, u1, u2, tRV)
        for period in P:
            phase_sorted_indices = np.argsort((t - t[0] + 0.5 * period) % period + 0.5 * period)
            t_phase_sorted = ((t - t[0] + 0.5 * period) % period + 0.5 * period)[phase_sorted_indices]
            LC_phase_sorted = LC[phase_sorted_indices]
            plt.plot(t_phase_sorted, LC_phase_sorted, label=f"ey0 is {ey0[i]}")

    plt.xlabel("phase")
    plt.ylabel("flux normalized")
    plt.legend()
    plt.show()
"""    
     

   

################################################################################
####################### Main modeling function #################################
################################################################################

def main():    
    start_time = time.time()
    parameters, fixed, rvdatalist, trdatalist, nkern, nlivepoints, savedatapath = parse_settings()
    ndim = len(parameters)
    now = time.time()-start_time
    print(f"after parse {now}")    # create total t, RV, TR data lists
    tRV = []
    RV = []
    tRVseen_tRV = set()
    for rvdata in rvdatalist:
        for i in range(len(rvdata[0])):
            if rvdata[0][i] not in tRVseen_tRV:
                tRVseen_tRV.add(rvdata[0][i])
                insert_pos = bisect.bisect_left(tRV, rvdata[0][i])
                tRV.insert(insert_pos, rvdata[0][i])
                RV.insert(insert_pos, rvdata[1][i])
                #tRV.append(rvdata[0][i])
                #RV.append(rvdata[1][i])
    t = []
    TR = []
    seen_t = set()
    for trdata in trdatalist:
        for i in range(len(trdata[0])):
            if trdata[0][i] not in seen_t:
                seen_t.add(trdata[0][i])
                insert_pos = bisect.bisect_left(t, trdata[0][i])
                t.insert(insert_pos, trdata[0][i])
                TR.insert(insert_pos, trdata[1][i])
                #t.append(trdata[0][i])
                #TR.append(trdata[1][i])

    now = time.time()-start_time
    print(f"after rv and tr {now}")
    
    fixed["RV"] = RV
    fixed["tRV"] = tRV
    fixed["TR"] = TR
    fixed["t"] = t
    
    for i, trdata in enumerate(trdatalist):
        t_inst, TR_inst, TRerr_inst, TRname_inst = trdata 
        fixed[f"t_inst_{i}"] = t_inst 
        fixed[f"TR_inst_{i}"] = TR_inst 
        fixed[f"TRerr_inst_{i}"] = TRerr_inst 
        fixed[f"TRname_inst_{i}"] = TRname_inst 

    for i, rvdata in enumerate(rvdatalist):
        t_inst, RV_inst, RVerr_inst, RVname_inst = rvdata 
        fixed[f"tRV_inst_{i}"] = t_inst 
        fixed[f"RV_inst_{i}"] = RV_inst 
        fixed[f"RVerr_inst_{i}"] = RVerr_inst 
        fixed[f"RVname_inst_{i}"] = RVname_inst
    #in case you want to use previously calculated data: use line with pickle/

    results = calculate(nkern, nlivepoints, parameters, fixed, savedatapath)
    #results = pickle.load(open(f"{savedatapath}/restart.pickle", "rb")) 
    print("got to after calculate")
    
    save_results_text(results, parameters, savedatapath)
    create_plot(results, parameters, fixed, rvdatalist, trdatalist, ndim, savedatapath)
def testforpybind():
    u1 = 0.33
    u2 = 0.32
    P = [3.5]
    Tmid0 = [1520]
    ex0 = [0.000001]
    ey0 = [0.000001]
    ror = [0.1]
    mu = [1.76e-5]
    aor = [25]
    I0 = [0.0001]
    Omega0 = [0.000001]
    t = np.arange(1518, 1530.25, 0.25)
    P = np.array(P, np.float64)
    Tmid0 = np.array(Tmid0, np.float64)
    ror = np.array(ror, np.float64)
    aor = np.array(aor, np.float64)
    mu = np.array(mu, np.float64)
    ex0 = np.array(ex0, np.float64)
    ey0 = np.array(ey0, np.float64)
    I0 = np.array(I0, np.float64)
    Omega0 = np.array(Omega0, np.float64)
    t = np.array(t, np.float64)
    #AnLC_params = pythonapi.AnalyticLCparams()
    #AnLC_params.set_P(a1)
    #AnLC_params = pythonapi.AnalyticLCparams(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10, 1.0,1.0)
    AnLC_params = pythonapi.AnalyticLCparams(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1,u2)
    MaxTTV = np.array([10], np.float64)
    AnLC_params.set_MaxTTV(MaxTTV)
    print(AnLC_params.get_P())
    print("Bi1:^")
    output = pythonapi.AnalyticLCfunc(AnLC_params)
    main_out = output[0]
    LC = main_out.get_LC()
    print(LC)
def compute_nbody_model(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV):
    # Constants
    M_sun = 1.9885e30  # Mass of the Sun (kg)
    M_earth = 5.972e24  # Mass of the Earth (kg)
    AU = 1.496e11  # Astronomical Unit (m)

# Masses (in Solar Masses)
    M_star = 0.362  # GJ 357 mass
    M_b = 1.84 * M_earth / M_sun  # GJ 357 b mass in Solar masses
    M_c = 3.4 * M_earth / M_sun  # GJ 357 c mass in Solar masses
    M_d = 6.1 * M_earth / M_sun  # GJ 357 d mass in Solar masses
    M_b = mu[0]*M_star
    M_c = mu[1]*M_star
    M_d = mu[2]*M_star
# Orbital periods in days (using Kepler's third law approximation)
    P_b = P[0] 
    P_c = P[1]    
    P_d = P[2]
    # Setup rebound simulation
    sim = rebound.Simulation()
    sim.units = ('m', 'days', 'Msun')

# Add the star
    sim.add(m=M_star)

# Add planets with initial conditions
    sim.add(m=M_b, P=P_b, h= ex0[0], k=ey0[0])  # GJ 357 b
    sim.add(m=M_c, P=P_c, h = ex0[1], k=ey0[1])  # GJ 357 c
    sim.add(m=M_d, P=P_d, h = ex0[2], k=ey0[2])  # GJ 357 d

# Set integrator and simulation parameters
    sim.move_to_com()  # Move to center of mass frame
    sim.integrator = "ias15"  # High precision integrator

# Set up arrays for storing time and radial velocity
    times = tRV 
    radial_velocity = np.zeros(len(times))

# Run the simulation and calculate radial velocity of the star
    for i, t in enumerate(times):
        sim.integrate(t)  # Integrate to time t
        star_vy = sim.particles[0].vy  # Radial velocity component (assumed along y-axis)
        radial_velocity[i] = star_vy * 1/(24*60*60)  # Convert AU/day to m/s
    return times, radial_velocity


def compute_nbody_model_old(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV):
    # Initialize the N-body simulation using REBOUND
    sim = rebound.Simulation()
    sim.units = ["msun", "m", "s"] # Units of solar mass, meters, and seconds

    # Add the star (mass = 1 solar mass for simplicity)
    sim.add(m=0.362)

    # Add planets with the parameters provided

    for i in range(len(P)):
        sim.add(P=P[i]*24*60*60, #e=0.0, omega=np.pi/2,
                inc=np.pi/2+I0[i], M=mu[i]*0.362)
        print(P[i])
        print(mu[i])
    print("got after add")
    # Move to the center of mass frame
    sim.move_to_com()
    #tRV = tRV[::]
    # Time points for RV and LC calculations
    RV_mod = []
    LC_mod = []
    tRV_mod = []
    print("before integrate")
    tRV = np.array(tRV)-tRV[0]
    #only tRV > 4418
    print(tRV)
    tRV = (np.array(tRV))*24*60*60
    for time in tRV[:20:]:
        sim.integrate(time)
        print("integrated")
        tRV_mod.append(time/(24*60*60)+tRV[0]/(24*60*60))
        # Compute Radial Velocity
        star = sim.particles[0]
        rv_star = star.vy  # Use the star's velocity in the x-direction (along the observer's line of sight)
        
        RV_mod.append(rv_star)
        
        # Compute Light Curve - Simplified transit model
        #separation = planet.d
        #if separation < (ror + 1):  # If planet is in transit
        #    LC_mod.append(1 - ror**2)
        #else:
        #    LC_mod.append(1.0)

    return tRV_mod, RV_mod

def testconfig(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, u1, u2, R_S):
    #plot model infront of data
    parameters, fixed, rvdatalist, trdatalist, nkern, nlivepoints, savedatapath = parse_settings()
    ndim = len(parameters)
    t = []
    TR = []
    tRV = []
    RV = []
    seen_t = set()
    for trdata in trdatalist:
        for i in range(len(trdata[0])):
            if trdata[0][i] not in seen_t:
                seen_t.add(trdata[0][i])
                t.append(trdata[0][i])
                TR.append(trdata[1][i])
    tRVseen_tRV = set()
    for rvdata in rvdatalist:
        for i in range(len(rvdata[0])):
            if rvdata[0][i] not in tRVseen_tRV:
                tRVseen_tRV.add(rvdata[0][i])
                insert_pos = bisect.bisect_left(tRV, rvdata[0][i])
                tRV.insert(insert_pos, rvdata[0][i])
                RV.insert(insert_pos, rvdata[1][i])
    
    fixed["TR"] = TR
    fixed["t"] = t
    fixed["RV"] = RV 
    fixed["tRV"] = tRV
    for i, trdata in enumerate(trdatalist):
        t_inst, TR_inst, TRerr_inst, TRname_inst = trdata 
        fixed[f"t_inst_{i}"] = t_inst 
        fixed[f"TR_inst_{i}"] = TR_inst 
        fixed[f"TRerr_inst_{i}"] = TRerr_inst 
        fixed[f"TRname_inst_{i}"] = TRname_inst 
    for i, rvdata in enumerate(rvdatalist):
        tRV_inst, RV_inst, RVerr_inst, RVname_inst = rvdata 
        fixed[f"tRV_inst_{i}"] = tRV_inst 
        fixed[f"RV_inst_{i}"] = RV_inst 
        fixed[f"RVerr_inst_{i}"] = RVerr_inst 
        fixed[f"RVname_inst_{i}"] = RVname_inst
    LC_mod, RV_mod, Y_mod,Z_mod = runsingle(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV)

    #plot LC_mod on top of TR 
    plt.plot(t, TR, label = 'data')
    plt.plot(t, LC_mod, label = 'model')
    plt.xlabel("time [d]")
    plt.ylabel("flux normalized")
    plt.legend()
    plt.grid()
    plt.show()
    plt.close()
    #plot for phase folded
    
    tRV_nbody = np.linspace(tRV[0], tRV[-1], 100)
    tRV_nbody, RV_nbody = compute_nbody_model(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV_nbody)
    print(f"Length of tRV: {len(tRV)}")
    print(f"Length of RV_nbody: {len(RV_nbody)}")
    #plot RV 
    #translate RV_mod to m/s  
    S_R = R_S*6.957e8 #stellar radii in meters
    d = 86400 #seconds in a days
    RV_mod = RV_mod*(S_R/d)        
    plt.plot(tRV, RV_mod, label = 'model')
    plt.plot(tRV_nbody, RV_nbody, label = 'nbody')
    plt.legend()
    plt.show()
    for period in P:
        plt.plot(tRV, RV_mod, 'o', label = 'model')
        plt.plot(tRV_nbody, RV_nbody, 'o', label = 'nbody')
        plt.plot(tRV, RV, 'o', label = 'data')
    plt.xlabel("time [d]")
    plt.ylabel("RV [R_S/d]")
    plt.legend()
    plt.show()
    plt.close()
    for period in P:
        plt.plot((tRV-tRV[0])%period, RV_mod, 'o', label = 'model')
        plt.plot((tRV-tRV[0])%period, RV, 'o', label = 'data')
        plt.plot((tRV_nbody-tRV[0])%period, RV_nbody, 'o', label = 'nbody')
        plt.xlabel("phase")
        plt.ylabel("RV [R_S/d]")
        plt.legend()
        plt.show()
    #rewrite plot RV for each period so that it is phase folded
    #y_min = 0.997
    #y_max = 1.002
    tRV_long = np.linspace(tRV[0], tRV[-1], 10000)
    print(tRV_long)
    LC_long, RV_long, Y_long, Z_long = runsingle(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV_long)
    RV_long = RV_long*(S_R/d)
    for count,period in enumerate(P):
        tRV_phase = (tRV-tRV[0])%period 
        phase_sorted_indices = np.argsort(tRV_phase)
        tRV_phase = np.array(tRV_phase)[phase_sorted_indices]
        RV = np.array(RV)[phase_sorted_indices]
        tRV_nbody_phase = (tRV_nbody-tRV[0])%period 
        phase_sorted_indices = np.argsort(tRV_nbody_phase)
        tRV_nbody_phase = np.array(tRV_nbody_phase)[phase_sorted_indices]
        RV_nbody = np.array(RV_nbody)[phase_sorted_indices]
        tRV_long_phase = (tRV_long-tRV_long[0])%period 
        phase_sorted_indices = np.argsort(tRV_long_phase)
        tRV_long_phase = np.array(tRV_long_phase)[phase_sorted_indices]
        RV_long = np.array(RV_long)[phase_sorted_indices]
        plt.plot(tRV_long_phase, RV_long, label = 'model')
        plt.plot(tRV_nbody_phase, RV_nbody, label = 'nbody')
        plt.plot(tRV_phase, RV,'o', label = 'data')
        plt.xlabel("phase")
        plt.ylabel("RV [R_S/d]")
        plt.legend()
        plt.show()
        plt.close()
# implement something to test different Tmid0 and see how it affects the model 
# also for mu 
# also for ror 
def testdifferentparams(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, u1, u2, R_S):
    #plot model infront of data
    parameters, fixed, rvdatalist, trdatalist, nkern, nlivepoints, savedatapath = parse_settings()
    ndim = len(parameters)
    t = []
    TR = []
    tRV = []
    RV = []
    seen_t = set()
    for trdata in trdatalist:
        for i in range(len(trdata[0])):
            if trdata[0][i] not in seen_t:
                seen_t.add(trdata[0][i])
                t.append(trdata[0][i])
                TR.append(trdata[1][i])
    for rvdata in rvdatalist:
        for i in range(len(rvdata[0])):
            if rvdata[0][i] not in t:
                tRV.append(rvdata[0][i])
                RV.append(rvdata[1][i])
    
    fixed["TR"] = TR
    fixed["t"] = t
    fixed["RV"] = RV 
    fixed["tRV"] = tRV
    for i, trdata in enumerate(trdatalist):
        t_inst, TR_inst, TRerr_inst, TRname_inst = trdata 
        fixed[f"t_inst_{i}"] = t_inst 
        fixed[f"TR_inst_{i}"] = TR_inst 
        fixed[f"TRerr_inst_{i}"] = TRerr_inst 
        fixed[f"TRname_inst_{i}"] = TRname_inst 
    for i, rvdata in enumerate(rvdatalist):
        tRV_inst, RV_inst, RVerr_inst, RVname_inst = rvdata 
        fixed[f"tRV_inst_{i}"] = tRV_inst 
        fixed[f"RV_inst_{i}"] = RV_inst 
        fixed[f"RVerr_inst_{i}"] = RVerr_inst 
        fixed[f"RVname_inst_{i}"] = RVname_inst
    
    #LC_mod, RV_mod, Y_mod,Z_mod = runsingle(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV)
    
   #write something like plotmultiple function, but for varying Tmid0, ror, mu
    #vary Tmid0 
    tRV_long = np.linspace(tRV[0], tRV[-1], 1000)
    Tmid0_vary_P2 = [1518, 1519, 1520, 1521, 1522, 1523, 1524, 1525]
    Tmid0_vary_P3 = [1518, 1522, 1526, 1530, 1534, 1538, 1542, 1546]
    fig, axes = plt.subplots(nrows=1, ncols=len(P), figsize=(12, 6))
    for i in range(len(Tmid0_vary_P2)):
        Tmid0_i = np.array([[Tmid0[0][0]], [Tmid0_vary_P2[i]], [Tmid0_vary_P3[i]]])
        LC_model, RV_model, Y, Z = runsingle(P, Tmid0_i, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV_long)
        #plot RV 
        S_R = R_S*6.957e8
        RV_model = RV_model*(S_R/86400)
        print(P)
        for j,period in enumerate(P):
            enumerate(P)
            print(j)
            tRVlong_phase = (tRV_long-tRV_long[0])%period 
            phase_sorted_indices = np.argsort(tRVlong_phase)
            tRV_phase = np.array(tRVlong_phase)[phase_sorted_indices]
            RV_model = np.array(RV_model)[phase_sorted_indices]

            axes[j].plot(tRV_phase, RV_model, label = f"Tmid0 P2 {Tmid0_vary_P2[i]}, Tmid0 P3 is {Tmid0_vary_P3[i]}")
    for j,period in enumerate(P):
        axes[j].plot((tRV-tRV[0])%period, RV, 'o', ms = 0.22, label = 'data')
        axes[j].set_xlabel("phase")
        axes[j].set_ylabel("RV [R_S/d]")
        axes[j].legend()
    plt.show()
    plt.close()



def testtimeforplanets(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV):
#function that takes in parameters for planets, then times how long it takes to run it with only the first planet, the first two...
# and then plots it 
    matplotlib.rcParams.update({'font.size': 20})
    times = []
    for i in range(0, len(P)):
        start_time = time.time()
        print("check1")
        print(P[:i+1], Tmid0[:i+1], ror[:i+1], aor[:i+1], mu[:i+1], ex0[:i+1], ey0[:i+1], I0[:i+1], Omega0[:i+1], t, u1, u2, tRV)
        LC, RV, Y, Z = runsingle(P[:i+1], Tmid0[:i+1], ror[:i+1], aor[:i+1], mu[:i+1], ex0[:i+1], ey0[:i+1], I0[:i+1], Omega0[:i+1], t, u1, u2, tRV)
        print("check2")
        times.append(time.time()-start_time)
    print(times)
    octave_times = [1.9, 39, 414.6]
    plt.figure(figsize= (10,10))
    plt.scatter(range(1, len(P)+1), times, label = "C++ via Python", s= 150)
    plt.scatter(range(1, len(P)), octave_times, label="Octave", s = 150)
    plt.yscale('log')
    plt.xticks([1, 2, 3, 4])
    plt.xlabel("Number of Planets")
    plt.ylabel("Total Runtime [s]")
    plt.legend()
    plt.savefig("log_runtimecomparison.png")
    plt.show()
    plt.figure(figsize= (10,10))
    plt.scatter(range(1, len(P)+1), times, label = "C++ via Python", s=150)
    plt.scatter(range(1, len(P)), octave_times, label = "Octave", s=150)
    plt.legend()
    plt.xticks([1, 2, 3, 4])
    plt.xlabel("Number of Planets")
    plt.ylabel("Total Runtime [s]")
    plt.savefig("runtimecomparison.png")
    plt.show()

def testtimeforlongspan(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, u1, u2):
#function that takes in parameters for planets, then times how long it takes for different lengths of t:
# 1 day, 10 days, 100 days, 1000 days, 10000 days, 100000 days
    matplotlib.rcParams.update({'font.size': 20})
    times = []
    times = [0.09147334098815918, 0.8020083904266357, 7.939796686172485, 79.43137764930725, 844.0184879302979]
    """
    for i in range(1,6):
        t = np.linspace(1518.2, 1518.2 + 10**i, 10**i*50)
        tRV = t
        start_time = time.time()
        LC, RV, Y, Z = runsingle(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV)
        times.append(time.time()-start_time)
        print("day done")
    """
    print(times)
    octave_times = [1.1179, 5.2169, 215.5427]

    #octave_times = [0.1, 0.9, 9.1, 91.1, 911.1, 9111.1]
    plt.figure(figsize= (10,10))
    plt.plot(range(1, 6), times, label = "C++ via Python")
    #plt.plot(range(1, 7), octave_times, label="Octave")
    #plt.yscale('log')
    plt.xticks([1, 2, 3, 4, 5], ["1", "10", "100", "1000", "10000"])
    plt.xlabel("Length of Time [days]")
    plt.ylabel("Total Runtime [s]")
    plt.legend()
    plt.savefig("runtimecomparisonlongspan.png")
    plt.show()

    plt.figure(figsize = (10,10))
    plt.scatter(range(1, 6), times, label = "C++ via Python", s=150)
    plt.scatter(range(1, 4), octave_times, label = "Octave", s = 150)
    plt.xticks([1, 2, 3, 4, 5], ["1", "10", "100", "1000", "10000"])
    plt.yscale('log')
    plt.xlabel("Length of Time [days]")
    plt.ylabel("Total Runtime [s]")
    plt.legend()
    plt.savefig("runtimecomparisonlongspan_log.png")
    plt.show()

def testtimeforfarawaypoints(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, u1, u2):
#function that takes in parameters for planets, then times how long it takes for different lengths of t, where t is far away from each other
# 1 day, 10 days, 100 days, 1000 days, 10000 days, 100000 _days
    matplotlib.rcParams.update({'font.size': 20})
    times = []
    t_basic = np.linspace(1518, 1548, 10000)
    #t_basic = []
    times = [3.1125810146331787, 2.9916651248931885, 3.1058945655822754, 3.251601219177246, 3.6340558528900146, 8.380172967910767, 57.50793266296387]
    #times = [2.73811936378479, 2.8784992694854736, 3.088618278503418, 2.929643392562866, 3.4685840606689453, 7.570941925048828, 53.94688081741333, 526.7809946537018]
    """
    for i in range(0,8):
        times_far_away = np.linspace(1548 + 10**i, 1578 + 10**i, 10000)
        t = np.concatenate((t_basic, times_far_away))
        tRV = t
        
        start_time = time.time()
        LC, RV, Y, Z = runsingle(P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, t, u1, u2, tRV)
        times.append(time.time()-start_time)
        print("one done")
    print(times)
    """
    #octave_times = [0.1, 0.9, 9.1, 91.1, 911.1, 9111.1]
    plt.figure(figsize= (10,10))
    plt.scatter(range(1, 8), times, label = "C++ via Python", s = 150)
    #plt.plot(range(1, 7), octave_times, label="Octave")
    plt.yscale('log')
    plt.xticks([1, 2, 3, 4, 5, 6, 7], ["1e0", "1e1", "1e2", "1e3", "1e4", "1e5", "1e6"])
    plt.xlabel("Time between points [days]")
    plt.ylabel("Total Runtime [s]")
    plt.legend()
    plt.savefig("runtimecomparisonfarawaypoints.png")
    plt.show()



main()

